<?php
class Payment_model extends CI_Model 
{

	public function __construct()
	{
		$this->load->database();
	}

	//public function getData($id = FALSE)
	public function getData($biller_abbrev, $account_no = FALSE)
	{
         if($id === FALSE)
		{
			$query = $this->db->get('payment_'.$biller_abbrev);
			return $query->result_array();
		}

		$query = $this->db->get_where('payment_'.$biller_abbrev, array('account_no' => $account_no));
		return $query->row_array();
	}

	public function getDataByUploadStatus($status = 0)
	{
		$sql = "SELECT * FROM payment WHERE uploadstatus = $status ORDER BY created_date DESC LIMIT 0,1000";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getDataByDate($sdate,$edate)
	{
		$sql = "SELECT * FROM payment WHERE created_date BETWEEN '$sdate' AND '$edate' ";
		$query = $this->db->query($sql);
	    return $query->result_array();
	}
	
	public function getDataByDate2($sdate)
	{
            $sql = "SELECT * FROM payment WHERE created_date > '$edate' ";
            $query = $this->db->query($sql);
	        return $query->result_array();
	}
	
	public function setData($tranc_id, $tranc_date, $acct_no, $amt, $sbank, $dbank, $phone, $email, $payment_channel, $merchant_id, $product_id)
	{
		$sql_biller_info = "SELECT * FROM biller_info WHERE merchant_id='$merchant_id'";
		$query_biller_info = $this->db->query($sql_biller_info);
		if ($query_biller_info->num_rows() > 0)
		{
			$row = $query_biller_info->row();
		    $biller_id = $row->id;
		    $biller_table_suffix = $row->biller_table_suffix;
		}
		/*
		$sql = "SELECT * FROM bill_category WHERE merchant_id='$merchant_id' and id = '$product_id'";
		$query_exe = $this->db->query($sql);
		
		if ($query_exe->num_rows() > 0)
		{
			$row = $query_exe->row();
		    $product_id = $row->id;
		}
		else
		{
			$sql_insert = "INSERT into bill_category (`merchant_id`, `product`) values ('$merchant_id', $product_id')";
			$query_exe_ins = $this->db->query($sql_insert);
			$product_id = $this->db->insert_id();
		}
		*/
		$paymentdata = array(
			'TransactionID' => $tranc_id,
			'TransactionDate' => $tranc_date,
			'account_no' => $acct_no,
			'amount_paid' => $amt,
			'SourceBank' => $sbank,
            'DestinationBank' => $dbank,
            'PaymentChannel' => $payment_channel
		);
		
		$biller_paymentdata = array(
			'TransactionID' => $tranc_id,
			'TransactionDate' => $tranc_date,
			'account_no' => $acct_no,
			'amount_paid' => $amt,
			'SourceBank' => $sbank,
            'DestinationBank' => $dbank,
            'PaymentChannel' => $payment_channel,
			'Email'=> $email,
			'Phone'=> $phone,
			'product_id'=> $product_id
		);
		
		//print_r($data); die();
		$this->db->insert('payment', $paymentdata);
		$this->db->insert('payment_'.$biller_table_suffix, $biller_paymentdata);
		
		$id = $this->db->insert_id();
		
		if($id > 0)
		{
			$result = true;
		}
		else
		{
			$result = false;
		}
	
		return $result;
	}
	
	public function updateStatus($biller_table_suffix)
	{
	   	$id = (int) $this->input->post('ID');

		$data = array(
			'uploadstatus' => 1
		);
	    $this->db->where('id',$id);
		$insertid =  $this->db->update('payment_'.$biller_table_suffix, $data);
		return $insertid;

	}
	
	/*
	public function updateStatus()
	{
		$data = array(
			'uploadstatus' => $this->input->post('status')
		);
	
		$insertid =  $this->db->update('payment', $data);
	
		$result = "Data Updated Successfully";
		return $result;
	}
	*/


   public function setTestData()
	{	
		
           $postdata = serialize($_POST);		

		$data = array(
			'col1' => $postdata
		);
		
	
		$insertid = $this->db->insert('test_table_nibss', $data);
		
		
	
		return $insertid ;
	}

}