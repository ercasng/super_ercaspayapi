<?php

use Base\PaymentGlistenQuery as BasePaymentGlistenQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'payment_glisten' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PaymentGlistenQuery extends BasePaymentGlistenQuery
{

}
