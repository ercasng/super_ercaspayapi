<?php

use Base\Customer1Fct-irs as BaseCustomer1Fct-irs;

/**
 * Skeleton subclass for representing a row from the 'customer1_fct-irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Customer1Fct-irs extends BaseCustomer1Fct-irs
{

}
