<?php

use Base\InvoiceField as BaseInvoiceField;

/**
 * Skeleton subclass for representing a row from the 'invoice_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class InvoiceField extends BaseInvoiceField
{

}
