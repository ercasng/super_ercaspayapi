<?php

use Base\PaymentInterprod as BasePaymentInterprod;

/**
 * Skeleton subclass for representing a row from the 'payment_interprod' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PaymentInterprod extends BasePaymentInterprod
{

}
