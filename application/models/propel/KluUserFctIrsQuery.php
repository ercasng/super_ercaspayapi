<?php

use Base\KluUserFctIrsQuery as BaseKluUserFctIrsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'klu_user_fct_irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluUserFctIrsQuery extends BaseKluUserFctIrsQuery
{

}
