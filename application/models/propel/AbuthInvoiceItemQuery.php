<?php

use Base\AbuthInvoiceItemQuery as BaseAbuthInvoiceItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'abuth_invoice_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AbuthInvoiceItemQuery extends BaseAbuthInvoiceItemQuery
{

}
