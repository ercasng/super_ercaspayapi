<?php

use Base\NewBillsArrived as BaseNewBillsArrived;

/**
 * Skeleton subclass for representing a row from the 'new_bills_arrived' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class NewBillsArrived extends BaseNewBillsArrived
{

}
