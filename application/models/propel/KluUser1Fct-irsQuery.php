<?php

use Base\KluUser1Fct-irsQuery as BaseKluUser1Fct-irsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'klu_user1_fct-irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluUser1Fct-irsQuery extends BaseKluUser1Fct-irsQuery
{

}
