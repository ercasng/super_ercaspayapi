<?php

use Base\AbuthInvoiceItem as BaseAbuthInvoiceItem;

/**
 * Skeleton subclass for representing a row from the 'abuth_invoice_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AbuthInvoiceItem extends BaseAbuthInvoiceItem
{

}
