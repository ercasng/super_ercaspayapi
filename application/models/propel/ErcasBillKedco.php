<?php

use Base\ErcasBillKedco as BaseErcasBillKedco;

/**
 * Skeleton subclass for representing a row from the 'ercas_bill_kedco' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ErcasBillKedco extends BaseErcasBillKedco
{

}
