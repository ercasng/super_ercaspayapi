<?php

use Base\CustomerFctIrsQuery as BaseCustomerFctIrsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_fct_irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerFctIrsQuery extends BaseCustomerFctIrsQuery
{

}
