<?php

use Base\KluBillNewKedcoQuery as BaseKluBillNewKedcoQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'klu_bill_new_kedco' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluBillNewKedcoQuery extends BaseKluBillNewKedcoQuery
{

}
