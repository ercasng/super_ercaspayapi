<?php

use Base\KluUser1Fct-irs as BaseKluUser1Fct-irs;

/**
 * Skeleton subclass for representing a row from the 'klu_user1_fct-irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluUser1Fct-irs extends BaseKluUser1Fct-irs
{

}
