<?php

namespace Base;

use \BillerSmsAccountdetails as ChildBillerSmsAccountdetails;
use \BillerSmsAccountdetailsQuery as ChildBillerSmsAccountdetailsQuery;
use \Exception;
use \PDO;
use Map\BillerSmsAccountdetailsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'biller_sms_accountdetails' table.
 *
 *
 *
 * @method     ChildBillerSmsAccountdetailsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBillerSmsAccountdetailsQuery orderByBillerId($order = Criteria::ASC) Order by the biller_id column
 * @method     ChildBillerSmsAccountdetailsQuery orderByBillerName($order = Criteria::ASC) Order by the biller_name column
 * @method     ChildBillerSmsAccountdetailsQuery orderByAccountOwner($order = Criteria::ASC) Order by the account_owner column
 * @method     ChildBillerSmsAccountdetailsQuery orderBySubaccount($order = Criteria::ASC) Order by the subaccount column
 * @method     ChildBillerSmsAccountdetailsQuery orderBySubaccountPassword($order = Criteria::ASC) Order by the subaccount_password column
 *
 * @method     ChildBillerSmsAccountdetailsQuery groupById() Group by the id column
 * @method     ChildBillerSmsAccountdetailsQuery groupByBillerId() Group by the biller_id column
 * @method     ChildBillerSmsAccountdetailsQuery groupByBillerName() Group by the biller_name column
 * @method     ChildBillerSmsAccountdetailsQuery groupByAccountOwner() Group by the account_owner column
 * @method     ChildBillerSmsAccountdetailsQuery groupBySubaccount() Group by the subaccount column
 * @method     ChildBillerSmsAccountdetailsQuery groupBySubaccountPassword() Group by the subaccount_password column
 *
 * @method     ChildBillerSmsAccountdetailsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBillerSmsAccountdetailsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBillerSmsAccountdetailsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBillerSmsAccountdetailsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBillerSmsAccountdetailsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBillerSmsAccountdetailsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBillerSmsAccountdetails findOne(ConnectionInterface $con = null) Return the first ChildBillerSmsAccountdetails matching the query
 * @method     ChildBillerSmsAccountdetails findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBillerSmsAccountdetails matching the query, or a new ChildBillerSmsAccountdetails object populated from the query conditions when no match is found
 *
 * @method     ChildBillerSmsAccountdetails findOneById(int $id) Return the first ChildBillerSmsAccountdetails filtered by the id column
 * @method     ChildBillerSmsAccountdetails findOneByBillerId(int $biller_id) Return the first ChildBillerSmsAccountdetails filtered by the biller_id column
 * @method     ChildBillerSmsAccountdetails findOneByBillerName(string $biller_name) Return the first ChildBillerSmsAccountdetails filtered by the biller_name column
 * @method     ChildBillerSmsAccountdetails findOneByAccountOwner(string $account_owner) Return the first ChildBillerSmsAccountdetails filtered by the account_owner column
 * @method     ChildBillerSmsAccountdetails findOneBySubaccount(string $subaccount) Return the first ChildBillerSmsAccountdetails filtered by the subaccount column
 * @method     ChildBillerSmsAccountdetails findOneBySubaccountPassword(string $subaccount_password) Return the first ChildBillerSmsAccountdetails filtered by the subaccount_password column *

 * @method     ChildBillerSmsAccountdetails requirePk($key, ConnectionInterface $con = null) Return the ChildBillerSmsAccountdetails by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOne(ConnectionInterface $con = null) Return the first ChildBillerSmsAccountdetails matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBillerSmsAccountdetails requireOneById(int $id) Return the first ChildBillerSmsAccountdetails filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOneByBillerId(int $biller_id) Return the first ChildBillerSmsAccountdetails filtered by the biller_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOneByBillerName(string $biller_name) Return the first ChildBillerSmsAccountdetails filtered by the biller_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOneByAccountOwner(string $account_owner) Return the first ChildBillerSmsAccountdetails filtered by the account_owner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOneBySubaccount(string $subaccount) Return the first ChildBillerSmsAccountdetails filtered by the subaccount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerSmsAccountdetails requireOneBySubaccountPassword(string $subaccount_password) Return the first ChildBillerSmsAccountdetails filtered by the subaccount_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBillerSmsAccountdetails objects based on current ModelCriteria
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findById(int $id) Return ChildBillerSmsAccountdetails objects filtered by the id column
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findByBillerId(int $biller_id) Return ChildBillerSmsAccountdetails objects filtered by the biller_id column
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findByBillerName(string $biller_name) Return ChildBillerSmsAccountdetails objects filtered by the biller_name column
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findByAccountOwner(string $account_owner) Return ChildBillerSmsAccountdetails objects filtered by the account_owner column
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findBySubaccount(string $subaccount) Return ChildBillerSmsAccountdetails objects filtered by the subaccount column
 * @method     ChildBillerSmsAccountdetails[]|ObjectCollection findBySubaccountPassword(string $subaccount_password) Return ChildBillerSmsAccountdetails objects filtered by the subaccount_password column
 * @method     ChildBillerSmsAccountdetails[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BillerSmsAccountdetailsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BillerSmsAccountdetailsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\BillerSmsAccountdetails', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBillerSmsAccountdetailsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBillerSmsAccountdetailsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBillerSmsAccountdetailsQuery) {
            return $criteria;
        }
        $query = new ChildBillerSmsAccountdetailsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBillerSmsAccountdetails|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BillerSmsAccountdetailsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBillerSmsAccountdetails A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, biller_id, biller_name, account_owner, subaccount, subaccount_password FROM biller_sms_accountdetails WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBillerSmsAccountdetails $obj */
            $obj = new ChildBillerSmsAccountdetails();
            $obj->hydrate($row);
            BillerSmsAccountdetailsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBillerSmsAccountdetails|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the biller_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerId(1234); // WHERE biller_id = 1234
     * $query->filterByBillerId(array(12, 34)); // WHERE biller_id IN (12, 34)
     * $query->filterByBillerId(array('min' => 12)); // WHERE biller_id > 12
     * </code>
     *
     * @param     mixed $billerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterByBillerId($billerId = null, $comparison = null)
    {
        if (is_array($billerId)) {
            $useMinMax = false;
            if (isset($billerId['min'])) {
                $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_BILLER_ID, $billerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($billerId['max'])) {
                $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_BILLER_ID, $billerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_BILLER_ID, $billerId, $comparison);
    }

    /**
     * Filter the query on the biller_name column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerName('fooValue');   // WHERE biller_name = 'fooValue'
     * $query->filterByBillerName('%fooValue%'); // WHERE biller_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billerName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterByBillerName($billerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billerName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billerName)) {
                $billerName = str_replace('*', '%', $billerName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_BILLER_NAME, $billerName, $comparison);
    }

    /**
     * Filter the query on the account_owner column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountOwner('fooValue');   // WHERE account_owner = 'fooValue'
     * $query->filterByAccountOwner('%fooValue%'); // WHERE account_owner LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountOwner The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterByAccountOwner($accountOwner = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountOwner)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountOwner)) {
                $accountOwner = str_replace('*', '%', $accountOwner);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ACCOUNT_OWNER, $accountOwner, $comparison);
    }

    /**
     * Filter the query on the subaccount column
     *
     * Example usage:
     * <code>
     * $query->filterBySubaccount('fooValue');   // WHERE subaccount = 'fooValue'
     * $query->filterBySubaccount('%fooValue%'); // WHERE subaccount LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subaccount The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterBySubaccount($subaccount = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subaccount)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $subaccount)) {
                $subaccount = str_replace('*', '%', $subaccount);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT, $subaccount, $comparison);
    }

    /**
     * Filter the query on the subaccount_password column
     *
     * Example usage:
     * <code>
     * $query->filterBySubaccountPassword('fooValue');   // WHERE subaccount_password = 'fooValue'
     * $query->filterBySubaccountPassword('%fooValue%'); // WHERE subaccount_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subaccountPassword The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function filterBySubaccountPassword($subaccountPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subaccountPassword)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $subaccountPassword)) {
                $subaccountPassword = str_replace('*', '%', $subaccountPassword);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT_PASSWORD, $subaccountPassword, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBillerSmsAccountdetails $billerSmsAccountdetails Object to remove from the list of results
     *
     * @return $this|ChildBillerSmsAccountdetailsQuery The current query, for fluid interface
     */
    public function prune($billerSmsAccountdetails = null)
    {
        if ($billerSmsAccountdetails) {
            $this->addUsingAlias(BillerSmsAccountdetailsTableMap::COL_ID, $billerSmsAccountdetails->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the biller_sms_accountdetails table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BillerSmsAccountdetailsTableMap::clearInstancePool();
            BillerSmsAccountdetailsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BillerSmsAccountdetailsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BillerSmsAccountdetailsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BillerSmsAccountdetailsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BillerSmsAccountdetailsQuery
