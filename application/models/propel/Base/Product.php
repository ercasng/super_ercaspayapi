<?php

namespace Base;

use \BillerInfo as ChildBillerInfo;
use \BillerInfoQuery as ChildBillerInfoQuery;
use \InvoiceField as ChildInvoiceField;
use \InvoiceFieldQuery as ChildInvoiceFieldQuery;
use \PaymentField as ChildPaymentField;
use \PaymentFieldQuery as ChildPaymentFieldQuery;
use \Product as ChildProduct;
use \ProductQuery as ChildProductQuery;
use \ProductValidation as ChildProductValidation;
use \ProductValidationQuery as ChildProductValidationQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\InvoiceFieldTableMap;
use Map\PaymentFieldTableMap;
use Map\ProductTableMap;
use Map\ProductValidationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'product' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Product implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ProductTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the biller_info_id field.
     *
     * @var        string
     */
    protected $biller_info_id;

    /**
     * The value for the product_code field.
     *
     * @var        string
     */
    protected $product_code;

    /**
     * The value for the product_type field.
     *
     * @var        string
     */
    protected $product_type;

    /**
     * The value for the requires_validation field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $requires_validation;

    /**
     * The value for the created_at field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildBillerInfo
     */
    protected $aBiller;

    /**
     * @var        ObjectCollection|ChildInvoiceField[] Collection to store aggregation of ChildInvoiceField objects.
     */
    protected $collInvoiceFields;
    protected $collInvoiceFieldsPartial;

    /**
     * @var        ObjectCollection|ChildPaymentField[] Collection to store aggregation of ChildPaymentField objects.
     */
    protected $collPaymentFields;
    protected $collPaymentFieldsPartial;

    /**
     * @var        ObjectCollection|ChildProductValidation[] Collection to store aggregation of ChildProductValidation objects.
     */
    protected $collProductValidations;
    protected $collProductValidationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildInvoiceField[]
     */
    protected $invoiceFieldsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPaymentField[]
     */
    protected $paymentFieldsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductValidation[]
     */
    protected $productValidationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->requires_validation = false;
        $this->updated_at = PropelDateTime::newInstance(NULL, null, 'DateTime');
    }

    /**
     * Initializes internal state of Base\Product object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Product</code> instance.  If
     * <code>obj</code> is an instance of <code>Product</code>, delegates to
     * <code>equals(Product)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Product The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [biller_info_id] column value.
     *
     * @return string
     */
    public function getBillerId()
    {
        return $this->biller_info_id;
    }

    /**
     * Get the [product_code] column value.
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->product_code;
    }

    /**
     * Get the [product_type] column value.
     *
     * @return string
     */
    public function getProductType()
    {
        return $this->product_type;
    }

    /**
     * Get the [requires_validation] column value.
     *
     * @return boolean
     */
    public function getRequiresValidation()
    {
        return $this->requires_validation;
    }

    /**
     * Get the [requires_validation] column value.
     *
     * @return boolean
     */
    public function isRequiresValidation()
    {
        return $this->getRequiresValidation();
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ProductTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [biller_info_id] column.
     *
     * @param string $v new value
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setBillerId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->biller_info_id !== $v) {
            $this->biller_info_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_BILLER_INFO_ID] = true;
        }

        if ($this->aBiller !== null && $this->aBiller->getMerchantId() !== $v) {
            $this->aBiller = null;
        }

        return $this;
    } // setBillerId()

    /**
     * Set the value of [product_code] column.
     *
     * @param string $v new value
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setProductCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->product_code !== $v) {
            $this->product_code = $v;
            $this->modifiedColumns[ProductTableMap::COL_PRODUCT_CODE] = true;
        }

        return $this;
    } // setProductCode()

    /**
     * Set the value of [product_type] column.
     *
     * @param string $v new value
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setProductType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->product_type !== $v) {
            $this->product_type = $v;
            $this->modifiedColumns[ProductTableMap::COL_PRODUCT_TYPE] = true;
        }

        return $this;
    } // setProductType()

    /**
     * Sets the value of the [requires_validation] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setRequiresValidation($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->requires_validation !== $v) {
            $this->requires_validation = $v;
            $this->modifiedColumns[ProductTableMap::COL_REQUIRES_VALIDATION] = true;
        }

        return $this;
    } // setRequiresValidation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Product The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ( ($dt != $this->updated_at) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->requires_validation !== false) {
                return false;
            }

            if ($this->updated_at && $this->updated_at->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProductTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProductTableMap::translateFieldName('BillerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->biller_info_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProductTableMap::translateFieldName('ProductCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->product_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProductTableMap::translateFieldName('ProductType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->product_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProductTableMap::translateFieldName('RequiresValidation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->requires_validation = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProductTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProductTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = ProductTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Product'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBiller !== null && $this->biller_info_id !== $this->aBiller->getMerchantId()) {
            $this->aBiller = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProductQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBiller = null;
            $this->collInvoiceFields = null;

            $this->collPaymentFields = null;

            $this->collProductValidations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Product::setDeleted()
     * @see Product::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProductQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProductTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBiller !== null) {
                if ($this->aBiller->isModified() || $this->aBiller->isNew()) {
                    $affectedRows += $this->aBiller->save($con);
                }
                $this->setBiller($this->aBiller);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->invoiceFieldsScheduledForDeletion !== null) {
                if (!$this->invoiceFieldsScheduledForDeletion->isEmpty()) {
                    \InvoiceFieldQuery::create()
                        ->filterByPrimaryKeys($this->invoiceFieldsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->invoiceFieldsScheduledForDeletion = null;
                }
            }

            if ($this->collInvoiceFields !== null) {
                foreach ($this->collInvoiceFields as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->paymentFieldsScheduledForDeletion !== null) {
                if (!$this->paymentFieldsScheduledForDeletion->isEmpty()) {
                    \PaymentFieldQuery::create()
                        ->filterByPrimaryKeys($this->paymentFieldsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->paymentFieldsScheduledForDeletion = null;
                }
            }

            if ($this->collPaymentFields !== null) {
                foreach ($this->collPaymentFields as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productValidationsScheduledForDeletion !== null) {
                if (!$this->productValidationsScheduledForDeletion->isEmpty()) {
                    \ProductValidationQuery::create()
                        ->filterByPrimaryKeys($this->productValidationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productValidationsScheduledForDeletion = null;
                }
            }

            if ($this->collProductValidations !== null) {
                foreach ($this->collProductValidations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProductTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProductTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProductTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_BILLER_INFO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'biller_info_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_PRODUCT_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'product_code';
        }
        if ($this->isColumnModified(ProductTableMap::COL_PRODUCT_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'product_type';
        }
        if ($this->isColumnModified(ProductTableMap::COL_REQUIRES_VALIDATION)) {
            $modifiedColumns[':p' . $index++]  = 'requires_validation';
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO product (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'biller_info_id':
                        $stmt->bindValue($identifier, $this->biller_info_id, PDO::PARAM_STR);
                        break;
                    case 'product_code':
                        $stmt->bindValue($identifier, $this->product_code, PDO::PARAM_STR);
                        break;
                    case 'product_type':
                        $stmt->bindValue($identifier, $this->product_type, PDO::PARAM_STR);
                        break;
                    case 'requires_validation':
                        $stmt->bindValue($identifier, (int) $this->requires_validation, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBillerId();
                break;
            case 2:
                return $this->getProductCode();
                break;
            case 3:
                return $this->getProductType();
                break;
            case 4:
                return $this->getRequiresValidation();
                break;
            case 5:
                return $this->getCreatedAt();
                break;
            case 6:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Product'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Product'][$this->hashCode()] = true;
        $keys = ProductTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBillerId(),
            $keys[2] => $this->getProductCode(),
            $keys[3] => $this->getProductType(),
            $keys[4] => $this->getRequiresValidation(),
            $keys[5] => $this->getCreatedAt(),
            $keys[6] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBiller) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'billerInfo';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'biller_info';
                        break;
                    default:
                        $key = 'Biller';
                }

                $result[$key] = $this->aBiller->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collInvoiceFields) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'invoiceFields';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'invoice_fields';
                        break;
                    default:
                        $key = 'InvoiceFields';
                }

                $result[$key] = $this->collInvoiceFields->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPaymentFields) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'paymentFields';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'payment_fields';
                        break;
                    default:
                        $key = 'PaymentFields';
                }

                $result[$key] = $this->collPaymentFields->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductValidations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productValidations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_validations';
                        break;
                    default:
                        $key = 'ProductValidations';
                }

                $result[$key] = $this->collProductValidations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Product
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Product
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBillerId($value);
                break;
            case 2:
                $this->setProductCode($value);
                break;
            case 3:
                $this->setProductType($value);
                break;
            case 4:
                $this->setRequiresValidation($value);
                break;
            case 5:
                $this->setCreatedAt($value);
                break;
            case 6:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProductTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBillerId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setProductCode($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setProductType($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setRequiresValidation($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCreatedAt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUpdatedAt($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Product The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProductTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProductTableMap::COL_ID)) {
            $criteria->add(ProductTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_BILLER_INFO_ID)) {
            $criteria->add(ProductTableMap::COL_BILLER_INFO_ID, $this->biller_info_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_PRODUCT_CODE)) {
            $criteria->add(ProductTableMap::COL_PRODUCT_CODE, $this->product_code);
        }
        if ($this->isColumnModified(ProductTableMap::COL_PRODUCT_TYPE)) {
            $criteria->add(ProductTableMap::COL_PRODUCT_TYPE, $this->product_type);
        }
        if ($this->isColumnModified(ProductTableMap::COL_REQUIRES_VALIDATION)) {
            $criteria->add(ProductTableMap::COL_REQUIRES_VALIDATION, $this->requires_validation);
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_AT)) {
            $criteria->add(ProductTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
            $criteria->add(ProductTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProductQuery::create();
        $criteria->add(ProductTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Product (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBillerId($this->getBillerId());
        $copyObj->setProductCode($this->getProductCode());
        $copyObj->setProductType($this->getProductType());
        $copyObj->setRequiresValidation($this->getRequiresValidation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getInvoiceFields() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInvoiceField($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPaymentFields() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPaymentField($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductValidations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductValidation($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Product Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBillerInfo object.
     *
     * @param  ChildBillerInfo $v
     * @return $this|\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBiller(ChildBillerInfo $v = null)
    {
        if ($v === null) {
            $this->setBillerId(NULL);
        } else {
            $this->setBillerId($v->getMerchantId());
        }

        $this->aBiller = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBillerInfo object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBillerInfo object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBillerInfo The associated ChildBillerInfo object.
     * @throws PropelException
     */
    public function getBiller(ConnectionInterface $con = null)
    {
        if ($this->aBiller === null && (($this->biller_info_id !== "" && $this->biller_info_id !== null))) {
            $this->aBiller = ChildBillerInfoQuery::create()
                ->filterByProduct($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBiller->addProducts($this);
             */
        }

        return $this->aBiller;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('InvoiceField' == $relationName) {
            return $this->initInvoiceFields();
        }
        if ('PaymentField' == $relationName) {
            return $this->initPaymentFields();
        }
        if ('ProductValidation' == $relationName) {
            return $this->initProductValidations();
        }
    }

    /**
     * Clears out the collInvoiceFields collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addInvoiceFields()
     */
    public function clearInvoiceFields()
    {
        $this->collInvoiceFields = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collInvoiceFields collection loaded partially.
     */
    public function resetPartialInvoiceFields($v = true)
    {
        $this->collInvoiceFieldsPartial = $v;
    }

    /**
     * Initializes the collInvoiceFields collection.
     *
     * By default this just sets the collInvoiceFields collection to an empty array (like clearcollInvoiceFields());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInvoiceFields($overrideExisting = true)
    {
        if (null !== $this->collInvoiceFields && !$overrideExisting) {
            return;
        }

        $collectionClassName = InvoiceFieldTableMap::getTableMap()->getCollectionClassName();

        $this->collInvoiceFields = new $collectionClassName;
        $this->collInvoiceFields->setModel('\InvoiceField');
    }

    /**
     * Gets an array of ChildInvoiceField objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildInvoiceField[] List of ChildInvoiceField objects
     * @throws PropelException
     */
    public function getInvoiceFields(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collInvoiceFieldsPartial && !$this->isNew();
        if (null === $this->collInvoiceFields || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInvoiceFields) {
                // return empty collection
                $this->initInvoiceFields();
            } else {
                $collInvoiceFields = ChildInvoiceFieldQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collInvoiceFieldsPartial && count($collInvoiceFields)) {
                        $this->initInvoiceFields(false);

                        foreach ($collInvoiceFields as $obj) {
                            if (false == $this->collInvoiceFields->contains($obj)) {
                                $this->collInvoiceFields->append($obj);
                            }
                        }

                        $this->collInvoiceFieldsPartial = true;
                    }

                    return $collInvoiceFields;
                }

                if ($partial && $this->collInvoiceFields) {
                    foreach ($this->collInvoiceFields as $obj) {
                        if ($obj->isNew()) {
                            $collInvoiceFields[] = $obj;
                        }
                    }
                }

                $this->collInvoiceFields = $collInvoiceFields;
                $this->collInvoiceFieldsPartial = false;
            }
        }

        return $this->collInvoiceFields;
    }

    /**
     * Sets a collection of ChildInvoiceField objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $invoiceFields A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setInvoiceFields(Collection $invoiceFields, ConnectionInterface $con = null)
    {
        /** @var ChildInvoiceField[] $invoiceFieldsToDelete */
        $invoiceFieldsToDelete = $this->getInvoiceFields(new Criteria(), $con)->diff($invoiceFields);


        $this->invoiceFieldsScheduledForDeletion = $invoiceFieldsToDelete;

        foreach ($invoiceFieldsToDelete as $invoiceFieldRemoved) {
            $invoiceFieldRemoved->setProduct(null);
        }

        $this->collInvoiceFields = null;
        foreach ($invoiceFields as $invoiceField) {
            $this->addInvoiceField($invoiceField);
        }

        $this->collInvoiceFields = $invoiceFields;
        $this->collInvoiceFieldsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related InvoiceField objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related InvoiceField objects.
     * @throws PropelException
     */
    public function countInvoiceFields(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collInvoiceFieldsPartial && !$this->isNew();
        if (null === $this->collInvoiceFields || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInvoiceFields) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInvoiceFields());
            }

            $query = ChildInvoiceFieldQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collInvoiceFields);
    }

    /**
     * Method called to associate a ChildInvoiceField object to this object
     * through the ChildInvoiceField foreign key attribute.
     *
     * @param  ChildInvoiceField $l ChildInvoiceField
     * @return $this|\Product The current object (for fluent API support)
     */
    public function addInvoiceField(ChildInvoiceField $l)
    {
        if ($this->collInvoiceFields === null) {
            $this->initInvoiceFields();
            $this->collInvoiceFieldsPartial = true;
        }

        if (!$this->collInvoiceFields->contains($l)) {
            $this->doAddInvoiceField($l);

            if ($this->invoiceFieldsScheduledForDeletion and $this->invoiceFieldsScheduledForDeletion->contains($l)) {
                $this->invoiceFieldsScheduledForDeletion->remove($this->invoiceFieldsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildInvoiceField $invoiceField The ChildInvoiceField object to add.
     */
    protected function doAddInvoiceField(ChildInvoiceField $invoiceField)
    {
        $this->collInvoiceFields[]= $invoiceField;
        $invoiceField->setProduct($this);
    }

    /**
     * @param  ChildInvoiceField $invoiceField The ChildInvoiceField object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeInvoiceField(ChildInvoiceField $invoiceField)
    {
        if ($this->getInvoiceFields()->contains($invoiceField)) {
            $pos = $this->collInvoiceFields->search($invoiceField);
            $this->collInvoiceFields->remove($pos);
            if (null === $this->invoiceFieldsScheduledForDeletion) {
                $this->invoiceFieldsScheduledForDeletion = clone $this->collInvoiceFields;
                $this->invoiceFieldsScheduledForDeletion->clear();
            }
            $this->invoiceFieldsScheduledForDeletion[]= clone $invoiceField;
            $invoiceField->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collPaymentFields collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPaymentFields()
     */
    public function clearPaymentFields()
    {
        $this->collPaymentFields = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPaymentFields collection loaded partially.
     */
    public function resetPartialPaymentFields($v = true)
    {
        $this->collPaymentFieldsPartial = $v;
    }

    /**
     * Initializes the collPaymentFields collection.
     *
     * By default this just sets the collPaymentFields collection to an empty array (like clearcollPaymentFields());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPaymentFields($overrideExisting = true)
    {
        if (null !== $this->collPaymentFields && !$overrideExisting) {
            return;
        }

        $collectionClassName = PaymentFieldTableMap::getTableMap()->getCollectionClassName();

        $this->collPaymentFields = new $collectionClassName;
        $this->collPaymentFields->setModel('\PaymentField');
    }

    /**
     * Gets an array of ChildPaymentField objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPaymentField[] List of ChildPaymentField objects
     * @throws PropelException
     */
    public function getPaymentFields(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPaymentFieldsPartial && !$this->isNew();
        if (null === $this->collPaymentFields || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPaymentFields) {
                // return empty collection
                $this->initPaymentFields();
            } else {
                $collPaymentFields = ChildPaymentFieldQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPaymentFieldsPartial && count($collPaymentFields)) {
                        $this->initPaymentFields(false);

                        foreach ($collPaymentFields as $obj) {
                            if (false == $this->collPaymentFields->contains($obj)) {
                                $this->collPaymentFields->append($obj);
                            }
                        }

                        $this->collPaymentFieldsPartial = true;
                    }

                    return $collPaymentFields;
                }

                if ($partial && $this->collPaymentFields) {
                    foreach ($this->collPaymentFields as $obj) {
                        if ($obj->isNew()) {
                            $collPaymentFields[] = $obj;
                        }
                    }
                }

                $this->collPaymentFields = $collPaymentFields;
                $this->collPaymentFieldsPartial = false;
            }
        }

        return $this->collPaymentFields;
    }

    /**
     * Sets a collection of ChildPaymentField objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $paymentFields A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setPaymentFields(Collection $paymentFields, ConnectionInterface $con = null)
    {
        /** @var ChildPaymentField[] $paymentFieldsToDelete */
        $paymentFieldsToDelete = $this->getPaymentFields(new Criteria(), $con)->diff($paymentFields);


        $this->paymentFieldsScheduledForDeletion = $paymentFieldsToDelete;

        foreach ($paymentFieldsToDelete as $paymentFieldRemoved) {
            $paymentFieldRemoved->setProduct(null);
        }

        $this->collPaymentFields = null;
        foreach ($paymentFields as $paymentField) {
            $this->addPaymentField($paymentField);
        }

        $this->collPaymentFields = $paymentFields;
        $this->collPaymentFieldsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PaymentField objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PaymentField objects.
     * @throws PropelException
     */
    public function countPaymentFields(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPaymentFieldsPartial && !$this->isNew();
        if (null === $this->collPaymentFields || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPaymentFields) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPaymentFields());
            }

            $query = ChildPaymentFieldQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collPaymentFields);
    }

    /**
     * Method called to associate a ChildPaymentField object to this object
     * through the ChildPaymentField foreign key attribute.
     *
     * @param  ChildPaymentField $l ChildPaymentField
     * @return $this|\Product The current object (for fluent API support)
     */
    public function addPaymentField(ChildPaymentField $l)
    {
        if ($this->collPaymentFields === null) {
            $this->initPaymentFields();
            $this->collPaymentFieldsPartial = true;
        }

        if (!$this->collPaymentFields->contains($l)) {
            $this->doAddPaymentField($l);

            if ($this->paymentFieldsScheduledForDeletion and $this->paymentFieldsScheduledForDeletion->contains($l)) {
                $this->paymentFieldsScheduledForDeletion->remove($this->paymentFieldsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPaymentField $paymentField The ChildPaymentField object to add.
     */
    protected function doAddPaymentField(ChildPaymentField $paymentField)
    {
        $this->collPaymentFields[]= $paymentField;
        $paymentField->setProduct($this);
    }

    /**
     * @param  ChildPaymentField $paymentField The ChildPaymentField object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removePaymentField(ChildPaymentField $paymentField)
    {
        if ($this->getPaymentFields()->contains($paymentField)) {
            $pos = $this->collPaymentFields->search($paymentField);
            $this->collPaymentFields->remove($pos);
            if (null === $this->paymentFieldsScheduledForDeletion) {
                $this->paymentFieldsScheduledForDeletion = clone $this->collPaymentFields;
                $this->paymentFieldsScheduledForDeletion->clear();
            }
            $this->paymentFieldsScheduledForDeletion[]= clone $paymentField;
            $paymentField->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collProductValidations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductValidations()
     */
    public function clearProductValidations()
    {
        $this->collProductValidations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductValidations collection loaded partially.
     */
    public function resetPartialProductValidations($v = true)
    {
        $this->collProductValidationsPartial = $v;
    }

    /**
     * Initializes the collProductValidations collection.
     *
     * By default this just sets the collProductValidations collection to an empty array (like clearcollProductValidations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductValidations($overrideExisting = true)
    {
        if (null !== $this->collProductValidations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductValidationTableMap::getTableMap()->getCollectionClassName();

        $this->collProductValidations = new $collectionClassName;
        $this->collProductValidations->setModel('\ProductValidation');
    }

    /**
     * Gets an array of ChildProductValidation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductValidation[] List of ChildProductValidation objects
     * @throws PropelException
     */
    public function getProductValidations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductValidationsPartial && !$this->isNew();
        if (null === $this->collProductValidations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProductValidations) {
                // return empty collection
                $this->initProductValidations();
            } else {
                $collProductValidations = ChildProductValidationQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductValidationsPartial && count($collProductValidations)) {
                        $this->initProductValidations(false);

                        foreach ($collProductValidations as $obj) {
                            if (false == $this->collProductValidations->contains($obj)) {
                                $this->collProductValidations->append($obj);
                            }
                        }

                        $this->collProductValidationsPartial = true;
                    }

                    return $collProductValidations;
                }

                if ($partial && $this->collProductValidations) {
                    foreach ($this->collProductValidations as $obj) {
                        if ($obj->isNew()) {
                            $collProductValidations[] = $obj;
                        }
                    }
                }

                $this->collProductValidations = $collProductValidations;
                $this->collProductValidationsPartial = false;
            }
        }

        return $this->collProductValidations;
    }

    /**
     * Sets a collection of ChildProductValidation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productValidations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductValidations(Collection $productValidations, ConnectionInterface $con = null)
    {
        /** @var ChildProductValidation[] $productValidationsToDelete */
        $productValidationsToDelete = $this->getProductValidations(new Criteria(), $con)->diff($productValidations);


        $this->productValidationsScheduledForDeletion = $productValidationsToDelete;

        foreach ($productValidationsToDelete as $productValidationRemoved) {
            $productValidationRemoved->setProduct(null);
        }

        $this->collProductValidations = null;
        foreach ($productValidations as $productValidation) {
            $this->addProductValidation($productValidation);
        }

        $this->collProductValidations = $productValidations;
        $this->collProductValidationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductValidation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductValidation objects.
     * @throws PropelException
     */
    public function countProductValidations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductValidationsPartial && !$this->isNew();
        if (null === $this->collProductValidations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductValidations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductValidations());
            }

            $query = ChildProductValidationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductValidations);
    }

    /**
     * Method called to associate a ChildProductValidation object to this object
     * through the ChildProductValidation foreign key attribute.
     *
     * @param  ChildProductValidation $l ChildProductValidation
     * @return $this|\Product The current object (for fluent API support)
     */
    public function addProductValidation(ChildProductValidation $l)
    {
        if ($this->collProductValidations === null) {
            $this->initProductValidations();
            $this->collProductValidationsPartial = true;
        }

        if (!$this->collProductValidations->contains($l)) {
            $this->doAddProductValidation($l);

            if ($this->productValidationsScheduledForDeletion and $this->productValidationsScheduledForDeletion->contains($l)) {
                $this->productValidationsScheduledForDeletion->remove($this->productValidationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductValidation $productValidation The ChildProductValidation object to add.
     */
    protected function doAddProductValidation(ChildProductValidation $productValidation)
    {
        $this->collProductValidations[]= $productValidation;
        $productValidation->setProduct($this);
    }

    /**
     * @param  ChildProductValidation $productValidation The ChildProductValidation object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductValidation(ChildProductValidation $productValidation)
    {
        if ($this->getProductValidations()->contains($productValidation)) {
            $pos = $this->collProductValidations->search($productValidation);
            $this->collProductValidations->remove($pos);
            if (null === $this->productValidationsScheduledForDeletion) {
                $this->productValidationsScheduledForDeletion = clone $this->collProductValidations;
                $this->productValidationsScheduledForDeletion->clear();
            }
            $this->productValidationsScheduledForDeletion[]= clone $productValidation;
            $productValidation->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBiller) {
            $this->aBiller->removeProduct($this);
        }
        $this->id = null;
        $this->biller_info_id = null;
        $this->product_code = null;
        $this->product_type = null;
        $this->requires_validation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collInvoiceFields) {
                foreach ($this->collInvoiceFields as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPaymentFields) {
                foreach ($this->collPaymentFields as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductValidations) {
                foreach ($this->collProductValidations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collInvoiceFields = null;
        $this->collPaymentFields = null;
        $this->collProductValidations = null;
        $this->aBiller = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProductTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
