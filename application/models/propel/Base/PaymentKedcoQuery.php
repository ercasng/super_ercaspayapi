<?php

namespace Base;

use \PaymentKedco as ChildPaymentKedco;
use \PaymentKedcoQuery as ChildPaymentKedcoQuery;
use \Exception;
use \PDO;
use Map\PaymentKedcoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_kedco' table.
 *
 *
 *
 * @method     ChildPaymentKedcoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentKedcoQuery orderByAccountNo($order = Criteria::ASC) Order by the account_no column
 * @method     ChildPaymentKedcoQuery orderByTransactionid($order = Criteria::ASC) Order by the TransactionID column
 * @method     ChildPaymentKedcoQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildPaymentKedcoQuery orderBySourcebank($order = Criteria::ASC) Order by the SourceBank column
 * @method     ChildPaymentKedcoQuery orderByDestinationbank($order = Criteria::ASC) Order by the DestinationBank column
 * @method     ChildPaymentKedcoQuery orderByEmail($order = Criteria::ASC) Order by the Email column
 * @method     ChildPaymentKedcoQuery orderByPhone($order = Criteria::ASC) Order by the Phone column
 * @method     ChildPaymentKedcoQuery orderByTransactiondate($order = Criteria::ASC) Order by the TransactionDate column
 * @method     ChildPaymentKedcoQuery orderByAmountPaid($order = Criteria::ASC) Order by the amount_paid column
 * @method     ChildPaymentKedcoQuery orderByPaymentchannel($order = Criteria::ASC) Order by the PaymentChannel column
 * @method     ChildPaymentKedcoQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildPaymentKedcoQuery orderByUploadstatus($order = Criteria::ASC) Order by the uploadstatus column
 *
 * @method     ChildPaymentKedcoQuery groupById() Group by the id column
 * @method     ChildPaymentKedcoQuery groupByAccountNo() Group by the account_no column
 * @method     ChildPaymentKedcoQuery groupByTransactionid() Group by the TransactionID column
 * @method     ChildPaymentKedcoQuery groupByProductId() Group by the product_id column
 * @method     ChildPaymentKedcoQuery groupBySourcebank() Group by the SourceBank column
 * @method     ChildPaymentKedcoQuery groupByDestinationbank() Group by the DestinationBank column
 * @method     ChildPaymentKedcoQuery groupByEmail() Group by the Email column
 * @method     ChildPaymentKedcoQuery groupByPhone() Group by the Phone column
 * @method     ChildPaymentKedcoQuery groupByTransactiondate() Group by the TransactionDate column
 * @method     ChildPaymentKedcoQuery groupByAmountPaid() Group by the amount_paid column
 * @method     ChildPaymentKedcoQuery groupByPaymentchannel() Group by the PaymentChannel column
 * @method     ChildPaymentKedcoQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildPaymentKedcoQuery groupByUploadstatus() Group by the uploadstatus column
 *
 * @method     ChildPaymentKedcoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentKedcoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentKedcoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentKedcoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentKedcoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentKedcoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentKedco findOne(ConnectionInterface $con = null) Return the first ChildPaymentKedco matching the query
 * @method     ChildPaymentKedco findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentKedco matching the query, or a new ChildPaymentKedco object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentKedco findOneById(int $id) Return the first ChildPaymentKedco filtered by the id column
 * @method     ChildPaymentKedco findOneByAccountNo(string $account_no) Return the first ChildPaymentKedco filtered by the account_no column
 * @method     ChildPaymentKedco findOneByTransactionid(string $TransactionID) Return the first ChildPaymentKedco filtered by the TransactionID column
 * @method     ChildPaymentKedco findOneByProductId(int $product_id) Return the first ChildPaymentKedco filtered by the product_id column
 * @method     ChildPaymentKedco findOneBySourcebank(string $SourceBank) Return the first ChildPaymentKedco filtered by the SourceBank column
 * @method     ChildPaymentKedco findOneByDestinationbank(string $DestinationBank) Return the first ChildPaymentKedco filtered by the DestinationBank column
 * @method     ChildPaymentKedco findOneByEmail(string $Email) Return the first ChildPaymentKedco filtered by the Email column
 * @method     ChildPaymentKedco findOneByPhone(string $Phone) Return the first ChildPaymentKedco filtered by the Phone column
 * @method     ChildPaymentKedco findOneByTransactiondate(string $TransactionDate) Return the first ChildPaymentKedco filtered by the TransactionDate column
 * @method     ChildPaymentKedco findOneByAmountPaid(string $amount_paid) Return the first ChildPaymentKedco filtered by the amount_paid column
 * @method     ChildPaymentKedco findOneByPaymentchannel(string $PaymentChannel) Return the first ChildPaymentKedco filtered by the PaymentChannel column
 * @method     ChildPaymentKedco findOneByCreatedDate(string $created_date) Return the first ChildPaymentKedco filtered by the created_date column
 * @method     ChildPaymentKedco findOneByUploadstatus(int $uploadstatus) Return the first ChildPaymentKedco filtered by the uploadstatus column *

 * @method     ChildPaymentKedco requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentKedco by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOne(ConnectionInterface $con = null) Return the first ChildPaymentKedco matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentKedco requireOneById(int $id) Return the first ChildPaymentKedco filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByAccountNo(string $account_no) Return the first ChildPaymentKedco filtered by the account_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByTransactionid(string $TransactionID) Return the first ChildPaymentKedco filtered by the TransactionID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByProductId(int $product_id) Return the first ChildPaymentKedco filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneBySourcebank(string $SourceBank) Return the first ChildPaymentKedco filtered by the SourceBank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByDestinationbank(string $DestinationBank) Return the first ChildPaymentKedco filtered by the DestinationBank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByEmail(string $Email) Return the first ChildPaymentKedco filtered by the Email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByPhone(string $Phone) Return the first ChildPaymentKedco filtered by the Phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByTransactiondate(string $TransactionDate) Return the first ChildPaymentKedco filtered by the TransactionDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByAmountPaid(string $amount_paid) Return the first ChildPaymentKedco filtered by the amount_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByPaymentchannel(string $PaymentChannel) Return the first ChildPaymentKedco filtered by the PaymentChannel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByCreatedDate(string $created_date) Return the first ChildPaymentKedco filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentKedco requireOneByUploadstatus(int $uploadstatus) Return the first ChildPaymentKedco filtered by the uploadstatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentKedco[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentKedco objects based on current ModelCriteria
 * @method     ChildPaymentKedco[]|ObjectCollection findById(int $id) Return ChildPaymentKedco objects filtered by the id column
 * @method     ChildPaymentKedco[]|ObjectCollection findByAccountNo(string $account_no) Return ChildPaymentKedco objects filtered by the account_no column
 * @method     ChildPaymentKedco[]|ObjectCollection findByTransactionid(string $TransactionID) Return ChildPaymentKedco objects filtered by the TransactionID column
 * @method     ChildPaymentKedco[]|ObjectCollection findByProductId(int $product_id) Return ChildPaymentKedco objects filtered by the product_id column
 * @method     ChildPaymentKedco[]|ObjectCollection findBySourcebank(string $SourceBank) Return ChildPaymentKedco objects filtered by the SourceBank column
 * @method     ChildPaymentKedco[]|ObjectCollection findByDestinationbank(string $DestinationBank) Return ChildPaymentKedco objects filtered by the DestinationBank column
 * @method     ChildPaymentKedco[]|ObjectCollection findByEmail(string $Email) Return ChildPaymentKedco objects filtered by the Email column
 * @method     ChildPaymentKedco[]|ObjectCollection findByPhone(string $Phone) Return ChildPaymentKedco objects filtered by the Phone column
 * @method     ChildPaymentKedco[]|ObjectCollection findByTransactiondate(string $TransactionDate) Return ChildPaymentKedco objects filtered by the TransactionDate column
 * @method     ChildPaymentKedco[]|ObjectCollection findByAmountPaid(string $amount_paid) Return ChildPaymentKedco objects filtered by the amount_paid column
 * @method     ChildPaymentKedco[]|ObjectCollection findByPaymentchannel(string $PaymentChannel) Return ChildPaymentKedco objects filtered by the PaymentChannel column
 * @method     ChildPaymentKedco[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildPaymentKedco objects filtered by the created_date column
 * @method     ChildPaymentKedco[]|ObjectCollection findByUploadstatus(int $uploadstatus) Return ChildPaymentKedco objects filtered by the uploadstatus column
 * @method     ChildPaymentKedco[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentKedcoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaymentKedcoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PaymentKedco', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentKedcoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentKedcoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentKedcoQuery) {
            return $criteria;
        }
        $query = new ChildPaymentKedcoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentKedco|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentKedcoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentKedcoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentKedco A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, account_no, TransactionID, product_id, SourceBank, DestinationBank, Email, Phone, TransactionDate, amount_paid, PaymentChannel, created_date, uploadstatus FROM payment_kedco WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentKedco $obj */
            $obj = new ChildPaymentKedco();
            $obj->hydrate($row);
            PaymentKedcoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentKedco|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the account_no column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountNo('fooValue');   // WHERE account_no = 'fooValue'
     * $query->filterByAccountNo('%fooValue%'); // WHERE account_no LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByAccountNo($accountNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountNo)) {
                $accountNo = str_replace('*', '%', $accountNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_ACCOUNT_NO, $accountNo, $comparison);
    }

    /**
     * Filter the query on the TransactionID column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionid('fooValue');   // WHERE TransactionID = 'fooValue'
     * $query->filterByTransactionid('%fooValue%'); // WHERE TransactionID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByTransactionid($transactionid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $transactionid)) {
                $transactionid = str_replace('*', '%', $transactionid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_TRANSACTIONID, $transactionid, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE product_id = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE product_id IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE product_id > 12
     * </code>
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the SourceBank column
     *
     * Example usage:
     * <code>
     * $query->filterBySourcebank('fooValue');   // WHERE SourceBank = 'fooValue'
     * $query->filterBySourcebank('%fooValue%'); // WHERE SourceBank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourcebank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterBySourcebank($sourcebank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourcebank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sourcebank)) {
                $sourcebank = str_replace('*', '%', $sourcebank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_SOURCEBANK, $sourcebank, $comparison);
    }

    /**
     * Filter the query on the DestinationBank column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationbank('fooValue');   // WHERE DestinationBank = 'fooValue'
     * $query->filterByDestinationbank('%fooValue%'); // WHERE DestinationBank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationbank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByDestinationbank($destinationbank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationbank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationbank)) {
                $destinationbank = str_replace('*', '%', $destinationbank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_DESTINATIONBANK, $destinationbank, $comparison);
    }

    /**
     * Filter the query on the Email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE Email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE Email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the Phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE Phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE Phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the TransactionDate column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactiondate('2011-03-14'); // WHERE TransactionDate = '2011-03-14'
     * $query->filterByTransactiondate('now'); // WHERE TransactionDate = '2011-03-14'
     * $query->filterByTransactiondate(array('max' => 'yesterday')); // WHERE TransactionDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $transactiondate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByTransactiondate($transactiondate = null, $comparison = null)
    {
        if (is_array($transactiondate)) {
            $useMinMax = false;
            if (isset($transactiondate['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_TRANSACTIONDATE, $transactiondate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($transactiondate['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_TRANSACTIONDATE, $transactiondate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_TRANSACTIONDATE, $transactiondate, $comparison);
    }

    /**
     * Filter the query on the amount_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountPaid(1234); // WHERE amount_paid = 1234
     * $query->filterByAmountPaid(array(12, 34)); // WHERE amount_paid IN (12, 34)
     * $query->filterByAmountPaid(array('min' => 12)); // WHERE amount_paid > 12
     * </code>
     *
     * @param     mixed $amountPaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByAmountPaid($amountPaid = null, $comparison = null)
    {
        if (is_array($amountPaid)) {
            $useMinMax = false;
            if (isset($amountPaid['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_AMOUNT_PAID, $amountPaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountPaid['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_AMOUNT_PAID, $amountPaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_AMOUNT_PAID, $amountPaid, $comparison);
    }

    /**
     * Filter the query on the PaymentChannel column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentchannel('fooValue');   // WHERE PaymentChannel = 'fooValue'
     * $query->filterByPaymentchannel('%fooValue%'); // WHERE PaymentChannel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentchannel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByPaymentchannel($paymentchannel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentchannel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentchannel)) {
                $paymentchannel = str_replace('*', '%', $paymentchannel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_PAYMENTCHANNEL, $paymentchannel, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the uploadstatus column
     *
     * Example usage:
     * <code>
     * $query->filterByUploadstatus(1234); // WHERE uploadstatus = 1234
     * $query->filterByUploadstatus(array(12, 34)); // WHERE uploadstatus IN (12, 34)
     * $query->filterByUploadstatus(array('min' => 12)); // WHERE uploadstatus > 12
     * </code>
     *
     * @param     mixed $uploadstatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function filterByUploadstatus($uploadstatus = null, $comparison = null)
    {
        if (is_array($uploadstatus)) {
            $useMinMax = false;
            if (isset($uploadstatus['min'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_UPLOADSTATUS, $uploadstatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uploadstatus['max'])) {
                $this->addUsingAlias(PaymentKedcoTableMap::COL_UPLOADSTATUS, $uploadstatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentKedcoTableMap::COL_UPLOADSTATUS, $uploadstatus, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentKedco $paymentKedco Object to remove from the list of results
     *
     * @return $this|ChildPaymentKedcoQuery The current query, for fluid interface
     */
    public function prune($paymentKedco = null)
    {
        if ($paymentKedco) {
            $this->addUsingAlias(PaymentKedcoTableMap::COL_ID, $paymentKedco->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_kedco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentKedcoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentKedcoTableMap::clearInstancePool();
            PaymentKedcoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentKedcoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentKedcoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentKedcoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentKedcoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentKedcoQuery
