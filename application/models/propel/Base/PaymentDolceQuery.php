<?php

namespace Base;

use \PaymentDolce as ChildPaymentDolce;
use \PaymentDolceQuery as ChildPaymentDolceQuery;
use \Exception;
use \PDO;
use Map\PaymentDolceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_dolce' table.
 *
 *
 *
 * @method     ChildPaymentDolceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentDolceQuery orderByTransactionId($order = Criteria::ASC) Order by the transaction_id column
 * @method     ChildPaymentDolceQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildPaymentDolceQuery orderByEvent($order = Criteria::ASC) Order by the event column
 * @method     ChildPaymentDolceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPaymentDolceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPaymentDolceQuery orderByTransactionDate($order = Criteria::ASC) Order by the transaction_date column
 * @method     ChildPaymentDolceQuery orderByCustomerName($order = Criteria::ASC) Order by the customer_name column
 * @method     ChildPaymentDolceQuery orderByAmountPaid($order = Criteria::ASC) Order by the amount_paid column
 * @method     ChildPaymentDolceQuery orderBySourceBank($order = Criteria::ASC) Order by the source_bank column
 * @method     ChildPaymentDolceQuery orderByDestinationBank($order = Criteria::ASC) Order by the destination_bank column
 * @method     ChildPaymentDolceQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildPaymentDolceQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildPaymentDolceQuery orderByPaymentChannel($order = Criteria::ASC) Order by the payment_channel column
 *
 * @method     ChildPaymentDolceQuery groupById() Group by the id column
 * @method     ChildPaymentDolceQuery groupByTransactionId() Group by the transaction_id column
 * @method     ChildPaymentDolceQuery groupByProductId() Group by the product_id column
 * @method     ChildPaymentDolceQuery groupByEvent() Group by the event column
 * @method     ChildPaymentDolceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPaymentDolceQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPaymentDolceQuery groupByTransactionDate() Group by the transaction_date column
 * @method     ChildPaymentDolceQuery groupByCustomerName() Group by the customer_name column
 * @method     ChildPaymentDolceQuery groupByAmountPaid() Group by the amount_paid column
 * @method     ChildPaymentDolceQuery groupBySourceBank() Group by the source_bank column
 * @method     ChildPaymentDolceQuery groupByDestinationBank() Group by the destination_bank column
 * @method     ChildPaymentDolceQuery groupByPhone() Group by the phone column
 * @method     ChildPaymentDolceQuery groupByEmail() Group by the email column
 * @method     ChildPaymentDolceQuery groupByPaymentChannel() Group by the payment_channel column
 *
 * @method     ChildPaymentDolceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentDolceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentDolceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentDolceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentDolceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentDolceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentDolce findOne(ConnectionInterface $con = null) Return the first ChildPaymentDolce matching the query
 * @method     ChildPaymentDolce findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentDolce matching the query, or a new ChildPaymentDolce object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentDolce findOneById(int $id) Return the first ChildPaymentDolce filtered by the id column
 * @method     ChildPaymentDolce findOneByTransactionId(string $transaction_id) Return the first ChildPaymentDolce filtered by the transaction_id column
 * @method     ChildPaymentDolce findOneByProductId(string $product_id) Return the first ChildPaymentDolce filtered by the product_id column
 * @method     ChildPaymentDolce findOneByEvent(string $event) Return the first ChildPaymentDolce filtered by the event column
 * @method     ChildPaymentDolce findOneByCreatedAt(string $created_at) Return the first ChildPaymentDolce filtered by the created_at column
 * @method     ChildPaymentDolce findOneByUpdatedAt(string $updated_at) Return the first ChildPaymentDolce filtered by the updated_at column
 * @method     ChildPaymentDolce findOneByTransactionDate(string $transaction_date) Return the first ChildPaymentDolce filtered by the transaction_date column
 * @method     ChildPaymentDolce findOneByCustomerName(string $customer_name) Return the first ChildPaymentDolce filtered by the customer_name column
 * @method     ChildPaymentDolce findOneByAmountPaid(string $amount_paid) Return the first ChildPaymentDolce filtered by the amount_paid column
 * @method     ChildPaymentDolce findOneBySourceBank(string $source_bank) Return the first ChildPaymentDolce filtered by the source_bank column
 * @method     ChildPaymentDolce findOneByDestinationBank(string $destination_bank) Return the first ChildPaymentDolce filtered by the destination_bank column
 * @method     ChildPaymentDolce findOneByPhone(string $phone) Return the first ChildPaymentDolce filtered by the phone column
 * @method     ChildPaymentDolce findOneByEmail(string $email) Return the first ChildPaymentDolce filtered by the email column
 * @method     ChildPaymentDolce findOneByPaymentChannel(string $payment_channel) Return the first ChildPaymentDolce filtered by the payment_channel column *

 * @method     ChildPaymentDolce requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentDolce by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOne(ConnectionInterface $con = null) Return the first ChildPaymentDolce matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentDolce requireOneById(int $id) Return the first ChildPaymentDolce filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByTransactionId(string $transaction_id) Return the first ChildPaymentDolce filtered by the transaction_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByProductId(string $product_id) Return the first ChildPaymentDolce filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByEvent(string $event) Return the first ChildPaymentDolce filtered by the event column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByCreatedAt(string $created_at) Return the first ChildPaymentDolce filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByUpdatedAt(string $updated_at) Return the first ChildPaymentDolce filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByTransactionDate(string $transaction_date) Return the first ChildPaymentDolce filtered by the transaction_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByCustomerName(string $customer_name) Return the first ChildPaymentDolce filtered by the customer_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByAmountPaid(string $amount_paid) Return the first ChildPaymentDolce filtered by the amount_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneBySourceBank(string $source_bank) Return the first ChildPaymentDolce filtered by the source_bank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByDestinationBank(string $destination_bank) Return the first ChildPaymentDolce filtered by the destination_bank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByPhone(string $phone) Return the first ChildPaymentDolce filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByEmail(string $email) Return the first ChildPaymentDolce filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentDolce requireOneByPaymentChannel(string $payment_channel) Return the first ChildPaymentDolce filtered by the payment_channel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentDolce[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentDolce objects based on current ModelCriteria
 * @method     ChildPaymentDolce[]|ObjectCollection findById(int $id) Return ChildPaymentDolce objects filtered by the id column
 * @method     ChildPaymentDolce[]|ObjectCollection findByTransactionId(string $transaction_id) Return ChildPaymentDolce objects filtered by the transaction_id column
 * @method     ChildPaymentDolce[]|ObjectCollection findByProductId(string $product_id) Return ChildPaymentDolce objects filtered by the product_id column
 * @method     ChildPaymentDolce[]|ObjectCollection findByEvent(string $event) Return ChildPaymentDolce objects filtered by the event column
 * @method     ChildPaymentDolce[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPaymentDolce objects filtered by the created_at column
 * @method     ChildPaymentDolce[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPaymentDolce objects filtered by the updated_at column
 * @method     ChildPaymentDolce[]|ObjectCollection findByTransactionDate(string $transaction_date) Return ChildPaymentDolce objects filtered by the transaction_date column
 * @method     ChildPaymentDolce[]|ObjectCollection findByCustomerName(string $customer_name) Return ChildPaymentDolce objects filtered by the customer_name column
 * @method     ChildPaymentDolce[]|ObjectCollection findByAmountPaid(string $amount_paid) Return ChildPaymentDolce objects filtered by the amount_paid column
 * @method     ChildPaymentDolce[]|ObjectCollection findBySourceBank(string $source_bank) Return ChildPaymentDolce objects filtered by the source_bank column
 * @method     ChildPaymentDolce[]|ObjectCollection findByDestinationBank(string $destination_bank) Return ChildPaymentDolce objects filtered by the destination_bank column
 * @method     ChildPaymentDolce[]|ObjectCollection findByPhone(string $phone) Return ChildPaymentDolce objects filtered by the phone column
 * @method     ChildPaymentDolce[]|ObjectCollection findByEmail(string $email) Return ChildPaymentDolce objects filtered by the email column
 * @method     ChildPaymentDolce[]|ObjectCollection findByPaymentChannel(string $payment_channel) Return ChildPaymentDolce objects filtered by the payment_channel column
 * @method     ChildPaymentDolce[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentDolceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaymentDolceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PaymentDolce', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentDolceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentDolceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentDolceQuery) {
            return $criteria;
        }
        $query = new ChildPaymentDolceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentDolce|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentDolceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentDolceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentDolce A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, transaction_id, product_id, event, created_at, updated_at, transaction_date, customer_name, amount_paid, source_bank, destination_bank, phone, email, payment_channel FROM payment_dolce WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentDolce $obj */
            $obj = new ChildPaymentDolce();
            $obj->hydrate($row);
            PaymentDolceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentDolce|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the transaction_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionId('fooValue');   // WHERE transaction_id = 'fooValue'
     * $query->filterByTransactionId('%fooValue%'); // WHERE transaction_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByTransactionId($transactionId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $transactionId)) {
                $transactionId = str_replace('*', '%', $transactionId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_TRANSACTION_ID, $transactionId, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId('fooValue');   // WHERE product_id = 'fooValue'
     * $query->filterByProductId('%fooValue%'); // WHERE product_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $productId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($productId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $productId)) {
                $productId = str_replace('*', '%', $productId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the event column
     *
     * Example usage:
     * <code>
     * $query->filterByEvent('fooValue');   // WHERE event = 'fooValue'
     * $query->filterByEvent('%fooValue%'); // WHERE event LIKE '%fooValue%'
     * </code>
     *
     * @param     string $event The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByEvent($event = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($event)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $event)) {
                $event = str_replace('*', '%', $event);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_EVENT, $event, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the transaction_date column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionDate('2011-03-14'); // WHERE transaction_date = '2011-03-14'
     * $query->filterByTransactionDate('now'); // WHERE transaction_date = '2011-03-14'
     * $query->filterByTransactionDate(array('max' => 'yesterday')); // WHERE transaction_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $transactionDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByTransactionDate($transactionDate = null, $comparison = null)
    {
        if (is_array($transactionDate)) {
            $useMinMax = false;
            if (isset($transactionDate['min'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_TRANSACTION_DATE, $transactionDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($transactionDate['max'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_TRANSACTION_DATE, $transactionDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_TRANSACTION_DATE, $transactionDate, $comparison);
    }

    /**
     * Filter the query on the customer_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerName('fooValue');   // WHERE customer_name = 'fooValue'
     * $query->filterByCustomerName('%fooValue%'); // WHERE customer_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByCustomerName($customerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $customerName)) {
                $customerName = str_replace('*', '%', $customerName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_CUSTOMER_NAME, $customerName, $comparison);
    }

    /**
     * Filter the query on the amount_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountPaid(1234); // WHERE amount_paid = 1234
     * $query->filterByAmountPaid(array(12, 34)); // WHERE amount_paid IN (12, 34)
     * $query->filterByAmountPaid(array('min' => 12)); // WHERE amount_paid > 12
     * </code>
     *
     * @param     mixed $amountPaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByAmountPaid($amountPaid = null, $comparison = null)
    {
        if (is_array($amountPaid)) {
            $useMinMax = false;
            if (isset($amountPaid['min'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_AMOUNT_PAID, $amountPaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountPaid['max'])) {
                $this->addUsingAlias(PaymentDolceTableMap::COL_AMOUNT_PAID, $amountPaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_AMOUNT_PAID, $amountPaid, $comparison);
    }

    /**
     * Filter the query on the source_bank column
     *
     * Example usage:
     * <code>
     * $query->filterBySourceBank('fooValue');   // WHERE source_bank = 'fooValue'
     * $query->filterBySourceBank('%fooValue%'); // WHERE source_bank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourceBank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterBySourceBank($sourceBank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourceBank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sourceBank)) {
                $sourceBank = str_replace('*', '%', $sourceBank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_SOURCE_BANK, $sourceBank, $comparison);
    }

    /**
     * Filter the query on the destination_bank column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationBank('fooValue');   // WHERE destination_bank = 'fooValue'
     * $query->filterByDestinationBank('%fooValue%'); // WHERE destination_bank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationBank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByDestinationBank($destinationBank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationBank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationBank)) {
                $destinationBank = str_replace('*', '%', $destinationBank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_DESTINATION_BANK, $destinationBank, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the payment_channel column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentChannel('fooValue');   // WHERE payment_channel = 'fooValue'
     * $query->filterByPaymentChannel('%fooValue%'); // WHERE payment_channel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentChannel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function filterByPaymentChannel($paymentChannel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentChannel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentChannel)) {
                $paymentChannel = str_replace('*', '%', $paymentChannel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentDolceTableMap::COL_PAYMENT_CHANNEL, $paymentChannel, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentDolce $paymentDolce Object to remove from the list of results
     *
     * @return $this|ChildPaymentDolceQuery The current query, for fluid interface
     */
    public function prune($paymentDolce = null)
    {
        if ($paymentDolce) {
            $this->addUsingAlias(PaymentDolceTableMap::COL_ID, $paymentDolce->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentDolceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentDolceTableMap::clearInstancePool();
            PaymentDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentDolceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentDolceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentDolceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentDolceQuery
