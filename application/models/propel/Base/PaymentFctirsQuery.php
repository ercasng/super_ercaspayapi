<?php

namespace Base;

use \PaymentFctirs as ChildPaymentFctirs;
use \PaymentFctirsQuery as ChildPaymentFctirsQuery;
use \Exception;
use \PDO;
use Map\PaymentFctirsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_fctirs' table.
 *
 *
 *
 * @method     ChildPaymentFctirsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentFctirsQuery orderBySessionId($order = Criteria::ASC) Order by the session_id column
 * @method     ChildPaymentFctirsQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method     ChildPaymentFctirsQuery orderByDateTime($order = Criteria::ASC) Order by the date_time column
 * @method     ChildPaymentFctirsQuery orderByExistingTin($order = Criteria::ASC) Order by the existing_tin column
 * @method     ChildPaymentFctirsQuery orderByJtbTin($order = Criteria::ASC) Order by the jtb_tin column
 * @method     ChildPaymentFctirsQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildPaymentFctirsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildPaymentFctirsQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildPaymentFctirsQuery orderByAmountPaid($order = Criteria::ASC) Order by the amount_paid column
 * @method     ChildPaymentFctirsQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method     ChildPaymentFctirsQuery orderByTax($order = Criteria::ASC) Order by the tax column
 * @method     ChildPaymentFctirsQuery orderByMonth($order = Criteria::ASC) Order by the month column
 * @method     ChildPaymentFctirsQuery orderByYear($order = Criteria::ASC) Order by the year column
 * @method     ChildPaymentFctirsQuery orderByCategory($order = Criteria::ASC) Order by the category column
 * @method     ChildPaymentFctirsQuery orderBySector($order = Criteria::ASC) Order by the sector column
 * @method     ChildPaymentFctirsQuery orderBySourceBank($order = Criteria::ASC) Order by the source_bank column
 * @method     ChildPaymentFctirsQuery orderByDestinationBank($order = Criteria::ASC) Order by the destination_bank column
 * @method     ChildPaymentFctirsQuery orderByDestinationAccountNumber($order = Criteria::ASC) Order by the destination_account_number column
 * @method     ChildPaymentFctirsQuery orderByDestinationAccountName($order = Criteria::ASC) Order by the destination_account_name column
 * @method     ChildPaymentFctirsQuery orderByInvoiceNumber($order = Criteria::ASC) Order by the invoice_number column
 * @method     ChildPaymentFctirsQuery orderByPaymentChannel($order = Criteria::ASC) Order by the payment_channel column
 * @method     ChildPaymentFctirsQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPaymentFctirsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPaymentFctirsQuery groupById() Group by the id column
 * @method     ChildPaymentFctirsQuery groupBySessionId() Group by the session_id column
 * @method     ChildPaymentFctirsQuery groupByDate() Group by the date column
 * @method     ChildPaymentFctirsQuery groupByDateTime() Group by the date_time column
 * @method     ChildPaymentFctirsQuery groupByExistingTin() Group by the existing_tin column
 * @method     ChildPaymentFctirsQuery groupByJtbTin() Group by the jtb_tin column
 * @method     ChildPaymentFctirsQuery groupByPhone() Group by the phone column
 * @method     ChildPaymentFctirsQuery groupByName() Group by the name column
 * @method     ChildPaymentFctirsQuery groupByEmail() Group by the email column
 * @method     ChildPaymentFctirsQuery groupByAmountPaid() Group by the amount_paid column
 * @method     ChildPaymentFctirsQuery groupByAddress() Group by the address column
 * @method     ChildPaymentFctirsQuery groupByTax() Group by the tax column
 * @method     ChildPaymentFctirsQuery groupByMonth() Group by the month column
 * @method     ChildPaymentFctirsQuery groupByYear() Group by the year column
 * @method     ChildPaymentFctirsQuery groupByCategory() Group by the category column
 * @method     ChildPaymentFctirsQuery groupBySector() Group by the sector column
 * @method     ChildPaymentFctirsQuery groupBySourceBank() Group by the source_bank column
 * @method     ChildPaymentFctirsQuery groupByDestinationBank() Group by the destination_bank column
 * @method     ChildPaymentFctirsQuery groupByDestinationAccountNumber() Group by the destination_account_number column
 * @method     ChildPaymentFctirsQuery groupByDestinationAccountName() Group by the destination_account_name column
 * @method     ChildPaymentFctirsQuery groupByInvoiceNumber() Group by the invoice_number column
 * @method     ChildPaymentFctirsQuery groupByPaymentChannel() Group by the payment_channel column
 * @method     ChildPaymentFctirsQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPaymentFctirsQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPaymentFctirsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentFctirsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentFctirsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentFctirsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentFctirsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentFctirsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentFctirsQuery leftJoinPaymentFctirsRecipients($relationAlias = null) Adds a LEFT JOIN clause to the query using the PaymentFctirsRecipients relation
 * @method     ChildPaymentFctirsQuery rightJoinPaymentFctirsRecipients($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PaymentFctirsRecipients relation
 * @method     ChildPaymentFctirsQuery innerJoinPaymentFctirsRecipients($relationAlias = null) Adds a INNER JOIN clause to the query using the PaymentFctirsRecipients relation
 *
 * @method     ChildPaymentFctirsQuery joinWithPaymentFctirsRecipients($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PaymentFctirsRecipients relation
 *
 * @method     ChildPaymentFctirsQuery leftJoinWithPaymentFctirsRecipients() Adds a LEFT JOIN clause and with to the query using the PaymentFctirsRecipients relation
 * @method     ChildPaymentFctirsQuery rightJoinWithPaymentFctirsRecipients() Adds a RIGHT JOIN clause and with to the query using the PaymentFctirsRecipients relation
 * @method     ChildPaymentFctirsQuery innerJoinWithPaymentFctirsRecipients() Adds a INNER JOIN clause and with to the query using the PaymentFctirsRecipients relation
 *
 * @method     \PaymentFctirsRecipientsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPaymentFctirs findOne(ConnectionInterface $con = null) Return the first ChildPaymentFctirs matching the query
 * @method     ChildPaymentFctirs findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentFctirs matching the query, or a new ChildPaymentFctirs object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentFctirs findOneById(int $id) Return the first ChildPaymentFctirs filtered by the id column
 * @method     ChildPaymentFctirs findOneBySessionId(string $session_id) Return the first ChildPaymentFctirs filtered by the session_id column
 * @method     ChildPaymentFctirs findOneByDate(string $date) Return the first ChildPaymentFctirs filtered by the date column
 * @method     ChildPaymentFctirs findOneByDateTime(string $date_time) Return the first ChildPaymentFctirs filtered by the date_time column
 * @method     ChildPaymentFctirs findOneByExistingTin(string $existing_tin) Return the first ChildPaymentFctirs filtered by the existing_tin column
 * @method     ChildPaymentFctirs findOneByJtbTin(string $jtb_tin) Return the first ChildPaymentFctirs filtered by the jtb_tin column
 * @method     ChildPaymentFctirs findOneByPhone(string $phone) Return the first ChildPaymentFctirs filtered by the phone column
 * @method     ChildPaymentFctirs findOneByName(string $name) Return the first ChildPaymentFctirs filtered by the name column
 * @method     ChildPaymentFctirs findOneByEmail(string $email) Return the first ChildPaymentFctirs filtered by the email column
 * @method     ChildPaymentFctirs findOneByAmountPaid(string $amount_paid) Return the first ChildPaymentFctirs filtered by the amount_paid column
 * @method     ChildPaymentFctirs findOneByAddress(string $address) Return the first ChildPaymentFctirs filtered by the address column
 * @method     ChildPaymentFctirs findOneByTax(string $tax) Return the first ChildPaymentFctirs filtered by the tax column
 * @method     ChildPaymentFctirs findOneByMonth(string $month) Return the first ChildPaymentFctirs filtered by the month column
 * @method     ChildPaymentFctirs findOneByYear(string $year) Return the first ChildPaymentFctirs filtered by the year column
 * @method     ChildPaymentFctirs findOneByCategory(string $category) Return the first ChildPaymentFctirs filtered by the category column
 * @method     ChildPaymentFctirs findOneBySector(string $sector) Return the first ChildPaymentFctirs filtered by the sector column
 * @method     ChildPaymentFctirs findOneBySourceBank(string $source_bank) Return the first ChildPaymentFctirs filtered by the source_bank column
 * @method     ChildPaymentFctirs findOneByDestinationBank(string $destination_bank) Return the first ChildPaymentFctirs filtered by the destination_bank column
 * @method     ChildPaymentFctirs findOneByDestinationAccountNumber(string $destination_account_number) Return the first ChildPaymentFctirs filtered by the destination_account_number column
 * @method     ChildPaymentFctirs findOneByDestinationAccountName(string $destination_account_name) Return the first ChildPaymentFctirs filtered by the destination_account_name column
 * @method     ChildPaymentFctirs findOneByInvoiceNumber(string $invoice_number) Return the first ChildPaymentFctirs filtered by the invoice_number column
 * @method     ChildPaymentFctirs findOneByPaymentChannel(string $payment_channel) Return the first ChildPaymentFctirs filtered by the payment_channel column
 * @method     ChildPaymentFctirs findOneByCreatedAt(string $created_at) Return the first ChildPaymentFctirs filtered by the created_at column
 * @method     ChildPaymentFctirs findOneByUpdatedAt(string $updated_at) Return the first ChildPaymentFctirs filtered by the updated_at column *

 * @method     ChildPaymentFctirs requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentFctirs by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOne(ConnectionInterface $con = null) Return the first ChildPaymentFctirs matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentFctirs requireOneById(int $id) Return the first ChildPaymentFctirs filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneBySessionId(string $session_id) Return the first ChildPaymentFctirs filtered by the session_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByDate(string $date) Return the first ChildPaymentFctirs filtered by the date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByDateTime(string $date_time) Return the first ChildPaymentFctirs filtered by the date_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByExistingTin(string $existing_tin) Return the first ChildPaymentFctirs filtered by the existing_tin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByJtbTin(string $jtb_tin) Return the first ChildPaymentFctirs filtered by the jtb_tin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByPhone(string $phone) Return the first ChildPaymentFctirs filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByName(string $name) Return the first ChildPaymentFctirs filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByEmail(string $email) Return the first ChildPaymentFctirs filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByAmountPaid(string $amount_paid) Return the first ChildPaymentFctirs filtered by the amount_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByAddress(string $address) Return the first ChildPaymentFctirs filtered by the address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByTax(string $tax) Return the first ChildPaymentFctirs filtered by the tax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByMonth(string $month) Return the first ChildPaymentFctirs filtered by the month column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByYear(string $year) Return the first ChildPaymentFctirs filtered by the year column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByCategory(string $category) Return the first ChildPaymentFctirs filtered by the category column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneBySector(string $sector) Return the first ChildPaymentFctirs filtered by the sector column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneBySourceBank(string $source_bank) Return the first ChildPaymentFctirs filtered by the source_bank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByDestinationBank(string $destination_bank) Return the first ChildPaymentFctirs filtered by the destination_bank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByDestinationAccountNumber(string $destination_account_number) Return the first ChildPaymentFctirs filtered by the destination_account_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByDestinationAccountName(string $destination_account_name) Return the first ChildPaymentFctirs filtered by the destination_account_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByInvoiceNumber(string $invoice_number) Return the first ChildPaymentFctirs filtered by the invoice_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByPaymentChannel(string $payment_channel) Return the first ChildPaymentFctirs filtered by the payment_channel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByCreatedAt(string $created_at) Return the first ChildPaymentFctirs filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentFctirs requireOneByUpdatedAt(string $updated_at) Return the first ChildPaymentFctirs filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentFctirs[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentFctirs objects based on current ModelCriteria
 * @method     ChildPaymentFctirs[]|ObjectCollection findById(int $id) Return ChildPaymentFctirs objects filtered by the id column
 * @method     ChildPaymentFctirs[]|ObjectCollection findBySessionId(string $session_id) Return ChildPaymentFctirs objects filtered by the session_id column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByDate(string $date) Return ChildPaymentFctirs objects filtered by the date column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByDateTime(string $date_time) Return ChildPaymentFctirs objects filtered by the date_time column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByExistingTin(string $existing_tin) Return ChildPaymentFctirs objects filtered by the existing_tin column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByJtbTin(string $jtb_tin) Return ChildPaymentFctirs objects filtered by the jtb_tin column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByPhone(string $phone) Return ChildPaymentFctirs objects filtered by the phone column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByName(string $name) Return ChildPaymentFctirs objects filtered by the name column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByEmail(string $email) Return ChildPaymentFctirs objects filtered by the email column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByAmountPaid(string $amount_paid) Return ChildPaymentFctirs objects filtered by the amount_paid column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByAddress(string $address) Return ChildPaymentFctirs objects filtered by the address column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByTax(string $tax) Return ChildPaymentFctirs objects filtered by the tax column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByMonth(string $month) Return ChildPaymentFctirs objects filtered by the month column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByYear(string $year) Return ChildPaymentFctirs objects filtered by the year column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByCategory(string $category) Return ChildPaymentFctirs objects filtered by the category column
 * @method     ChildPaymentFctirs[]|ObjectCollection findBySector(string $sector) Return ChildPaymentFctirs objects filtered by the sector column
 * @method     ChildPaymentFctirs[]|ObjectCollection findBySourceBank(string $source_bank) Return ChildPaymentFctirs objects filtered by the source_bank column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByDestinationBank(string $destination_bank) Return ChildPaymentFctirs objects filtered by the destination_bank column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByDestinationAccountNumber(string $destination_account_number) Return ChildPaymentFctirs objects filtered by the destination_account_number column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByDestinationAccountName(string $destination_account_name) Return ChildPaymentFctirs objects filtered by the destination_account_name column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByInvoiceNumber(string $invoice_number) Return ChildPaymentFctirs objects filtered by the invoice_number column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByPaymentChannel(string $payment_channel) Return ChildPaymentFctirs objects filtered by the payment_channel column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPaymentFctirs objects filtered by the created_at column
 * @method     ChildPaymentFctirs[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPaymentFctirs objects filtered by the updated_at column
 * @method     ChildPaymentFctirs[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentFctirsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaymentFctirsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PaymentFctirs', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentFctirsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentFctirsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentFctirsQuery) {
            return $criteria;
        }
        $query = new ChildPaymentFctirsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentFctirs|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentFctirsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentFctirsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentFctirs A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, session_id, date, date_time, existing_tin, jtb_tin, phone, name, email, amount_paid, address, tax, month, year, category, sector, source_bank, destination_bank, destination_account_number, destination_account_name, invoice_number, payment_channel, created_at, updated_at FROM payment_fctirs WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentFctirs $obj */
            $obj = new ChildPaymentFctirs();
            $obj->hydrate($row);
            PaymentFctirsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentFctirs|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the session_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySessionId('fooValue');   // WHERE session_id = 'fooValue'
     * $query->filterBySessionId('%fooValue%'); // WHERE session_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sessionId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterBySessionId($sessionId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sessionId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sessionId)) {
                $sessionId = str_replace('*', '%', $sessionId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_SESSION_ID, $sessionId, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE, $date, $comparison);
    }

    /**
     * Filter the query on the date_time column
     *
     * Example usage:
     * <code>
     * $query->filterByDateTime('2011-03-14'); // WHERE date_time = '2011-03-14'
     * $query->filterByDateTime('now'); // WHERE date_time = '2011-03-14'
     * $query->filterByDateTime(array('max' => 'yesterday')); // WHERE date_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByDateTime($dateTime = null, $comparison = null)
    {
        if (is_array($dateTime)) {
            $useMinMax = false;
            if (isset($dateTime['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE_TIME, $dateTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateTime['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE_TIME, $dateTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_DATE_TIME, $dateTime, $comparison);
    }

    /**
     * Filter the query on the existing_tin column
     *
     * Example usage:
     * <code>
     * $query->filterByExistingTin('fooValue');   // WHERE existing_tin = 'fooValue'
     * $query->filterByExistingTin('%fooValue%'); // WHERE existing_tin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $existingTin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByExistingTin($existingTin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($existingTin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $existingTin)) {
                $existingTin = str_replace('*', '%', $existingTin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_EXISTING_TIN, $existingTin, $comparison);
    }

    /**
     * Filter the query on the jtb_tin column
     *
     * Example usage:
     * <code>
     * $query->filterByJtbTin('fooValue');   // WHERE jtb_tin = 'fooValue'
     * $query->filterByJtbTin('%fooValue%'); // WHERE jtb_tin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jtbTin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByJtbTin($jtbTin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jtbTin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jtbTin)) {
                $jtbTin = str_replace('*', '%', $jtbTin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_JTB_TIN, $jtbTin, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the amount_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountPaid(1234); // WHERE amount_paid = 1234
     * $query->filterByAmountPaid(array(12, 34)); // WHERE amount_paid IN (12, 34)
     * $query->filterByAmountPaid(array('min' => 12)); // WHERE amount_paid > 12
     * </code>
     *
     * @param     mixed $amountPaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByAmountPaid($amountPaid = null, $comparison = null)
    {
        if (is_array($amountPaid)) {
            $useMinMax = false;
            if (isset($amountPaid['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_AMOUNT_PAID, $amountPaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountPaid['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_AMOUNT_PAID, $amountPaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_AMOUNT_PAID, $amountPaid, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the tax column
     *
     * Example usage:
     * <code>
     * $query->filterByTax('fooValue');   // WHERE tax = 'fooValue'
     * $query->filterByTax('%fooValue%'); // WHERE tax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByTax($tax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tax)) {
                $tax = str_replace('*', '%', $tax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_TAX, $tax, $comparison);
    }

    /**
     * Filter the query on the month column
     *
     * Example usage:
     * <code>
     * $query->filterByMonth('fooValue');   // WHERE month = 'fooValue'
     * $query->filterByMonth('%fooValue%'); // WHERE month LIKE '%fooValue%'
     * </code>
     *
     * @param     string $month The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByMonth($month = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($month)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $month)) {
                $month = str_replace('*', '%', $month);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_MONTH, $month, $comparison);
    }

    /**
     * Filter the query on the year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear('fooValue');   // WHERE year = 'fooValue'
     * $query->filterByYear('%fooValue%'); // WHERE year LIKE '%fooValue%'
     * </code>
     *
     * @param     string $year The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($year)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $year)) {
                $year = str_replace('*', '%', $year);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_YEAR, $year, $comparison);
    }

    /**
     * Filter the query on the category column
     *
     * Example usage:
     * <code>
     * $query->filterByCategory('fooValue');   // WHERE category = 'fooValue'
     * $query->filterByCategory('%fooValue%'); // WHERE category LIKE '%fooValue%'
     * </code>
     *
     * @param     string $category The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByCategory($category = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($category)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $category)) {
                $category = str_replace('*', '%', $category);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_CATEGORY, $category, $comparison);
    }

    /**
     * Filter the query on the sector column
     *
     * Example usage:
     * <code>
     * $query->filterBySector('fooValue');   // WHERE sector = 'fooValue'
     * $query->filterBySector('%fooValue%'); // WHERE sector LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sector The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterBySector($sector = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sector)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sector)) {
                $sector = str_replace('*', '%', $sector);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_SECTOR, $sector, $comparison);
    }

    /**
     * Filter the query on the source_bank column
     *
     * Example usage:
     * <code>
     * $query->filterBySourceBank('fooValue');   // WHERE source_bank = 'fooValue'
     * $query->filterBySourceBank('%fooValue%'); // WHERE source_bank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourceBank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterBySourceBank($sourceBank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourceBank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sourceBank)) {
                $sourceBank = str_replace('*', '%', $sourceBank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_SOURCE_BANK, $sourceBank, $comparison);
    }

    /**
     * Filter the query on the destination_bank column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationBank('fooValue');   // WHERE destination_bank = 'fooValue'
     * $query->filterByDestinationBank('%fooValue%'); // WHERE destination_bank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationBank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByDestinationBank($destinationBank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationBank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationBank)) {
                $destinationBank = str_replace('*', '%', $destinationBank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_DESTINATION_BANK, $destinationBank, $comparison);
    }

    /**
     * Filter the query on the destination_account_number column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationAccountNumber('fooValue');   // WHERE destination_account_number = 'fooValue'
     * $query->filterByDestinationAccountNumber('%fooValue%'); // WHERE destination_account_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationAccountNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByDestinationAccountNumber($destinationAccountNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationAccountNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationAccountNumber)) {
                $destinationAccountNumber = str_replace('*', '%', $destinationAccountNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NUMBER, $destinationAccountNumber, $comparison);
    }

    /**
     * Filter the query on the destination_account_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationAccountName('fooValue');   // WHERE destination_account_name = 'fooValue'
     * $query->filterByDestinationAccountName('%fooValue%'); // WHERE destination_account_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationAccountName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByDestinationAccountName($destinationAccountName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationAccountName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationAccountName)) {
                $destinationAccountName = str_replace('*', '%', $destinationAccountName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NAME, $destinationAccountName, $comparison);
    }

    /**
     * Filter the query on the invoice_number column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceNumber('fooValue');   // WHERE invoice_number = 'fooValue'
     * $query->filterByInvoiceNumber('%fooValue%'); // WHERE invoice_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByInvoiceNumber($invoiceNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $invoiceNumber)) {
                $invoiceNumber = str_replace('*', '%', $invoiceNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_INVOICE_NUMBER, $invoiceNumber, $comparison);
    }

    /**
     * Filter the query on the payment_channel column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentChannel('fooValue');   // WHERE payment_channel = 'fooValue'
     * $query->filterByPaymentChannel('%fooValue%'); // WHERE payment_channel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentChannel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByPaymentChannel($paymentChannel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentChannel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentChannel)) {
                $paymentChannel = str_replace('*', '%', $paymentChannel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_PAYMENT_CHANNEL, $paymentChannel, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PaymentFctirsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentFctirsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \PaymentFctirsRecipients object
     *
     * @param \PaymentFctirsRecipients|ObjectCollection $paymentFctirsRecipients the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function filterByPaymentFctirsRecipients($paymentFctirsRecipients, $comparison = null)
    {
        if ($paymentFctirsRecipients instanceof \PaymentFctirsRecipients) {
            return $this
                ->addUsingAlias(PaymentFctirsTableMap::COL_ID, $paymentFctirsRecipients->getPaymentFctirsId(), $comparison);
        } elseif ($paymentFctirsRecipients instanceof ObjectCollection) {
            return $this
                ->usePaymentFctirsRecipientsQuery()
                ->filterByPrimaryKeys($paymentFctirsRecipients->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPaymentFctirsRecipients() only accepts arguments of type \PaymentFctirsRecipients or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PaymentFctirsRecipients relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function joinPaymentFctirsRecipients($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PaymentFctirsRecipients');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PaymentFctirsRecipients');
        }

        return $this;
    }

    /**
     * Use the PaymentFctirsRecipients relation PaymentFctirsRecipients object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaymentFctirsRecipientsQuery A secondary query class using the current class as primary query
     */
    public function usePaymentFctirsRecipientsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPaymentFctirsRecipients($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PaymentFctirsRecipients', '\PaymentFctirsRecipientsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentFctirs $paymentFctirs Object to remove from the list of results
     *
     * @return $this|ChildPaymentFctirsQuery The current query, for fluid interface
     */
    public function prune($paymentFctirs = null)
    {
        if ($paymentFctirs) {
            $this->addUsingAlias(PaymentFctirsTableMap::COL_ID, $paymentFctirs->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_fctirs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentFctirsTableMap::clearInstancePool();
            PaymentFctirsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentFctirsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentFctirsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentFctirsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentFctirsQuery
