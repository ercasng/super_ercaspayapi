<?php

namespace Base;

use \FctirsInvoiceItem as ChildFctirsInvoiceItem;
use \FctirsInvoiceItemQuery as ChildFctirsInvoiceItemQuery;
use \Exception;
use \PDO;
use Map\FctirsInvoiceItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'fctirs_invoice_item' table.
 *
 *
 *
 * @method     ChildFctirsInvoiceItemQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFctirsInvoiceItemQuery orderByServiceName($order = Criteria::ASC) Order by the service_name column
 * @method     ChildFctirsInvoiceItemQuery orderByServicePaidforName($order = Criteria::ASC) Order by the service_paidfor_name column
 * @method     ChildFctirsInvoiceItemQuery orderByServicePaidforCode($order = Criteria::ASC) Order by the service_paidfor_code column
 * @method     ChildFctirsInvoiceItemQuery orderByDepartmentName($order = Criteria::ASC) Order by the department_name column
 * @method     ChildFctirsInvoiceItemQuery orderByDepartmentCode($order = Criteria::ASC) Order by the department_code column
 * @method     ChildFctirsInvoiceItemQuery orderByServiceFixedPrice($order = Criteria::ASC) Order by the service_fixed_price column
 * @method     ChildFctirsInvoiceItemQuery orderByInvoiceId($order = Criteria::ASC) Order by the invoice_id column
 * @method     ChildFctirsInvoiceItemQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFctirsInvoiceItemQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFctirsInvoiceItemQuery groupById() Group by the id column
 * @method     ChildFctirsInvoiceItemQuery groupByServiceName() Group by the service_name column
 * @method     ChildFctirsInvoiceItemQuery groupByServicePaidforName() Group by the service_paidfor_name column
 * @method     ChildFctirsInvoiceItemQuery groupByServicePaidforCode() Group by the service_paidfor_code column
 * @method     ChildFctirsInvoiceItemQuery groupByDepartmentName() Group by the department_name column
 * @method     ChildFctirsInvoiceItemQuery groupByDepartmentCode() Group by the department_code column
 * @method     ChildFctirsInvoiceItemQuery groupByServiceFixedPrice() Group by the service_fixed_price column
 * @method     ChildFctirsInvoiceItemQuery groupByInvoiceId() Group by the invoice_id column
 * @method     ChildFctirsInvoiceItemQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFctirsInvoiceItemQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFctirsInvoiceItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFctirsInvoiceItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFctirsInvoiceItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFctirsInvoiceItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFctirsInvoiceItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFctirsInvoiceItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFctirsInvoiceItemQuery leftJoinFctirsInvoice($relationAlias = null) Adds a LEFT JOIN clause to the query using the FctirsInvoice relation
 * @method     ChildFctirsInvoiceItemQuery rightJoinFctirsInvoice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FctirsInvoice relation
 * @method     ChildFctirsInvoiceItemQuery innerJoinFctirsInvoice($relationAlias = null) Adds a INNER JOIN clause to the query using the FctirsInvoice relation
 *
 * @method     ChildFctirsInvoiceItemQuery joinWithFctirsInvoice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FctirsInvoice relation
 *
 * @method     ChildFctirsInvoiceItemQuery leftJoinWithFctirsInvoice() Adds a LEFT JOIN clause and with to the query using the FctirsInvoice relation
 * @method     ChildFctirsInvoiceItemQuery rightJoinWithFctirsInvoice() Adds a RIGHT JOIN clause and with to the query using the FctirsInvoice relation
 * @method     ChildFctirsInvoiceItemQuery innerJoinWithFctirsInvoice() Adds a INNER JOIN clause and with to the query using the FctirsInvoice relation
 *
 * @method     \FctirsInvoiceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFctirsInvoiceItem findOne(ConnectionInterface $con = null) Return the first ChildFctirsInvoiceItem matching the query
 * @method     ChildFctirsInvoiceItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFctirsInvoiceItem matching the query, or a new ChildFctirsInvoiceItem object populated from the query conditions when no match is found
 *
 * @method     ChildFctirsInvoiceItem findOneById(int $id) Return the first ChildFctirsInvoiceItem filtered by the id column
 * @method     ChildFctirsInvoiceItem findOneByServiceName(string $service_name) Return the first ChildFctirsInvoiceItem filtered by the service_name column
 * @method     ChildFctirsInvoiceItem findOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFctirsInvoiceItem filtered by the service_paidfor_name column
 * @method     ChildFctirsInvoiceItem findOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFctirsInvoiceItem filtered by the service_paidfor_code column
 * @method     ChildFctirsInvoiceItem findOneByDepartmentName(string $department_name) Return the first ChildFctirsInvoiceItem filtered by the department_name column
 * @method     ChildFctirsInvoiceItem findOneByDepartmentCode(int $department_code) Return the first ChildFctirsInvoiceItem filtered by the department_code column
 * @method     ChildFctirsInvoiceItem findOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFctirsInvoiceItem filtered by the service_fixed_price column
 * @method     ChildFctirsInvoiceItem findOneByInvoiceId(int $invoice_id) Return the first ChildFctirsInvoiceItem filtered by the invoice_id column
 * @method     ChildFctirsInvoiceItem findOneByCreatedAt(string $created_at) Return the first ChildFctirsInvoiceItem filtered by the created_at column
 * @method     ChildFctirsInvoiceItem findOneByUpdatedAt(string $updated_at) Return the first ChildFctirsInvoiceItem filtered by the updated_at column *

 * @method     ChildFctirsInvoiceItem requirePk($key, ConnectionInterface $con = null) Return the ChildFctirsInvoiceItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOne(ConnectionInterface $con = null) Return the first ChildFctirsInvoiceItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctirsInvoiceItem requireOneById(int $id) Return the first ChildFctirsInvoiceItem filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByServiceName(string $service_name) Return the first ChildFctirsInvoiceItem filtered by the service_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFctirsInvoiceItem filtered by the service_paidfor_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFctirsInvoiceItem filtered by the service_paidfor_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByDepartmentName(string $department_name) Return the first ChildFctirsInvoiceItem filtered by the department_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByDepartmentCode(int $department_code) Return the first ChildFctirsInvoiceItem filtered by the department_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFctirsInvoiceItem filtered by the service_fixed_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByInvoiceId(int $invoice_id) Return the first ChildFctirsInvoiceItem filtered by the invoice_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByCreatedAt(string $created_at) Return the first ChildFctirsInvoiceItem filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoiceItem requireOneByUpdatedAt(string $updated_at) Return the first ChildFctirsInvoiceItem filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFctirsInvoiceItem objects based on current ModelCriteria
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findById(int $id) Return ChildFctirsInvoiceItem objects filtered by the id column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByServiceName(string $service_name) Return ChildFctirsInvoiceItem objects filtered by the service_name column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByServicePaidforName(string $service_paidfor_name) Return ChildFctirsInvoiceItem objects filtered by the service_paidfor_name column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByServicePaidforCode(int $service_paidfor_code) Return ChildFctirsInvoiceItem objects filtered by the service_paidfor_code column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByDepartmentName(string $department_name) Return ChildFctirsInvoiceItem objects filtered by the department_name column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByDepartmentCode(int $department_code) Return ChildFctirsInvoiceItem objects filtered by the department_code column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByServiceFixedPrice(string $service_fixed_price) Return ChildFctirsInvoiceItem objects filtered by the service_fixed_price column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByInvoiceId(int $invoice_id) Return ChildFctirsInvoiceItem objects filtered by the invoice_id column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFctirsInvoiceItem objects filtered by the created_at column
 * @method     ChildFctirsInvoiceItem[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFctirsInvoiceItem objects filtered by the updated_at column
 * @method     ChildFctirsInvoiceItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FctirsInvoiceItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FctirsInvoiceItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FctirsInvoiceItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFctirsInvoiceItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFctirsInvoiceItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFctirsInvoiceItemQuery) {
            return $criteria;
        }
        $query = new ChildFctirsInvoiceItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFctirsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FctirsInvoiceItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FctirsInvoiceItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFctirsInvoiceItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, service_name, service_paidfor_name, service_paidfor_code, department_name, department_code, service_fixed_price, invoice_id, created_at, updated_at FROM fctirs_invoice_item WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFctirsInvoiceItem $obj */
            $obj = new ChildFctirsInvoiceItem();
            $obj->hydrate($row);
            FctirsInvoiceItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFctirsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the service_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceName('fooValue');   // WHERE service_name = 'fooValue'
     * $query->filterByServiceName('%fooValue%'); // WHERE service_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serviceName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceName($serviceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serviceName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $serviceName)) {
                $serviceName = str_replace('*', '%', $serviceName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_NAME, $serviceName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforName('fooValue');   // WHERE service_paidfor_name = 'fooValue'
     * $query->filterByServicePaidforName('%fooValue%'); // WHERE service_paidfor_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicePaidforName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforName($servicePaidforName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicePaidforName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicePaidforName)) {
                $servicePaidforName = str_replace('*', '%', $servicePaidforName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_NAME, $servicePaidforName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_code column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforCode(1234); // WHERE service_paidfor_code = 1234
     * $query->filterByServicePaidforCode(array(12, 34)); // WHERE service_paidfor_code IN (12, 34)
     * $query->filterByServicePaidforCode(array('min' => 12)); // WHERE service_paidfor_code > 12
     * </code>
     *
     * @param     mixed $servicePaidforCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforCode($servicePaidforCode = null, $comparison = null)
    {
        if (is_array($servicePaidforCode)) {
            $useMinMax = false;
            if (isset($servicePaidforCode['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicePaidforCode['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode, $comparison);
    }

    /**
     * Filter the query on the department_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentName('fooValue');   // WHERE department_name = 'fooValue'
     * $query->filterByDepartmentName('%fooValue%'); // WHERE department_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departmentName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentName($departmentName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departmentName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departmentName)) {
                $departmentName = str_replace('*', '%', $departmentName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_DEPARTMENT_NAME, $departmentName, $comparison);
    }

    /**
     * Filter the query on the department_code column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentCode(1234); // WHERE department_code = 1234
     * $query->filterByDepartmentCode(array(12, 34)); // WHERE department_code IN (12, 34)
     * $query->filterByDepartmentCode(array('min' => 12)); // WHERE department_code > 12
     * </code>
     *
     * @param     mixed $departmentCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentCode($departmentCode = null, $comparison = null)
    {
        if (is_array($departmentCode)) {
            $useMinMax = false;
            if (isset($departmentCode['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($departmentCode['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode, $comparison);
    }

    /**
     * Filter the query on the service_fixed_price column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceFixedPrice(1234); // WHERE service_fixed_price = 1234
     * $query->filterByServiceFixedPrice(array(12, 34)); // WHERE service_fixed_price IN (12, 34)
     * $query->filterByServiceFixedPrice(array('min' => 12)); // WHERE service_fixed_price > 12
     * </code>
     *
     * @param     mixed $serviceFixedPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceFixedPrice($serviceFixedPrice = null, $comparison = null)
    {
        if (is_array($serviceFixedPrice)) {
            $useMinMax = false;
            if (isset($serviceFixedPrice['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceFixedPrice['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice, $comparison);
    }

    /**
     * Filter the query on the invoice_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceId(1234); // WHERE invoice_id = 1234
     * $query->filterByInvoiceId(array(12, 34)); // WHERE invoice_id IN (12, 34)
     * $query->filterByInvoiceId(array('min' => 12)); // WHERE invoice_id > 12
     * </code>
     *
     * @see       filterByFctirsInvoice()
     *
     * @param     mixed $invoiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByInvoiceId($invoiceId = null, $comparison = null)
    {
        if (is_array($invoiceId)) {
            $useMinMax = false;
            if (isset($invoiceId['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceId['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \FctirsInvoice object
     *
     * @param \FctirsInvoice|ObjectCollection $fctirsInvoice The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByFctirsInvoice($fctirsInvoice, $comparison = null)
    {
        if ($fctirsInvoice instanceof \FctirsInvoice) {
            return $this
                ->addUsingAlias(FctirsInvoiceItemTableMap::COL_INVOICE_ID, $fctirsInvoice->getId(), $comparison);
        } elseif ($fctirsInvoice instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FctirsInvoiceItemTableMap::COL_INVOICE_ID, $fctirsInvoice->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFctirsInvoice() only accepts arguments of type \FctirsInvoice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FctirsInvoice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function joinFctirsInvoice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FctirsInvoice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FctirsInvoice');
        }

        return $this;
    }

    /**
     * Use the FctirsInvoice relation FctirsInvoice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FctirsInvoiceQuery A secondary query class using the current class as primary query
     */
    public function useFctirsInvoiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFctirsInvoice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FctirsInvoice', '\FctirsInvoiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFctirsInvoiceItem $fctirsInvoiceItem Object to remove from the list of results
     *
     * @return $this|ChildFctirsInvoiceItemQuery The current query, for fluid interface
     */
    public function prune($fctirsInvoiceItem = null)
    {
        if ($fctirsInvoiceItem) {
            $this->addUsingAlias(FctirsInvoiceItemTableMap::COL_ID, $fctirsInvoiceItem->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fctirs_invoice_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctirsInvoiceItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FctirsInvoiceItemTableMap::clearInstancePool();
            FctirsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctirsInvoiceItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FctirsInvoiceItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FctirsInvoiceItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FctirsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FctirsInvoiceItemQuery
