<?php

namespace Base;

use \KedcoInvoice2Query as ChildKedcoInvoice2Query;
use \DateTime;
use \Exception;
use \PDO;
use Map\KedcoInvoice2TableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'kedco_invoice2' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class KedcoInvoice2 implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\KedcoInvoice2TableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the tx_code field.
     *
     * @var        string
     */
    protected $tx_code;

    /**
     * The value for the terminal field.
     *
     * @var        string
     */
    protected $terminal;

    /**
     * The value for the operator_code field.
     *
     * @var        string
     */
    protected $operator_code;

    /**
     * The value for the operator_name field.
     *
     * @var        string
     */
    protected $operator_name;

    /**
     * The value for the tx_date field.
     *
     * @var        DateTime
     */
    protected $tx_date;

    /**
     * The value for the tx_status field.
     *
     * @var        string
     */
    protected $tx_status;

    /**
     * The value for the total_amount_string field.
     *
     * @var        string
     */
    protected $total_amount_string;

    /**
     * The value for the total_amount field.
     *
     * @var        string
     */
    protected $total_amount;

    /**
     * The value for the payment_mode field.
     *
     * @var        string
     */
    protected $payment_mode;

    /**
     * The value for the payment_type field.
     *
     * @var        string
     */
    protected $payment_type;

    /**
     * The value for the cust_meter_id field.
     *
     * @var        string
     */
    protected $cust_meter_id;

    /**
     * The value for the customer_name field.
     *
     * @var        string
     */
    protected $customer_name;

    /**
     * The value for the customer_phone field.
     *
     * @var        string
     */
    protected $customer_phone;

    /**
     * The value for the app_version field.
     *
     * @var        string
     */
    protected $app_version;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\KedcoInvoice2 object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>KedcoInvoice2</code> instance.  If
     * <code>obj</code> is an instance of <code>KedcoInvoice2</code>, delegates to
     * <code>equals(KedcoInvoice2)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|KedcoInvoice2 The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [tx_code] column value.
     *
     * @return string
     */
    public function getTxCode()
    {
        return $this->tx_code;
    }

    /**
     * Get the [terminal] column value.
     *
     * @return string
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * Get the [operator_code] column value.
     *
     * @return string
     */
    public function getOperatorCode()
    {
        return $this->operator_code;
    }

    /**
     * Get the [operator_name] column value.
     *
     * @return string
     */
    public function getOperatorName()
    {
        return $this->operator_name;
    }

    /**
     * Get the [optionally formatted] temporal [tx_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTxDate($format = NULL)
    {
        if ($format === null) {
            return $this->tx_date;
        } else {
            return $this->tx_date instanceof \DateTimeInterface ? $this->tx_date->format($format) : null;
        }
    }

    /**
     * Get the [tx_status] column value.
     *
     * @return string
     */
    public function getTxStatus()
    {
        return $this->tx_status;
    }

    /**
     * Get the [total_amount_string] column value.
     *
     * @return string
     */
    public function getTotalAmountString()
    {
        return $this->total_amount_string;
    }

    /**
     * Get the [total_amount] column value.
     *
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * Get the [payment_mode] column value.
     *
     * @return string
     */
    public function getPaymentMode()
    {
        return $this->payment_mode;
    }

    /**
     * Get the [payment_type] column value.
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * Get the [cust_meter_id] column value.
     *
     * @return string
     */
    public function getCustMeterId()
    {
        return $this->cust_meter_id;
    }

    /**
     * Get the [customer_name] column value.
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer_name;
    }

    /**
     * Get the [customer_phone] column value.
     *
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * Get the [app_version] column value.
     *
     * @return string
     */
    public function getAppVersion()
    {
        return $this->app_version;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [tx_code] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTxCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tx_code !== $v) {
            $this->tx_code = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_TX_CODE] = true;
        }

        return $this;
    } // setTxCode()

    /**
     * Set the value of [terminal] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTerminal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->terminal !== $v) {
            $this->terminal = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_TERMINAL] = true;
        }

        return $this;
    } // setTerminal()

    /**
     * Set the value of [operator_code] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setOperatorCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->operator_code !== $v) {
            $this->operator_code = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_OPERATOR_CODE] = true;
        }

        return $this;
    } // setOperatorCode()

    /**
     * Set the value of [operator_name] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setOperatorName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->operator_name !== $v) {
            $this->operator_name = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_OPERATOR_NAME] = true;
        }

        return $this;
    } // setOperatorName()

    /**
     * Sets the value of [tx_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTxDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tx_date !== null || $dt !== null) {
            if ($this->tx_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->tx_date->format("Y-m-d H:i:s.u")) {
                $this->tx_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[KedcoInvoice2TableMap::COL_TX_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setTxDate()

    /**
     * Set the value of [tx_status] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTxStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tx_status !== $v) {
            $this->tx_status = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_TX_STATUS] = true;
        }

        return $this;
    } // setTxStatus()

    /**
     * Set the value of [total_amount_string] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTotalAmountString($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->total_amount_string !== $v) {
            $this->total_amount_string = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_TOTAL_AMOUNT_STRING] = true;
        }

        return $this;
    } // setTotalAmountString()

    /**
     * Set the value of [total_amount] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setTotalAmount($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->total_amount !== $v) {
            $this->total_amount = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_TOTAL_AMOUNT] = true;
        }

        return $this;
    } // setTotalAmount()

    /**
     * Set the value of [payment_mode] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setPaymentMode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_mode !== $v) {
            $this->payment_mode = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_PAYMENT_MODE] = true;
        }

        return $this;
    } // setPaymentMode()

    /**
     * Set the value of [payment_type] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setPaymentType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_type !== $v) {
            $this->payment_type = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_PAYMENT_TYPE] = true;
        }

        return $this;
    } // setPaymentType()

    /**
     * Set the value of [cust_meter_id] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setCustMeterId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cust_meter_id !== $v) {
            $this->cust_meter_id = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_CUST_METER_ID] = true;
        }

        return $this;
    } // setCustMeterId()

    /**
     * Set the value of [customer_name] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setCustomerName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_name !== $v) {
            $this->customer_name = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_CUSTOMER_NAME] = true;
        }

        return $this;
    } // setCustomerName()

    /**
     * Set the value of [customer_phone] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setCustomerPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_phone !== $v) {
            $this->customer_phone = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_CUSTOMER_PHONE] = true;
        }

        return $this;
    } // setCustomerPhone()

    /**
     * Set the value of [app_version] column.
     *
     * @param string $v new value
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setAppVersion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->app_version !== $v) {
            $this->app_version = $v;
            $this->modifiedColumns[KedcoInvoice2TableMap::COL_APP_VERSION] = true;
        }

        return $this;
    } // setAppVersion()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[KedcoInvoice2TableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\KedcoInvoice2 The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[KedcoInvoice2TableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : KedcoInvoice2TableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : KedcoInvoice2TableMap::translateFieldName('TxCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tx_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : KedcoInvoice2TableMap::translateFieldName('Terminal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->terminal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : KedcoInvoice2TableMap::translateFieldName('OperatorCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->operator_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : KedcoInvoice2TableMap::translateFieldName('OperatorName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->operator_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : KedcoInvoice2TableMap::translateFieldName('TxDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->tx_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : KedcoInvoice2TableMap::translateFieldName('TxStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tx_status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : KedcoInvoice2TableMap::translateFieldName('TotalAmountString', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total_amount_string = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : KedcoInvoice2TableMap::translateFieldName('TotalAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total_amount = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : KedcoInvoice2TableMap::translateFieldName('PaymentMode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payment_mode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : KedcoInvoice2TableMap::translateFieldName('PaymentType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payment_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : KedcoInvoice2TableMap::translateFieldName('CustMeterId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cust_meter_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : KedcoInvoice2TableMap::translateFieldName('CustomerName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : KedcoInvoice2TableMap::translateFieldName('CustomerPhone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : KedcoInvoice2TableMap::translateFieldName('AppVersion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->app_version = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : KedcoInvoice2TableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : KedcoInvoice2TableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 17; // 17 = KedcoInvoice2TableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\KedcoInvoice2'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(KedcoInvoice2TableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildKedcoInvoice2Query::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see KedcoInvoice2::setDeleted()
     * @see KedcoInvoice2::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice2TableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildKedcoInvoice2Query::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice2TableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                KedcoInvoice2TableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[KedcoInvoice2TableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . KedcoInvoice2TableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'tx_code';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TERMINAL)) {
            $modifiedColumns[':p' . $index++]  = 'terminal';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_OPERATOR_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'operator_code';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_OPERATOR_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'operator_name';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'tx_date';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'tx_status';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT_STRING)) {
            $modifiedColumns[':p' . $index++]  = 'total_amount_string';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'total_amount';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_PAYMENT_MODE)) {
            $modifiedColumns[':p' . $index++]  = 'payment_mode';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_PAYMENT_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'payment_type';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUST_METER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cust_meter_id';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUSTOMER_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'customer_name';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUSTOMER_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_phone';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_APP_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'app_version';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO kedco_invoice2 (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'tx_code':
                        $stmt->bindValue($identifier, $this->tx_code, PDO::PARAM_STR);
                        break;
                    case 'terminal':
                        $stmt->bindValue($identifier, $this->terminal, PDO::PARAM_INT);
                        break;
                    case 'operator_code':
                        $stmt->bindValue($identifier, $this->operator_code, PDO::PARAM_STR);
                        break;
                    case 'operator_name':
                        $stmt->bindValue($identifier, $this->operator_name, PDO::PARAM_STR);
                        break;
                    case 'tx_date':
                        $stmt->bindValue($identifier, $this->tx_date ? $this->tx_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'tx_status':
                        $stmt->bindValue($identifier, $this->tx_status, PDO::PARAM_STR);
                        break;
                    case 'total_amount_string':
                        $stmt->bindValue($identifier, $this->total_amount_string, PDO::PARAM_STR);
                        break;
                    case 'total_amount':
                        $stmt->bindValue($identifier, $this->total_amount, PDO::PARAM_STR);
                        break;
                    case 'payment_mode':
                        $stmt->bindValue($identifier, $this->payment_mode, PDO::PARAM_STR);
                        break;
                    case 'payment_type':
                        $stmt->bindValue($identifier, $this->payment_type, PDO::PARAM_STR);
                        break;
                    case 'cust_meter_id':
                        $stmt->bindValue($identifier, $this->cust_meter_id, PDO::PARAM_STR);
                        break;
                    case 'customer_name':
                        $stmt->bindValue($identifier, $this->customer_name, PDO::PARAM_STR);
                        break;
                    case 'customer_phone':
                        $stmt->bindValue($identifier, $this->customer_phone, PDO::PARAM_STR);
                        break;
                    case 'app_version':
                        $stmt->bindValue($identifier, $this->app_version, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = KedcoInvoice2TableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTxCode();
                break;
            case 2:
                return $this->getTerminal();
                break;
            case 3:
                return $this->getOperatorCode();
                break;
            case 4:
                return $this->getOperatorName();
                break;
            case 5:
                return $this->getTxDate();
                break;
            case 6:
                return $this->getTxStatus();
                break;
            case 7:
                return $this->getTotalAmountString();
                break;
            case 8:
                return $this->getTotalAmount();
                break;
            case 9:
                return $this->getPaymentMode();
                break;
            case 10:
                return $this->getPaymentType();
                break;
            case 11:
                return $this->getCustMeterId();
                break;
            case 12:
                return $this->getCustomerName();
                break;
            case 13:
                return $this->getCustomerPhone();
                break;
            case 14:
                return $this->getAppVersion();
                break;
            case 15:
                return $this->getCreatedAt();
                break;
            case 16:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['KedcoInvoice2'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['KedcoInvoice2'][$this->hashCode()] = true;
        $keys = KedcoInvoice2TableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTxCode(),
            $keys[2] => $this->getTerminal(),
            $keys[3] => $this->getOperatorCode(),
            $keys[4] => $this->getOperatorName(),
            $keys[5] => $this->getTxDate(),
            $keys[6] => $this->getTxStatus(),
            $keys[7] => $this->getTotalAmountString(),
            $keys[8] => $this->getTotalAmount(),
            $keys[9] => $this->getPaymentMode(),
            $keys[10] => $this->getPaymentType(),
            $keys[11] => $this->getCustMeterId(),
            $keys[12] => $this->getCustomerName(),
            $keys[13] => $this->getCustomerPhone(),
            $keys[14] => $this->getAppVersion(),
            $keys[15] => $this->getCreatedAt(),
            $keys[16] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\KedcoInvoice2
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = KedcoInvoice2TableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\KedcoInvoice2
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTxCode($value);
                break;
            case 2:
                $this->setTerminal($value);
                break;
            case 3:
                $this->setOperatorCode($value);
                break;
            case 4:
                $this->setOperatorName($value);
                break;
            case 5:
                $this->setTxDate($value);
                break;
            case 6:
                $this->setTxStatus($value);
                break;
            case 7:
                $this->setTotalAmountString($value);
                break;
            case 8:
                $this->setTotalAmount($value);
                break;
            case 9:
                $this->setPaymentMode($value);
                break;
            case 10:
                $this->setPaymentType($value);
                break;
            case 11:
                $this->setCustMeterId($value);
                break;
            case 12:
                $this->setCustomerName($value);
                break;
            case 13:
                $this->setCustomerPhone($value);
                break;
            case 14:
                $this->setAppVersion($value);
                break;
            case 15:
                $this->setCreatedAt($value);
                break;
            case 16:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = KedcoInvoice2TableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTxCode($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setTerminal($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setOperatorCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setOperatorName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTxDate($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTxStatus($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTotalAmountString($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTotalAmount($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPaymentMode($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPaymentType($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCustMeterId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCustomerName($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCustomerPhone($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setAppVersion($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCreatedAt($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setUpdatedAt($arr[$keys[16]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\KedcoInvoice2 The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(KedcoInvoice2TableMap::DATABASE_NAME);

        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_ID)) {
            $criteria->add(KedcoInvoice2TableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_CODE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TX_CODE, $this->tx_code);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TERMINAL)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TERMINAL, $this->terminal);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_OPERATOR_CODE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_OPERATOR_CODE, $this->operator_code);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_OPERATOR_NAME)) {
            $criteria->add(KedcoInvoice2TableMap::COL_OPERATOR_NAME, $this->operator_name);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_DATE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TX_DATE, $this->tx_date);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TX_STATUS)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TX_STATUS, $this->tx_status);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT_STRING)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT_STRING, $this->total_amount_string);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT)) {
            $criteria->add(KedcoInvoice2TableMap::COL_TOTAL_AMOUNT, $this->total_amount);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_PAYMENT_MODE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_PAYMENT_MODE, $this->payment_mode);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_PAYMENT_TYPE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_PAYMENT_TYPE, $this->payment_type);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUST_METER_ID)) {
            $criteria->add(KedcoInvoice2TableMap::COL_CUST_METER_ID, $this->cust_meter_id);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUSTOMER_NAME)) {
            $criteria->add(KedcoInvoice2TableMap::COL_CUSTOMER_NAME, $this->customer_name);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CUSTOMER_PHONE)) {
            $criteria->add(KedcoInvoice2TableMap::COL_CUSTOMER_PHONE, $this->customer_phone);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_APP_VERSION)) {
            $criteria->add(KedcoInvoice2TableMap::COL_APP_VERSION, $this->app_version);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_CREATED_AT)) {
            $criteria->add(KedcoInvoice2TableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(KedcoInvoice2TableMap::COL_UPDATED_AT)) {
            $criteria->add(KedcoInvoice2TableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildKedcoInvoice2Query::create();
        $criteria->add(KedcoInvoice2TableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \KedcoInvoice2 (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTxCode($this->getTxCode());
        $copyObj->setTerminal($this->getTerminal());
        $copyObj->setOperatorCode($this->getOperatorCode());
        $copyObj->setOperatorName($this->getOperatorName());
        $copyObj->setTxDate($this->getTxDate());
        $copyObj->setTxStatus($this->getTxStatus());
        $copyObj->setTotalAmountString($this->getTotalAmountString());
        $copyObj->setTotalAmount($this->getTotalAmount());
        $copyObj->setPaymentMode($this->getPaymentMode());
        $copyObj->setPaymentType($this->getPaymentType());
        $copyObj->setCustMeterId($this->getCustMeterId());
        $copyObj->setCustomerName($this->getCustomerName());
        $copyObj->setCustomerPhone($this->getCustomerPhone());
        $copyObj->setAppVersion($this->getAppVersion());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \KedcoInvoice2 Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->tx_code = null;
        $this->terminal = null;
        $this->operator_code = null;
        $this->operator_name = null;
        $this->tx_date = null;
        $this->tx_status = null;
        $this->total_amount_string = null;
        $this->total_amount = null;
        $this->payment_mode = null;
        $this->payment_type = null;
        $this->cust_meter_id = null;
        $this->customer_name = null;
        $this->customer_phone = null;
        $this->app_version = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(KedcoInvoice2TableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
