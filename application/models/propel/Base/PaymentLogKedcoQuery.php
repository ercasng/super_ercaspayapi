<?php

namespace Base;

use \PaymentLogKedco as ChildPaymentLogKedco;
use \PaymentLogKedcoQuery as ChildPaymentLogKedcoQuery;
use \Exception;
use \PDO;
use Map\PaymentLogKedcoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_log_kedco' table.
 *
 *
 *
 * @method     ChildPaymentLogKedcoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentLogKedcoQuery orderByCpayRef($order = Criteria::ASC) Order by the cpay_ref column
 * @method     ChildPaymentLogKedcoQuery orderByTransactionId($order = Criteria::ASC) Order by the transaction_id column
 * @method     ChildPaymentLogKedcoQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildPaymentLogKedcoQuery orderByProductDescription($order = Criteria::ASC) Order by the product_description column
 * @method     ChildPaymentLogKedcoQuery orderByMerchantId($order = Criteria::ASC) Order by the merchant_id column
 * @method     ChildPaymentLogKedcoQuery orderByResponseUrl($order = Criteria::ASC) Order by the response_url column
 * @method     ChildPaymentLogKedcoQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 * @method     ChildPaymentLogKedcoQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildPaymentLogKedcoQuery orderByHash($order = Criteria::ASC) Order by the hash column
 * @method     ChildPaymentLogKedcoQuery orderByTimeStamp($order = Criteria::ASC) Order by the time_stamp column
 *
 * @method     ChildPaymentLogKedcoQuery groupById() Group by the id column
 * @method     ChildPaymentLogKedcoQuery groupByCpayRef() Group by the cpay_ref column
 * @method     ChildPaymentLogKedcoQuery groupByTransactionId() Group by the transaction_id column
 * @method     ChildPaymentLogKedcoQuery groupByProductId() Group by the product_id column
 * @method     ChildPaymentLogKedcoQuery groupByProductDescription() Group by the product_description column
 * @method     ChildPaymentLogKedcoQuery groupByMerchantId() Group by the merchant_id column
 * @method     ChildPaymentLogKedcoQuery groupByResponseUrl() Group by the response_url column
 * @method     ChildPaymentLogKedcoQuery groupByCurrency() Group by the currency column
 * @method     ChildPaymentLogKedcoQuery groupByAmount() Group by the amount column
 * @method     ChildPaymentLogKedcoQuery groupByHash() Group by the hash column
 * @method     ChildPaymentLogKedcoQuery groupByTimeStamp() Group by the time_stamp column
 *
 * @method     ChildPaymentLogKedcoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentLogKedcoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentLogKedcoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentLogKedcoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentLogKedcoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentLogKedcoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentLogKedco findOne(ConnectionInterface $con = null) Return the first ChildPaymentLogKedco matching the query
 * @method     ChildPaymentLogKedco findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentLogKedco matching the query, or a new ChildPaymentLogKedco object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentLogKedco findOneById(int $id) Return the first ChildPaymentLogKedco filtered by the id column
 * @method     ChildPaymentLogKedco findOneByCpayRef(string $cpay_ref) Return the first ChildPaymentLogKedco filtered by the cpay_ref column
 * @method     ChildPaymentLogKedco findOneByTransactionId(string $transaction_id) Return the first ChildPaymentLogKedco filtered by the transaction_id column
 * @method     ChildPaymentLogKedco findOneByProductId(string $product_id) Return the first ChildPaymentLogKedco filtered by the product_id column
 * @method     ChildPaymentLogKedco findOneByProductDescription(string $product_description) Return the first ChildPaymentLogKedco filtered by the product_description column
 * @method     ChildPaymentLogKedco findOneByMerchantId(string $merchant_id) Return the first ChildPaymentLogKedco filtered by the merchant_id column
 * @method     ChildPaymentLogKedco findOneByResponseUrl(string $response_url) Return the first ChildPaymentLogKedco filtered by the response_url column
 * @method     ChildPaymentLogKedco findOneByCurrency(string $currency) Return the first ChildPaymentLogKedco filtered by the currency column
 * @method     ChildPaymentLogKedco findOneByAmount(string $amount) Return the first ChildPaymentLogKedco filtered by the amount column
 * @method     ChildPaymentLogKedco findOneByHash(string $hash) Return the first ChildPaymentLogKedco filtered by the hash column
 * @method     ChildPaymentLogKedco findOneByTimeStamp(string $time_stamp) Return the first ChildPaymentLogKedco filtered by the time_stamp column *

 * @method     ChildPaymentLogKedco requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentLogKedco by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOne(ConnectionInterface $con = null) Return the first ChildPaymentLogKedco matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentLogKedco requireOneById(int $id) Return the first ChildPaymentLogKedco filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByCpayRef(string $cpay_ref) Return the first ChildPaymentLogKedco filtered by the cpay_ref column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByTransactionId(string $transaction_id) Return the first ChildPaymentLogKedco filtered by the transaction_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByProductId(string $product_id) Return the first ChildPaymentLogKedco filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByProductDescription(string $product_description) Return the first ChildPaymentLogKedco filtered by the product_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByMerchantId(string $merchant_id) Return the first ChildPaymentLogKedco filtered by the merchant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByResponseUrl(string $response_url) Return the first ChildPaymentLogKedco filtered by the response_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByCurrency(string $currency) Return the first ChildPaymentLogKedco filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByAmount(string $amount) Return the first ChildPaymentLogKedco filtered by the amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByHash(string $hash) Return the first ChildPaymentLogKedco filtered by the hash column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentLogKedco requireOneByTimeStamp(string $time_stamp) Return the first ChildPaymentLogKedco filtered by the time_stamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentLogKedco[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentLogKedco objects based on current ModelCriteria
 * @method     ChildPaymentLogKedco[]|ObjectCollection findById(int $id) Return ChildPaymentLogKedco objects filtered by the id column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByCpayRef(string $cpay_ref) Return ChildPaymentLogKedco objects filtered by the cpay_ref column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByTransactionId(string $transaction_id) Return ChildPaymentLogKedco objects filtered by the transaction_id column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByProductId(string $product_id) Return ChildPaymentLogKedco objects filtered by the product_id column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByProductDescription(string $product_description) Return ChildPaymentLogKedco objects filtered by the product_description column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByMerchantId(string $merchant_id) Return ChildPaymentLogKedco objects filtered by the merchant_id column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByResponseUrl(string $response_url) Return ChildPaymentLogKedco objects filtered by the response_url column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByCurrency(string $currency) Return ChildPaymentLogKedco objects filtered by the currency column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByAmount(string $amount) Return ChildPaymentLogKedco objects filtered by the amount column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByHash(string $hash) Return ChildPaymentLogKedco objects filtered by the hash column
 * @method     ChildPaymentLogKedco[]|ObjectCollection findByTimeStamp(string $time_stamp) Return ChildPaymentLogKedco objects filtered by the time_stamp column
 * @method     ChildPaymentLogKedco[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentLogKedcoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaymentLogKedcoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PaymentLogKedco', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentLogKedcoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentLogKedcoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentLogKedcoQuery) {
            return $criteria;
        }
        $query = new ChildPaymentLogKedcoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentLogKedco|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentLogKedcoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentLogKedcoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentLogKedco A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, cpay_ref, transaction_id, product_id, product_description, merchant_id, response_url, currency, amount, hash, time_stamp FROM payment_log_kedco WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentLogKedco $obj */
            $obj = new ChildPaymentLogKedco();
            $obj->hydrate($row);
            PaymentLogKedcoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentLogKedco|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the cpay_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByCpayRef('fooValue');   // WHERE cpay_ref = 'fooValue'
     * $query->filterByCpayRef('%fooValue%'); // WHERE cpay_ref LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpayRef The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByCpayRef($cpayRef = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpayRef)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cpayRef)) {
                $cpayRef = str_replace('*', '%', $cpayRef);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_CPAY_REF, $cpayRef, $comparison);
    }

    /**
     * Filter the query on the transaction_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionId('fooValue');   // WHERE transaction_id = 'fooValue'
     * $query->filterByTransactionId('%fooValue%'); // WHERE transaction_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByTransactionId($transactionId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $transactionId)) {
                $transactionId = str_replace('*', '%', $transactionId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_TRANSACTION_ID, $transactionId, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId('fooValue');   // WHERE product_id = 'fooValue'
     * $query->filterByProductId('%fooValue%'); // WHERE product_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $productId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($productId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $productId)) {
                $productId = str_replace('*', '%', $productId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the product_description column
     *
     * Example usage:
     * <code>
     * $query->filterByProductDescription('fooValue');   // WHERE product_description = 'fooValue'
     * $query->filterByProductDescription('%fooValue%'); // WHERE product_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $productDescription The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByProductDescription($productDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($productDescription)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $productDescription)) {
                $productDescription = str_replace('*', '%', $productDescription);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_PRODUCT_DESCRIPTION, $productDescription, $comparison);
    }

    /**
     * Filter the query on the merchant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMerchantId('fooValue');   // WHERE merchant_id = 'fooValue'
     * $query->filterByMerchantId('%fooValue%'); // WHERE merchant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $merchantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByMerchantId($merchantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($merchantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $merchantId)) {
                $merchantId = str_replace('*', '%', $merchantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_MERCHANT_ID, $merchantId, $comparison);
    }

    /**
     * Filter the query on the response_url column
     *
     * Example usage:
     * <code>
     * $query->filterByResponseUrl('fooValue');   // WHERE response_url = 'fooValue'
     * $query->filterByResponseUrl('%fooValue%'); // WHERE response_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responseUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByResponseUrl($responseUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responseUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responseUrl)) {
                $responseUrl = str_replace('*', '%', $responseUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_RESPONSE_URL, $responseUrl, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%'); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $currency)) {
                $currency = str_replace('*', '%', $currency);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount('fooValue');   // WHERE amount = 'fooValue'
     * $query->filterByAmount('%fooValue%'); // WHERE amount LIKE '%fooValue%'
     * </code>
     *
     * @param     string $amount The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($amount)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $amount)) {
                $amount = str_replace('*', '%', $amount);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_HASH, $hash, $comparison);
    }

    /**
     * Filter the query on the time_stamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeStamp('2011-03-14'); // WHERE time_stamp = '2011-03-14'
     * $query->filterByTimeStamp('now'); // WHERE time_stamp = '2011-03-14'
     * $query->filterByTimeStamp(array('max' => 'yesterday')); // WHERE time_stamp > '2011-03-13'
     * </code>
     *
     * @param     mixed $timeStamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function filterByTimeStamp($timeStamp = null, $comparison = null)
    {
        if (is_array($timeStamp)) {
            $useMinMax = false;
            if (isset($timeStamp['min'])) {
                $this->addUsingAlias(PaymentLogKedcoTableMap::COL_TIME_STAMP, $timeStamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timeStamp['max'])) {
                $this->addUsingAlias(PaymentLogKedcoTableMap::COL_TIME_STAMP, $timeStamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentLogKedcoTableMap::COL_TIME_STAMP, $timeStamp, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentLogKedco $paymentLogKedco Object to remove from the list of results
     *
     * @return $this|ChildPaymentLogKedcoQuery The current query, for fluid interface
     */
    public function prune($paymentLogKedco = null)
    {
        if ($paymentLogKedco) {
            $this->addUsingAlias(PaymentLogKedcoTableMap::COL_ID, $paymentLogKedco->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_log_kedco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentLogKedcoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentLogKedcoTableMap::clearInstancePool();
            PaymentLogKedcoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentLogKedcoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentLogKedcoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentLogKedcoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentLogKedcoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentLogKedcoQuery
