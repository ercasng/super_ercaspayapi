<?php

namespace Base;

use \ErcasBillTravelfix as ChildErcasBillTravelfix;
use \ErcasBillTravelfixQuery as ChildErcasBillTravelfixQuery;
use \Exception;
use \PDO;
use Map\ErcasBillTravelfixTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ercas_bill_travelfix' table.
 *
 *
 *
 * @method     ChildErcasBillTravelfixQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildErcasBillTravelfixQuery orderByBillId($order = Criteria::ASC) Order by the bill_id column
 * @method     ChildErcasBillTravelfixQuery orderByMerchantId($order = Criteria::ASC) Order by the merchant_id column
 * @method     ChildErcasBillTravelfixQuery orderByCustomerName($order = Criteria::ASC) Order by the customer_name column
 * @method     ChildErcasBillTravelfixQuery orderByAmountDue($order = Criteria::ASC) Order by the amount_due column
 * @method     ChildErcasBillTravelfixQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildErcasBillTravelfixQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildErcasBillTravelfixQuery groupById() Group by the id column
 * @method     ChildErcasBillTravelfixQuery groupByBillId() Group by the bill_id column
 * @method     ChildErcasBillTravelfixQuery groupByMerchantId() Group by the merchant_id column
 * @method     ChildErcasBillTravelfixQuery groupByCustomerName() Group by the customer_name column
 * @method     ChildErcasBillTravelfixQuery groupByAmountDue() Group by the amount_due column
 * @method     ChildErcasBillTravelfixQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildErcasBillTravelfixQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildErcasBillTravelfixQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildErcasBillTravelfixQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildErcasBillTravelfixQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildErcasBillTravelfixQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildErcasBillTravelfixQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildErcasBillTravelfixQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildErcasBillTravelfix findOne(ConnectionInterface $con = null) Return the first ChildErcasBillTravelfix matching the query
 * @method     ChildErcasBillTravelfix findOneOrCreate(ConnectionInterface $con = null) Return the first ChildErcasBillTravelfix matching the query, or a new ChildErcasBillTravelfix object populated from the query conditions when no match is found
 *
 * @method     ChildErcasBillTravelfix findOneById(int $id) Return the first ChildErcasBillTravelfix filtered by the id column
 * @method     ChildErcasBillTravelfix findOneByBillId(string $bill_id) Return the first ChildErcasBillTravelfix filtered by the bill_id column
 * @method     ChildErcasBillTravelfix findOneByMerchantId(string $merchant_id) Return the first ChildErcasBillTravelfix filtered by the merchant_id column
 * @method     ChildErcasBillTravelfix findOneByCustomerName(string $customer_name) Return the first ChildErcasBillTravelfix filtered by the customer_name column
 * @method     ChildErcasBillTravelfix findOneByAmountDue(string $amount_due) Return the first ChildErcasBillTravelfix filtered by the amount_due column
 * @method     ChildErcasBillTravelfix findOneByCreatedAt(string $created_at) Return the first ChildErcasBillTravelfix filtered by the created_at column
 * @method     ChildErcasBillTravelfix findOneByUpdatedAt(string $updated_at) Return the first ChildErcasBillTravelfix filtered by the updated_at column *

 * @method     ChildErcasBillTravelfix requirePk($key, ConnectionInterface $con = null) Return the ChildErcasBillTravelfix by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOne(ConnectionInterface $con = null) Return the first ChildErcasBillTravelfix matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBillTravelfix requireOneById(int $id) Return the first ChildErcasBillTravelfix filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByBillId(string $bill_id) Return the first ChildErcasBillTravelfix filtered by the bill_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByMerchantId(string $merchant_id) Return the first ChildErcasBillTravelfix filtered by the merchant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByCustomerName(string $customer_name) Return the first ChildErcasBillTravelfix filtered by the customer_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByAmountDue(string $amount_due) Return the first ChildErcasBillTravelfix filtered by the amount_due column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByCreatedAt(string $created_at) Return the first ChildErcasBillTravelfix filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillTravelfix requireOneByUpdatedAt(string $updated_at) Return the first ChildErcasBillTravelfix filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBillTravelfix[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildErcasBillTravelfix objects based on current ModelCriteria
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findById(int $id) Return ChildErcasBillTravelfix objects filtered by the id column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByBillId(string $bill_id) Return ChildErcasBillTravelfix objects filtered by the bill_id column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByMerchantId(string $merchant_id) Return ChildErcasBillTravelfix objects filtered by the merchant_id column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByCustomerName(string $customer_name) Return ChildErcasBillTravelfix objects filtered by the customer_name column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByAmountDue(string $amount_due) Return ChildErcasBillTravelfix objects filtered by the amount_due column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildErcasBillTravelfix objects filtered by the created_at column
 * @method     ChildErcasBillTravelfix[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildErcasBillTravelfix objects filtered by the updated_at column
 * @method     ChildErcasBillTravelfix[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ErcasBillTravelfixQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ErcasBillTravelfixQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ErcasBillTravelfix', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildErcasBillTravelfixQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildErcasBillTravelfixQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildErcasBillTravelfixQuery) {
            return $criteria;
        }
        $query = new ChildErcasBillTravelfixQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildErcasBillTravelfix|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ErcasBillTravelfixTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ErcasBillTravelfixTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildErcasBillTravelfix A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, bill_id, merchant_id, customer_name, amount_due, created_at, updated_at FROM ercas_bill_travelfix WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildErcasBillTravelfix $obj */
            $obj = new ChildErcasBillTravelfix();
            $obj->hydrate($row);
            ErcasBillTravelfixTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildErcasBillTravelfix|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the bill_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBillId('fooValue');   // WHERE bill_id = 'fooValue'
     * $query->filterByBillId('%fooValue%'); // WHERE bill_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByBillId($billId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billId)) {
                $billId = str_replace('*', '%', $billId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_BILL_ID, $billId, $comparison);
    }

    /**
     * Filter the query on the merchant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMerchantId('fooValue');   // WHERE merchant_id = 'fooValue'
     * $query->filterByMerchantId('%fooValue%'); // WHERE merchant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $merchantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByMerchantId($merchantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($merchantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $merchantId)) {
                $merchantId = str_replace('*', '%', $merchantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_MERCHANT_ID, $merchantId, $comparison);
    }

    /**
     * Filter the query on the customer_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerName('fooValue');   // WHERE customer_name = 'fooValue'
     * $query->filterByCustomerName('%fooValue%'); // WHERE customer_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByCustomerName($customerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $customerName)) {
                $customerName = str_replace('*', '%', $customerName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_CUSTOMER_NAME, $customerName, $comparison);
    }

    /**
     * Filter the query on the amount_due column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountDue(1234); // WHERE amount_due = 1234
     * $query->filterByAmountDue(array(12, 34)); // WHERE amount_due IN (12, 34)
     * $query->filterByAmountDue(array('min' => 12)); // WHERE amount_due > 12
     * </code>
     *
     * @param     mixed $amountDue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByAmountDue($amountDue = null, $comparison = null)
    {
        if (is_array($amountDue)) {
            $useMinMax = false;
            if (isset($amountDue['min'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_AMOUNT_DUE, $amountDue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountDue['max'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_AMOUNT_DUE, $amountDue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_AMOUNT_DUE, $amountDue, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildErcasBillTravelfix $ercasBillTravelfix Object to remove from the list of results
     *
     * @return $this|ChildErcasBillTravelfixQuery The current query, for fluid interface
     */
    public function prune($ercasBillTravelfix = null)
    {
        if ($ercasBillTravelfix) {
            $this->addUsingAlias(ErcasBillTravelfixTableMap::COL_ID, $ercasBillTravelfix->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ercas_bill_travelfix table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTravelfixTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ErcasBillTravelfixTableMap::clearInstancePool();
            ErcasBillTravelfixTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTravelfixTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ErcasBillTravelfixTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ErcasBillTravelfixTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ErcasBillTravelfixTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ErcasBillTravelfixQuery
