<?php

namespace Base;

use \ErcasBill as ChildErcasBill;
use \ErcasBillQuery as ChildErcasBillQuery;
use \Exception;
use \PDO;
use Map\ErcasBillTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ercas_bill' table.
 *
 *
 *
 * @method     ChildErcasBillQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildErcasBillQuery orderByRefNo($order = Criteria::ASC) Order by the ref_no column
 * @method     ChildErcasBillQuery orderByUniquekeyid($order = Criteria::ASC) Order by the UniqueKeyID column
 * @method     ChildErcasBillQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildErcasBillQuery orderByAccountNo($order = Criteria::ASC) Order by the account_no column
 * @method     ChildErcasBillQuery orderByServiceType($order = Criteria::ASC) Order by the service_type column
 * @method     ChildErcasBillQuery orderByDueDate($order = Criteria::ASC) Order by the due_date column
 * @method     ChildErcasBillQuery orderByBillingFrom($order = Criteria::ASC) Order by the billing_from column
 * @method     ChildErcasBillQuery orderByBillingTo($order = Criteria::ASC) Order by the billing_to column
 * @method     ChildErcasBillQuery orderByBillDate($order = Criteria::ASC) Order by the bill_date column
 * @method     ChildErcasBillQuery orderByCurrentCharge($order = Criteria::ASC) Order by the current_charge column
 * @method     ChildErcasBillQuery orderByServiceCharge($order = Criteria::ASC) Order by the service_charge column
 * @method     ChildErcasBillQuery orderByTotalDue($order = Criteria::ASC) Order by the total_due column
 * @method     ChildErcasBillQuery orderByDateCreated($order = Criteria::ASC) Order by the date_created column
 * @method     ChildErcasBillQuery orderByCreator($order = Criteria::ASC) Order by the creator column
 * @method     ChildErcasBillQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildErcasBillQuery groupById() Group by the id column
 * @method     ChildErcasBillQuery groupByRefNo() Group by the ref_no column
 * @method     ChildErcasBillQuery groupByUniquekeyid() Group by the UniqueKeyID column
 * @method     ChildErcasBillQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildErcasBillQuery groupByAccountNo() Group by the account_no column
 * @method     ChildErcasBillQuery groupByServiceType() Group by the service_type column
 * @method     ChildErcasBillQuery groupByDueDate() Group by the due_date column
 * @method     ChildErcasBillQuery groupByBillingFrom() Group by the billing_from column
 * @method     ChildErcasBillQuery groupByBillingTo() Group by the billing_to column
 * @method     ChildErcasBillQuery groupByBillDate() Group by the bill_date column
 * @method     ChildErcasBillQuery groupByCurrentCharge() Group by the current_charge column
 * @method     ChildErcasBillQuery groupByServiceCharge() Group by the service_charge column
 * @method     ChildErcasBillQuery groupByTotalDue() Group by the total_due column
 * @method     ChildErcasBillQuery groupByDateCreated() Group by the date_created column
 * @method     ChildErcasBillQuery groupByCreator() Group by the creator column
 * @method     ChildErcasBillQuery groupByStatus() Group by the status column
 *
 * @method     ChildErcasBillQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildErcasBillQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildErcasBillQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildErcasBillQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildErcasBillQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildErcasBillQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildErcasBill findOne(ConnectionInterface $con = null) Return the first ChildErcasBill matching the query
 * @method     ChildErcasBill findOneOrCreate(ConnectionInterface $con = null) Return the first ChildErcasBill matching the query, or a new ChildErcasBill object populated from the query conditions when no match is found
 *
 * @method     ChildErcasBill findOneById(int $id) Return the first ChildErcasBill filtered by the id column
 * @method     ChildErcasBill findOneByRefNo(string $ref_no) Return the first ChildErcasBill filtered by the ref_no column
 * @method     ChildErcasBill findOneByUniquekeyid(int $UniqueKeyID) Return the first ChildErcasBill filtered by the UniqueKeyID column
 * @method     ChildErcasBill findOneByCustomerId(int $customer_id) Return the first ChildErcasBill filtered by the customer_id column
 * @method     ChildErcasBill findOneByAccountNo(string $account_no) Return the first ChildErcasBill filtered by the account_no column
 * @method     ChildErcasBill findOneByServiceType(string $service_type) Return the first ChildErcasBill filtered by the service_type column
 * @method     ChildErcasBill findOneByDueDate(string $due_date) Return the first ChildErcasBill filtered by the due_date column
 * @method     ChildErcasBill findOneByBillingFrom(string $billing_from) Return the first ChildErcasBill filtered by the billing_from column
 * @method     ChildErcasBill findOneByBillingTo(string $billing_to) Return the first ChildErcasBill filtered by the billing_to column
 * @method     ChildErcasBill findOneByBillDate(string $bill_date) Return the first ChildErcasBill filtered by the bill_date column
 * @method     ChildErcasBill findOneByCurrentCharge(string $current_charge) Return the first ChildErcasBill filtered by the current_charge column
 * @method     ChildErcasBill findOneByServiceCharge(string $service_charge) Return the first ChildErcasBill filtered by the service_charge column
 * @method     ChildErcasBill findOneByTotalDue(string $total_due) Return the first ChildErcasBill filtered by the total_due column
 * @method     ChildErcasBill findOneByDateCreated(string $date_created) Return the first ChildErcasBill filtered by the date_created column
 * @method     ChildErcasBill findOneByCreator(string $creator) Return the first ChildErcasBill filtered by the creator column
 * @method     ChildErcasBill findOneByStatus(boolean $status) Return the first ChildErcasBill filtered by the status column *

 * @method     ChildErcasBill requirePk($key, ConnectionInterface $con = null) Return the ChildErcasBill by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOne(ConnectionInterface $con = null) Return the first ChildErcasBill matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBill requireOneById(int $id) Return the first ChildErcasBill filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByRefNo(string $ref_no) Return the first ChildErcasBill filtered by the ref_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByUniquekeyid(int $UniqueKeyID) Return the first ChildErcasBill filtered by the UniqueKeyID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByCustomerId(int $customer_id) Return the first ChildErcasBill filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByAccountNo(string $account_no) Return the first ChildErcasBill filtered by the account_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByServiceType(string $service_type) Return the first ChildErcasBill filtered by the service_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByDueDate(string $due_date) Return the first ChildErcasBill filtered by the due_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByBillingFrom(string $billing_from) Return the first ChildErcasBill filtered by the billing_from column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByBillingTo(string $billing_to) Return the first ChildErcasBill filtered by the billing_to column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByBillDate(string $bill_date) Return the first ChildErcasBill filtered by the bill_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByCurrentCharge(string $current_charge) Return the first ChildErcasBill filtered by the current_charge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByServiceCharge(string $service_charge) Return the first ChildErcasBill filtered by the service_charge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByTotalDue(string $total_due) Return the first ChildErcasBill filtered by the total_due column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByDateCreated(string $date_created) Return the first ChildErcasBill filtered by the date_created column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByCreator(string $creator) Return the first ChildErcasBill filtered by the creator column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBill requireOneByStatus(boolean $status) Return the first ChildErcasBill filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBill[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildErcasBill objects based on current ModelCriteria
 * @method     ChildErcasBill[]|ObjectCollection findById(int $id) Return ChildErcasBill objects filtered by the id column
 * @method     ChildErcasBill[]|ObjectCollection findByRefNo(string $ref_no) Return ChildErcasBill objects filtered by the ref_no column
 * @method     ChildErcasBill[]|ObjectCollection findByUniquekeyid(int $UniqueKeyID) Return ChildErcasBill objects filtered by the UniqueKeyID column
 * @method     ChildErcasBill[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildErcasBill objects filtered by the customer_id column
 * @method     ChildErcasBill[]|ObjectCollection findByAccountNo(string $account_no) Return ChildErcasBill objects filtered by the account_no column
 * @method     ChildErcasBill[]|ObjectCollection findByServiceType(string $service_type) Return ChildErcasBill objects filtered by the service_type column
 * @method     ChildErcasBill[]|ObjectCollection findByDueDate(string $due_date) Return ChildErcasBill objects filtered by the due_date column
 * @method     ChildErcasBill[]|ObjectCollection findByBillingFrom(string $billing_from) Return ChildErcasBill objects filtered by the billing_from column
 * @method     ChildErcasBill[]|ObjectCollection findByBillingTo(string $billing_to) Return ChildErcasBill objects filtered by the billing_to column
 * @method     ChildErcasBill[]|ObjectCollection findByBillDate(string $bill_date) Return ChildErcasBill objects filtered by the bill_date column
 * @method     ChildErcasBill[]|ObjectCollection findByCurrentCharge(string $current_charge) Return ChildErcasBill objects filtered by the current_charge column
 * @method     ChildErcasBill[]|ObjectCollection findByServiceCharge(string $service_charge) Return ChildErcasBill objects filtered by the service_charge column
 * @method     ChildErcasBill[]|ObjectCollection findByTotalDue(string $total_due) Return ChildErcasBill objects filtered by the total_due column
 * @method     ChildErcasBill[]|ObjectCollection findByDateCreated(string $date_created) Return ChildErcasBill objects filtered by the date_created column
 * @method     ChildErcasBill[]|ObjectCollection findByCreator(string $creator) Return ChildErcasBill objects filtered by the creator column
 * @method     ChildErcasBill[]|ObjectCollection findByStatus(boolean $status) Return ChildErcasBill objects filtered by the status column
 * @method     ChildErcasBill[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ErcasBillQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ErcasBillQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ErcasBill', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildErcasBillQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildErcasBillQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildErcasBillQuery) {
            return $criteria;
        }
        $query = new ChildErcasBillQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildErcasBill|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ErcasBillTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ErcasBillTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildErcasBill A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, ref_no, UniqueKeyID, customer_id, account_no, service_type, due_date, billing_from, billing_to, bill_date, current_charge, service_charge, total_due, date_created, creator, status FROM ercas_bill WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildErcasBill $obj */
            $obj = new ChildErcasBill();
            $obj->hydrate($row);
            ErcasBillTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildErcasBill|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ErcasBillTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ErcasBillTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ref_no column
     *
     * Example usage:
     * <code>
     * $query->filterByRefNo('fooValue');   // WHERE ref_no = 'fooValue'
     * $query->filterByRefNo('%fooValue%'); // WHERE ref_no LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByRefNo($refNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refNo)) {
                $refNo = str_replace('*', '%', $refNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_REF_NO, $refNo, $comparison);
    }

    /**
     * Filter the query on the UniqueKeyID column
     *
     * Example usage:
     * <code>
     * $query->filterByUniquekeyid(1234); // WHERE UniqueKeyID = 1234
     * $query->filterByUniquekeyid(array(12, 34)); // WHERE UniqueKeyID IN (12, 34)
     * $query->filterByUniquekeyid(array('min' => 12)); // WHERE UniqueKeyID > 12
     * </code>
     *
     * @param     mixed $uniquekeyid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByUniquekeyid($uniquekeyid = null, $comparison = null)
    {
        if (is_array($uniquekeyid)) {
            $useMinMax = false;
            if (isset($uniquekeyid['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_UNIQUEKEYID, $uniquekeyid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uniquekeyid['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_UNIQUEKEYID, $uniquekeyid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_UNIQUEKEYID, $uniquekeyid, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the account_no column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountNo('fooValue');   // WHERE account_no = 'fooValue'
     * $query->filterByAccountNo('%fooValue%'); // WHERE account_no LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByAccountNo($accountNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountNo)) {
                $accountNo = str_replace('*', '%', $accountNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_ACCOUNT_NO, $accountNo, $comparison);
    }

    /**
     * Filter the query on the service_type column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceType('fooValue');   // WHERE service_type = 'fooValue'
     * $query->filterByServiceType('%fooValue%'); // WHERE service_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serviceType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByServiceType($serviceType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serviceType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $serviceType)) {
                $serviceType = str_replace('*', '%', $serviceType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_SERVICE_TYPE, $serviceType, $comparison);
    }

    /**
     * Filter the query on the due_date column
     *
     * Example usage:
     * <code>
     * $query->filterByDueDate('fooValue');   // WHERE due_date = 'fooValue'
     * $query->filterByDueDate('%fooValue%'); // WHERE due_date LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dueDate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByDueDate($dueDate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dueDate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dueDate)) {
                $dueDate = str_replace('*', '%', $dueDate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_DUE_DATE, $dueDate, $comparison);
    }

    /**
     * Filter the query on the billing_from column
     *
     * Example usage:
     * <code>
     * $query->filterByBillingFrom('fooValue');   // WHERE billing_from = 'fooValue'
     * $query->filterByBillingFrom('%fooValue%'); // WHERE billing_from LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billingFrom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByBillingFrom($billingFrom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billingFrom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billingFrom)) {
                $billingFrom = str_replace('*', '%', $billingFrom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_BILLING_FROM, $billingFrom, $comparison);
    }

    /**
     * Filter the query on the billing_to column
     *
     * Example usage:
     * <code>
     * $query->filterByBillingTo('fooValue');   // WHERE billing_to = 'fooValue'
     * $query->filterByBillingTo('%fooValue%'); // WHERE billing_to LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billingTo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByBillingTo($billingTo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billingTo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billingTo)) {
                $billingTo = str_replace('*', '%', $billingTo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_BILLING_TO, $billingTo, $comparison);
    }

    /**
     * Filter the query on the bill_date column
     *
     * Example usage:
     * <code>
     * $query->filterByBillDate('2011-03-14'); // WHERE bill_date = '2011-03-14'
     * $query->filterByBillDate('now'); // WHERE bill_date = '2011-03-14'
     * $query->filterByBillDate(array('max' => 'yesterday')); // WHERE bill_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $billDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByBillDate($billDate = null, $comparison = null)
    {
        if (is_array($billDate)) {
            $useMinMax = false;
            if (isset($billDate['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_BILL_DATE, $billDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($billDate['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_BILL_DATE, $billDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_BILL_DATE, $billDate, $comparison);
    }

    /**
     * Filter the query on the current_charge column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentCharge(1234); // WHERE current_charge = 1234
     * $query->filterByCurrentCharge(array(12, 34)); // WHERE current_charge IN (12, 34)
     * $query->filterByCurrentCharge(array('min' => 12)); // WHERE current_charge > 12
     * </code>
     *
     * @param     mixed $currentCharge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByCurrentCharge($currentCharge = null, $comparison = null)
    {
        if (is_array($currentCharge)) {
            $useMinMax = false;
            if (isset($currentCharge['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_CURRENT_CHARGE, $currentCharge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentCharge['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_CURRENT_CHARGE, $currentCharge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_CURRENT_CHARGE, $currentCharge, $comparison);
    }

    /**
     * Filter the query on the service_charge column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceCharge(1234); // WHERE service_charge = 1234
     * $query->filterByServiceCharge(array(12, 34)); // WHERE service_charge IN (12, 34)
     * $query->filterByServiceCharge(array('min' => 12)); // WHERE service_charge > 12
     * </code>
     *
     * @param     mixed $serviceCharge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByServiceCharge($serviceCharge = null, $comparison = null)
    {
        if (is_array($serviceCharge)) {
            $useMinMax = false;
            if (isset($serviceCharge['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_SERVICE_CHARGE, $serviceCharge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceCharge['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_SERVICE_CHARGE, $serviceCharge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_SERVICE_CHARGE, $serviceCharge, $comparison);
    }

    /**
     * Filter the query on the total_due column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalDue(1234); // WHERE total_due = 1234
     * $query->filterByTotalDue(array(12, 34)); // WHERE total_due IN (12, 34)
     * $query->filterByTotalDue(array('min' => 12)); // WHERE total_due > 12
     * </code>
     *
     * @param     mixed $totalDue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByTotalDue($totalDue = null, $comparison = null)
    {
        if (is_array($totalDue)) {
            $useMinMax = false;
            if (isset($totalDue['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_TOTAL_DUE, $totalDue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalDue['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_TOTAL_DUE, $totalDue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_TOTAL_DUE, $totalDue, $comparison);
    }

    /**
     * Filter the query on the date_created column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreated('2011-03-14'); // WHERE date_created = '2011-03-14'
     * $query->filterByDateCreated('now'); // WHERE date_created = '2011-03-14'
     * $query->filterByDateCreated(array('max' => 'yesterday')); // WHERE date_created > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByDateCreated($dateCreated = null, $comparison = null)
    {
        if (is_array($dateCreated)) {
            $useMinMax = false;
            if (isset($dateCreated['min'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_DATE_CREATED, $dateCreated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreated['max'])) {
                $this->addUsingAlias(ErcasBillTableMap::COL_DATE_CREATED, $dateCreated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_DATE_CREATED, $dateCreated, $comparison);
    }

    /**
     * Filter the query on the creator column
     *
     * Example usage:
     * <code>
     * $query->filterByCreator('fooValue');   // WHERE creator = 'fooValue'
     * $query->filterByCreator('%fooValue%'); // WHERE creator LIKE '%fooValue%'
     * </code>
     *
     * @param     string $creator The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByCreator($creator = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($creator)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $creator)) {
                $creator = str_replace('*', '%', $creator);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_CREATOR, $creator, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ErcasBillTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildErcasBill $ercasBill Object to remove from the list of results
     *
     * @return $this|ChildErcasBillQuery The current query, for fluid interface
     */
    public function prune($ercasBill = null)
    {
        if ($ercasBill) {
            $this->addUsingAlias(ErcasBillTableMap::COL_ID, $ercasBill->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ercas_bill table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ErcasBillTableMap::clearInstancePool();
            ErcasBillTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ErcasBillTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ErcasBillTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ErcasBillTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ErcasBillQuery
