<?php

namespace Base;

use \KluBillNewDolce as ChildKluBillNewDolce;
use \KluBillNewDolceQuery as ChildKluBillNewDolceQuery;
use \Exception;
use \PDO;
use Map\KluBillNewDolceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'klu_bill_new_dolce' table.
 *
 *
 *
 * @method     ChildKluBillNewDolceQuery orderByUniquekeyid($order = Criteria::ASC) Order by the UniqueKeyID column
 * @method     ChildKluBillNewDolceQuery orderByAccountno($order = Criteria::ASC) Order by the AccountNo column
 * @method     ChildKluBillNewDolceQuery orderByServicedistrict($order = Criteria::ASC) Order by the ServiceDistrict column
 * @method     ChildKluBillNewDolceQuery orderByLastmeterreading($order = Criteria::ASC) Order by the LastMeterReading column
 * @method     ChildKluBillNewDolceQuery orderByCurrentmeterreading($order = Criteria::ASC) Order by the CurrentMeterReading column
 * @method     ChildKluBillNewDolceQuery orderByUnitsconsumed($order = Criteria::ASC) Order by the UnitsConsumed column
 * @method     ChildKluBillNewDolceQuery orderByLastpaydate($order = Criteria::ASC) Order by the LastPayDate column
 * @method     ChildKluBillNewDolceQuery orderByLastpayamt($order = Criteria::ASC) Order by the LastPayAmt column
 * @method     ChildKluBillNewDolceQuery orderByPriorbalance($order = Criteria::ASC) Order by the PriorBalance column
 * @method     ChildKluBillNewDolceQuery orderByOutstandingbalance($order = Criteria::ASC) Order by the OutstandingBalance column
 * @method     ChildKluBillNewDolceQuery orderByAmountdue($order = Criteria::ASC) Order by the AmountDue column
 * @method     ChildKluBillNewDolceQuery orderByMetermaintenancecharge($order = Criteria::ASC) Order by the MeterMaintenanceCharge column
 * @method     ChildKluBillNewDolceQuery orderByDiscounts($order = Criteria::ASC) Order by the Discounts column
 * @method     ChildKluBillNewDolceQuery orderByOthercharges($order = Criteria::ASC) Order by the OtherCharges column
 * @method     ChildKluBillNewDolceQuery orderByPenaltycharges($order = Criteria::ASC) Order by the PenaltyCharges column
 * @method     ChildKluBillNewDolceQuery orderByStampdutycharges($order = Criteria::ASC) Order by the StampDutyCharges column
 * @method     ChildKluBillNewDolceQuery orderByServicecharges($order = Criteria::ASC) Order by the ServiceCharges column
 * @method     ChildKluBillNewDolceQuery orderByRoutinecharges($order = Criteria::ASC) Order by the RoutineCharges column
 * @method     ChildKluBillNewDolceQuery orderByBillservicerate($order = Criteria::ASC) Order by the BillServiceRate column
 * @method     ChildKluBillNewDolceQuery orderByServicetypedesc($order = Criteria::ASC) Order by the ServiceTypeDesc column
 * @method     ChildKluBillNewDolceQuery orderByBillperiod($order = Criteria::ASC) Order by the BillPeriod column
 * @method     ChildKluBillNewDolceQuery orderByUsagetype($order = Criteria::ASC) Order by the UsageType column
 * @method     ChildKluBillNewDolceQuery orderByMeternumber($order = Criteria::ASC) Order by the MeterNumber column
 * @method     ChildKluBillNewDolceQuery orderByMetertype($order = Criteria::ASC) Order by the MeterType column
 * @method     ChildKluBillNewDolceQuery orderByMetercondition($order = Criteria::ASC) Order by the MeterCondition column
 * @method     ChildKluBillNewDolceQuery orderByLeakagestatus($order = Criteria::ASC) Order by the LeakageStatus column
 * @method     ChildKluBillNewDolceQuery orderByPropertytype($order = Criteria::ASC) Order by the PropertyType column
 * @method     ChildKluBillNewDolceQuery orderByMeterreaddevice($order = Criteria::ASC) Order by the MeterReadDevice column
 * @method     ChildKluBillNewDolceQuery orderByBillmethod($order = Criteria::ASC) Order by the Billmethod column
 * @method     ChildKluBillNewDolceQuery orderByDatecreated($order = Criteria::ASC) Order by the DateCreated column
 * @method     ChildKluBillNewDolceQuery orderByIsMigrated($order = Criteria::ASC) Order by the is_migrated column
 *
 * @method     ChildKluBillNewDolceQuery groupByUniquekeyid() Group by the UniqueKeyID column
 * @method     ChildKluBillNewDolceQuery groupByAccountno() Group by the AccountNo column
 * @method     ChildKluBillNewDolceQuery groupByServicedistrict() Group by the ServiceDistrict column
 * @method     ChildKluBillNewDolceQuery groupByLastmeterreading() Group by the LastMeterReading column
 * @method     ChildKluBillNewDolceQuery groupByCurrentmeterreading() Group by the CurrentMeterReading column
 * @method     ChildKluBillNewDolceQuery groupByUnitsconsumed() Group by the UnitsConsumed column
 * @method     ChildKluBillNewDolceQuery groupByLastpaydate() Group by the LastPayDate column
 * @method     ChildKluBillNewDolceQuery groupByLastpayamt() Group by the LastPayAmt column
 * @method     ChildKluBillNewDolceQuery groupByPriorbalance() Group by the PriorBalance column
 * @method     ChildKluBillNewDolceQuery groupByOutstandingbalance() Group by the OutstandingBalance column
 * @method     ChildKluBillNewDolceQuery groupByAmountdue() Group by the AmountDue column
 * @method     ChildKluBillNewDolceQuery groupByMetermaintenancecharge() Group by the MeterMaintenanceCharge column
 * @method     ChildKluBillNewDolceQuery groupByDiscounts() Group by the Discounts column
 * @method     ChildKluBillNewDolceQuery groupByOthercharges() Group by the OtherCharges column
 * @method     ChildKluBillNewDolceQuery groupByPenaltycharges() Group by the PenaltyCharges column
 * @method     ChildKluBillNewDolceQuery groupByStampdutycharges() Group by the StampDutyCharges column
 * @method     ChildKluBillNewDolceQuery groupByServicecharges() Group by the ServiceCharges column
 * @method     ChildKluBillNewDolceQuery groupByRoutinecharges() Group by the RoutineCharges column
 * @method     ChildKluBillNewDolceQuery groupByBillservicerate() Group by the BillServiceRate column
 * @method     ChildKluBillNewDolceQuery groupByServicetypedesc() Group by the ServiceTypeDesc column
 * @method     ChildKluBillNewDolceQuery groupByBillperiod() Group by the BillPeriod column
 * @method     ChildKluBillNewDolceQuery groupByUsagetype() Group by the UsageType column
 * @method     ChildKluBillNewDolceQuery groupByMeternumber() Group by the MeterNumber column
 * @method     ChildKluBillNewDolceQuery groupByMetertype() Group by the MeterType column
 * @method     ChildKluBillNewDolceQuery groupByMetercondition() Group by the MeterCondition column
 * @method     ChildKluBillNewDolceQuery groupByLeakagestatus() Group by the LeakageStatus column
 * @method     ChildKluBillNewDolceQuery groupByPropertytype() Group by the PropertyType column
 * @method     ChildKluBillNewDolceQuery groupByMeterreaddevice() Group by the MeterReadDevice column
 * @method     ChildKluBillNewDolceQuery groupByBillmethod() Group by the Billmethod column
 * @method     ChildKluBillNewDolceQuery groupByDatecreated() Group by the DateCreated column
 * @method     ChildKluBillNewDolceQuery groupByIsMigrated() Group by the is_migrated column
 *
 * @method     ChildKluBillNewDolceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildKluBillNewDolceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildKluBillNewDolceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildKluBillNewDolceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildKluBillNewDolceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildKluBillNewDolceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildKluBillNewDolce findOne(ConnectionInterface $con = null) Return the first ChildKluBillNewDolce matching the query
 * @method     ChildKluBillNewDolce findOneOrCreate(ConnectionInterface $con = null) Return the first ChildKluBillNewDolce matching the query, or a new ChildKluBillNewDolce object populated from the query conditions when no match is found
 *
 * @method     ChildKluBillNewDolce findOneByUniquekeyid(int $UniqueKeyID) Return the first ChildKluBillNewDolce filtered by the UniqueKeyID column
 * @method     ChildKluBillNewDolce findOneByAccountno(string $AccountNo) Return the first ChildKluBillNewDolce filtered by the AccountNo column
 * @method     ChildKluBillNewDolce findOneByServicedistrict(string $ServiceDistrict) Return the first ChildKluBillNewDolce filtered by the ServiceDistrict column
 * @method     ChildKluBillNewDolce findOneByLastmeterreading(double $LastMeterReading) Return the first ChildKluBillNewDolce filtered by the LastMeterReading column
 * @method     ChildKluBillNewDolce findOneByCurrentmeterreading(double $CurrentMeterReading) Return the first ChildKluBillNewDolce filtered by the CurrentMeterReading column
 * @method     ChildKluBillNewDolce findOneByUnitsconsumed(double $UnitsConsumed) Return the first ChildKluBillNewDolce filtered by the UnitsConsumed column
 * @method     ChildKluBillNewDolce findOneByLastpaydate(string $LastPayDate) Return the first ChildKluBillNewDolce filtered by the LastPayDate column
 * @method     ChildKluBillNewDolce findOneByLastpayamt(string $LastPayAmt) Return the first ChildKluBillNewDolce filtered by the LastPayAmt column
 * @method     ChildKluBillNewDolce findOneByPriorbalance(string $PriorBalance) Return the first ChildKluBillNewDolce filtered by the PriorBalance column
 * @method     ChildKluBillNewDolce findOneByOutstandingbalance(string $OutstandingBalance) Return the first ChildKluBillNewDolce filtered by the OutstandingBalance column
 * @method     ChildKluBillNewDolce findOneByAmountdue(string $AmountDue) Return the first ChildKluBillNewDolce filtered by the AmountDue column
 * @method     ChildKluBillNewDolce findOneByMetermaintenancecharge(string $MeterMaintenanceCharge) Return the first ChildKluBillNewDolce filtered by the MeterMaintenanceCharge column
 * @method     ChildKluBillNewDolce findOneByDiscounts(string $Discounts) Return the first ChildKluBillNewDolce filtered by the Discounts column
 * @method     ChildKluBillNewDolce findOneByOthercharges(string $OtherCharges) Return the first ChildKluBillNewDolce filtered by the OtherCharges column
 * @method     ChildKluBillNewDolce findOneByPenaltycharges(string $PenaltyCharges) Return the first ChildKluBillNewDolce filtered by the PenaltyCharges column
 * @method     ChildKluBillNewDolce findOneByStampdutycharges(string $StampDutyCharges) Return the first ChildKluBillNewDolce filtered by the StampDutyCharges column
 * @method     ChildKluBillNewDolce findOneByServicecharges(string $ServiceCharges) Return the first ChildKluBillNewDolce filtered by the ServiceCharges column
 * @method     ChildKluBillNewDolce findOneByRoutinecharges(string $RoutineCharges) Return the first ChildKluBillNewDolce filtered by the RoutineCharges column
 * @method     ChildKluBillNewDolce findOneByBillservicerate(string $BillServiceRate) Return the first ChildKluBillNewDolce filtered by the BillServiceRate column
 * @method     ChildKluBillNewDolce findOneByServicetypedesc(string $ServiceTypeDesc) Return the first ChildKluBillNewDolce filtered by the ServiceTypeDesc column
 * @method     ChildKluBillNewDolce findOneByBillperiod(string $BillPeriod) Return the first ChildKluBillNewDolce filtered by the BillPeriod column
 * @method     ChildKluBillNewDolce findOneByUsagetype(string $UsageType) Return the first ChildKluBillNewDolce filtered by the UsageType column
 * @method     ChildKluBillNewDolce findOneByMeternumber(string $MeterNumber) Return the first ChildKluBillNewDolce filtered by the MeterNumber column
 * @method     ChildKluBillNewDolce findOneByMetertype(string $MeterType) Return the first ChildKluBillNewDolce filtered by the MeterType column
 * @method     ChildKluBillNewDolce findOneByMetercondition(string $MeterCondition) Return the first ChildKluBillNewDolce filtered by the MeterCondition column
 * @method     ChildKluBillNewDolce findOneByLeakagestatus(string $LeakageStatus) Return the first ChildKluBillNewDolce filtered by the LeakageStatus column
 * @method     ChildKluBillNewDolce findOneByPropertytype(string $PropertyType) Return the first ChildKluBillNewDolce filtered by the PropertyType column
 * @method     ChildKluBillNewDolce findOneByMeterreaddevice(string $MeterReadDevice) Return the first ChildKluBillNewDolce filtered by the MeterReadDevice column
 * @method     ChildKluBillNewDolce findOneByBillmethod(string $Billmethod) Return the first ChildKluBillNewDolce filtered by the Billmethod column
 * @method     ChildKluBillNewDolce findOneByDatecreated(string $DateCreated) Return the first ChildKluBillNewDolce filtered by the DateCreated column
 * @method     ChildKluBillNewDolce findOneByIsMigrated(int $is_migrated) Return the first ChildKluBillNewDolce filtered by the is_migrated column *

 * @method     ChildKluBillNewDolce requirePk($key, ConnectionInterface $con = null) Return the ChildKluBillNewDolce by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOne(ConnectionInterface $con = null) Return the first ChildKluBillNewDolce matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKluBillNewDolce requireOneByUniquekeyid(int $UniqueKeyID) Return the first ChildKluBillNewDolce filtered by the UniqueKeyID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByAccountno(string $AccountNo) Return the first ChildKluBillNewDolce filtered by the AccountNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByServicedistrict(string $ServiceDistrict) Return the first ChildKluBillNewDolce filtered by the ServiceDistrict column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByLastmeterreading(double $LastMeterReading) Return the first ChildKluBillNewDolce filtered by the LastMeterReading column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByCurrentmeterreading(double $CurrentMeterReading) Return the first ChildKluBillNewDolce filtered by the CurrentMeterReading column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByUnitsconsumed(double $UnitsConsumed) Return the first ChildKluBillNewDolce filtered by the UnitsConsumed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByLastpaydate(string $LastPayDate) Return the first ChildKluBillNewDolce filtered by the LastPayDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByLastpayamt(string $LastPayAmt) Return the first ChildKluBillNewDolce filtered by the LastPayAmt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByPriorbalance(string $PriorBalance) Return the first ChildKluBillNewDolce filtered by the PriorBalance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByOutstandingbalance(string $OutstandingBalance) Return the first ChildKluBillNewDolce filtered by the OutstandingBalance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByAmountdue(string $AmountDue) Return the first ChildKluBillNewDolce filtered by the AmountDue column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByMetermaintenancecharge(string $MeterMaintenanceCharge) Return the first ChildKluBillNewDolce filtered by the MeterMaintenanceCharge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByDiscounts(string $Discounts) Return the first ChildKluBillNewDolce filtered by the Discounts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByOthercharges(string $OtherCharges) Return the first ChildKluBillNewDolce filtered by the OtherCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByPenaltycharges(string $PenaltyCharges) Return the first ChildKluBillNewDolce filtered by the PenaltyCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByStampdutycharges(string $StampDutyCharges) Return the first ChildKluBillNewDolce filtered by the StampDutyCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByServicecharges(string $ServiceCharges) Return the first ChildKluBillNewDolce filtered by the ServiceCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByRoutinecharges(string $RoutineCharges) Return the first ChildKluBillNewDolce filtered by the RoutineCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByBillservicerate(string $BillServiceRate) Return the first ChildKluBillNewDolce filtered by the BillServiceRate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByServicetypedesc(string $ServiceTypeDesc) Return the first ChildKluBillNewDolce filtered by the ServiceTypeDesc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByBillperiod(string $BillPeriod) Return the first ChildKluBillNewDolce filtered by the BillPeriod column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByUsagetype(string $UsageType) Return the first ChildKluBillNewDolce filtered by the UsageType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByMeternumber(string $MeterNumber) Return the first ChildKluBillNewDolce filtered by the MeterNumber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByMetertype(string $MeterType) Return the first ChildKluBillNewDolce filtered by the MeterType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByMetercondition(string $MeterCondition) Return the first ChildKluBillNewDolce filtered by the MeterCondition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByLeakagestatus(string $LeakageStatus) Return the first ChildKluBillNewDolce filtered by the LeakageStatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByPropertytype(string $PropertyType) Return the first ChildKluBillNewDolce filtered by the PropertyType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByMeterreaddevice(string $MeterReadDevice) Return the first ChildKluBillNewDolce filtered by the MeterReadDevice column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByBillmethod(string $Billmethod) Return the first ChildKluBillNewDolce filtered by the Billmethod column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByDatecreated(string $DateCreated) Return the first ChildKluBillNewDolce filtered by the DateCreated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKluBillNewDolce requireOneByIsMigrated(int $is_migrated) Return the first ChildKluBillNewDolce filtered by the is_migrated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKluBillNewDolce[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildKluBillNewDolce objects based on current ModelCriteria
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByUniquekeyid(int $UniqueKeyID) Return ChildKluBillNewDolce objects filtered by the UniqueKeyID column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByAccountno(string $AccountNo) Return ChildKluBillNewDolce objects filtered by the AccountNo column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByServicedistrict(string $ServiceDistrict) Return ChildKluBillNewDolce objects filtered by the ServiceDistrict column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByLastmeterreading(double $LastMeterReading) Return ChildKluBillNewDolce objects filtered by the LastMeterReading column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByCurrentmeterreading(double $CurrentMeterReading) Return ChildKluBillNewDolce objects filtered by the CurrentMeterReading column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByUnitsconsumed(double $UnitsConsumed) Return ChildKluBillNewDolce objects filtered by the UnitsConsumed column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByLastpaydate(string $LastPayDate) Return ChildKluBillNewDolce objects filtered by the LastPayDate column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByLastpayamt(string $LastPayAmt) Return ChildKluBillNewDolce objects filtered by the LastPayAmt column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByPriorbalance(string $PriorBalance) Return ChildKluBillNewDolce objects filtered by the PriorBalance column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByOutstandingbalance(string $OutstandingBalance) Return ChildKluBillNewDolce objects filtered by the OutstandingBalance column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByAmountdue(string $AmountDue) Return ChildKluBillNewDolce objects filtered by the AmountDue column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByMetermaintenancecharge(string $MeterMaintenanceCharge) Return ChildKluBillNewDolce objects filtered by the MeterMaintenanceCharge column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByDiscounts(string $Discounts) Return ChildKluBillNewDolce objects filtered by the Discounts column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByOthercharges(string $OtherCharges) Return ChildKluBillNewDolce objects filtered by the OtherCharges column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByPenaltycharges(string $PenaltyCharges) Return ChildKluBillNewDolce objects filtered by the PenaltyCharges column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByStampdutycharges(string $StampDutyCharges) Return ChildKluBillNewDolce objects filtered by the StampDutyCharges column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByServicecharges(string $ServiceCharges) Return ChildKluBillNewDolce objects filtered by the ServiceCharges column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByRoutinecharges(string $RoutineCharges) Return ChildKluBillNewDolce objects filtered by the RoutineCharges column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByBillservicerate(string $BillServiceRate) Return ChildKluBillNewDolce objects filtered by the BillServiceRate column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByServicetypedesc(string $ServiceTypeDesc) Return ChildKluBillNewDolce objects filtered by the ServiceTypeDesc column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByBillperiod(string $BillPeriod) Return ChildKluBillNewDolce objects filtered by the BillPeriod column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByUsagetype(string $UsageType) Return ChildKluBillNewDolce objects filtered by the UsageType column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByMeternumber(string $MeterNumber) Return ChildKluBillNewDolce objects filtered by the MeterNumber column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByMetertype(string $MeterType) Return ChildKluBillNewDolce objects filtered by the MeterType column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByMetercondition(string $MeterCondition) Return ChildKluBillNewDolce objects filtered by the MeterCondition column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByLeakagestatus(string $LeakageStatus) Return ChildKluBillNewDolce objects filtered by the LeakageStatus column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByPropertytype(string $PropertyType) Return ChildKluBillNewDolce objects filtered by the PropertyType column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByMeterreaddevice(string $MeterReadDevice) Return ChildKluBillNewDolce objects filtered by the MeterReadDevice column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByBillmethod(string $Billmethod) Return ChildKluBillNewDolce objects filtered by the Billmethod column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByDatecreated(string $DateCreated) Return ChildKluBillNewDolce objects filtered by the DateCreated column
 * @method     ChildKluBillNewDolce[]|ObjectCollection findByIsMigrated(int $is_migrated) Return ChildKluBillNewDolce objects filtered by the is_migrated column
 * @method     ChildKluBillNewDolce[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class KluBillNewDolceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\KluBillNewDolceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\KluBillNewDolce', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildKluBillNewDolceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildKluBillNewDolceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildKluBillNewDolceQuery) {
            return $criteria;
        }
        $query = new ChildKluBillNewDolceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildKluBillNewDolce|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(KluBillNewDolceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = KluBillNewDolceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildKluBillNewDolce A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT UniqueKeyID, AccountNo, ServiceDistrict, LastMeterReading, CurrentMeterReading, UnitsConsumed, LastPayDate, LastPayAmt, PriorBalance, OutstandingBalance, AmountDue, MeterMaintenanceCharge, Discounts, OtherCharges, PenaltyCharges, StampDutyCharges, ServiceCharges, RoutineCharges, BillServiceRate, ServiceTypeDesc, BillPeriod, UsageType, MeterNumber, MeterType, MeterCondition, LeakageStatus, PropertyType, MeterReadDevice, Billmethod, DateCreated, is_migrated FROM klu_bill_new_dolce WHERE UniqueKeyID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildKluBillNewDolce $obj */
            $obj = new ChildKluBillNewDolce();
            $obj->hydrate($row);
            KluBillNewDolceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildKluBillNewDolce|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the UniqueKeyID column
     *
     * Example usage:
     * <code>
     * $query->filterByUniquekeyid(1234); // WHERE UniqueKeyID = 1234
     * $query->filterByUniquekeyid(array(12, 34)); // WHERE UniqueKeyID IN (12, 34)
     * $query->filterByUniquekeyid(array('min' => 12)); // WHERE UniqueKeyID > 12
     * </code>
     *
     * @param     mixed $uniquekeyid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByUniquekeyid($uniquekeyid = null, $comparison = null)
    {
        if (is_array($uniquekeyid)) {
            $useMinMax = false;
            if (isset($uniquekeyid['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uniquekeyid['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid, $comparison);
    }

    /**
     * Filter the query on the AccountNo column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountno('fooValue');   // WHERE AccountNo = 'fooValue'
     * $query->filterByAccountno('%fooValue%'); // WHERE AccountNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountno The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByAccountno($accountno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountno)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountno)) {
                $accountno = str_replace('*', '%', $accountno);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_ACCOUNTNO, $accountno, $comparison);
    }

    /**
     * Filter the query on the ServiceDistrict column
     *
     * Example usage:
     * <code>
     * $query->filterByServicedistrict('fooValue');   // WHERE ServiceDistrict = 'fooValue'
     * $query->filterByServicedistrict('%fooValue%'); // WHERE ServiceDistrict LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicedistrict The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByServicedistrict($servicedistrict = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicedistrict)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicedistrict)) {
                $servicedistrict = str_replace('*', '%', $servicedistrict);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_SERVICEDISTRICT, $servicedistrict, $comparison);
    }

    /**
     * Filter the query on the LastMeterReading column
     *
     * Example usage:
     * <code>
     * $query->filterByLastmeterreading(1234); // WHERE LastMeterReading = 1234
     * $query->filterByLastmeterreading(array(12, 34)); // WHERE LastMeterReading IN (12, 34)
     * $query->filterByLastmeterreading(array('min' => 12)); // WHERE LastMeterReading > 12
     * </code>
     *
     * @param     mixed $lastmeterreading The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByLastmeterreading($lastmeterreading = null, $comparison = null)
    {
        if (is_array($lastmeterreading)) {
            $useMinMax = false;
            if (isset($lastmeterreading['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTMETERREADING, $lastmeterreading['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastmeterreading['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTMETERREADING, $lastmeterreading['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTMETERREADING, $lastmeterreading, $comparison);
    }

    /**
     * Filter the query on the CurrentMeterReading column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentmeterreading(1234); // WHERE CurrentMeterReading = 1234
     * $query->filterByCurrentmeterreading(array(12, 34)); // WHERE CurrentMeterReading IN (12, 34)
     * $query->filterByCurrentmeterreading(array('min' => 12)); // WHERE CurrentMeterReading > 12
     * </code>
     *
     * @param     mixed $currentmeterreading The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByCurrentmeterreading($currentmeterreading = null, $comparison = null)
    {
        if (is_array($currentmeterreading)) {
            $useMinMax = false;
            if (isset($currentmeterreading['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentmeterreading['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading, $comparison);
    }

    /**
     * Filter the query on the UnitsConsumed column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitsconsumed(1234); // WHERE UnitsConsumed = 1234
     * $query->filterByUnitsconsumed(array(12, 34)); // WHERE UnitsConsumed IN (12, 34)
     * $query->filterByUnitsconsumed(array('min' => 12)); // WHERE UnitsConsumed > 12
     * </code>
     *
     * @param     mixed $unitsconsumed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByUnitsconsumed($unitsconsumed = null, $comparison = null)
    {
        if (is_array($unitsconsumed)) {
            $useMinMax = false;
            if (isset($unitsconsumed['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitsconsumed['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed, $comparison);
    }

    /**
     * Filter the query on the LastPayDate column
     *
     * Example usage:
     * <code>
     * $query->filterByLastpaydate('fooValue');   // WHERE LastPayDate = 'fooValue'
     * $query->filterByLastpaydate('%fooValue%'); // WHERE LastPayDate LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastpaydate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByLastpaydate($lastpaydate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastpaydate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastpaydate)) {
                $lastpaydate = str_replace('*', '%', $lastpaydate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTPAYDATE, $lastpaydate, $comparison);
    }

    /**
     * Filter the query on the LastPayAmt column
     *
     * Example usage:
     * <code>
     * $query->filterByLastpayamt(1234); // WHERE LastPayAmt = 1234
     * $query->filterByLastpayamt(array(12, 34)); // WHERE LastPayAmt IN (12, 34)
     * $query->filterByLastpayamt(array('min' => 12)); // WHERE LastPayAmt > 12
     * </code>
     *
     * @param     mixed $lastpayamt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByLastpayamt($lastpayamt = null, $comparison = null)
    {
        if (is_array($lastpayamt)) {
            $useMinMax = false;
            if (isset($lastpayamt['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTPAYAMT, $lastpayamt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastpayamt['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTPAYAMT, $lastpayamt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_LASTPAYAMT, $lastpayamt, $comparison);
    }

    /**
     * Filter the query on the PriorBalance column
     *
     * Example usage:
     * <code>
     * $query->filterByPriorbalance(1234); // WHERE PriorBalance = 1234
     * $query->filterByPriorbalance(array(12, 34)); // WHERE PriorBalance IN (12, 34)
     * $query->filterByPriorbalance(array('min' => 12)); // WHERE PriorBalance > 12
     * </code>
     *
     * @param     mixed $priorbalance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByPriorbalance($priorbalance = null, $comparison = null)
    {
        if (is_array($priorbalance)) {
            $useMinMax = false;
            if (isset($priorbalance['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_PRIORBALANCE, $priorbalance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priorbalance['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_PRIORBALANCE, $priorbalance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_PRIORBALANCE, $priorbalance, $comparison);
    }

    /**
     * Filter the query on the OutstandingBalance column
     *
     * Example usage:
     * <code>
     * $query->filterByOutstandingbalance(1234); // WHERE OutstandingBalance = 1234
     * $query->filterByOutstandingbalance(array(12, 34)); // WHERE OutstandingBalance IN (12, 34)
     * $query->filterByOutstandingbalance(array('min' => 12)); // WHERE OutstandingBalance > 12
     * </code>
     *
     * @param     mixed $outstandingbalance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByOutstandingbalance($outstandingbalance = null, $comparison = null)
    {
        if (is_array($outstandingbalance)) {
            $useMinMax = false;
            if (isset($outstandingbalance['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($outstandingbalance['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance, $comparison);
    }

    /**
     * Filter the query on the AmountDue column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountdue(1234); // WHERE AmountDue = 1234
     * $query->filterByAmountdue(array(12, 34)); // WHERE AmountDue IN (12, 34)
     * $query->filterByAmountdue(array('min' => 12)); // WHERE AmountDue > 12
     * </code>
     *
     * @param     mixed $amountdue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByAmountdue($amountdue = null, $comparison = null)
    {
        if (is_array($amountdue)) {
            $useMinMax = false;
            if (isset($amountdue['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_AMOUNTDUE, $amountdue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountdue['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_AMOUNTDUE, $amountdue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_AMOUNTDUE, $amountdue, $comparison);
    }

    /**
     * Filter the query on the MeterMaintenanceCharge column
     *
     * Example usage:
     * <code>
     * $query->filterByMetermaintenancecharge(1234); // WHERE MeterMaintenanceCharge = 1234
     * $query->filterByMetermaintenancecharge(array(12, 34)); // WHERE MeterMaintenanceCharge IN (12, 34)
     * $query->filterByMetermaintenancecharge(array('min' => 12)); // WHERE MeterMaintenanceCharge > 12
     * </code>
     *
     * @param     mixed $metermaintenancecharge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByMetermaintenancecharge($metermaintenancecharge = null, $comparison = null)
    {
        if (is_array($metermaintenancecharge)) {
            $useMinMax = false;
            if (isset($metermaintenancecharge['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($metermaintenancecharge['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge, $comparison);
    }

    /**
     * Filter the query on the Discounts column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscounts(1234); // WHERE Discounts = 1234
     * $query->filterByDiscounts(array(12, 34)); // WHERE Discounts IN (12, 34)
     * $query->filterByDiscounts(array('min' => 12)); // WHERE Discounts > 12
     * </code>
     *
     * @param     mixed $discounts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByDiscounts($discounts = null, $comparison = null)
    {
        if (is_array($discounts)) {
            $useMinMax = false;
            if (isset($discounts['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_DISCOUNTS, $discounts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discounts['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_DISCOUNTS, $discounts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_DISCOUNTS, $discounts, $comparison);
    }

    /**
     * Filter the query on the OtherCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByOthercharges(1234); // WHERE OtherCharges = 1234
     * $query->filterByOthercharges(array(12, 34)); // WHERE OtherCharges IN (12, 34)
     * $query->filterByOthercharges(array('min' => 12)); // WHERE OtherCharges > 12
     * </code>
     *
     * @param     mixed $othercharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByOthercharges($othercharges = null, $comparison = null)
    {
        if (is_array($othercharges)) {
            $useMinMax = false;
            if (isset($othercharges['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_OTHERCHARGES, $othercharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($othercharges['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_OTHERCHARGES, $othercharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_OTHERCHARGES, $othercharges, $comparison);
    }

    /**
     * Filter the query on the PenaltyCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByPenaltycharges(1234); // WHERE PenaltyCharges = 1234
     * $query->filterByPenaltycharges(array(12, 34)); // WHERE PenaltyCharges IN (12, 34)
     * $query->filterByPenaltycharges(array('min' => 12)); // WHERE PenaltyCharges > 12
     * </code>
     *
     * @param     mixed $penaltycharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByPenaltycharges($penaltycharges = null, $comparison = null)
    {
        if (is_array($penaltycharges)) {
            $useMinMax = false;
            if (isset($penaltycharges['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_PENALTYCHARGES, $penaltycharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penaltycharges['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_PENALTYCHARGES, $penaltycharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_PENALTYCHARGES, $penaltycharges, $comparison);
    }

    /**
     * Filter the query on the StampDutyCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByStampdutycharges(1234); // WHERE StampDutyCharges = 1234
     * $query->filterByStampdutycharges(array(12, 34)); // WHERE StampDutyCharges IN (12, 34)
     * $query->filterByStampdutycharges(array('min' => 12)); // WHERE StampDutyCharges > 12
     * </code>
     *
     * @param     mixed $stampdutycharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByStampdutycharges($stampdutycharges = null, $comparison = null)
    {
        if (is_array($stampdutycharges)) {
            $useMinMax = false;
            if (isset($stampdutycharges['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stampdutycharges['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges, $comparison);
    }

    /**
     * Filter the query on the ServiceCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByServicecharges(1234); // WHERE ServiceCharges = 1234
     * $query->filterByServicecharges(array(12, 34)); // WHERE ServiceCharges IN (12, 34)
     * $query->filterByServicecharges(array('min' => 12)); // WHERE ServiceCharges > 12
     * </code>
     *
     * @param     mixed $servicecharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByServicecharges($servicecharges = null, $comparison = null)
    {
        if (is_array($servicecharges)) {
            $useMinMax = false;
            if (isset($servicecharges['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_SERVICECHARGES, $servicecharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicecharges['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_SERVICECHARGES, $servicecharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_SERVICECHARGES, $servicecharges, $comparison);
    }

    /**
     * Filter the query on the RoutineCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByRoutinecharges(1234); // WHERE RoutineCharges = 1234
     * $query->filterByRoutinecharges(array(12, 34)); // WHERE RoutineCharges IN (12, 34)
     * $query->filterByRoutinecharges(array('min' => 12)); // WHERE RoutineCharges > 12
     * </code>
     *
     * @param     mixed $routinecharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByRoutinecharges($routinecharges = null, $comparison = null)
    {
        if (is_array($routinecharges)) {
            $useMinMax = false;
            if (isset($routinecharges['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_ROUTINECHARGES, $routinecharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($routinecharges['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_ROUTINECHARGES, $routinecharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_ROUTINECHARGES, $routinecharges, $comparison);
    }

    /**
     * Filter the query on the BillServiceRate column
     *
     * Example usage:
     * <code>
     * $query->filterByBillservicerate(1234); // WHERE BillServiceRate = 1234
     * $query->filterByBillservicerate(array(12, 34)); // WHERE BillServiceRate IN (12, 34)
     * $query->filterByBillservicerate(array('min' => 12)); // WHERE BillServiceRate > 12
     * </code>
     *
     * @param     mixed $billservicerate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByBillservicerate($billservicerate = null, $comparison = null)
    {
        if (is_array($billservicerate)) {
            $useMinMax = false;
            if (isset($billservicerate['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_BILLSERVICERATE, $billservicerate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($billservicerate['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_BILLSERVICERATE, $billservicerate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_BILLSERVICERATE, $billservicerate, $comparison);
    }

    /**
     * Filter the query on the ServiceTypeDesc column
     *
     * Example usage:
     * <code>
     * $query->filterByServicetypedesc('fooValue');   // WHERE ServiceTypeDesc = 'fooValue'
     * $query->filterByServicetypedesc('%fooValue%'); // WHERE ServiceTypeDesc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicetypedesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByServicetypedesc($servicetypedesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicetypedesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicetypedesc)) {
                $servicetypedesc = str_replace('*', '%', $servicetypedesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_SERVICETYPEDESC, $servicetypedesc, $comparison);
    }

    /**
     * Filter the query on the BillPeriod column
     *
     * Example usage:
     * <code>
     * $query->filterByBillperiod('fooValue');   // WHERE BillPeriod = 'fooValue'
     * $query->filterByBillperiod('%fooValue%'); // WHERE BillPeriod LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billperiod The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByBillperiod($billperiod = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billperiod)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billperiod)) {
                $billperiod = str_replace('*', '%', $billperiod);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_BILLPERIOD, $billperiod, $comparison);
    }

    /**
     * Filter the query on the UsageType column
     *
     * Example usage:
     * <code>
     * $query->filterByUsagetype('fooValue');   // WHERE UsageType = 'fooValue'
     * $query->filterByUsagetype('%fooValue%'); // WHERE UsageType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usagetype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByUsagetype($usagetype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usagetype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $usagetype)) {
                $usagetype = str_replace('*', '%', $usagetype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_USAGETYPE, $usagetype, $comparison);
    }

    /**
     * Filter the query on the MeterNumber column
     *
     * Example usage:
     * <code>
     * $query->filterByMeternumber('fooValue');   // WHERE MeterNumber = 'fooValue'
     * $query->filterByMeternumber('%fooValue%'); // WHERE MeterNumber LIKE '%fooValue%'
     * </code>
     *
     * @param     string $meternumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByMeternumber($meternumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($meternumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $meternumber)) {
                $meternumber = str_replace('*', '%', $meternumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERNUMBER, $meternumber, $comparison);
    }

    /**
     * Filter the query on the MeterType column
     *
     * Example usage:
     * <code>
     * $query->filterByMetertype('fooValue');   // WHERE MeterType = 'fooValue'
     * $query->filterByMetertype('%fooValue%'); // WHERE MeterType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metertype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByMetertype($metertype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metertype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metertype)) {
                $metertype = str_replace('*', '%', $metertype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERTYPE, $metertype, $comparison);
    }

    /**
     * Filter the query on the MeterCondition column
     *
     * Example usage:
     * <code>
     * $query->filterByMetercondition('fooValue');   // WHERE MeterCondition = 'fooValue'
     * $query->filterByMetercondition('%fooValue%'); // WHERE MeterCondition LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metercondition The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByMetercondition($metercondition = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metercondition)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metercondition)) {
                $metercondition = str_replace('*', '%', $metercondition);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERCONDITION, $metercondition, $comparison);
    }

    /**
     * Filter the query on the LeakageStatus column
     *
     * Example usage:
     * <code>
     * $query->filterByLeakagestatus('fooValue');   // WHERE LeakageStatus = 'fooValue'
     * $query->filterByLeakagestatus('%fooValue%'); // WHERE LeakageStatus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $leakagestatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByLeakagestatus($leakagestatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($leakagestatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $leakagestatus)) {
                $leakagestatus = str_replace('*', '%', $leakagestatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_LEAKAGESTATUS, $leakagestatus, $comparison);
    }

    /**
     * Filter the query on the PropertyType column
     *
     * Example usage:
     * <code>
     * $query->filterByPropertytype('fooValue');   // WHERE PropertyType = 'fooValue'
     * $query->filterByPropertytype('%fooValue%'); // WHERE PropertyType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $propertytype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByPropertytype($propertytype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($propertytype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $propertytype)) {
                $propertytype = str_replace('*', '%', $propertytype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_PROPERTYTYPE, $propertytype, $comparison);
    }

    /**
     * Filter the query on the MeterReadDevice column
     *
     * Example usage:
     * <code>
     * $query->filterByMeterreaddevice('fooValue');   // WHERE MeterReadDevice = 'fooValue'
     * $query->filterByMeterreaddevice('%fooValue%'); // WHERE MeterReadDevice LIKE '%fooValue%'
     * </code>
     *
     * @param     string $meterreaddevice The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByMeterreaddevice($meterreaddevice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($meterreaddevice)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $meterreaddevice)) {
                $meterreaddevice = str_replace('*', '%', $meterreaddevice);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_METERREADDEVICE, $meterreaddevice, $comparison);
    }

    /**
     * Filter the query on the Billmethod column
     *
     * Example usage:
     * <code>
     * $query->filterByBillmethod('fooValue');   // WHERE Billmethod = 'fooValue'
     * $query->filterByBillmethod('%fooValue%'); // WHERE Billmethod LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billmethod The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByBillmethod($billmethod = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billmethod)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billmethod)) {
                $billmethod = str_replace('*', '%', $billmethod);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_BILLMETHOD, $billmethod, $comparison);
    }

    /**
     * Filter the query on the DateCreated column
     *
     * Example usage:
     * <code>
     * $query->filterByDatecreated('2011-03-14'); // WHERE DateCreated = '2011-03-14'
     * $query->filterByDatecreated('now'); // WHERE DateCreated = '2011-03-14'
     * $query->filterByDatecreated(array('max' => 'yesterday')); // WHERE DateCreated > '2011-03-13'
     * </code>
     *
     * @param     mixed $datecreated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByDatecreated($datecreated = null, $comparison = null)
    {
        if (is_array($datecreated)) {
            $useMinMax = false;
            if (isset($datecreated['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_DATECREATED, $datecreated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datecreated['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_DATECREATED, $datecreated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_DATECREATED, $datecreated, $comparison);
    }

    /**
     * Filter the query on the is_migrated column
     *
     * Example usage:
     * <code>
     * $query->filterByIsMigrated(1234); // WHERE is_migrated = 1234
     * $query->filterByIsMigrated(array(12, 34)); // WHERE is_migrated IN (12, 34)
     * $query->filterByIsMigrated(array('min' => 12)); // WHERE is_migrated > 12
     * </code>
     *
     * @param     mixed $isMigrated The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function filterByIsMigrated($isMigrated = null, $comparison = null)
    {
        if (is_array($isMigrated)) {
            $useMinMax = false;
            if (isset($isMigrated['min'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_IS_MIGRATED, $isMigrated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isMigrated['max'])) {
                $this->addUsingAlias(KluBillNewDolceTableMap::COL_IS_MIGRATED, $isMigrated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KluBillNewDolceTableMap::COL_IS_MIGRATED, $isMigrated, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildKluBillNewDolce $kluBillNewDolce Object to remove from the list of results
     *
     * @return $this|ChildKluBillNewDolceQuery The current query, for fluid interface
     */
    public function prune($kluBillNewDolce = null)
    {
        if ($kluBillNewDolce) {
            $this->addUsingAlias(KluBillNewDolceTableMap::COL_UNIQUEKEYID, $kluBillNewDolce->getUniquekeyid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the klu_bill_new_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluBillNewDolceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            KluBillNewDolceTableMap::clearInstancePool();
            KluBillNewDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluBillNewDolceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(KluBillNewDolceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            KluBillNewDolceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            KluBillNewDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // KluBillNewDolceQuery
