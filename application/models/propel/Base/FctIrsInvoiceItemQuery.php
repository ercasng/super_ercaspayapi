<?php

namespace Base;

use \FctIrsInvoiceItem as ChildFctIrsInvoiceItem;
use \FctIrsInvoiceItemQuery as ChildFctIrsInvoiceItemQuery;
use \Exception;
use \PDO;
use Map\FctIrsInvoiceItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'fct_irs_invoice_item' table.
 *
 *
 *
 * @method     ChildFctIrsInvoiceItemQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFctIrsInvoiceItemQuery orderByServiceName($order = Criteria::ASC) Order by the service_name column
 * @method     ChildFctIrsInvoiceItemQuery orderByServicePaidforName($order = Criteria::ASC) Order by the service_paidfor_name column
 * @method     ChildFctIrsInvoiceItemQuery orderByServicePaidforCode($order = Criteria::ASC) Order by the service_paidfor_code column
 * @method     ChildFctIrsInvoiceItemQuery orderByDepartmentName($order = Criteria::ASC) Order by the department_name column
 * @method     ChildFctIrsInvoiceItemQuery orderByDepartmentCode($order = Criteria::ASC) Order by the department_code column
 * @method     ChildFctIrsInvoiceItemQuery orderByServiceFixedPrice($order = Criteria::ASC) Order by the service_fixed_price column
 * @method     ChildFctIrsInvoiceItemQuery orderByInvoiceId($order = Criteria::ASC) Order by the invoice_id column
 * @method     ChildFctIrsInvoiceItemQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFctIrsInvoiceItemQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFctIrsInvoiceItemQuery groupById() Group by the id column
 * @method     ChildFctIrsInvoiceItemQuery groupByServiceName() Group by the service_name column
 * @method     ChildFctIrsInvoiceItemQuery groupByServicePaidforName() Group by the service_paidfor_name column
 * @method     ChildFctIrsInvoiceItemQuery groupByServicePaidforCode() Group by the service_paidfor_code column
 * @method     ChildFctIrsInvoiceItemQuery groupByDepartmentName() Group by the department_name column
 * @method     ChildFctIrsInvoiceItemQuery groupByDepartmentCode() Group by the department_code column
 * @method     ChildFctIrsInvoiceItemQuery groupByServiceFixedPrice() Group by the service_fixed_price column
 * @method     ChildFctIrsInvoiceItemQuery groupByInvoiceId() Group by the invoice_id column
 * @method     ChildFctIrsInvoiceItemQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFctIrsInvoiceItemQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFctIrsInvoiceItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFctIrsInvoiceItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFctIrsInvoiceItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFctIrsInvoiceItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFctIrsInvoiceItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFctIrsInvoiceItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFctIrsInvoiceItemQuery leftJoinFctIrsInvoice($relationAlias = null) Adds a LEFT JOIN clause to the query using the FctIrsInvoice relation
 * @method     ChildFctIrsInvoiceItemQuery rightJoinFctIrsInvoice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FctIrsInvoice relation
 * @method     ChildFctIrsInvoiceItemQuery innerJoinFctIrsInvoice($relationAlias = null) Adds a INNER JOIN clause to the query using the FctIrsInvoice relation
 *
 * @method     ChildFctIrsInvoiceItemQuery joinWithFctIrsInvoice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FctIrsInvoice relation
 *
 * @method     ChildFctIrsInvoiceItemQuery leftJoinWithFctIrsInvoice() Adds a LEFT JOIN clause and with to the query using the FctIrsInvoice relation
 * @method     ChildFctIrsInvoiceItemQuery rightJoinWithFctIrsInvoice() Adds a RIGHT JOIN clause and with to the query using the FctIrsInvoice relation
 * @method     ChildFctIrsInvoiceItemQuery innerJoinWithFctIrsInvoice() Adds a INNER JOIN clause and with to the query using the FctIrsInvoice relation
 *
 * @method     \FctIrsInvoiceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFctIrsInvoiceItem findOne(ConnectionInterface $con = null) Return the first ChildFctIrsInvoiceItem matching the query
 * @method     ChildFctIrsInvoiceItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFctIrsInvoiceItem matching the query, or a new ChildFctIrsInvoiceItem object populated from the query conditions when no match is found
 *
 * @method     ChildFctIrsInvoiceItem findOneById(int $id) Return the first ChildFctIrsInvoiceItem filtered by the id column
 * @method     ChildFctIrsInvoiceItem findOneByServiceName(string $service_name) Return the first ChildFctIrsInvoiceItem filtered by the service_name column
 * @method     ChildFctIrsInvoiceItem findOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFctIrsInvoiceItem filtered by the service_paidfor_name column
 * @method     ChildFctIrsInvoiceItem findOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFctIrsInvoiceItem filtered by the service_paidfor_code column
 * @method     ChildFctIrsInvoiceItem findOneByDepartmentName(string $department_name) Return the first ChildFctIrsInvoiceItem filtered by the department_name column
 * @method     ChildFctIrsInvoiceItem findOneByDepartmentCode(int $department_code) Return the first ChildFctIrsInvoiceItem filtered by the department_code column
 * @method     ChildFctIrsInvoiceItem findOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFctIrsInvoiceItem filtered by the service_fixed_price column
 * @method     ChildFctIrsInvoiceItem findOneByInvoiceId(int $invoice_id) Return the first ChildFctIrsInvoiceItem filtered by the invoice_id column
 * @method     ChildFctIrsInvoiceItem findOneByCreatedAt(string $created_at) Return the first ChildFctIrsInvoiceItem filtered by the created_at column
 * @method     ChildFctIrsInvoiceItem findOneByUpdatedAt(string $updated_at) Return the first ChildFctIrsInvoiceItem filtered by the updated_at column *

 * @method     ChildFctIrsInvoiceItem requirePk($key, ConnectionInterface $con = null) Return the ChildFctIrsInvoiceItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOne(ConnectionInterface $con = null) Return the first ChildFctIrsInvoiceItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctIrsInvoiceItem requireOneById(int $id) Return the first ChildFctIrsInvoiceItem filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByServiceName(string $service_name) Return the first ChildFctIrsInvoiceItem filtered by the service_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFctIrsInvoiceItem filtered by the service_paidfor_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFctIrsInvoiceItem filtered by the service_paidfor_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByDepartmentName(string $department_name) Return the first ChildFctIrsInvoiceItem filtered by the department_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByDepartmentCode(int $department_code) Return the first ChildFctIrsInvoiceItem filtered by the department_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFctIrsInvoiceItem filtered by the service_fixed_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByInvoiceId(int $invoice_id) Return the first ChildFctIrsInvoiceItem filtered by the invoice_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByCreatedAt(string $created_at) Return the first ChildFctIrsInvoiceItem filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctIrsInvoiceItem requireOneByUpdatedAt(string $updated_at) Return the first ChildFctIrsInvoiceItem filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFctIrsInvoiceItem objects based on current ModelCriteria
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findById(int $id) Return ChildFctIrsInvoiceItem objects filtered by the id column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByServiceName(string $service_name) Return ChildFctIrsInvoiceItem objects filtered by the service_name column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByServicePaidforName(string $service_paidfor_name) Return ChildFctIrsInvoiceItem objects filtered by the service_paidfor_name column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByServicePaidforCode(int $service_paidfor_code) Return ChildFctIrsInvoiceItem objects filtered by the service_paidfor_code column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByDepartmentName(string $department_name) Return ChildFctIrsInvoiceItem objects filtered by the department_name column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByDepartmentCode(int $department_code) Return ChildFctIrsInvoiceItem objects filtered by the department_code column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByServiceFixedPrice(string $service_fixed_price) Return ChildFctIrsInvoiceItem objects filtered by the service_fixed_price column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByInvoiceId(int $invoice_id) Return ChildFctIrsInvoiceItem objects filtered by the invoice_id column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFctIrsInvoiceItem objects filtered by the created_at column
 * @method     ChildFctIrsInvoiceItem[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFctIrsInvoiceItem objects filtered by the updated_at column
 * @method     ChildFctIrsInvoiceItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FctIrsInvoiceItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FctIrsInvoiceItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FctIrsInvoiceItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFctIrsInvoiceItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFctIrsInvoiceItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFctIrsInvoiceItemQuery) {
            return $criteria;
        }
        $query = new ChildFctIrsInvoiceItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFctIrsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FctIrsInvoiceItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FctIrsInvoiceItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFctIrsInvoiceItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, service_name, service_paidfor_name, service_paidfor_code, department_name, department_code, service_fixed_price, invoice_id, created_at, updated_at FROM fct_irs_invoice_item WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFctIrsInvoiceItem $obj */
            $obj = new ChildFctIrsInvoiceItem();
            $obj->hydrate($row);
            FctIrsInvoiceItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFctIrsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the service_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceName('fooValue');   // WHERE service_name = 'fooValue'
     * $query->filterByServiceName('%fooValue%'); // WHERE service_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serviceName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceName($serviceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serviceName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $serviceName)) {
                $serviceName = str_replace('*', '%', $serviceName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_NAME, $serviceName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforName('fooValue');   // WHERE service_paidfor_name = 'fooValue'
     * $query->filterByServicePaidforName('%fooValue%'); // WHERE service_paidfor_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicePaidforName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforName($servicePaidforName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicePaidforName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicePaidforName)) {
                $servicePaidforName = str_replace('*', '%', $servicePaidforName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_NAME, $servicePaidforName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_code column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforCode(1234); // WHERE service_paidfor_code = 1234
     * $query->filterByServicePaidforCode(array(12, 34)); // WHERE service_paidfor_code IN (12, 34)
     * $query->filterByServicePaidforCode(array('min' => 12)); // WHERE service_paidfor_code > 12
     * </code>
     *
     * @param     mixed $servicePaidforCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforCode($servicePaidforCode = null, $comparison = null)
    {
        if (is_array($servicePaidforCode)) {
            $useMinMax = false;
            if (isset($servicePaidforCode['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicePaidforCode['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode, $comparison);
    }

    /**
     * Filter the query on the department_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentName('fooValue');   // WHERE department_name = 'fooValue'
     * $query->filterByDepartmentName('%fooValue%'); // WHERE department_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departmentName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentName($departmentName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departmentName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departmentName)) {
                $departmentName = str_replace('*', '%', $departmentName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_DEPARTMENT_NAME, $departmentName, $comparison);
    }

    /**
     * Filter the query on the department_code column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentCode(1234); // WHERE department_code = 1234
     * $query->filterByDepartmentCode(array(12, 34)); // WHERE department_code IN (12, 34)
     * $query->filterByDepartmentCode(array('min' => 12)); // WHERE department_code > 12
     * </code>
     *
     * @param     mixed $departmentCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentCode($departmentCode = null, $comparison = null)
    {
        if (is_array($departmentCode)) {
            $useMinMax = false;
            if (isset($departmentCode['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($departmentCode['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode, $comparison);
    }

    /**
     * Filter the query on the service_fixed_price column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceFixedPrice(1234); // WHERE service_fixed_price = 1234
     * $query->filterByServiceFixedPrice(array(12, 34)); // WHERE service_fixed_price IN (12, 34)
     * $query->filterByServiceFixedPrice(array('min' => 12)); // WHERE service_fixed_price > 12
     * </code>
     *
     * @param     mixed $serviceFixedPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceFixedPrice($serviceFixedPrice = null, $comparison = null)
    {
        if (is_array($serviceFixedPrice)) {
            $useMinMax = false;
            if (isset($serviceFixedPrice['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceFixedPrice['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice, $comparison);
    }

    /**
     * Filter the query on the invoice_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceId(1234); // WHERE invoice_id = 1234
     * $query->filterByInvoiceId(array(12, 34)); // WHERE invoice_id IN (12, 34)
     * $query->filterByInvoiceId(array('min' => 12)); // WHERE invoice_id > 12
     * </code>
     *
     * @see       filterByFctIrsInvoice()
     *
     * @param     mixed $invoiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByInvoiceId($invoiceId = null, $comparison = null)
    {
        if (is_array($invoiceId)) {
            $useMinMax = false;
            if (isset($invoiceId['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceId['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \FctIrsInvoice object
     *
     * @param \FctIrsInvoice|ObjectCollection $fctIrsInvoice The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByFctIrsInvoice($fctIrsInvoice, $comparison = null)
    {
        if ($fctIrsInvoice instanceof \FctIrsInvoice) {
            return $this
                ->addUsingAlias(FctIrsInvoiceItemTableMap::COL_INVOICE_ID, $fctIrsInvoice->getId(), $comparison);
        } elseif ($fctIrsInvoice instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FctIrsInvoiceItemTableMap::COL_INVOICE_ID, $fctIrsInvoice->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFctIrsInvoice() only accepts arguments of type \FctIrsInvoice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FctIrsInvoice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function joinFctIrsInvoice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FctIrsInvoice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FctIrsInvoice');
        }

        return $this;
    }

    /**
     * Use the FctIrsInvoice relation FctIrsInvoice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FctIrsInvoiceQuery A secondary query class using the current class as primary query
     */
    public function useFctIrsInvoiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFctIrsInvoice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FctIrsInvoice', '\FctIrsInvoiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFctIrsInvoiceItem $fctIrsInvoiceItem Object to remove from the list of results
     *
     * @return $this|ChildFctIrsInvoiceItemQuery The current query, for fluid interface
     */
    public function prune($fctIrsInvoiceItem = null)
    {
        if ($fctIrsInvoiceItem) {
            $this->addUsingAlias(FctIrsInvoiceItemTableMap::COL_ID, $fctIrsInvoiceItem->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fct_irs_invoice_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctIrsInvoiceItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FctIrsInvoiceItemTableMap::clearInstancePool();
            FctIrsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctIrsInvoiceItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FctIrsInvoiceItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FctIrsInvoiceItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FctIrsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FctIrsInvoiceItemQuery
