<?php

namespace Base;

use \KedcoInvoice3 as ChildKedcoInvoice3;
use \KedcoInvoice3Query as ChildKedcoInvoice3Query;
use \Exception;
use \PDO;
use Map\KedcoInvoice3TableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'kedco_invoice3' table.
 *
 *
 *
 * @method     ChildKedcoInvoice3Query orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildKedcoInvoice3Query orderByTxCode($order = Criteria::ASC) Order by the tx_code column
 * @method     ChildKedcoInvoice3Query orderByTerminal($order = Criteria::ASC) Order by the terminal column
 * @method     ChildKedcoInvoice3Query orderByOperatorCode($order = Criteria::ASC) Order by the operator_code column
 * @method     ChildKedcoInvoice3Query orderByOperatorName($order = Criteria::ASC) Order by the operator_name column
 * @method     ChildKedcoInvoice3Query orderByTxDate($order = Criteria::ASC) Order by the tx_date column
 * @method     ChildKedcoInvoice3Query orderByTxStatus($order = Criteria::ASC) Order by the tx_status column
 * @method     ChildKedcoInvoice3Query orderByTotalAmountString($order = Criteria::ASC) Order by the total_amount_string column
 * @method     ChildKedcoInvoice3Query orderByTotalAmount($order = Criteria::ASC) Order by the total_amount column
 * @method     ChildKedcoInvoice3Query orderByPaymentMode($order = Criteria::ASC) Order by the payment_mode column
 * @method     ChildKedcoInvoice3Query orderByPaymentType($order = Criteria::ASC) Order by the payment_type column
 * @method     ChildKedcoInvoice3Query orderByCustMeterId($order = Criteria::ASC) Order by the cust_meter_id column
 * @method     ChildKedcoInvoice3Query orderByCustomerName($order = Criteria::ASC) Order by the customer_name column
 * @method     ChildKedcoInvoice3Query orderByCustomerPhone($order = Criteria::ASC) Order by the customer_phone column
 * @method     ChildKedcoInvoice3Query orderByAppVersion($order = Criteria::ASC) Order by the app_version column
 * @method     ChildKedcoInvoice3Query orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildKedcoInvoice3Query orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildKedcoInvoice3Query groupById() Group by the id column
 * @method     ChildKedcoInvoice3Query groupByTxCode() Group by the tx_code column
 * @method     ChildKedcoInvoice3Query groupByTerminal() Group by the terminal column
 * @method     ChildKedcoInvoice3Query groupByOperatorCode() Group by the operator_code column
 * @method     ChildKedcoInvoice3Query groupByOperatorName() Group by the operator_name column
 * @method     ChildKedcoInvoice3Query groupByTxDate() Group by the tx_date column
 * @method     ChildKedcoInvoice3Query groupByTxStatus() Group by the tx_status column
 * @method     ChildKedcoInvoice3Query groupByTotalAmountString() Group by the total_amount_string column
 * @method     ChildKedcoInvoice3Query groupByTotalAmount() Group by the total_amount column
 * @method     ChildKedcoInvoice3Query groupByPaymentMode() Group by the payment_mode column
 * @method     ChildKedcoInvoice3Query groupByPaymentType() Group by the payment_type column
 * @method     ChildKedcoInvoice3Query groupByCustMeterId() Group by the cust_meter_id column
 * @method     ChildKedcoInvoice3Query groupByCustomerName() Group by the customer_name column
 * @method     ChildKedcoInvoice3Query groupByCustomerPhone() Group by the customer_phone column
 * @method     ChildKedcoInvoice3Query groupByAppVersion() Group by the app_version column
 * @method     ChildKedcoInvoice3Query groupByCreatedAt() Group by the created_at column
 * @method     ChildKedcoInvoice3Query groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildKedcoInvoice3Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildKedcoInvoice3Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildKedcoInvoice3Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildKedcoInvoice3Query leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildKedcoInvoice3Query rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildKedcoInvoice3Query innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildKedcoInvoice3 findOne(ConnectionInterface $con = null) Return the first ChildKedcoInvoice3 matching the query
 * @method     ChildKedcoInvoice3 findOneOrCreate(ConnectionInterface $con = null) Return the first ChildKedcoInvoice3 matching the query, or a new ChildKedcoInvoice3 object populated from the query conditions when no match is found
 *
 * @method     ChildKedcoInvoice3 findOneById(int $id) Return the first ChildKedcoInvoice3 filtered by the id column
 * @method     ChildKedcoInvoice3 findOneByTxCode(string $tx_code) Return the first ChildKedcoInvoice3 filtered by the tx_code column
 * @method     ChildKedcoInvoice3 findOneByTerminal(string $terminal) Return the first ChildKedcoInvoice3 filtered by the terminal column
 * @method     ChildKedcoInvoice3 findOneByOperatorCode(string $operator_code) Return the first ChildKedcoInvoice3 filtered by the operator_code column
 * @method     ChildKedcoInvoice3 findOneByOperatorName(string $operator_name) Return the first ChildKedcoInvoice3 filtered by the operator_name column
 * @method     ChildKedcoInvoice3 findOneByTxDate(string $tx_date) Return the first ChildKedcoInvoice3 filtered by the tx_date column
 * @method     ChildKedcoInvoice3 findOneByTxStatus(string $tx_status) Return the first ChildKedcoInvoice3 filtered by the tx_status column
 * @method     ChildKedcoInvoice3 findOneByTotalAmountString(string $total_amount_string) Return the first ChildKedcoInvoice3 filtered by the total_amount_string column
 * @method     ChildKedcoInvoice3 findOneByTotalAmount(string $total_amount) Return the first ChildKedcoInvoice3 filtered by the total_amount column
 * @method     ChildKedcoInvoice3 findOneByPaymentMode(string $payment_mode) Return the first ChildKedcoInvoice3 filtered by the payment_mode column
 * @method     ChildKedcoInvoice3 findOneByPaymentType(string $payment_type) Return the first ChildKedcoInvoice3 filtered by the payment_type column
 * @method     ChildKedcoInvoice3 findOneByCustMeterId(string $cust_meter_id) Return the first ChildKedcoInvoice3 filtered by the cust_meter_id column
 * @method     ChildKedcoInvoice3 findOneByCustomerName(string $customer_name) Return the first ChildKedcoInvoice3 filtered by the customer_name column
 * @method     ChildKedcoInvoice3 findOneByCustomerPhone(string $customer_phone) Return the first ChildKedcoInvoice3 filtered by the customer_phone column
 * @method     ChildKedcoInvoice3 findOneByAppVersion(string $app_version) Return the first ChildKedcoInvoice3 filtered by the app_version column
 * @method     ChildKedcoInvoice3 findOneByCreatedAt(string $created_at) Return the first ChildKedcoInvoice3 filtered by the created_at column
 * @method     ChildKedcoInvoice3 findOneByUpdatedAt(string $updated_at) Return the first ChildKedcoInvoice3 filtered by the updated_at column *

 * @method     ChildKedcoInvoice3 requirePk($key, ConnectionInterface $con = null) Return the ChildKedcoInvoice3 by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOne(ConnectionInterface $con = null) Return the first ChildKedcoInvoice3 matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKedcoInvoice3 requireOneById(int $id) Return the first ChildKedcoInvoice3 filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTxCode(string $tx_code) Return the first ChildKedcoInvoice3 filtered by the tx_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTerminal(string $terminal) Return the first ChildKedcoInvoice3 filtered by the terminal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByOperatorCode(string $operator_code) Return the first ChildKedcoInvoice3 filtered by the operator_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByOperatorName(string $operator_name) Return the first ChildKedcoInvoice3 filtered by the operator_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTxDate(string $tx_date) Return the first ChildKedcoInvoice3 filtered by the tx_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTxStatus(string $tx_status) Return the first ChildKedcoInvoice3 filtered by the tx_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTotalAmountString(string $total_amount_string) Return the first ChildKedcoInvoice3 filtered by the total_amount_string column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByTotalAmount(string $total_amount) Return the first ChildKedcoInvoice3 filtered by the total_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByPaymentMode(string $payment_mode) Return the first ChildKedcoInvoice3 filtered by the payment_mode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByPaymentType(string $payment_type) Return the first ChildKedcoInvoice3 filtered by the payment_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByCustMeterId(string $cust_meter_id) Return the first ChildKedcoInvoice3 filtered by the cust_meter_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByCustomerName(string $customer_name) Return the first ChildKedcoInvoice3 filtered by the customer_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByCustomerPhone(string $customer_phone) Return the first ChildKedcoInvoice3 filtered by the customer_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByAppVersion(string $app_version) Return the first ChildKedcoInvoice3 filtered by the app_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByCreatedAt(string $created_at) Return the first ChildKedcoInvoice3 filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKedcoInvoice3 requireOneByUpdatedAt(string $updated_at) Return the first ChildKedcoInvoice3 filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKedcoInvoice3[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildKedcoInvoice3 objects based on current ModelCriteria
 * @method     ChildKedcoInvoice3[]|ObjectCollection findById(int $id) Return ChildKedcoInvoice3 objects filtered by the id column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTxCode(string $tx_code) Return ChildKedcoInvoice3 objects filtered by the tx_code column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTerminal(string $terminal) Return ChildKedcoInvoice3 objects filtered by the terminal column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByOperatorCode(string $operator_code) Return ChildKedcoInvoice3 objects filtered by the operator_code column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByOperatorName(string $operator_name) Return ChildKedcoInvoice3 objects filtered by the operator_name column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTxDate(string $tx_date) Return ChildKedcoInvoice3 objects filtered by the tx_date column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTxStatus(string $tx_status) Return ChildKedcoInvoice3 objects filtered by the tx_status column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTotalAmountString(string $total_amount_string) Return ChildKedcoInvoice3 objects filtered by the total_amount_string column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByTotalAmount(string $total_amount) Return ChildKedcoInvoice3 objects filtered by the total_amount column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByPaymentMode(string $payment_mode) Return ChildKedcoInvoice3 objects filtered by the payment_mode column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByPaymentType(string $payment_type) Return ChildKedcoInvoice3 objects filtered by the payment_type column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByCustMeterId(string $cust_meter_id) Return ChildKedcoInvoice3 objects filtered by the cust_meter_id column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByCustomerName(string $customer_name) Return ChildKedcoInvoice3 objects filtered by the customer_name column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByCustomerPhone(string $customer_phone) Return ChildKedcoInvoice3 objects filtered by the customer_phone column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByAppVersion(string $app_version) Return ChildKedcoInvoice3 objects filtered by the app_version column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildKedcoInvoice3 objects filtered by the created_at column
 * @method     ChildKedcoInvoice3[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildKedcoInvoice3 objects filtered by the updated_at column
 * @method     ChildKedcoInvoice3[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class KedcoInvoice3Query extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\KedcoInvoice3Query object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\KedcoInvoice3', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildKedcoInvoice3Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildKedcoInvoice3Query
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildKedcoInvoice3Query) {
            return $criteria;
        }
        $query = new ChildKedcoInvoice3Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildKedcoInvoice3|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(KedcoInvoice3TableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = KedcoInvoice3TableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildKedcoInvoice3 A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, tx_code, terminal, operator_code, operator_name, tx_date, tx_status, total_amount_string, total_amount, payment_mode, payment_type, cust_meter_id, customer_name, customer_phone, app_version, created_at, updated_at FROM kedco_invoice3 WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildKedcoInvoice3 $obj */
            $obj = new ChildKedcoInvoice3();
            $obj->hydrate($row);
            KedcoInvoice3TableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildKedcoInvoice3|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the tx_code column
     *
     * Example usage:
     * <code>
     * $query->filterByTxCode('fooValue');   // WHERE tx_code = 'fooValue'
     * $query->filterByTxCode('%fooValue%'); // WHERE tx_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $txCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTxCode($txCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($txCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $txCode)) {
                $txCode = str_replace('*', '%', $txCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TX_CODE, $txCode, $comparison);
    }

    /**
     * Filter the query on the terminal column
     *
     * Example usage:
     * <code>
     * $query->filterByTerminal(1234); // WHERE terminal = 1234
     * $query->filterByTerminal(array(12, 34)); // WHERE terminal IN (12, 34)
     * $query->filterByTerminal(array('min' => 12)); // WHERE terminal > 12
     * </code>
     *
     * @param     mixed $terminal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTerminal($terminal = null, $comparison = null)
    {
        if (is_array($terminal)) {
            $useMinMax = false;
            if (isset($terminal['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TERMINAL, $terminal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($terminal['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TERMINAL, $terminal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TERMINAL, $terminal, $comparison);
    }

    /**
     * Filter the query on the operator_code column
     *
     * Example usage:
     * <code>
     * $query->filterByOperatorCode('fooValue');   // WHERE operator_code = 'fooValue'
     * $query->filterByOperatorCode('%fooValue%'); // WHERE operator_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $operatorCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByOperatorCode($operatorCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($operatorCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $operatorCode)) {
                $operatorCode = str_replace('*', '%', $operatorCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_OPERATOR_CODE, $operatorCode, $comparison);
    }

    /**
     * Filter the query on the operator_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOperatorName('fooValue');   // WHERE operator_name = 'fooValue'
     * $query->filterByOperatorName('%fooValue%'); // WHERE operator_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $operatorName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByOperatorName($operatorName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($operatorName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $operatorName)) {
                $operatorName = str_replace('*', '%', $operatorName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_OPERATOR_NAME, $operatorName, $comparison);
    }

    /**
     * Filter the query on the tx_date column
     *
     * Example usage:
     * <code>
     * $query->filterByTxDate('2011-03-14'); // WHERE tx_date = '2011-03-14'
     * $query->filterByTxDate('now'); // WHERE tx_date = '2011-03-14'
     * $query->filterByTxDate(array('max' => 'yesterday')); // WHERE tx_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $txDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTxDate($txDate = null, $comparison = null)
    {
        if (is_array($txDate)) {
            $useMinMax = false;
            if (isset($txDate['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TX_DATE, $txDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($txDate['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TX_DATE, $txDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TX_DATE, $txDate, $comparison);
    }

    /**
     * Filter the query on the tx_status column
     *
     * Example usage:
     * <code>
     * $query->filterByTxStatus('fooValue');   // WHERE tx_status = 'fooValue'
     * $query->filterByTxStatus('%fooValue%'); // WHERE tx_status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $txStatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTxStatus($txStatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($txStatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $txStatus)) {
                $txStatus = str_replace('*', '%', $txStatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TX_STATUS, $txStatus, $comparison);
    }

    /**
     * Filter the query on the total_amount_string column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalAmountString('fooValue');   // WHERE total_amount_string = 'fooValue'
     * $query->filterByTotalAmountString('%fooValue%'); // WHERE total_amount_string LIKE '%fooValue%'
     * </code>
     *
     * @param     string $totalAmountString The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTotalAmountString($totalAmountString = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($totalAmountString)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $totalAmountString)) {
                $totalAmountString = str_replace('*', '%', $totalAmountString);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT_STRING, $totalAmountString, $comparison);
    }

    /**
     * Filter the query on the total_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalAmount(1234); // WHERE total_amount = 1234
     * $query->filterByTotalAmount(array(12, 34)); // WHERE total_amount IN (12, 34)
     * $query->filterByTotalAmount(array('min' => 12)); // WHERE total_amount > 12
     * </code>
     *
     * @param     mixed $totalAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByTotalAmount($totalAmount = null, $comparison = null)
    {
        if (is_array($totalAmount)) {
            $useMinMax = false;
            if (isset($totalAmount['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT, $totalAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalAmount['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT, $totalAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT, $totalAmount, $comparison);
    }

    /**
     * Filter the query on the payment_mode column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentMode('fooValue');   // WHERE payment_mode = 'fooValue'
     * $query->filterByPaymentMode('%fooValue%'); // WHERE payment_mode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentMode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByPaymentMode($paymentMode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentMode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentMode)) {
                $paymentMode = str_replace('*', '%', $paymentMode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_PAYMENT_MODE, $paymentMode, $comparison);
    }

    /**
     * Filter the query on the payment_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentType('fooValue');   // WHERE payment_type = 'fooValue'
     * $query->filterByPaymentType('%fooValue%'); // WHERE payment_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByPaymentType($paymentType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentType)) {
                $paymentType = str_replace('*', '%', $paymentType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_PAYMENT_TYPE, $paymentType, $comparison);
    }

    /**
     * Filter the query on the cust_meter_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustMeterId('fooValue');   // WHERE cust_meter_id = 'fooValue'
     * $query->filterByCustMeterId('%fooValue%'); // WHERE cust_meter_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $custMeterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByCustMeterId($custMeterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($custMeterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $custMeterId)) {
                $custMeterId = str_replace('*', '%', $custMeterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_CUST_METER_ID, $custMeterId, $comparison);
    }

    /**
     * Filter the query on the customer_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerName('fooValue');   // WHERE customer_name = 'fooValue'
     * $query->filterByCustomerName('%fooValue%'); // WHERE customer_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByCustomerName($customerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $customerName)) {
                $customerName = str_replace('*', '%', $customerName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_CUSTOMER_NAME, $customerName, $comparison);
    }

    /**
     * Filter the query on the customer_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerPhone('fooValue');   // WHERE customer_phone = 'fooValue'
     * $query->filterByCustomerPhone('%fooValue%'); // WHERE customer_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerPhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByCustomerPhone($customerPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerPhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $customerPhone)) {
                $customerPhone = str_replace('*', '%', $customerPhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_CUSTOMER_PHONE, $customerPhone, $comparison);
    }

    /**
     * Filter the query on the app_version column
     *
     * Example usage:
     * <code>
     * $query->filterByAppVersion('fooValue');   // WHERE app_version = 'fooValue'
     * $query->filterByAppVersion('%fooValue%'); // WHERE app_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appVersion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByAppVersion($appVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appVersion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appVersion)) {
                $appVersion = str_replace('*', '%', $appVersion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_APP_VERSION, $appVersion, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(KedcoInvoice3TableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KedcoInvoice3TableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildKedcoInvoice3 $kedcoInvoice3 Object to remove from the list of results
     *
     * @return $this|ChildKedcoInvoice3Query The current query, for fluid interface
     */
    public function prune($kedcoInvoice3 = null)
    {
        if ($kedcoInvoice3) {
            $this->addUsingAlias(KedcoInvoice3TableMap::COL_ID, $kedcoInvoice3->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the kedco_invoice3 table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice3TableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            KedcoInvoice3TableMap::clearInstancePool();
            KedcoInvoice3TableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice3TableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(KedcoInvoice3TableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            KedcoInvoice3TableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            KedcoInvoice3TableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // KedcoInvoice3Query
