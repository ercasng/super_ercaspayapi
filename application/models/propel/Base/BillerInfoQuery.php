<?php

namespace Base;

use \BillerInfo as ChildBillerInfo;
use \BillerInfoQuery as ChildBillerInfoQuery;
use \Exception;
use \PDO;
use Map\BillerInfoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'biller_info' table.
 *
 *
 *
 * @method     ChildBillerInfoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBillerInfoQuery orderByMerchantId($order = Criteria::ASC) Order by the merchant_id column
 * @method     ChildBillerInfoQuery orderByBillerTableSuffix($order = Criteria::ASC) Order by the biller_table_suffix column
 * @method     ChildBillerInfoQuery orderByBillerAbbrev($order = Criteria::ASC) Order by the biller_abbrev column
 * @method     ChildBillerInfoQuery orderByBillerName($order = Criteria::ASC) Order by the biller_name column
 * @method     ChildBillerInfoQuery orderByBillerDetail($order = Criteria::ASC) Order by the biller_detail column
 *
 * @method     ChildBillerInfoQuery groupById() Group by the id column
 * @method     ChildBillerInfoQuery groupByMerchantId() Group by the merchant_id column
 * @method     ChildBillerInfoQuery groupByBillerTableSuffix() Group by the biller_table_suffix column
 * @method     ChildBillerInfoQuery groupByBillerAbbrev() Group by the biller_abbrev column
 * @method     ChildBillerInfoQuery groupByBillerName() Group by the biller_name column
 * @method     ChildBillerInfoQuery groupByBillerDetail() Group by the biller_detail column
 *
 * @method     ChildBillerInfoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBillerInfoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBillerInfoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBillerInfoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBillerInfoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBillerInfoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBillerInfoQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildBillerInfoQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildBillerInfoQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildBillerInfoQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildBillerInfoQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildBillerInfoQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildBillerInfoQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     \ProductQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBillerInfo findOne(ConnectionInterface $con = null) Return the first ChildBillerInfo matching the query
 * @method     ChildBillerInfo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBillerInfo matching the query, or a new ChildBillerInfo object populated from the query conditions when no match is found
 *
 * @method     ChildBillerInfo findOneById(int $id) Return the first ChildBillerInfo filtered by the id column
 * @method     ChildBillerInfo findOneByMerchantId(string $merchant_id) Return the first ChildBillerInfo filtered by the merchant_id column
 * @method     ChildBillerInfo findOneByBillerTableSuffix(string $biller_table_suffix) Return the first ChildBillerInfo filtered by the biller_table_suffix column
 * @method     ChildBillerInfo findOneByBillerAbbrev(string $biller_abbrev) Return the first ChildBillerInfo filtered by the biller_abbrev column
 * @method     ChildBillerInfo findOneByBillerName(string $biller_name) Return the first ChildBillerInfo filtered by the biller_name column
 * @method     ChildBillerInfo findOneByBillerDetail(string $biller_detail) Return the first ChildBillerInfo filtered by the biller_detail column *

 * @method     ChildBillerInfo requirePk($key, ConnectionInterface $con = null) Return the ChildBillerInfo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOne(ConnectionInterface $con = null) Return the first ChildBillerInfo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBillerInfo requireOneById(int $id) Return the first ChildBillerInfo filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOneByMerchantId(string $merchant_id) Return the first ChildBillerInfo filtered by the merchant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOneByBillerTableSuffix(string $biller_table_suffix) Return the first ChildBillerInfo filtered by the biller_table_suffix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOneByBillerAbbrev(string $biller_abbrev) Return the first ChildBillerInfo filtered by the biller_abbrev column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOneByBillerName(string $biller_name) Return the first ChildBillerInfo filtered by the biller_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBillerInfo requireOneByBillerDetail(string $biller_detail) Return the first ChildBillerInfo filtered by the biller_detail column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBillerInfo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBillerInfo objects based on current ModelCriteria
 * @method     ChildBillerInfo[]|ObjectCollection findById(int $id) Return ChildBillerInfo objects filtered by the id column
 * @method     ChildBillerInfo[]|ObjectCollection findByMerchantId(string $merchant_id) Return ChildBillerInfo objects filtered by the merchant_id column
 * @method     ChildBillerInfo[]|ObjectCollection findByBillerTableSuffix(string $biller_table_suffix) Return ChildBillerInfo objects filtered by the biller_table_suffix column
 * @method     ChildBillerInfo[]|ObjectCollection findByBillerAbbrev(string $biller_abbrev) Return ChildBillerInfo objects filtered by the biller_abbrev column
 * @method     ChildBillerInfo[]|ObjectCollection findByBillerName(string $biller_name) Return ChildBillerInfo objects filtered by the biller_name column
 * @method     ChildBillerInfo[]|ObjectCollection findByBillerDetail(string $biller_detail) Return ChildBillerInfo objects filtered by the biller_detail column
 * @method     ChildBillerInfo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BillerInfoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BillerInfoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\BillerInfo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBillerInfoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBillerInfoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBillerInfoQuery) {
            return $criteria;
        }
        $query = new ChildBillerInfoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBillerInfo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BillerInfoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BillerInfoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBillerInfo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, merchant_id, biller_table_suffix, biller_abbrev, biller_name, biller_detail FROM biller_info WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBillerInfo $obj */
            $obj = new ChildBillerInfo();
            $obj->hydrate($row);
            BillerInfoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBillerInfo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BillerInfoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BillerInfoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BillerInfoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BillerInfoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the merchant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMerchantId('fooValue');   // WHERE merchant_id = 'fooValue'
     * $query->filterByMerchantId('%fooValue%'); // WHERE merchant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $merchantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByMerchantId($merchantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($merchantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $merchantId)) {
                $merchantId = str_replace('*', '%', $merchantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_MERCHANT_ID, $merchantId, $comparison);
    }

    /**
     * Filter the query on the biller_table_suffix column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerTableSuffix('fooValue');   // WHERE biller_table_suffix = 'fooValue'
     * $query->filterByBillerTableSuffix('%fooValue%'); // WHERE biller_table_suffix LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billerTableSuffix The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByBillerTableSuffix($billerTableSuffix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billerTableSuffix)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billerTableSuffix)) {
                $billerTableSuffix = str_replace('*', '%', $billerTableSuffix);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_BILLER_TABLE_SUFFIX, $billerTableSuffix, $comparison);
    }

    /**
     * Filter the query on the biller_abbrev column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerAbbrev('fooValue');   // WHERE biller_abbrev = 'fooValue'
     * $query->filterByBillerAbbrev('%fooValue%'); // WHERE biller_abbrev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billerAbbrev The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByBillerAbbrev($billerAbbrev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billerAbbrev)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billerAbbrev)) {
                $billerAbbrev = str_replace('*', '%', $billerAbbrev);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_BILLER_ABBREV, $billerAbbrev, $comparison);
    }

    /**
     * Filter the query on the biller_name column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerName('fooValue');   // WHERE biller_name = 'fooValue'
     * $query->filterByBillerName('%fooValue%'); // WHERE biller_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billerName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByBillerName($billerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billerName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billerName)) {
                $billerName = str_replace('*', '%', $billerName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_BILLER_NAME, $billerName, $comparison);
    }

    /**
     * Filter the query on the biller_detail column
     *
     * Example usage:
     * <code>
     * $query->filterByBillerDetail('fooValue');   // WHERE biller_detail = 'fooValue'
     * $query->filterByBillerDetail('%fooValue%'); // WHERE biller_detail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billerDetail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByBillerDetail($billerDetail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billerDetail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billerDetail)) {
                $billerDetail = str_replace('*', '%', $billerDetail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BillerInfoTableMap::COL_BILLER_DETAIL, $billerDetail, $comparison);
    }

    /**
     * Filter the query by a related \Product object
     *
     * @param \Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBillerInfoQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Product) {
            return $this
                ->addUsingAlias(BillerInfoTableMap::COL_MERCHANT_ID, $product->getBillerId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\ProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBillerInfo $billerInfo Object to remove from the list of results
     *
     * @return $this|ChildBillerInfoQuery The current query, for fluid interface
     */
    public function prune($billerInfo = null)
    {
        if ($billerInfo) {
            $this->addUsingAlias(BillerInfoTableMap::COL_ID, $billerInfo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the biller_info table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerInfoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BillerInfoTableMap::clearInstancePool();
            BillerInfoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerInfoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BillerInfoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BillerInfoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BillerInfoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BillerInfoQuery
