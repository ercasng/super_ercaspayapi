<?php

namespace Base;

use \FctirsInvoice as ChildFctirsInvoice;
use \FctirsInvoiceQuery as ChildFctirsInvoiceQuery;
use \Exception;
use \PDO;
use Map\FctirsInvoiceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'fctirs_invoice' table.
 *
 *
 *
 * @method     ChildFctirsInvoiceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFctirsInvoiceQuery orderByTxCode($order = Criteria::ASC) Order by the tx_code column
 * @method     ChildFctirsInvoiceQuery orderByTerminal($order = Criteria::ASC) Order by the terminal column
 * @method     ChildFctirsInvoiceQuery orderByOperatorCode($order = Criteria::ASC) Order by the operator_code column
 * @method     ChildFctirsInvoiceQuery orderByOperatorName($order = Criteria::ASC) Order by the operator_name column
 * @method     ChildFctirsInvoiceQuery orderByTxDate($order = Criteria::ASC) Order by the tx_date column
 * @method     ChildFctirsInvoiceQuery orderByTotalAmountString($order = Criteria::ASC) Order by the total_amount_string column
 * @method     ChildFctirsInvoiceQuery orderByTotalAmount($order = Criteria::ASC) Order by the total_amount column
 * @method     ChildFctirsInvoiceQuery orderByPaymentMode($order = Criteria::ASC) Order by the payment_mode column
 * @method     ChildFctirsInvoiceQuery orderByPaymentType($order = Criteria::ASC) Order by the payment_type column
 * @method     ChildFctirsInvoiceQuery orderByPatientPhone($order = Criteria::ASC) Order by the patient_phone column
 * @method     ChildFctirsInvoiceQuery orderByAppVersion($order = Criteria::ASC) Order by the app_version column
 * @method     ChildFctirsInvoiceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFctirsInvoiceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFctirsInvoiceQuery groupById() Group by the id column
 * @method     ChildFctirsInvoiceQuery groupByTxCode() Group by the tx_code column
 * @method     ChildFctirsInvoiceQuery groupByTerminal() Group by the terminal column
 * @method     ChildFctirsInvoiceQuery groupByOperatorCode() Group by the operator_code column
 * @method     ChildFctirsInvoiceQuery groupByOperatorName() Group by the operator_name column
 * @method     ChildFctirsInvoiceQuery groupByTxDate() Group by the tx_date column
 * @method     ChildFctirsInvoiceQuery groupByTotalAmountString() Group by the total_amount_string column
 * @method     ChildFctirsInvoiceQuery groupByTotalAmount() Group by the total_amount column
 * @method     ChildFctirsInvoiceQuery groupByPaymentMode() Group by the payment_mode column
 * @method     ChildFctirsInvoiceQuery groupByPaymentType() Group by the payment_type column
 * @method     ChildFctirsInvoiceQuery groupByPatientPhone() Group by the patient_phone column
 * @method     ChildFctirsInvoiceQuery groupByAppVersion() Group by the app_version column
 * @method     ChildFctirsInvoiceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFctirsInvoiceQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFctirsInvoiceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFctirsInvoiceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFctirsInvoiceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFctirsInvoiceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFctirsInvoiceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFctirsInvoiceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFctirsInvoiceQuery leftJoinFctirsInvoiceItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the FctirsInvoiceItem relation
 * @method     ChildFctirsInvoiceQuery rightJoinFctirsInvoiceItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FctirsInvoiceItem relation
 * @method     ChildFctirsInvoiceQuery innerJoinFctirsInvoiceItem($relationAlias = null) Adds a INNER JOIN clause to the query using the FctirsInvoiceItem relation
 *
 * @method     ChildFctirsInvoiceQuery joinWithFctirsInvoiceItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FctirsInvoiceItem relation
 *
 * @method     ChildFctirsInvoiceQuery leftJoinWithFctirsInvoiceItem() Adds a LEFT JOIN clause and with to the query using the FctirsInvoiceItem relation
 * @method     ChildFctirsInvoiceQuery rightJoinWithFctirsInvoiceItem() Adds a RIGHT JOIN clause and with to the query using the FctirsInvoiceItem relation
 * @method     ChildFctirsInvoiceQuery innerJoinWithFctirsInvoiceItem() Adds a INNER JOIN clause and with to the query using the FctirsInvoiceItem relation
 *
 * @method     \FctirsInvoiceItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFctirsInvoice findOne(ConnectionInterface $con = null) Return the first ChildFctirsInvoice matching the query
 * @method     ChildFctirsInvoice findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFctirsInvoice matching the query, or a new ChildFctirsInvoice object populated from the query conditions when no match is found
 *
 * @method     ChildFctirsInvoice findOneById(int $id) Return the first ChildFctirsInvoice filtered by the id column
 * @method     ChildFctirsInvoice findOneByTxCode(string $tx_code) Return the first ChildFctirsInvoice filtered by the tx_code column
 * @method     ChildFctirsInvoice findOneByTerminal(string $terminal) Return the first ChildFctirsInvoice filtered by the terminal column
 * @method     ChildFctirsInvoice findOneByOperatorCode(int $operator_code) Return the first ChildFctirsInvoice filtered by the operator_code column
 * @method     ChildFctirsInvoice findOneByOperatorName(string $operator_name) Return the first ChildFctirsInvoice filtered by the operator_name column
 * @method     ChildFctirsInvoice findOneByTxDate(string $tx_date) Return the first ChildFctirsInvoice filtered by the tx_date column
 * @method     ChildFctirsInvoice findOneByTotalAmountString(string $total_amount_string) Return the first ChildFctirsInvoice filtered by the total_amount_string column
 * @method     ChildFctirsInvoice findOneByTotalAmount(string $total_amount) Return the first ChildFctirsInvoice filtered by the total_amount column
 * @method     ChildFctirsInvoice findOneByPaymentMode(string $payment_mode) Return the first ChildFctirsInvoice filtered by the payment_mode column
 * @method     ChildFctirsInvoice findOneByPaymentType(string $payment_type) Return the first ChildFctirsInvoice filtered by the payment_type column
 * @method     ChildFctirsInvoice findOneByPatientPhone(string $patient_phone) Return the first ChildFctirsInvoice filtered by the patient_phone column
 * @method     ChildFctirsInvoice findOneByAppVersion(string $app_version) Return the first ChildFctirsInvoice filtered by the app_version column
 * @method     ChildFctirsInvoice findOneByCreatedAt(string $created_at) Return the first ChildFctirsInvoice filtered by the created_at column
 * @method     ChildFctirsInvoice findOneByUpdatedAt(string $updated_at) Return the first ChildFctirsInvoice filtered by the updated_at column *

 * @method     ChildFctirsInvoice requirePk($key, ConnectionInterface $con = null) Return the ChildFctirsInvoice by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOne(ConnectionInterface $con = null) Return the first ChildFctirsInvoice matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctirsInvoice requireOneById(int $id) Return the first ChildFctirsInvoice filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByTxCode(string $tx_code) Return the first ChildFctirsInvoice filtered by the tx_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByTerminal(string $terminal) Return the first ChildFctirsInvoice filtered by the terminal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByOperatorCode(int $operator_code) Return the first ChildFctirsInvoice filtered by the operator_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByOperatorName(string $operator_name) Return the first ChildFctirsInvoice filtered by the operator_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByTxDate(string $tx_date) Return the first ChildFctirsInvoice filtered by the tx_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByTotalAmountString(string $total_amount_string) Return the first ChildFctirsInvoice filtered by the total_amount_string column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByTotalAmount(string $total_amount) Return the first ChildFctirsInvoice filtered by the total_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByPaymentMode(string $payment_mode) Return the first ChildFctirsInvoice filtered by the payment_mode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByPaymentType(string $payment_type) Return the first ChildFctirsInvoice filtered by the payment_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByPatientPhone(string $patient_phone) Return the first ChildFctirsInvoice filtered by the patient_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByAppVersion(string $app_version) Return the first ChildFctirsInvoice filtered by the app_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByCreatedAt(string $created_at) Return the first ChildFctirsInvoice filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFctirsInvoice requireOneByUpdatedAt(string $updated_at) Return the first ChildFctirsInvoice filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFctirsInvoice[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFctirsInvoice objects based on current ModelCriteria
 * @method     ChildFctirsInvoice[]|ObjectCollection findById(int $id) Return ChildFctirsInvoice objects filtered by the id column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByTxCode(string $tx_code) Return ChildFctirsInvoice objects filtered by the tx_code column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByTerminal(string $terminal) Return ChildFctirsInvoice objects filtered by the terminal column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByOperatorCode(int $operator_code) Return ChildFctirsInvoice objects filtered by the operator_code column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByOperatorName(string $operator_name) Return ChildFctirsInvoice objects filtered by the operator_name column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByTxDate(string $tx_date) Return ChildFctirsInvoice objects filtered by the tx_date column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByTotalAmountString(string $total_amount_string) Return ChildFctirsInvoice objects filtered by the total_amount_string column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByTotalAmount(string $total_amount) Return ChildFctirsInvoice objects filtered by the total_amount column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByPaymentMode(string $payment_mode) Return ChildFctirsInvoice objects filtered by the payment_mode column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByPaymentType(string $payment_type) Return ChildFctirsInvoice objects filtered by the payment_type column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByPatientPhone(string $patient_phone) Return ChildFctirsInvoice objects filtered by the patient_phone column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByAppVersion(string $app_version) Return ChildFctirsInvoice objects filtered by the app_version column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFctirsInvoice objects filtered by the created_at column
 * @method     ChildFctirsInvoice[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFctirsInvoice objects filtered by the updated_at column
 * @method     ChildFctirsInvoice[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FctirsInvoiceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FctirsInvoiceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FctirsInvoice', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFctirsInvoiceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFctirsInvoiceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFctirsInvoiceQuery) {
            return $criteria;
        }
        $query = new ChildFctirsInvoiceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFctirsInvoice|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FctirsInvoiceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FctirsInvoiceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFctirsInvoice A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, tx_code, terminal, operator_code, operator_name, tx_date, total_amount_string, total_amount, payment_mode, payment_type, patient_phone, app_version, created_at, updated_at FROM fctirs_invoice WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFctirsInvoice $obj */
            $obj = new ChildFctirsInvoice();
            $obj->hydrate($row);
            FctirsInvoiceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFctirsInvoice|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the tx_code column
     *
     * Example usage:
     * <code>
     * $query->filterByTxCode(1234); // WHERE tx_code = 1234
     * $query->filterByTxCode(array(12, 34)); // WHERE tx_code IN (12, 34)
     * $query->filterByTxCode(array('min' => 12)); // WHERE tx_code > 12
     * </code>
     *
     * @param     mixed $txCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByTxCode($txCode = null, $comparison = null)
    {
        if (is_array($txCode)) {
            $useMinMax = false;
            if (isset($txCode['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_CODE, $txCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($txCode['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_CODE, $txCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_CODE, $txCode, $comparison);
    }

    /**
     * Filter the query on the terminal column
     *
     * Example usage:
     * <code>
     * $query->filterByTerminal(1234); // WHERE terminal = 1234
     * $query->filterByTerminal(array(12, 34)); // WHERE terminal IN (12, 34)
     * $query->filterByTerminal(array('min' => 12)); // WHERE terminal > 12
     * </code>
     *
     * @param     mixed $terminal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByTerminal($terminal = null, $comparison = null)
    {
        if (is_array($terminal)) {
            $useMinMax = false;
            if (isset($terminal['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TERMINAL, $terminal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($terminal['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TERMINAL, $terminal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_TERMINAL, $terminal, $comparison);
    }

    /**
     * Filter the query on the operator_code column
     *
     * Example usage:
     * <code>
     * $query->filterByOperatorCode(1234); // WHERE operator_code = 1234
     * $query->filterByOperatorCode(array(12, 34)); // WHERE operator_code IN (12, 34)
     * $query->filterByOperatorCode(array('min' => 12)); // WHERE operator_code > 12
     * </code>
     *
     * @param     mixed $operatorCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByOperatorCode($operatorCode = null, $comparison = null)
    {
        if (is_array($operatorCode)) {
            $useMinMax = false;
            if (isset($operatorCode['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_OPERATOR_CODE, $operatorCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($operatorCode['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_OPERATOR_CODE, $operatorCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_OPERATOR_CODE, $operatorCode, $comparison);
    }

    /**
     * Filter the query on the operator_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOperatorName('fooValue');   // WHERE operator_name = 'fooValue'
     * $query->filterByOperatorName('%fooValue%'); // WHERE operator_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $operatorName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByOperatorName($operatorName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($operatorName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $operatorName)) {
                $operatorName = str_replace('*', '%', $operatorName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_OPERATOR_NAME, $operatorName, $comparison);
    }

    /**
     * Filter the query on the tx_date column
     *
     * Example usage:
     * <code>
     * $query->filterByTxDate('2011-03-14'); // WHERE tx_date = '2011-03-14'
     * $query->filterByTxDate('now'); // WHERE tx_date = '2011-03-14'
     * $query->filterByTxDate(array('max' => 'yesterday')); // WHERE tx_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $txDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByTxDate($txDate = null, $comparison = null)
    {
        if (is_array($txDate)) {
            $useMinMax = false;
            if (isset($txDate['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_DATE, $txDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($txDate['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_DATE, $txDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_TX_DATE, $txDate, $comparison);
    }

    /**
     * Filter the query on the total_amount_string column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalAmountString('fooValue');   // WHERE total_amount_string = 'fooValue'
     * $query->filterByTotalAmountString('%fooValue%'); // WHERE total_amount_string LIKE '%fooValue%'
     * </code>
     *
     * @param     string $totalAmountString The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByTotalAmountString($totalAmountString = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($totalAmountString)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $totalAmountString)) {
                $totalAmountString = str_replace('*', '%', $totalAmountString);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_TOTAL_AMOUNT_STRING, $totalAmountString, $comparison);
    }

    /**
     * Filter the query on the total_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalAmount(1234); // WHERE total_amount = 1234
     * $query->filterByTotalAmount(array(12, 34)); // WHERE total_amount IN (12, 34)
     * $query->filterByTotalAmount(array('min' => 12)); // WHERE total_amount > 12
     * </code>
     *
     * @param     mixed $totalAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByTotalAmount($totalAmount = null, $comparison = null)
    {
        if (is_array($totalAmount)) {
            $useMinMax = false;
            if (isset($totalAmount['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TOTAL_AMOUNT, $totalAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalAmount['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_TOTAL_AMOUNT, $totalAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_TOTAL_AMOUNT, $totalAmount, $comparison);
    }

    /**
     * Filter the query on the payment_mode column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentMode('fooValue');   // WHERE payment_mode = 'fooValue'
     * $query->filterByPaymentMode('%fooValue%'); // WHERE payment_mode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentMode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByPaymentMode($paymentMode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentMode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentMode)) {
                $paymentMode = str_replace('*', '%', $paymentMode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_PAYMENT_MODE, $paymentMode, $comparison);
    }

    /**
     * Filter the query on the payment_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentType('fooValue');   // WHERE payment_type = 'fooValue'
     * $query->filterByPaymentType('%fooValue%'); // WHERE payment_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByPaymentType($paymentType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentType)) {
                $paymentType = str_replace('*', '%', $paymentType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_PAYMENT_TYPE, $paymentType, $comparison);
    }

    /**
     * Filter the query on the patient_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientPhone('fooValue');   // WHERE patient_phone = 'fooValue'
     * $query->filterByPatientPhone('%fooValue%'); // WHERE patient_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $patientPhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByPatientPhone($patientPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientPhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $patientPhone)) {
                $patientPhone = str_replace('*', '%', $patientPhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_PATIENT_PHONE, $patientPhone, $comparison);
    }

    /**
     * Filter the query on the app_version column
     *
     * Example usage:
     * <code>
     * $query->filterByAppVersion('fooValue');   // WHERE app_version = 'fooValue'
     * $query->filterByAppVersion('%fooValue%'); // WHERE app_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appVersion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByAppVersion($appVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appVersion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appVersion)) {
                $appVersion = str_replace('*', '%', $appVersion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_APP_VERSION, $appVersion, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FctirsInvoiceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FctirsInvoiceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \FctirsInvoiceItem object
     *
     * @param \FctirsInvoiceItem|ObjectCollection $fctirsInvoiceItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function filterByFctirsInvoiceItem($fctirsInvoiceItem, $comparison = null)
    {
        if ($fctirsInvoiceItem instanceof \FctirsInvoiceItem) {
            return $this
                ->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $fctirsInvoiceItem->getInvoiceId(), $comparison);
        } elseif ($fctirsInvoiceItem instanceof ObjectCollection) {
            return $this
                ->useFctirsInvoiceItemQuery()
                ->filterByPrimaryKeys($fctirsInvoiceItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFctirsInvoiceItem() only accepts arguments of type \FctirsInvoiceItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FctirsInvoiceItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function joinFctirsInvoiceItem($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FctirsInvoiceItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FctirsInvoiceItem');
        }

        return $this;
    }

    /**
     * Use the FctirsInvoiceItem relation FctirsInvoiceItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FctirsInvoiceItemQuery A secondary query class using the current class as primary query
     */
    public function useFctirsInvoiceItemQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFctirsInvoiceItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FctirsInvoiceItem', '\FctirsInvoiceItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFctirsInvoice $fctirsInvoice Object to remove from the list of results
     *
     * @return $this|ChildFctirsInvoiceQuery The current query, for fluid interface
     */
    public function prune($fctirsInvoice = null)
    {
        if ($fctirsInvoice) {
            $this->addUsingAlias(FctirsInvoiceTableMap::COL_ID, $fctirsInvoice->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fctirs_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctirsInvoiceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FctirsInvoiceTableMap::clearInstancePool();
            FctirsInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FctirsInvoiceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FctirsInvoiceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FctirsInvoiceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FctirsInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FctirsInvoiceQuery
