<?php

namespace Base;

use \CronjobLog as ChildCronjobLog;
use \CronjobLogQuery as ChildCronjobLogQuery;
use \Exception;
use Map\CronjobLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cronjob_log' table.
 *
 *
 *
 * @method     ChildCronjobLogQuery orderByLogMsg($order = Criteria::ASC) Order by the log_msg column
 * @method     ChildCronjobLogQuery orderByProcessName($order = Criteria::ASC) Order by the process_name column
 * @method     ChildCronjobLogQuery orderByTimeStamp($order = Criteria::ASC) Order by the time_stamp column
 *
 * @method     ChildCronjobLogQuery groupByLogMsg() Group by the log_msg column
 * @method     ChildCronjobLogQuery groupByProcessName() Group by the process_name column
 * @method     ChildCronjobLogQuery groupByTimeStamp() Group by the time_stamp column
 *
 * @method     ChildCronjobLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCronjobLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCronjobLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCronjobLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCronjobLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCronjobLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCronjobLog findOne(ConnectionInterface $con = null) Return the first ChildCronjobLog matching the query
 * @method     ChildCronjobLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCronjobLog matching the query, or a new ChildCronjobLog object populated from the query conditions when no match is found
 *
 * @method     ChildCronjobLog findOneByLogMsg(string $log_msg) Return the first ChildCronjobLog filtered by the log_msg column
 * @method     ChildCronjobLog findOneByProcessName(string $process_name) Return the first ChildCronjobLog filtered by the process_name column
 * @method     ChildCronjobLog findOneByTimeStamp(string $time_stamp) Return the first ChildCronjobLog filtered by the time_stamp column *

 * @method     ChildCronjobLog requirePk($key, ConnectionInterface $con = null) Return the ChildCronjobLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronjobLog requireOne(ConnectionInterface $con = null) Return the first ChildCronjobLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronjobLog requireOneByLogMsg(string $log_msg) Return the first ChildCronjobLog filtered by the log_msg column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronjobLog requireOneByProcessName(string $process_name) Return the first ChildCronjobLog filtered by the process_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronjobLog requireOneByTimeStamp(string $time_stamp) Return the first ChildCronjobLog filtered by the time_stamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronjobLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCronjobLog objects based on current ModelCriteria
 * @method     ChildCronjobLog[]|ObjectCollection findByLogMsg(string $log_msg) Return ChildCronjobLog objects filtered by the log_msg column
 * @method     ChildCronjobLog[]|ObjectCollection findByProcessName(string $process_name) Return ChildCronjobLog objects filtered by the process_name column
 * @method     ChildCronjobLog[]|ObjectCollection findByTimeStamp(string $time_stamp) Return ChildCronjobLog objects filtered by the time_stamp column
 * @method     ChildCronjobLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CronjobLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CronjobLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CronjobLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCronjobLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCronjobLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCronjobLogQuery) {
            return $criteria;
        }
        $query = new ChildCronjobLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCronjobLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The CronjobLog object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The CronjobLog object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The CronjobLog object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The CronjobLog object has no primary key');
    }

    /**
     * Filter the query on the log_msg column
     *
     * Example usage:
     * <code>
     * $query->filterByLogMsg('fooValue');   // WHERE log_msg = 'fooValue'
     * $query->filterByLogMsg('%fooValue%'); // WHERE log_msg LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logMsg The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function filterByLogMsg($logMsg = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logMsg)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $logMsg)) {
                $logMsg = str_replace('*', '%', $logMsg);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CronjobLogTableMap::COL_LOG_MSG, $logMsg, $comparison);
    }

    /**
     * Filter the query on the process_name column
     *
     * Example usage:
     * <code>
     * $query->filterByProcessName('fooValue');   // WHERE process_name = 'fooValue'
     * $query->filterByProcessName('%fooValue%'); // WHERE process_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $processName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function filterByProcessName($processName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($processName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $processName)) {
                $processName = str_replace('*', '%', $processName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CronjobLogTableMap::COL_PROCESS_NAME, $processName, $comparison);
    }

    /**
     * Filter the query on the time_stamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeStamp('2011-03-14'); // WHERE time_stamp = '2011-03-14'
     * $query->filterByTimeStamp('now'); // WHERE time_stamp = '2011-03-14'
     * $query->filterByTimeStamp(array('max' => 'yesterday')); // WHERE time_stamp > '2011-03-13'
     * </code>
     *
     * @param     mixed $timeStamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function filterByTimeStamp($timeStamp = null, $comparison = null)
    {
        if (is_array($timeStamp)) {
            $useMinMax = false;
            if (isset($timeStamp['min'])) {
                $this->addUsingAlias(CronjobLogTableMap::COL_TIME_STAMP, $timeStamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timeStamp['max'])) {
                $this->addUsingAlias(CronjobLogTableMap::COL_TIME_STAMP, $timeStamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronjobLogTableMap::COL_TIME_STAMP, $timeStamp, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCronjobLog $cronjobLog Object to remove from the list of results
     *
     * @return $this|ChildCronjobLogQuery The current query, for fluid interface
     */
    public function prune($cronjobLog = null)
    {
        if ($cronjobLog) {
            throw new LogicException('CronjobLog object has no primary key');

        }

        return $this;
    }

    /**
     * Deletes all rows from the cronjob_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronjobLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CronjobLogTableMap::clearInstancePool();
            CronjobLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronjobLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CronjobLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CronjobLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CronjobLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CronjobLogQuery
