<?php

namespace Base;

use \ErcasBillDolce as ChildErcasBillDolce;
use \ErcasBillDolceQuery as ChildErcasBillDolceQuery;
use \Exception;
use Map\ErcasBillDolceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ercas_bill_dolce' table.
 *
 *
 *
 * @method     ChildErcasBillDolceQuery orderByUniquekeyid($order = Criteria::ASC) Order by the UniqueKeyID column
 * @method     ChildErcasBillDolceQuery orderByAccountno($order = Criteria::ASC) Order by the AccountNo column
 * @method     ChildErcasBillDolceQuery orderByServicedistrict($order = Criteria::ASC) Order by the ServiceDistrict column
 * @method     ChildErcasBillDolceQuery orderByLastmeterreading($order = Criteria::ASC) Order by the LastMeterReading column
 * @method     ChildErcasBillDolceQuery orderByCurrentmeterreading($order = Criteria::ASC) Order by the CurrentMeterReading column
 * @method     ChildErcasBillDolceQuery orderByUnitsconsumed($order = Criteria::ASC) Order by the UnitsConsumed column
 * @method     ChildErcasBillDolceQuery orderByLastpaydate($order = Criteria::ASC) Order by the LastPayDate column
 * @method     ChildErcasBillDolceQuery orderByLastpayamt($order = Criteria::ASC) Order by the LastPayAmt column
 * @method     ChildErcasBillDolceQuery orderByPriorbalance($order = Criteria::ASC) Order by the PriorBalance column
 * @method     ChildErcasBillDolceQuery orderByOutstandingbalance($order = Criteria::ASC) Order by the OutstandingBalance column
 * @method     ChildErcasBillDolceQuery orderByAmountdue($order = Criteria::ASC) Order by the AmountDue column
 * @method     ChildErcasBillDolceQuery orderByMetermaintenancecharge($order = Criteria::ASC) Order by the MeterMaintenanceCharge column
 * @method     ChildErcasBillDolceQuery orderByDiscounts($order = Criteria::ASC) Order by the Discounts column
 * @method     ChildErcasBillDolceQuery orderByOthercharges($order = Criteria::ASC) Order by the OtherCharges column
 * @method     ChildErcasBillDolceQuery orderByPenaltycharges($order = Criteria::ASC) Order by the PenaltyCharges column
 * @method     ChildErcasBillDolceQuery orderByStampdutycharges($order = Criteria::ASC) Order by the StampDutyCharges column
 * @method     ChildErcasBillDolceQuery orderByServicecharges($order = Criteria::ASC) Order by the ServiceCharges column
 * @method     ChildErcasBillDolceQuery orderByRoutinecharges($order = Criteria::ASC) Order by the RoutineCharges column
 * @method     ChildErcasBillDolceQuery orderByBillservicerate($order = Criteria::ASC) Order by the BillServiceRate column
 * @method     ChildErcasBillDolceQuery orderByServicetypedesc($order = Criteria::ASC) Order by the ServiceTypeDesc column
 * @method     ChildErcasBillDolceQuery orderByBillperiod($order = Criteria::ASC) Order by the BillPeriod column
 * @method     ChildErcasBillDolceQuery orderByUsagetype($order = Criteria::ASC) Order by the UsageType column
 * @method     ChildErcasBillDolceQuery orderByMeternumber($order = Criteria::ASC) Order by the MeterNumber column
 * @method     ChildErcasBillDolceQuery orderByMetertype($order = Criteria::ASC) Order by the MeterType column
 * @method     ChildErcasBillDolceQuery orderByMetercondition($order = Criteria::ASC) Order by the MeterCondition column
 * @method     ChildErcasBillDolceQuery orderByLeakagestatus($order = Criteria::ASC) Order by the LeakageStatus column
 * @method     ChildErcasBillDolceQuery orderByPropertytype($order = Criteria::ASC) Order by the PropertyType column
 * @method     ChildErcasBillDolceQuery orderByMeterreaddevice($order = Criteria::ASC) Order by the MeterReadDevice column
 * @method     ChildErcasBillDolceQuery orderByBillmethod($order = Criteria::ASC) Order by the Billmethod column
 * @method     ChildErcasBillDolceQuery orderByDatecreated($order = Criteria::ASC) Order by the DateCreated column
 * @method     ChildErcasBillDolceQuery orderByIsMigrated($order = Criteria::ASC) Order by the is_migrated column
 *
 * @method     ChildErcasBillDolceQuery groupByUniquekeyid() Group by the UniqueKeyID column
 * @method     ChildErcasBillDolceQuery groupByAccountno() Group by the AccountNo column
 * @method     ChildErcasBillDolceQuery groupByServicedistrict() Group by the ServiceDistrict column
 * @method     ChildErcasBillDolceQuery groupByLastmeterreading() Group by the LastMeterReading column
 * @method     ChildErcasBillDolceQuery groupByCurrentmeterreading() Group by the CurrentMeterReading column
 * @method     ChildErcasBillDolceQuery groupByUnitsconsumed() Group by the UnitsConsumed column
 * @method     ChildErcasBillDolceQuery groupByLastpaydate() Group by the LastPayDate column
 * @method     ChildErcasBillDolceQuery groupByLastpayamt() Group by the LastPayAmt column
 * @method     ChildErcasBillDolceQuery groupByPriorbalance() Group by the PriorBalance column
 * @method     ChildErcasBillDolceQuery groupByOutstandingbalance() Group by the OutstandingBalance column
 * @method     ChildErcasBillDolceQuery groupByAmountdue() Group by the AmountDue column
 * @method     ChildErcasBillDolceQuery groupByMetermaintenancecharge() Group by the MeterMaintenanceCharge column
 * @method     ChildErcasBillDolceQuery groupByDiscounts() Group by the Discounts column
 * @method     ChildErcasBillDolceQuery groupByOthercharges() Group by the OtherCharges column
 * @method     ChildErcasBillDolceQuery groupByPenaltycharges() Group by the PenaltyCharges column
 * @method     ChildErcasBillDolceQuery groupByStampdutycharges() Group by the StampDutyCharges column
 * @method     ChildErcasBillDolceQuery groupByServicecharges() Group by the ServiceCharges column
 * @method     ChildErcasBillDolceQuery groupByRoutinecharges() Group by the RoutineCharges column
 * @method     ChildErcasBillDolceQuery groupByBillservicerate() Group by the BillServiceRate column
 * @method     ChildErcasBillDolceQuery groupByServicetypedesc() Group by the ServiceTypeDesc column
 * @method     ChildErcasBillDolceQuery groupByBillperiod() Group by the BillPeriod column
 * @method     ChildErcasBillDolceQuery groupByUsagetype() Group by the UsageType column
 * @method     ChildErcasBillDolceQuery groupByMeternumber() Group by the MeterNumber column
 * @method     ChildErcasBillDolceQuery groupByMetertype() Group by the MeterType column
 * @method     ChildErcasBillDolceQuery groupByMetercondition() Group by the MeterCondition column
 * @method     ChildErcasBillDolceQuery groupByLeakagestatus() Group by the LeakageStatus column
 * @method     ChildErcasBillDolceQuery groupByPropertytype() Group by the PropertyType column
 * @method     ChildErcasBillDolceQuery groupByMeterreaddevice() Group by the MeterReadDevice column
 * @method     ChildErcasBillDolceQuery groupByBillmethod() Group by the Billmethod column
 * @method     ChildErcasBillDolceQuery groupByDatecreated() Group by the DateCreated column
 * @method     ChildErcasBillDolceQuery groupByIsMigrated() Group by the is_migrated column
 *
 * @method     ChildErcasBillDolceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildErcasBillDolceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildErcasBillDolceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildErcasBillDolceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildErcasBillDolceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildErcasBillDolceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildErcasBillDolce findOne(ConnectionInterface $con = null) Return the first ChildErcasBillDolce matching the query
 * @method     ChildErcasBillDolce findOneOrCreate(ConnectionInterface $con = null) Return the first ChildErcasBillDolce matching the query, or a new ChildErcasBillDolce object populated from the query conditions when no match is found
 *
 * @method     ChildErcasBillDolce findOneByUniquekeyid(int $UniqueKeyID) Return the first ChildErcasBillDolce filtered by the UniqueKeyID column
 * @method     ChildErcasBillDolce findOneByAccountno(string $AccountNo) Return the first ChildErcasBillDolce filtered by the AccountNo column
 * @method     ChildErcasBillDolce findOneByServicedistrict(string $ServiceDistrict) Return the first ChildErcasBillDolce filtered by the ServiceDistrict column
 * @method     ChildErcasBillDolce findOneByLastmeterreading(double $LastMeterReading) Return the first ChildErcasBillDolce filtered by the LastMeterReading column
 * @method     ChildErcasBillDolce findOneByCurrentmeterreading(double $CurrentMeterReading) Return the first ChildErcasBillDolce filtered by the CurrentMeterReading column
 * @method     ChildErcasBillDolce findOneByUnitsconsumed(double $UnitsConsumed) Return the first ChildErcasBillDolce filtered by the UnitsConsumed column
 * @method     ChildErcasBillDolce findOneByLastpaydate(string $LastPayDate) Return the first ChildErcasBillDolce filtered by the LastPayDate column
 * @method     ChildErcasBillDolce findOneByLastpayamt(string $LastPayAmt) Return the first ChildErcasBillDolce filtered by the LastPayAmt column
 * @method     ChildErcasBillDolce findOneByPriorbalance(string $PriorBalance) Return the first ChildErcasBillDolce filtered by the PriorBalance column
 * @method     ChildErcasBillDolce findOneByOutstandingbalance(string $OutstandingBalance) Return the first ChildErcasBillDolce filtered by the OutstandingBalance column
 * @method     ChildErcasBillDolce findOneByAmountdue(string $AmountDue) Return the first ChildErcasBillDolce filtered by the AmountDue column
 * @method     ChildErcasBillDolce findOneByMetermaintenancecharge(string $MeterMaintenanceCharge) Return the first ChildErcasBillDolce filtered by the MeterMaintenanceCharge column
 * @method     ChildErcasBillDolce findOneByDiscounts(string $Discounts) Return the first ChildErcasBillDolce filtered by the Discounts column
 * @method     ChildErcasBillDolce findOneByOthercharges(string $OtherCharges) Return the first ChildErcasBillDolce filtered by the OtherCharges column
 * @method     ChildErcasBillDolce findOneByPenaltycharges(string $PenaltyCharges) Return the first ChildErcasBillDolce filtered by the PenaltyCharges column
 * @method     ChildErcasBillDolce findOneByStampdutycharges(string $StampDutyCharges) Return the first ChildErcasBillDolce filtered by the StampDutyCharges column
 * @method     ChildErcasBillDolce findOneByServicecharges(string $ServiceCharges) Return the first ChildErcasBillDolce filtered by the ServiceCharges column
 * @method     ChildErcasBillDolce findOneByRoutinecharges(string $RoutineCharges) Return the first ChildErcasBillDolce filtered by the RoutineCharges column
 * @method     ChildErcasBillDolce findOneByBillservicerate(string $BillServiceRate) Return the first ChildErcasBillDolce filtered by the BillServiceRate column
 * @method     ChildErcasBillDolce findOneByServicetypedesc(string $ServiceTypeDesc) Return the first ChildErcasBillDolce filtered by the ServiceTypeDesc column
 * @method     ChildErcasBillDolce findOneByBillperiod(string $BillPeriod) Return the first ChildErcasBillDolce filtered by the BillPeriod column
 * @method     ChildErcasBillDolce findOneByUsagetype(string $UsageType) Return the first ChildErcasBillDolce filtered by the UsageType column
 * @method     ChildErcasBillDolce findOneByMeternumber(string $MeterNumber) Return the first ChildErcasBillDolce filtered by the MeterNumber column
 * @method     ChildErcasBillDolce findOneByMetertype(string $MeterType) Return the first ChildErcasBillDolce filtered by the MeterType column
 * @method     ChildErcasBillDolce findOneByMetercondition(string $MeterCondition) Return the first ChildErcasBillDolce filtered by the MeterCondition column
 * @method     ChildErcasBillDolce findOneByLeakagestatus(string $LeakageStatus) Return the first ChildErcasBillDolce filtered by the LeakageStatus column
 * @method     ChildErcasBillDolce findOneByPropertytype(string $PropertyType) Return the first ChildErcasBillDolce filtered by the PropertyType column
 * @method     ChildErcasBillDolce findOneByMeterreaddevice(string $MeterReadDevice) Return the first ChildErcasBillDolce filtered by the MeterReadDevice column
 * @method     ChildErcasBillDolce findOneByBillmethod(string $Billmethod) Return the first ChildErcasBillDolce filtered by the Billmethod column
 * @method     ChildErcasBillDolce findOneByDatecreated(string $DateCreated) Return the first ChildErcasBillDolce filtered by the DateCreated column
 * @method     ChildErcasBillDolce findOneByIsMigrated(int $is_migrated) Return the first ChildErcasBillDolce filtered by the is_migrated column *

 * @method     ChildErcasBillDolce requirePk($key, ConnectionInterface $con = null) Return the ChildErcasBillDolce by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOne(ConnectionInterface $con = null) Return the first ChildErcasBillDolce matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBillDolce requireOneByUniquekeyid(int $UniqueKeyID) Return the first ChildErcasBillDolce filtered by the UniqueKeyID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByAccountno(string $AccountNo) Return the first ChildErcasBillDolce filtered by the AccountNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByServicedistrict(string $ServiceDistrict) Return the first ChildErcasBillDolce filtered by the ServiceDistrict column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByLastmeterreading(double $LastMeterReading) Return the first ChildErcasBillDolce filtered by the LastMeterReading column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByCurrentmeterreading(double $CurrentMeterReading) Return the first ChildErcasBillDolce filtered by the CurrentMeterReading column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByUnitsconsumed(double $UnitsConsumed) Return the first ChildErcasBillDolce filtered by the UnitsConsumed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByLastpaydate(string $LastPayDate) Return the first ChildErcasBillDolce filtered by the LastPayDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByLastpayamt(string $LastPayAmt) Return the first ChildErcasBillDolce filtered by the LastPayAmt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByPriorbalance(string $PriorBalance) Return the first ChildErcasBillDolce filtered by the PriorBalance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByOutstandingbalance(string $OutstandingBalance) Return the first ChildErcasBillDolce filtered by the OutstandingBalance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByAmountdue(string $AmountDue) Return the first ChildErcasBillDolce filtered by the AmountDue column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByMetermaintenancecharge(string $MeterMaintenanceCharge) Return the first ChildErcasBillDolce filtered by the MeterMaintenanceCharge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByDiscounts(string $Discounts) Return the first ChildErcasBillDolce filtered by the Discounts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByOthercharges(string $OtherCharges) Return the first ChildErcasBillDolce filtered by the OtherCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByPenaltycharges(string $PenaltyCharges) Return the first ChildErcasBillDolce filtered by the PenaltyCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByStampdutycharges(string $StampDutyCharges) Return the first ChildErcasBillDolce filtered by the StampDutyCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByServicecharges(string $ServiceCharges) Return the first ChildErcasBillDolce filtered by the ServiceCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByRoutinecharges(string $RoutineCharges) Return the first ChildErcasBillDolce filtered by the RoutineCharges column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByBillservicerate(string $BillServiceRate) Return the first ChildErcasBillDolce filtered by the BillServiceRate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByServicetypedesc(string $ServiceTypeDesc) Return the first ChildErcasBillDolce filtered by the ServiceTypeDesc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByBillperiod(string $BillPeriod) Return the first ChildErcasBillDolce filtered by the BillPeriod column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByUsagetype(string $UsageType) Return the first ChildErcasBillDolce filtered by the UsageType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByMeternumber(string $MeterNumber) Return the first ChildErcasBillDolce filtered by the MeterNumber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByMetertype(string $MeterType) Return the first ChildErcasBillDolce filtered by the MeterType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByMetercondition(string $MeterCondition) Return the first ChildErcasBillDolce filtered by the MeterCondition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByLeakagestatus(string $LeakageStatus) Return the first ChildErcasBillDolce filtered by the LeakageStatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByPropertytype(string $PropertyType) Return the first ChildErcasBillDolce filtered by the PropertyType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByMeterreaddevice(string $MeterReadDevice) Return the first ChildErcasBillDolce filtered by the MeterReadDevice column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByBillmethod(string $Billmethod) Return the first ChildErcasBillDolce filtered by the Billmethod column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByDatecreated(string $DateCreated) Return the first ChildErcasBillDolce filtered by the DateCreated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildErcasBillDolce requireOneByIsMigrated(int $is_migrated) Return the first ChildErcasBillDolce filtered by the is_migrated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildErcasBillDolce[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildErcasBillDolce objects based on current ModelCriteria
 * @method     ChildErcasBillDolce[]|ObjectCollection findByUniquekeyid(int $UniqueKeyID) Return ChildErcasBillDolce objects filtered by the UniqueKeyID column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByAccountno(string $AccountNo) Return ChildErcasBillDolce objects filtered by the AccountNo column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByServicedistrict(string $ServiceDistrict) Return ChildErcasBillDolce objects filtered by the ServiceDistrict column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByLastmeterreading(double $LastMeterReading) Return ChildErcasBillDolce objects filtered by the LastMeterReading column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByCurrentmeterreading(double $CurrentMeterReading) Return ChildErcasBillDolce objects filtered by the CurrentMeterReading column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByUnitsconsumed(double $UnitsConsumed) Return ChildErcasBillDolce objects filtered by the UnitsConsumed column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByLastpaydate(string $LastPayDate) Return ChildErcasBillDolce objects filtered by the LastPayDate column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByLastpayamt(string $LastPayAmt) Return ChildErcasBillDolce objects filtered by the LastPayAmt column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByPriorbalance(string $PriorBalance) Return ChildErcasBillDolce objects filtered by the PriorBalance column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByOutstandingbalance(string $OutstandingBalance) Return ChildErcasBillDolce objects filtered by the OutstandingBalance column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByAmountdue(string $AmountDue) Return ChildErcasBillDolce objects filtered by the AmountDue column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByMetermaintenancecharge(string $MeterMaintenanceCharge) Return ChildErcasBillDolce objects filtered by the MeterMaintenanceCharge column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByDiscounts(string $Discounts) Return ChildErcasBillDolce objects filtered by the Discounts column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByOthercharges(string $OtherCharges) Return ChildErcasBillDolce objects filtered by the OtherCharges column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByPenaltycharges(string $PenaltyCharges) Return ChildErcasBillDolce objects filtered by the PenaltyCharges column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByStampdutycharges(string $StampDutyCharges) Return ChildErcasBillDolce objects filtered by the StampDutyCharges column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByServicecharges(string $ServiceCharges) Return ChildErcasBillDolce objects filtered by the ServiceCharges column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByRoutinecharges(string $RoutineCharges) Return ChildErcasBillDolce objects filtered by the RoutineCharges column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByBillservicerate(string $BillServiceRate) Return ChildErcasBillDolce objects filtered by the BillServiceRate column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByServicetypedesc(string $ServiceTypeDesc) Return ChildErcasBillDolce objects filtered by the ServiceTypeDesc column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByBillperiod(string $BillPeriod) Return ChildErcasBillDolce objects filtered by the BillPeriod column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByUsagetype(string $UsageType) Return ChildErcasBillDolce objects filtered by the UsageType column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByMeternumber(string $MeterNumber) Return ChildErcasBillDolce objects filtered by the MeterNumber column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByMetertype(string $MeterType) Return ChildErcasBillDolce objects filtered by the MeterType column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByMetercondition(string $MeterCondition) Return ChildErcasBillDolce objects filtered by the MeterCondition column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByLeakagestatus(string $LeakageStatus) Return ChildErcasBillDolce objects filtered by the LeakageStatus column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByPropertytype(string $PropertyType) Return ChildErcasBillDolce objects filtered by the PropertyType column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByMeterreaddevice(string $MeterReadDevice) Return ChildErcasBillDolce objects filtered by the MeterReadDevice column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByBillmethod(string $Billmethod) Return ChildErcasBillDolce objects filtered by the Billmethod column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByDatecreated(string $DateCreated) Return ChildErcasBillDolce objects filtered by the DateCreated column
 * @method     ChildErcasBillDolce[]|ObjectCollection findByIsMigrated(int $is_migrated) Return ChildErcasBillDolce objects filtered by the is_migrated column
 * @method     ChildErcasBillDolce[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ErcasBillDolceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ErcasBillDolceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ErcasBillDolce', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildErcasBillDolceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildErcasBillDolceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildErcasBillDolceQuery) {
            return $criteria;
        }
        $query = new ChildErcasBillDolceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildErcasBillDolce|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The ErcasBillDolce object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The ErcasBillDolce object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The ErcasBillDolce object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The ErcasBillDolce object has no primary key');
    }

    /**
     * Filter the query on the UniqueKeyID column
     *
     * Example usage:
     * <code>
     * $query->filterByUniquekeyid(1234); // WHERE UniqueKeyID = 1234
     * $query->filterByUniquekeyid(array(12, 34)); // WHERE UniqueKeyID IN (12, 34)
     * $query->filterByUniquekeyid(array('min' => 12)); // WHERE UniqueKeyID > 12
     * </code>
     *
     * @param     mixed $uniquekeyid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByUniquekeyid($uniquekeyid = null, $comparison = null)
    {
        if (is_array($uniquekeyid)) {
            $useMinMax = false;
            if (isset($uniquekeyid['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uniquekeyid['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNIQUEKEYID, $uniquekeyid, $comparison);
    }

    /**
     * Filter the query on the AccountNo column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountno('fooValue');   // WHERE AccountNo = 'fooValue'
     * $query->filterByAccountno('%fooValue%'); // WHERE AccountNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountno The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByAccountno($accountno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountno)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountno)) {
                $accountno = str_replace('*', '%', $accountno);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_ACCOUNTNO, $accountno, $comparison);
    }

    /**
     * Filter the query on the ServiceDistrict column
     *
     * Example usage:
     * <code>
     * $query->filterByServicedistrict('fooValue');   // WHERE ServiceDistrict = 'fooValue'
     * $query->filterByServicedistrict('%fooValue%'); // WHERE ServiceDistrict LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicedistrict The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByServicedistrict($servicedistrict = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicedistrict)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicedistrict)) {
                $servicedistrict = str_replace('*', '%', $servicedistrict);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_SERVICEDISTRICT, $servicedistrict, $comparison);
    }

    /**
     * Filter the query on the LastMeterReading column
     *
     * Example usage:
     * <code>
     * $query->filterByLastmeterreading(1234); // WHERE LastMeterReading = 1234
     * $query->filterByLastmeterreading(array(12, 34)); // WHERE LastMeterReading IN (12, 34)
     * $query->filterByLastmeterreading(array('min' => 12)); // WHERE LastMeterReading > 12
     * </code>
     *
     * @param     mixed $lastmeterreading The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByLastmeterreading($lastmeterreading = null, $comparison = null)
    {
        if (is_array($lastmeterreading)) {
            $useMinMax = false;
            if (isset($lastmeterreading['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTMETERREADING, $lastmeterreading['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastmeterreading['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTMETERREADING, $lastmeterreading['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTMETERREADING, $lastmeterreading, $comparison);
    }

    /**
     * Filter the query on the CurrentMeterReading column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentmeterreading(1234); // WHERE CurrentMeterReading = 1234
     * $query->filterByCurrentmeterreading(array(12, 34)); // WHERE CurrentMeterReading IN (12, 34)
     * $query->filterByCurrentmeterreading(array('min' => 12)); // WHERE CurrentMeterReading > 12
     * </code>
     *
     * @param     mixed $currentmeterreading The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByCurrentmeterreading($currentmeterreading = null, $comparison = null)
    {
        if (is_array($currentmeterreading)) {
            $useMinMax = false;
            if (isset($currentmeterreading['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentmeterreading['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_CURRENTMETERREADING, $currentmeterreading, $comparison);
    }

    /**
     * Filter the query on the UnitsConsumed column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitsconsumed(1234); // WHERE UnitsConsumed = 1234
     * $query->filterByUnitsconsumed(array(12, 34)); // WHERE UnitsConsumed IN (12, 34)
     * $query->filterByUnitsconsumed(array('min' => 12)); // WHERE UnitsConsumed > 12
     * </code>
     *
     * @param     mixed $unitsconsumed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByUnitsconsumed($unitsconsumed = null, $comparison = null)
    {
        if (is_array($unitsconsumed)) {
            $useMinMax = false;
            if (isset($unitsconsumed['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitsconsumed['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_UNITSCONSUMED, $unitsconsumed, $comparison);
    }

    /**
     * Filter the query on the LastPayDate column
     *
     * Example usage:
     * <code>
     * $query->filterByLastpaydate('fooValue');   // WHERE LastPayDate = 'fooValue'
     * $query->filterByLastpaydate('%fooValue%'); // WHERE LastPayDate LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastpaydate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByLastpaydate($lastpaydate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastpaydate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastpaydate)) {
                $lastpaydate = str_replace('*', '%', $lastpaydate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTPAYDATE, $lastpaydate, $comparison);
    }

    /**
     * Filter the query on the LastPayAmt column
     *
     * Example usage:
     * <code>
     * $query->filterByLastpayamt(1234); // WHERE LastPayAmt = 1234
     * $query->filterByLastpayamt(array(12, 34)); // WHERE LastPayAmt IN (12, 34)
     * $query->filterByLastpayamt(array('min' => 12)); // WHERE LastPayAmt > 12
     * </code>
     *
     * @param     mixed $lastpayamt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByLastpayamt($lastpayamt = null, $comparison = null)
    {
        if (is_array($lastpayamt)) {
            $useMinMax = false;
            if (isset($lastpayamt['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTPAYAMT, $lastpayamt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastpayamt['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTPAYAMT, $lastpayamt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_LASTPAYAMT, $lastpayamt, $comparison);
    }

    /**
     * Filter the query on the PriorBalance column
     *
     * Example usage:
     * <code>
     * $query->filterByPriorbalance(1234); // WHERE PriorBalance = 1234
     * $query->filterByPriorbalance(array(12, 34)); // WHERE PriorBalance IN (12, 34)
     * $query->filterByPriorbalance(array('min' => 12)); // WHERE PriorBalance > 12
     * </code>
     *
     * @param     mixed $priorbalance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByPriorbalance($priorbalance = null, $comparison = null)
    {
        if (is_array($priorbalance)) {
            $useMinMax = false;
            if (isset($priorbalance['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_PRIORBALANCE, $priorbalance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priorbalance['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_PRIORBALANCE, $priorbalance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_PRIORBALANCE, $priorbalance, $comparison);
    }

    /**
     * Filter the query on the OutstandingBalance column
     *
     * Example usage:
     * <code>
     * $query->filterByOutstandingbalance(1234); // WHERE OutstandingBalance = 1234
     * $query->filterByOutstandingbalance(array(12, 34)); // WHERE OutstandingBalance IN (12, 34)
     * $query->filterByOutstandingbalance(array('min' => 12)); // WHERE OutstandingBalance > 12
     * </code>
     *
     * @param     mixed $outstandingbalance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByOutstandingbalance($outstandingbalance = null, $comparison = null)
    {
        if (is_array($outstandingbalance)) {
            $useMinMax = false;
            if (isset($outstandingbalance['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($outstandingbalance['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE, $outstandingbalance, $comparison);
    }

    /**
     * Filter the query on the AmountDue column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountdue(1234); // WHERE AmountDue = 1234
     * $query->filterByAmountdue(array(12, 34)); // WHERE AmountDue IN (12, 34)
     * $query->filterByAmountdue(array('min' => 12)); // WHERE AmountDue > 12
     * </code>
     *
     * @param     mixed $amountdue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByAmountdue($amountdue = null, $comparison = null)
    {
        if (is_array($amountdue)) {
            $useMinMax = false;
            if (isset($amountdue['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_AMOUNTDUE, $amountdue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountdue['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_AMOUNTDUE, $amountdue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_AMOUNTDUE, $amountdue, $comparison);
    }

    /**
     * Filter the query on the MeterMaintenanceCharge column
     *
     * Example usage:
     * <code>
     * $query->filterByMetermaintenancecharge(1234); // WHERE MeterMaintenanceCharge = 1234
     * $query->filterByMetermaintenancecharge(array(12, 34)); // WHERE MeterMaintenanceCharge IN (12, 34)
     * $query->filterByMetermaintenancecharge(array('min' => 12)); // WHERE MeterMaintenanceCharge > 12
     * </code>
     *
     * @param     mixed $metermaintenancecharge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByMetermaintenancecharge($metermaintenancecharge = null, $comparison = null)
    {
        if (is_array($metermaintenancecharge)) {
            $useMinMax = false;
            if (isset($metermaintenancecharge['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($metermaintenancecharge['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE, $metermaintenancecharge, $comparison);
    }

    /**
     * Filter the query on the Discounts column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscounts(1234); // WHERE Discounts = 1234
     * $query->filterByDiscounts(array(12, 34)); // WHERE Discounts IN (12, 34)
     * $query->filterByDiscounts(array('min' => 12)); // WHERE Discounts > 12
     * </code>
     *
     * @param     mixed $discounts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByDiscounts($discounts = null, $comparison = null)
    {
        if (is_array($discounts)) {
            $useMinMax = false;
            if (isset($discounts['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_DISCOUNTS, $discounts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discounts['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_DISCOUNTS, $discounts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_DISCOUNTS, $discounts, $comparison);
    }

    /**
     * Filter the query on the OtherCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByOthercharges(1234); // WHERE OtherCharges = 1234
     * $query->filterByOthercharges(array(12, 34)); // WHERE OtherCharges IN (12, 34)
     * $query->filterByOthercharges(array('min' => 12)); // WHERE OtherCharges > 12
     * </code>
     *
     * @param     mixed $othercharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByOthercharges($othercharges = null, $comparison = null)
    {
        if (is_array($othercharges)) {
            $useMinMax = false;
            if (isset($othercharges['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_OTHERCHARGES, $othercharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($othercharges['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_OTHERCHARGES, $othercharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_OTHERCHARGES, $othercharges, $comparison);
    }

    /**
     * Filter the query on the PenaltyCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByPenaltycharges(1234); // WHERE PenaltyCharges = 1234
     * $query->filterByPenaltycharges(array(12, 34)); // WHERE PenaltyCharges IN (12, 34)
     * $query->filterByPenaltycharges(array('min' => 12)); // WHERE PenaltyCharges > 12
     * </code>
     *
     * @param     mixed $penaltycharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByPenaltycharges($penaltycharges = null, $comparison = null)
    {
        if (is_array($penaltycharges)) {
            $useMinMax = false;
            if (isset($penaltycharges['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_PENALTYCHARGES, $penaltycharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penaltycharges['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_PENALTYCHARGES, $penaltycharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_PENALTYCHARGES, $penaltycharges, $comparison);
    }

    /**
     * Filter the query on the StampDutyCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByStampdutycharges(1234); // WHERE StampDutyCharges = 1234
     * $query->filterByStampdutycharges(array(12, 34)); // WHERE StampDutyCharges IN (12, 34)
     * $query->filterByStampdutycharges(array('min' => 12)); // WHERE StampDutyCharges > 12
     * </code>
     *
     * @param     mixed $stampdutycharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByStampdutycharges($stampdutycharges = null, $comparison = null)
    {
        if (is_array($stampdutycharges)) {
            $useMinMax = false;
            if (isset($stampdutycharges['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stampdutycharges['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES, $stampdutycharges, $comparison);
    }

    /**
     * Filter the query on the ServiceCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByServicecharges(1234); // WHERE ServiceCharges = 1234
     * $query->filterByServicecharges(array(12, 34)); // WHERE ServiceCharges IN (12, 34)
     * $query->filterByServicecharges(array('min' => 12)); // WHERE ServiceCharges > 12
     * </code>
     *
     * @param     mixed $servicecharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByServicecharges($servicecharges = null, $comparison = null)
    {
        if (is_array($servicecharges)) {
            $useMinMax = false;
            if (isset($servicecharges['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_SERVICECHARGES, $servicecharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicecharges['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_SERVICECHARGES, $servicecharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_SERVICECHARGES, $servicecharges, $comparison);
    }

    /**
     * Filter the query on the RoutineCharges column
     *
     * Example usage:
     * <code>
     * $query->filterByRoutinecharges(1234); // WHERE RoutineCharges = 1234
     * $query->filterByRoutinecharges(array(12, 34)); // WHERE RoutineCharges IN (12, 34)
     * $query->filterByRoutinecharges(array('min' => 12)); // WHERE RoutineCharges > 12
     * </code>
     *
     * @param     mixed $routinecharges The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByRoutinecharges($routinecharges = null, $comparison = null)
    {
        if (is_array($routinecharges)) {
            $useMinMax = false;
            if (isset($routinecharges['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_ROUTINECHARGES, $routinecharges['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($routinecharges['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_ROUTINECHARGES, $routinecharges['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_ROUTINECHARGES, $routinecharges, $comparison);
    }

    /**
     * Filter the query on the BillServiceRate column
     *
     * Example usage:
     * <code>
     * $query->filterByBillservicerate(1234); // WHERE BillServiceRate = 1234
     * $query->filterByBillservicerate(array(12, 34)); // WHERE BillServiceRate IN (12, 34)
     * $query->filterByBillservicerate(array('min' => 12)); // WHERE BillServiceRate > 12
     * </code>
     *
     * @param     mixed $billservicerate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByBillservicerate($billservicerate = null, $comparison = null)
    {
        if (is_array($billservicerate)) {
            $useMinMax = false;
            if (isset($billservicerate['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_BILLSERVICERATE, $billservicerate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($billservicerate['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_BILLSERVICERATE, $billservicerate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_BILLSERVICERATE, $billservicerate, $comparison);
    }

    /**
     * Filter the query on the ServiceTypeDesc column
     *
     * Example usage:
     * <code>
     * $query->filterByServicetypedesc('fooValue');   // WHERE ServiceTypeDesc = 'fooValue'
     * $query->filterByServicetypedesc('%fooValue%'); // WHERE ServiceTypeDesc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicetypedesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByServicetypedesc($servicetypedesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicetypedesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicetypedesc)) {
                $servicetypedesc = str_replace('*', '%', $servicetypedesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_SERVICETYPEDESC, $servicetypedesc, $comparison);
    }

    /**
     * Filter the query on the BillPeriod column
     *
     * Example usage:
     * <code>
     * $query->filterByBillperiod('fooValue');   // WHERE BillPeriod = 'fooValue'
     * $query->filterByBillperiod('%fooValue%'); // WHERE BillPeriod LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billperiod The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByBillperiod($billperiod = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billperiod)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billperiod)) {
                $billperiod = str_replace('*', '%', $billperiod);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_BILLPERIOD, $billperiod, $comparison);
    }

    /**
     * Filter the query on the UsageType column
     *
     * Example usage:
     * <code>
     * $query->filterByUsagetype('fooValue');   // WHERE UsageType = 'fooValue'
     * $query->filterByUsagetype('%fooValue%'); // WHERE UsageType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usagetype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByUsagetype($usagetype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usagetype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $usagetype)) {
                $usagetype = str_replace('*', '%', $usagetype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_USAGETYPE, $usagetype, $comparison);
    }

    /**
     * Filter the query on the MeterNumber column
     *
     * Example usage:
     * <code>
     * $query->filterByMeternumber('fooValue');   // WHERE MeterNumber = 'fooValue'
     * $query->filterByMeternumber('%fooValue%'); // WHERE MeterNumber LIKE '%fooValue%'
     * </code>
     *
     * @param     string $meternumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByMeternumber($meternumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($meternumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $meternumber)) {
                $meternumber = str_replace('*', '%', $meternumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERNUMBER, $meternumber, $comparison);
    }

    /**
     * Filter the query on the MeterType column
     *
     * Example usage:
     * <code>
     * $query->filterByMetertype('fooValue');   // WHERE MeterType = 'fooValue'
     * $query->filterByMetertype('%fooValue%'); // WHERE MeterType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metertype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByMetertype($metertype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metertype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metertype)) {
                $metertype = str_replace('*', '%', $metertype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERTYPE, $metertype, $comparison);
    }

    /**
     * Filter the query on the MeterCondition column
     *
     * Example usage:
     * <code>
     * $query->filterByMetercondition('fooValue');   // WHERE MeterCondition = 'fooValue'
     * $query->filterByMetercondition('%fooValue%'); // WHERE MeterCondition LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metercondition The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByMetercondition($metercondition = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metercondition)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metercondition)) {
                $metercondition = str_replace('*', '%', $metercondition);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERCONDITION, $metercondition, $comparison);
    }

    /**
     * Filter the query on the LeakageStatus column
     *
     * Example usage:
     * <code>
     * $query->filterByLeakagestatus('fooValue');   // WHERE LeakageStatus = 'fooValue'
     * $query->filterByLeakagestatus('%fooValue%'); // WHERE LeakageStatus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $leakagestatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByLeakagestatus($leakagestatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($leakagestatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $leakagestatus)) {
                $leakagestatus = str_replace('*', '%', $leakagestatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_LEAKAGESTATUS, $leakagestatus, $comparison);
    }

    /**
     * Filter the query on the PropertyType column
     *
     * Example usage:
     * <code>
     * $query->filterByPropertytype('fooValue');   // WHERE PropertyType = 'fooValue'
     * $query->filterByPropertytype('%fooValue%'); // WHERE PropertyType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $propertytype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByPropertytype($propertytype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($propertytype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $propertytype)) {
                $propertytype = str_replace('*', '%', $propertytype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_PROPERTYTYPE, $propertytype, $comparison);
    }

    /**
     * Filter the query on the MeterReadDevice column
     *
     * Example usage:
     * <code>
     * $query->filterByMeterreaddevice('fooValue');   // WHERE MeterReadDevice = 'fooValue'
     * $query->filterByMeterreaddevice('%fooValue%'); // WHERE MeterReadDevice LIKE '%fooValue%'
     * </code>
     *
     * @param     string $meterreaddevice The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByMeterreaddevice($meterreaddevice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($meterreaddevice)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $meterreaddevice)) {
                $meterreaddevice = str_replace('*', '%', $meterreaddevice);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_METERREADDEVICE, $meterreaddevice, $comparison);
    }

    /**
     * Filter the query on the Billmethod column
     *
     * Example usage:
     * <code>
     * $query->filterByBillmethod('fooValue');   // WHERE Billmethod = 'fooValue'
     * $query->filterByBillmethod('%fooValue%'); // WHERE Billmethod LIKE '%fooValue%'
     * </code>
     *
     * @param     string $billmethod The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByBillmethod($billmethod = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($billmethod)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $billmethod)) {
                $billmethod = str_replace('*', '%', $billmethod);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_BILLMETHOD, $billmethod, $comparison);
    }

    /**
     * Filter the query on the DateCreated column
     *
     * Example usage:
     * <code>
     * $query->filterByDatecreated('2011-03-14'); // WHERE DateCreated = '2011-03-14'
     * $query->filterByDatecreated('now'); // WHERE DateCreated = '2011-03-14'
     * $query->filterByDatecreated(array('max' => 'yesterday')); // WHERE DateCreated > '2011-03-13'
     * </code>
     *
     * @param     mixed $datecreated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByDatecreated($datecreated = null, $comparison = null)
    {
        if (is_array($datecreated)) {
            $useMinMax = false;
            if (isset($datecreated['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_DATECREATED, $datecreated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datecreated['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_DATECREATED, $datecreated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_DATECREATED, $datecreated, $comparison);
    }

    /**
     * Filter the query on the is_migrated column
     *
     * Example usage:
     * <code>
     * $query->filterByIsMigrated(1234); // WHERE is_migrated = 1234
     * $query->filterByIsMigrated(array(12, 34)); // WHERE is_migrated IN (12, 34)
     * $query->filterByIsMigrated(array('min' => 12)); // WHERE is_migrated > 12
     * </code>
     *
     * @param     mixed $isMigrated The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function filterByIsMigrated($isMigrated = null, $comparison = null)
    {
        if (is_array($isMigrated)) {
            $useMinMax = false;
            if (isset($isMigrated['min'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_IS_MIGRATED, $isMigrated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isMigrated['max'])) {
                $this->addUsingAlias(ErcasBillDolceTableMap::COL_IS_MIGRATED, $isMigrated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErcasBillDolceTableMap::COL_IS_MIGRATED, $isMigrated, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildErcasBillDolce $ercasBillDolce Object to remove from the list of results
     *
     * @return $this|ChildErcasBillDolceQuery The current query, for fluid interface
     */
    public function prune($ercasBillDolce = null)
    {
        if ($ercasBillDolce) {
            throw new LogicException('ErcasBillDolce object has no primary key');

        }

        return $this;
    }

    /**
     * Deletes all rows from the ercas_bill_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillDolceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ErcasBillDolceTableMap::clearInstancePool();
            ErcasBillDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillDolceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ErcasBillDolceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ErcasBillDolceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ErcasBillDolceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ErcasBillDolceQuery
