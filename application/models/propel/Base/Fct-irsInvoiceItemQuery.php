<?php

namespace Base;

use \Fct-irsInvoiceItem as ChildFct-irsInvoiceItem;
use \Fct-irsInvoiceItemQuery as ChildFct-irsInvoiceItemQuery;
use \Exception;
use \PDO;
use Map\Fct-irsInvoiceItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'fct-irs_invoice_item' table.
 *
 *
 *
 * @method     ChildFct-irsInvoiceItemQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFct-irsInvoiceItemQuery orderByServiceName($order = Criteria::ASC) Order by the service_name column
 * @method     ChildFct-irsInvoiceItemQuery orderByServicePaidforName($order = Criteria::ASC) Order by the service_paidfor_name column
 * @method     ChildFct-irsInvoiceItemQuery orderByServicePaidforCode($order = Criteria::ASC) Order by the service_paidfor_code column
 * @method     ChildFct-irsInvoiceItemQuery orderByDepartmentName($order = Criteria::ASC) Order by the department_name column
 * @method     ChildFct-irsInvoiceItemQuery orderByDepartmentCode($order = Criteria::ASC) Order by the department_code column
 * @method     ChildFct-irsInvoiceItemQuery orderByServiceFixedPrice($order = Criteria::ASC) Order by the service_fixed_price column
 * @method     ChildFct-irsInvoiceItemQuery orderByInvoiceId($order = Criteria::ASC) Order by the invoice_id column
 * @method     ChildFct-irsInvoiceItemQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFct-irsInvoiceItemQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFct-irsInvoiceItemQuery groupById() Group by the id column
 * @method     ChildFct-irsInvoiceItemQuery groupByServiceName() Group by the service_name column
 * @method     ChildFct-irsInvoiceItemQuery groupByServicePaidforName() Group by the service_paidfor_name column
 * @method     ChildFct-irsInvoiceItemQuery groupByServicePaidforCode() Group by the service_paidfor_code column
 * @method     ChildFct-irsInvoiceItemQuery groupByDepartmentName() Group by the department_name column
 * @method     ChildFct-irsInvoiceItemQuery groupByDepartmentCode() Group by the department_code column
 * @method     ChildFct-irsInvoiceItemQuery groupByServiceFixedPrice() Group by the service_fixed_price column
 * @method     ChildFct-irsInvoiceItemQuery groupByInvoiceId() Group by the invoice_id column
 * @method     ChildFct-irsInvoiceItemQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFct-irsInvoiceItemQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFct-irsInvoiceItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFct-irsInvoiceItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFct-irsInvoiceItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFct-irsInvoiceItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFct-irsInvoiceItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFct-irsInvoiceItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFct-irsInvoiceItemQuery leftJoinFct-irsInvoice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fct-irsInvoice relation
 * @method     ChildFct-irsInvoiceItemQuery rightJoinFct-irsInvoice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fct-irsInvoice relation
 * @method     ChildFct-irsInvoiceItemQuery innerJoinFct-irsInvoice($relationAlias = null) Adds a INNER JOIN clause to the query using the Fct-irsInvoice relation
 *
 * @method     ChildFct-irsInvoiceItemQuery joinWithFct-irsInvoice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fct-irsInvoice relation
 *
 * @method     ChildFct-irsInvoiceItemQuery leftJoinWithFct-irsInvoice() Adds a LEFT JOIN clause and with to the query using the Fct-irsInvoice relation
 * @method     ChildFct-irsInvoiceItemQuery rightJoinWithFct-irsInvoice() Adds a RIGHT JOIN clause and with to the query using the Fct-irsInvoice relation
 * @method     ChildFct-irsInvoiceItemQuery innerJoinWithFct-irsInvoice() Adds a INNER JOIN clause and with to the query using the Fct-irsInvoice relation
 *
 * @method     \Fct-irsInvoiceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFct-irsInvoiceItem findOne(ConnectionInterface $con = null) Return the first ChildFct-irsInvoiceItem matching the query
 * @method     ChildFct-irsInvoiceItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFct-irsInvoiceItem matching the query, or a new ChildFct-irsInvoiceItem object populated from the query conditions when no match is found
 *
 * @method     ChildFct-irsInvoiceItem findOneById(int $id) Return the first ChildFct-irsInvoiceItem filtered by the id column
 * @method     ChildFct-irsInvoiceItem findOneByServiceName(string $service_name) Return the first ChildFct-irsInvoiceItem filtered by the service_name column
 * @method     ChildFct-irsInvoiceItem findOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFct-irsInvoiceItem filtered by the service_paidfor_name column
 * @method     ChildFct-irsInvoiceItem findOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFct-irsInvoiceItem filtered by the service_paidfor_code column
 * @method     ChildFct-irsInvoiceItem findOneByDepartmentName(string $department_name) Return the first ChildFct-irsInvoiceItem filtered by the department_name column
 * @method     ChildFct-irsInvoiceItem findOneByDepartmentCode(int $department_code) Return the first ChildFct-irsInvoiceItem filtered by the department_code column
 * @method     ChildFct-irsInvoiceItem findOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFct-irsInvoiceItem filtered by the service_fixed_price column
 * @method     ChildFct-irsInvoiceItem findOneByInvoiceId(int $invoice_id) Return the first ChildFct-irsInvoiceItem filtered by the invoice_id column
 * @method     ChildFct-irsInvoiceItem findOneByCreatedAt(string $created_at) Return the first ChildFct-irsInvoiceItem filtered by the created_at column
 * @method     ChildFct-irsInvoiceItem findOneByUpdatedAt(string $updated_at) Return the first ChildFct-irsInvoiceItem filtered by the updated_at column *

 * @method     ChildFct-irsInvoiceItem requirePk($key, ConnectionInterface $con = null) Return the ChildFct-irsInvoiceItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOne(ConnectionInterface $con = null) Return the first ChildFct-irsInvoiceItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFct-irsInvoiceItem requireOneById(int $id) Return the first ChildFct-irsInvoiceItem filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByServiceName(string $service_name) Return the first ChildFct-irsInvoiceItem filtered by the service_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByServicePaidforName(string $service_paidfor_name) Return the first ChildFct-irsInvoiceItem filtered by the service_paidfor_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByServicePaidforCode(int $service_paidfor_code) Return the first ChildFct-irsInvoiceItem filtered by the service_paidfor_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByDepartmentName(string $department_name) Return the first ChildFct-irsInvoiceItem filtered by the department_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByDepartmentCode(int $department_code) Return the first ChildFct-irsInvoiceItem filtered by the department_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByServiceFixedPrice(string $service_fixed_price) Return the first ChildFct-irsInvoiceItem filtered by the service_fixed_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByInvoiceId(int $invoice_id) Return the first ChildFct-irsInvoiceItem filtered by the invoice_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByCreatedAt(string $created_at) Return the first ChildFct-irsInvoiceItem filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFct-irsInvoiceItem requireOneByUpdatedAt(string $updated_at) Return the first ChildFct-irsInvoiceItem filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFct-irsInvoiceItem objects based on current ModelCriteria
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findById(int $id) Return ChildFct-irsInvoiceItem objects filtered by the id column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByServiceName(string $service_name) Return ChildFct-irsInvoiceItem objects filtered by the service_name column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByServicePaidforName(string $service_paidfor_name) Return ChildFct-irsInvoiceItem objects filtered by the service_paidfor_name column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByServicePaidforCode(int $service_paidfor_code) Return ChildFct-irsInvoiceItem objects filtered by the service_paidfor_code column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByDepartmentName(string $department_name) Return ChildFct-irsInvoiceItem objects filtered by the department_name column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByDepartmentCode(int $department_code) Return ChildFct-irsInvoiceItem objects filtered by the department_code column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByServiceFixedPrice(string $service_fixed_price) Return ChildFct-irsInvoiceItem objects filtered by the service_fixed_price column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByInvoiceId(int $invoice_id) Return ChildFct-irsInvoiceItem objects filtered by the invoice_id column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFct-irsInvoiceItem objects filtered by the created_at column
 * @method     ChildFct-irsInvoiceItem[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFct-irsInvoiceItem objects filtered by the updated_at column
 * @method     ChildFct-irsInvoiceItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Fct-irsInvoiceItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\Fct-irsInvoiceItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Fct-irsInvoiceItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFct-irsInvoiceItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFct-irsInvoiceItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFct-irsInvoiceItemQuery) {
            return $criteria;
        }
        $query = new ChildFct-irsInvoiceItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFct-irsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Fct-irsInvoiceItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Fct-irsInvoiceItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFct-irsInvoiceItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, service_name, service_paidfor_name, service_paidfor_code, department_name, department_code, service_fixed_price, invoice_id, created_at, updated_at FROM fct-irs_invoice_item WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFct-irsInvoiceItem $obj */
            $obj = new ChildFct-irsInvoiceItem();
            $obj->hydrate($row);
            Fct-irsInvoiceItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFct-irsInvoiceItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the service_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceName('fooValue');   // WHERE service_name = 'fooValue'
     * $query->filterByServiceName('%fooValue%'); // WHERE service_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serviceName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceName($serviceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serviceName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $serviceName)) {
                $serviceName = str_replace('*', '%', $serviceName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_NAME, $serviceName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforName('fooValue');   // WHERE service_paidfor_name = 'fooValue'
     * $query->filterByServicePaidforName('%fooValue%'); // WHERE service_paidfor_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $servicePaidforName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforName($servicePaidforName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($servicePaidforName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $servicePaidforName)) {
                $servicePaidforName = str_replace('*', '%', $servicePaidforName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_NAME, $servicePaidforName, $comparison);
    }

    /**
     * Filter the query on the service_paidfor_code column
     *
     * Example usage:
     * <code>
     * $query->filterByServicePaidforCode(1234); // WHERE service_paidfor_code = 1234
     * $query->filterByServicePaidforCode(array(12, 34)); // WHERE service_paidfor_code IN (12, 34)
     * $query->filterByServicePaidforCode(array('min' => 12)); // WHERE service_paidfor_code > 12
     * </code>
     *
     * @param     mixed $servicePaidforCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServicePaidforCode($servicePaidforCode = null, $comparison = null)
    {
        if (is_array($servicePaidforCode)) {
            $useMinMax = false;
            if (isset($servicePaidforCode['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicePaidforCode['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_PAIDFOR_CODE, $servicePaidforCode, $comparison);
    }

    /**
     * Filter the query on the department_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentName('fooValue');   // WHERE department_name = 'fooValue'
     * $query->filterByDepartmentName('%fooValue%'); // WHERE department_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departmentName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentName($departmentName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departmentName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departmentName)) {
                $departmentName = str_replace('*', '%', $departmentName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_DEPARTMENT_NAME, $departmentName, $comparison);
    }

    /**
     * Filter the query on the department_code column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentCode(1234); // WHERE department_code = 1234
     * $query->filterByDepartmentCode(array(12, 34)); // WHERE department_code IN (12, 34)
     * $query->filterByDepartmentCode(array('min' => 12)); // WHERE department_code > 12
     * </code>
     *
     * @param     mixed $departmentCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByDepartmentCode($departmentCode = null, $comparison = null)
    {
        if (is_array($departmentCode)) {
            $useMinMax = false;
            if (isset($departmentCode['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($departmentCode['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_DEPARTMENT_CODE, $departmentCode, $comparison);
    }

    /**
     * Filter the query on the service_fixed_price column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceFixedPrice(1234); // WHERE service_fixed_price = 1234
     * $query->filterByServiceFixedPrice(array(12, 34)); // WHERE service_fixed_price IN (12, 34)
     * $query->filterByServiceFixedPrice(array('min' => 12)); // WHERE service_fixed_price > 12
     * </code>
     *
     * @param     mixed $serviceFixedPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByServiceFixedPrice($serviceFixedPrice = null, $comparison = null)
    {
        if (is_array($serviceFixedPrice)) {
            $useMinMax = false;
            if (isset($serviceFixedPrice['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceFixedPrice['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_SERVICE_FIXED_PRICE, $serviceFixedPrice, $comparison);
    }

    /**
     * Filter the query on the invoice_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceId(1234); // WHERE invoice_id = 1234
     * $query->filterByInvoiceId(array(12, 34)); // WHERE invoice_id IN (12, 34)
     * $query->filterByInvoiceId(array('min' => 12)); // WHERE invoice_id > 12
     * </code>
     *
     * @see       filterByFct-irsInvoice()
     *
     * @param     mixed $invoiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByInvoiceId($invoiceId = null, $comparison = null)
    {
        if (is_array($invoiceId)) {
            $useMinMax = false;
            if (isset($invoiceId['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceId['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_INVOICE_ID, $invoiceId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Fct-irsInvoice object
     *
     * @param \Fct-irsInvoice|ObjectCollection $fct-irsInvoice The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function filterByFct-irsInvoice($fct-irsInvoice, $comparison = null)
    {
        if ($fct-irsInvoice instanceof \Fct-irsInvoice) {
            return $this
                ->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_INVOICE_ID, $fct-irsInvoice->getId(), $comparison);
        } elseif ($fct-irsInvoice instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_INVOICE_ID, $fct-irsInvoice->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFct-irsInvoice() only accepts arguments of type \Fct-irsInvoice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fct-irsInvoice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function joinFct-irsInvoice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fct-irsInvoice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fct-irsInvoice');
        }

        return $this;
    }

    /**
     * Use the Fct-irsInvoice relation Fct-irsInvoice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Fct-irsInvoiceQuery A secondary query class using the current class as primary query
     */
    public function useFct-irsInvoiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFct-irsInvoice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fct-irsInvoice', '\Fct-irsInvoiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFct-irsInvoiceItem $fct-irsInvoiceItem Object to remove from the list of results
     *
     * @return $this|ChildFct-irsInvoiceItemQuery The current query, for fluid interface
     */
    public function prune($fct-irsInvoiceItem = null)
    {
        if ($fct-irsInvoiceItem) {
            $this->addUsingAlias(Fct-irsInvoiceItemTableMap::COL_ID, $fct-irsInvoiceItem->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fct-irs_invoice_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Fct-irsInvoiceItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Fct-irsInvoiceItemTableMap::clearInstancePool();
            Fct-irsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Fct-irsInvoiceItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Fct-irsInvoiceItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Fct-irsInvoiceItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Fct-irsInvoiceItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // Fct-irsInvoiceItemQuery
