<?php

namespace Base;

use \PaymentGlisten as ChildPaymentGlisten;
use \PaymentGlistenQuery as ChildPaymentGlistenQuery;
use \Exception;
use \PDO;
use Map\PaymentGlistenTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_glisten' table.
 *
 *
 *
 * @method     ChildPaymentGlistenQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentGlistenQuery orderByAccountNo($order = Criteria::ASC) Order by the account_no column
 * @method     ChildPaymentGlistenQuery orderByTransactionid($order = Criteria::ASC) Order by the TransactionID column
 * @method     ChildPaymentGlistenQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildPaymentGlistenQuery orderBySourcebank($order = Criteria::ASC) Order by the SourceBank column
 * @method     ChildPaymentGlistenQuery orderByDestinationbank($order = Criteria::ASC) Order by the DestinationBank column
 * @method     ChildPaymentGlistenQuery orderByEmail($order = Criteria::ASC) Order by the Email column
 * @method     ChildPaymentGlistenQuery orderByPhone($order = Criteria::ASC) Order by the Phone column
 * @method     ChildPaymentGlistenQuery orderByTransactiondate($order = Criteria::ASC) Order by the TransactionDate column
 * @method     ChildPaymentGlistenQuery orderByAmountPaid($order = Criteria::ASC) Order by the amount_paid column
 * @method     ChildPaymentGlistenQuery orderByPaymentchannel($order = Criteria::ASC) Order by the PaymentChannel column
 * @method     ChildPaymentGlistenQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildPaymentGlistenQuery orderByUploadstatus($order = Criteria::ASC) Order by the uploadstatus column
 *
 * @method     ChildPaymentGlistenQuery groupById() Group by the id column
 * @method     ChildPaymentGlistenQuery groupByAccountNo() Group by the account_no column
 * @method     ChildPaymentGlistenQuery groupByTransactionid() Group by the TransactionID column
 * @method     ChildPaymentGlistenQuery groupByProductId() Group by the product_id column
 * @method     ChildPaymentGlistenQuery groupBySourcebank() Group by the SourceBank column
 * @method     ChildPaymentGlistenQuery groupByDestinationbank() Group by the DestinationBank column
 * @method     ChildPaymentGlistenQuery groupByEmail() Group by the Email column
 * @method     ChildPaymentGlistenQuery groupByPhone() Group by the Phone column
 * @method     ChildPaymentGlistenQuery groupByTransactiondate() Group by the TransactionDate column
 * @method     ChildPaymentGlistenQuery groupByAmountPaid() Group by the amount_paid column
 * @method     ChildPaymentGlistenQuery groupByPaymentchannel() Group by the PaymentChannel column
 * @method     ChildPaymentGlistenQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildPaymentGlistenQuery groupByUploadstatus() Group by the uploadstatus column
 *
 * @method     ChildPaymentGlistenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentGlistenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentGlistenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentGlistenQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentGlistenQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentGlistenQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentGlisten findOne(ConnectionInterface $con = null) Return the first ChildPaymentGlisten matching the query
 * @method     ChildPaymentGlisten findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentGlisten matching the query, or a new ChildPaymentGlisten object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentGlisten findOneById(int $id) Return the first ChildPaymentGlisten filtered by the id column
 * @method     ChildPaymentGlisten findOneByAccountNo(string $account_no) Return the first ChildPaymentGlisten filtered by the account_no column
 * @method     ChildPaymentGlisten findOneByTransactionid(string $TransactionID) Return the first ChildPaymentGlisten filtered by the TransactionID column
 * @method     ChildPaymentGlisten findOneByProductId(int $product_id) Return the first ChildPaymentGlisten filtered by the product_id column
 * @method     ChildPaymentGlisten findOneBySourcebank(string $SourceBank) Return the first ChildPaymentGlisten filtered by the SourceBank column
 * @method     ChildPaymentGlisten findOneByDestinationbank(string $DestinationBank) Return the first ChildPaymentGlisten filtered by the DestinationBank column
 * @method     ChildPaymentGlisten findOneByEmail(string $Email) Return the first ChildPaymentGlisten filtered by the Email column
 * @method     ChildPaymentGlisten findOneByPhone(string $Phone) Return the first ChildPaymentGlisten filtered by the Phone column
 * @method     ChildPaymentGlisten findOneByTransactiondate(string $TransactionDate) Return the first ChildPaymentGlisten filtered by the TransactionDate column
 * @method     ChildPaymentGlisten findOneByAmountPaid(string $amount_paid) Return the first ChildPaymentGlisten filtered by the amount_paid column
 * @method     ChildPaymentGlisten findOneByPaymentchannel(string $PaymentChannel) Return the first ChildPaymentGlisten filtered by the PaymentChannel column
 * @method     ChildPaymentGlisten findOneByCreatedDate(string $created_date) Return the first ChildPaymentGlisten filtered by the created_date column
 * @method     ChildPaymentGlisten findOneByUploadstatus(int $uploadstatus) Return the first ChildPaymentGlisten filtered by the uploadstatus column *

 * @method     ChildPaymentGlisten requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentGlisten by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOne(ConnectionInterface $con = null) Return the first ChildPaymentGlisten matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentGlisten requireOneById(int $id) Return the first ChildPaymentGlisten filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByAccountNo(string $account_no) Return the first ChildPaymentGlisten filtered by the account_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByTransactionid(string $TransactionID) Return the first ChildPaymentGlisten filtered by the TransactionID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByProductId(int $product_id) Return the first ChildPaymentGlisten filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneBySourcebank(string $SourceBank) Return the first ChildPaymentGlisten filtered by the SourceBank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByDestinationbank(string $DestinationBank) Return the first ChildPaymentGlisten filtered by the DestinationBank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByEmail(string $Email) Return the first ChildPaymentGlisten filtered by the Email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByPhone(string $Phone) Return the first ChildPaymentGlisten filtered by the Phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByTransactiondate(string $TransactionDate) Return the first ChildPaymentGlisten filtered by the TransactionDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByAmountPaid(string $amount_paid) Return the first ChildPaymentGlisten filtered by the amount_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByPaymentchannel(string $PaymentChannel) Return the first ChildPaymentGlisten filtered by the PaymentChannel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByCreatedDate(string $created_date) Return the first ChildPaymentGlisten filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentGlisten requireOneByUploadstatus(int $uploadstatus) Return the first ChildPaymentGlisten filtered by the uploadstatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentGlisten[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentGlisten objects based on current ModelCriteria
 * @method     ChildPaymentGlisten[]|ObjectCollection findById(int $id) Return ChildPaymentGlisten objects filtered by the id column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByAccountNo(string $account_no) Return ChildPaymentGlisten objects filtered by the account_no column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByTransactionid(string $TransactionID) Return ChildPaymentGlisten objects filtered by the TransactionID column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByProductId(int $product_id) Return ChildPaymentGlisten objects filtered by the product_id column
 * @method     ChildPaymentGlisten[]|ObjectCollection findBySourcebank(string $SourceBank) Return ChildPaymentGlisten objects filtered by the SourceBank column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByDestinationbank(string $DestinationBank) Return ChildPaymentGlisten objects filtered by the DestinationBank column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByEmail(string $Email) Return ChildPaymentGlisten objects filtered by the Email column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByPhone(string $Phone) Return ChildPaymentGlisten objects filtered by the Phone column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByTransactiondate(string $TransactionDate) Return ChildPaymentGlisten objects filtered by the TransactionDate column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByAmountPaid(string $amount_paid) Return ChildPaymentGlisten objects filtered by the amount_paid column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByPaymentchannel(string $PaymentChannel) Return ChildPaymentGlisten objects filtered by the PaymentChannel column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildPaymentGlisten objects filtered by the created_date column
 * @method     ChildPaymentGlisten[]|ObjectCollection findByUploadstatus(int $uploadstatus) Return ChildPaymentGlisten objects filtered by the uploadstatus column
 * @method     ChildPaymentGlisten[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentGlistenQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaymentGlistenQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PaymentGlisten', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentGlistenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentGlistenQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentGlistenQuery) {
            return $criteria;
        }
        $query = new ChildPaymentGlistenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentGlisten|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentGlistenTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentGlistenTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentGlisten A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, account_no, TransactionID, product_id, SourceBank, DestinationBank, Email, Phone, TransactionDate, amount_paid, PaymentChannel, created_date, uploadstatus FROM payment_glisten WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentGlisten $obj */
            $obj = new ChildPaymentGlisten();
            $obj->hydrate($row);
            PaymentGlistenTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentGlisten|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the account_no column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountNo('fooValue');   // WHERE account_no = 'fooValue'
     * $query->filterByAccountNo('%fooValue%'); // WHERE account_no LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByAccountNo($accountNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accountNo)) {
                $accountNo = str_replace('*', '%', $accountNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_ACCOUNT_NO, $accountNo, $comparison);
    }

    /**
     * Filter the query on the TransactionID column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionid('fooValue');   // WHERE TransactionID = 'fooValue'
     * $query->filterByTransactionid('%fooValue%'); // WHERE TransactionID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByTransactionid($transactionid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $transactionid)) {
                $transactionid = str_replace('*', '%', $transactionid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_TRANSACTIONID, $transactionid, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE product_id = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE product_id IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE product_id > 12
     * </code>
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the SourceBank column
     *
     * Example usage:
     * <code>
     * $query->filterBySourcebank('fooValue');   // WHERE SourceBank = 'fooValue'
     * $query->filterBySourcebank('%fooValue%'); // WHERE SourceBank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourcebank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterBySourcebank($sourcebank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourcebank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sourcebank)) {
                $sourcebank = str_replace('*', '%', $sourcebank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_SOURCEBANK, $sourcebank, $comparison);
    }

    /**
     * Filter the query on the DestinationBank column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationbank('fooValue');   // WHERE DestinationBank = 'fooValue'
     * $query->filterByDestinationbank('%fooValue%'); // WHERE DestinationBank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationbank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByDestinationbank($destinationbank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationbank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationbank)) {
                $destinationbank = str_replace('*', '%', $destinationbank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_DESTINATIONBANK, $destinationbank, $comparison);
    }

    /**
     * Filter the query on the Email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE Email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE Email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the Phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE Phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE Phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the TransactionDate column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactiondate('2011-03-14'); // WHERE TransactionDate = '2011-03-14'
     * $query->filterByTransactiondate('now'); // WHERE TransactionDate = '2011-03-14'
     * $query->filterByTransactiondate(array('max' => 'yesterday')); // WHERE TransactionDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $transactiondate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByTransactiondate($transactiondate = null, $comparison = null)
    {
        if (is_array($transactiondate)) {
            $useMinMax = false;
            if (isset($transactiondate['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_TRANSACTIONDATE, $transactiondate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($transactiondate['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_TRANSACTIONDATE, $transactiondate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_TRANSACTIONDATE, $transactiondate, $comparison);
    }

    /**
     * Filter the query on the amount_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountPaid(1234); // WHERE amount_paid = 1234
     * $query->filterByAmountPaid(array(12, 34)); // WHERE amount_paid IN (12, 34)
     * $query->filterByAmountPaid(array('min' => 12)); // WHERE amount_paid > 12
     * </code>
     *
     * @param     mixed $amountPaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByAmountPaid($amountPaid = null, $comparison = null)
    {
        if (is_array($amountPaid)) {
            $useMinMax = false;
            if (isset($amountPaid['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_AMOUNT_PAID, $amountPaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountPaid['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_AMOUNT_PAID, $amountPaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_AMOUNT_PAID, $amountPaid, $comparison);
    }

    /**
     * Filter the query on the PaymentChannel column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentchannel('fooValue');   // WHERE PaymentChannel = 'fooValue'
     * $query->filterByPaymentchannel('%fooValue%'); // WHERE PaymentChannel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentchannel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByPaymentchannel($paymentchannel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentchannel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentchannel)) {
                $paymentchannel = str_replace('*', '%', $paymentchannel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_PAYMENTCHANNEL, $paymentchannel, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the uploadstatus column
     *
     * Example usage:
     * <code>
     * $query->filterByUploadstatus(1234); // WHERE uploadstatus = 1234
     * $query->filterByUploadstatus(array(12, 34)); // WHERE uploadstatus IN (12, 34)
     * $query->filterByUploadstatus(array('min' => 12)); // WHERE uploadstatus > 12
     * </code>
     *
     * @param     mixed $uploadstatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function filterByUploadstatus($uploadstatus = null, $comparison = null)
    {
        if (is_array($uploadstatus)) {
            $useMinMax = false;
            if (isset($uploadstatus['min'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_UPLOADSTATUS, $uploadstatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uploadstatus['max'])) {
                $this->addUsingAlias(PaymentGlistenTableMap::COL_UPLOADSTATUS, $uploadstatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentGlistenTableMap::COL_UPLOADSTATUS, $uploadstatus, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentGlisten $paymentGlisten Object to remove from the list of results
     *
     * @return $this|ChildPaymentGlistenQuery The current query, for fluid interface
     */
    public function prune($paymentGlisten = null)
    {
        if ($paymentGlisten) {
            $this->addUsingAlias(PaymentGlistenTableMap::COL_ID, $paymentGlisten->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_glisten table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentGlistenTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentGlistenTableMap::clearInstancePool();
            PaymentGlistenTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentGlistenTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentGlistenTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentGlistenTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentGlistenTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentGlistenQuery
