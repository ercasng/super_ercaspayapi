<?php

use Base\KluBillDolceQuery as BaseKluBillDolceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'klu_bill_dolce' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluBillDolceQuery extends BaseKluBillDolceQuery
{

}
