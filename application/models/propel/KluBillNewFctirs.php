<?php

use Base\KluBillNewFctirs as BaseKluBillNewFctirs;

/**
 * Skeleton subclass for representing a row from the 'klu_bill_new_fctirs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KluBillNewFctirs extends BaseKluBillNewFctirs
{

}
