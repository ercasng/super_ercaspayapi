<?php

use Base\ErcasBillFct-irsQuery as BaseErcasBillFct-irsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'ercas_bill_fct-irs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ErcasBillFct-irsQuery extends BaseErcasBillFct-irsQuery
{

}
