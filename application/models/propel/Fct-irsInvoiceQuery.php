<?php

use Base\Fct-irsInvoiceQuery as BaseFct-irsInvoiceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'fct-irs_invoice' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Fct-irsInvoiceQuery extends BaseFct-irsInvoiceQuery
{

}
