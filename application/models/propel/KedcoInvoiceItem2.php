<?php

use Base\KedcoInvoiceItem2 as BaseKedcoInvoiceItem2;

/**
 * Skeleton subclass for representing a row from the 'kedco_invoice_item2' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KedcoInvoiceItem2 extends BaseKedcoInvoiceItem2
{

}
