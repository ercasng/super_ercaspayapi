<?php

namespace Map;

use \Customer1Fct-irs;
use \Customer1Fct-irsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'customer1_fct-irs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class Customer1Fct-irsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.Customer1Fct-irsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'customer1_fct-irs';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Customer1Fct-irs';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Customer1Fct-irs';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'customer1_fct-irs.id';

    /**
     * the column name for the passw field
     */
    const COL_PASSW = 'customer1_fct-irs.passw';

    /**
     * the column name for the account_no field
     */
    const COL_ACCOUNT_NO = 'customer1_fct-irs.account_no';

    /**
     * the column name for the customer_name field
     */
    const COL_CUSTOMER_NAME = 'customer1_fct-irs.customer_name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'customer1_fct-irs.email';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'customer1_fct-irs.phone';

    /**
     * the column name for the date_added field
     */
    const COL_DATE_ADDED = 'customer1_fct-irs.date_added';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'customer1_fct-irs.last_login';

    /**
     * the column name for the key field
     */
    const COL_KEY = 'customer1_fct-irs.key';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Passw', 'AccountNo', 'CustomerName', 'Email', 'Phone', 'DateAdded', 'LastLogin', 'Key', ),
        self::TYPE_CAMELNAME     => array('id', 'passw', 'accountNo', 'customerName', 'email', 'phone', 'dateAdded', 'lastLogin', 'key', ),
        self::TYPE_COLNAME       => array(Customer1Fct-irsTableMap::COL_ID, Customer1Fct-irsTableMap::COL_PASSW, Customer1Fct-irsTableMap::COL_ACCOUNT_NO, Customer1Fct-irsTableMap::COL_CUSTOMER_NAME, Customer1Fct-irsTableMap::COL_EMAIL, Customer1Fct-irsTableMap::COL_PHONE, Customer1Fct-irsTableMap::COL_DATE_ADDED, Customer1Fct-irsTableMap::COL_LAST_LOGIN, Customer1Fct-irsTableMap::COL_KEY, ),
        self::TYPE_FIELDNAME     => array('id', 'passw', 'account_no', 'customer_name', 'email', 'phone', 'date_added', 'last_login', 'key', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Passw' => 1, 'AccountNo' => 2, 'CustomerName' => 3, 'Email' => 4, 'Phone' => 5, 'DateAdded' => 6, 'LastLogin' => 7, 'Key' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'passw' => 1, 'accountNo' => 2, 'customerName' => 3, 'email' => 4, 'phone' => 5, 'dateAdded' => 6, 'lastLogin' => 7, 'key' => 8, ),
        self::TYPE_COLNAME       => array(Customer1Fct-irsTableMap::COL_ID => 0, Customer1Fct-irsTableMap::COL_PASSW => 1, Customer1Fct-irsTableMap::COL_ACCOUNT_NO => 2, Customer1Fct-irsTableMap::COL_CUSTOMER_NAME => 3, Customer1Fct-irsTableMap::COL_EMAIL => 4, Customer1Fct-irsTableMap::COL_PHONE => 5, Customer1Fct-irsTableMap::COL_DATE_ADDED => 6, Customer1Fct-irsTableMap::COL_LAST_LOGIN => 7, Customer1Fct-irsTableMap::COL_KEY => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'passw' => 1, 'account_no' => 2, 'customer_name' => 3, 'email' => 4, 'phone' => 5, 'date_added' => 6, 'last_login' => 7, 'key' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('customer1_fct-irs');
        $this->setPhpName('Customer1Fct-irs');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Customer1Fct-irs');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('passw', 'Passw', 'VARCHAR', false, 200, null);
        $this->addColumn('account_no', 'AccountNo', 'VARCHAR', true, 50, null);
        $this->addColumn('customer_name', 'CustomerName', 'VARCHAR', true, 150, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', true, 15, null);
        $this->addColumn('date_added', 'DateAdded', 'TIMESTAMP', false, null, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', false, null, null);
        $this->addColumn('key', 'Key', 'VARCHAR', false, 6, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Customer1Fct-irsTableMap::CLASS_DEFAULT : Customer1Fct-irsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Customer1Fct-irs object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Customer1Fct-irsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Customer1Fct-irsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Customer1Fct-irsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Customer1Fct-irsTableMap::OM_CLASS;
            /** @var Customer1Fct-irs $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Customer1Fct-irsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Customer1Fct-irsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Customer1Fct-irsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Customer1Fct-irs $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Customer1Fct-irsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_ID);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_PASSW);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_ACCOUNT_NO);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_CUSTOMER_NAME);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_EMAIL);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_PHONE);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_DATE_ADDED);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(Customer1Fct-irsTableMap::COL_KEY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.passw');
            $criteria->addSelectColumn($alias . '.account_no');
            $criteria->addSelectColumn($alias . '.customer_name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.date_added');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.key');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Customer1Fct-irsTableMap::DATABASE_NAME)->getTable(Customer1Fct-irsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Customer1Fct-irsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Customer1Fct-irsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Customer1Fct-irsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Customer1Fct-irs or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Customer1Fct-irs object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Customer1Fct-irsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Customer1Fct-irs) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Customer1Fct-irsTableMap::DATABASE_NAME);
            $criteria->add(Customer1Fct-irsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Customer1Fct-irsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Customer1Fct-irsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Customer1Fct-irsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the customer1_fct-irs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Customer1Fct-irsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Customer1Fct-irs or Criteria object.
     *
     * @param mixed               $criteria Criteria or Customer1Fct-irs object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Customer1Fct-irsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Customer1Fct-irs object
        }

        if ($criteria->containsKey(Customer1Fct-irsTableMap::COL_ID) && $criteria->keyContainsValue(Customer1Fct-irsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Customer1Fct-irsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Customer1Fct-irsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Customer1Fct-irsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Customer1Fct-irsTableMap::buildTableMap();
