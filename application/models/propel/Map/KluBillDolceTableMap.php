<?php

namespace Map;

use \KluBillDolce;
use \KluBillDolceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'klu_bill_dolce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class KluBillDolceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.KluBillDolceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'klu_bill_dolce';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\KluBillDolce';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'KluBillDolce';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id field
     */
    const COL_ID = 'klu_bill_dolce.id';

    /**
     * the column name for the ref_no field
     */
    const COL_REF_NO = 'klu_bill_dolce.ref_no';

    /**
     * the column name for the UniqueKeyID field
     */
    const COL_UNIQUEKEYID = 'klu_bill_dolce.UniqueKeyID';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'klu_bill_dolce.customer_id';

    /**
     * the column name for the account_no field
     */
    const COL_ACCOUNT_NO = 'klu_bill_dolce.account_no';

    /**
     * the column name for the service_type field
     */
    const COL_SERVICE_TYPE = 'klu_bill_dolce.service_type';

    /**
     * the column name for the due_date field
     */
    const COL_DUE_DATE = 'klu_bill_dolce.due_date';

    /**
     * the column name for the billing_from field
     */
    const COL_BILLING_FROM = 'klu_bill_dolce.billing_from';

    /**
     * the column name for the billing_to field
     */
    const COL_BILLING_TO = 'klu_bill_dolce.billing_to';

    /**
     * the column name for the bill_date field
     */
    const COL_BILL_DATE = 'klu_bill_dolce.bill_date';

    /**
     * the column name for the billing_cycle field
     */
    const COL_BILLING_CYCLE = 'klu_bill_dolce.billing_cycle';

    /**
     * the column name for the RoutineCharges field
     */
    const COL_ROUTINECHARGES = 'klu_bill_dolce.RoutineCharges';

    /**
     * the column name for the current_charge field
     */
    const COL_CURRENT_CHARGE = 'klu_bill_dolce.current_charge';

    /**
     * the column name for the service_charge field
     */
    const COL_SERVICE_CHARGE = 'klu_bill_dolce.service_charge';

    /**
     * the column name for the total_due field
     */
    const COL_TOTAL_DUE = 'klu_bill_dolce.total_due';

    /**
     * the column name for the date_created field
     */
    const COL_DATE_CREATED = 'klu_bill_dolce.date_created';

    /**
     * the column name for the creator field
     */
    const COL_CREATOR = 'klu_bill_dolce.creator';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'klu_bill_dolce.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'RefNo', 'Uniquekeyid', 'CustomerId', 'AccountNo', 'ServiceType', 'DueDate', 'BillingFrom', 'BillingTo', 'BillDate', 'BillingCycle', 'Routinecharges', 'CurrentCharge', 'ServiceCharge', 'TotalDue', 'DateCreated', 'Creator', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'refNo', 'uniquekeyid', 'customerId', 'accountNo', 'serviceType', 'dueDate', 'billingFrom', 'billingTo', 'billDate', 'billingCycle', 'routinecharges', 'currentCharge', 'serviceCharge', 'totalDue', 'dateCreated', 'creator', 'status', ),
        self::TYPE_COLNAME       => array(KluBillDolceTableMap::COL_ID, KluBillDolceTableMap::COL_REF_NO, KluBillDolceTableMap::COL_UNIQUEKEYID, KluBillDolceTableMap::COL_CUSTOMER_ID, KluBillDolceTableMap::COL_ACCOUNT_NO, KluBillDolceTableMap::COL_SERVICE_TYPE, KluBillDolceTableMap::COL_DUE_DATE, KluBillDolceTableMap::COL_BILLING_FROM, KluBillDolceTableMap::COL_BILLING_TO, KluBillDolceTableMap::COL_BILL_DATE, KluBillDolceTableMap::COL_BILLING_CYCLE, KluBillDolceTableMap::COL_ROUTINECHARGES, KluBillDolceTableMap::COL_CURRENT_CHARGE, KluBillDolceTableMap::COL_SERVICE_CHARGE, KluBillDolceTableMap::COL_TOTAL_DUE, KluBillDolceTableMap::COL_DATE_CREATED, KluBillDolceTableMap::COL_CREATOR, KluBillDolceTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'ref_no', 'UniqueKeyID', 'customer_id', 'account_no', 'service_type', 'due_date', 'billing_from', 'billing_to', 'bill_date', 'billing_cycle', 'RoutineCharges', 'current_charge', 'service_charge', 'total_due', 'date_created', 'creator', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'RefNo' => 1, 'Uniquekeyid' => 2, 'CustomerId' => 3, 'AccountNo' => 4, 'ServiceType' => 5, 'DueDate' => 6, 'BillingFrom' => 7, 'BillingTo' => 8, 'BillDate' => 9, 'BillingCycle' => 10, 'Routinecharges' => 11, 'CurrentCharge' => 12, 'ServiceCharge' => 13, 'TotalDue' => 14, 'DateCreated' => 15, 'Creator' => 16, 'Status' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'refNo' => 1, 'uniquekeyid' => 2, 'customerId' => 3, 'accountNo' => 4, 'serviceType' => 5, 'dueDate' => 6, 'billingFrom' => 7, 'billingTo' => 8, 'billDate' => 9, 'billingCycle' => 10, 'routinecharges' => 11, 'currentCharge' => 12, 'serviceCharge' => 13, 'totalDue' => 14, 'dateCreated' => 15, 'creator' => 16, 'status' => 17, ),
        self::TYPE_COLNAME       => array(KluBillDolceTableMap::COL_ID => 0, KluBillDolceTableMap::COL_REF_NO => 1, KluBillDolceTableMap::COL_UNIQUEKEYID => 2, KluBillDolceTableMap::COL_CUSTOMER_ID => 3, KluBillDolceTableMap::COL_ACCOUNT_NO => 4, KluBillDolceTableMap::COL_SERVICE_TYPE => 5, KluBillDolceTableMap::COL_DUE_DATE => 6, KluBillDolceTableMap::COL_BILLING_FROM => 7, KluBillDolceTableMap::COL_BILLING_TO => 8, KluBillDolceTableMap::COL_BILL_DATE => 9, KluBillDolceTableMap::COL_BILLING_CYCLE => 10, KluBillDolceTableMap::COL_ROUTINECHARGES => 11, KluBillDolceTableMap::COL_CURRENT_CHARGE => 12, KluBillDolceTableMap::COL_SERVICE_CHARGE => 13, KluBillDolceTableMap::COL_TOTAL_DUE => 14, KluBillDolceTableMap::COL_DATE_CREATED => 15, KluBillDolceTableMap::COL_CREATOR => 16, KluBillDolceTableMap::COL_STATUS => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'ref_no' => 1, 'UniqueKeyID' => 2, 'customer_id' => 3, 'account_no' => 4, 'service_type' => 5, 'due_date' => 6, 'billing_from' => 7, 'billing_to' => 8, 'bill_date' => 9, 'billing_cycle' => 10, 'RoutineCharges' => 11, 'current_charge' => 12, 'service_charge' => 13, 'total_due' => 14, 'date_created' => 15, 'creator' => 16, 'status' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('klu_bill_dolce');
        $this->setPhpName('KluBillDolce');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\KluBillDolce');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 20, null);
        $this->addColumn('ref_no', 'RefNo', 'VARCHAR', true, 50, null);
        $this->addColumn('UniqueKeyID', 'Uniquekeyid', 'INTEGER', true, 50, null);
        $this->addColumn('customer_id', 'CustomerId', 'INTEGER', true, null, null);
        $this->addColumn('account_no', 'AccountNo', 'VARCHAR', true, 50, null);
        $this->addColumn('service_type', 'ServiceType', 'VARCHAR', true, 50, null);
        $this->addColumn('due_date', 'DueDate', 'VARCHAR', true, 50, null);
        $this->addColumn('billing_from', 'BillingFrom', 'VARCHAR', true, 50, null);
        $this->addColumn('billing_to', 'BillingTo', 'VARCHAR', true, 50, null);
        $this->addColumn('bill_date', 'BillDate', 'DATE', true, null, null);
        $this->addColumn('billing_cycle', 'BillingCycle', 'VARCHAR', true, 50, null);
        $this->addColumn('RoutineCharges', 'Routinecharges', 'DECIMAL', true, 20, null);
        $this->addColumn('current_charge', 'CurrentCharge', 'DECIMAL', true, 11, null);
        $this->addColumn('service_charge', 'ServiceCharge', 'DECIMAL', true, 11, null);
        $this->addColumn('total_due', 'TotalDue', 'DECIMAL', true, 20, null);
        $this->addColumn('date_created', 'DateCreated', 'TIMESTAMP', true, null, null);
        $this->addColumn('creator', 'Creator', 'VARCHAR', true, 50, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', true, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? KluBillDolceTableMap::CLASS_DEFAULT : KluBillDolceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (KluBillDolce object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = KluBillDolceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = KluBillDolceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + KluBillDolceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = KluBillDolceTableMap::OM_CLASS;
            /** @var KluBillDolce $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            KluBillDolceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = KluBillDolceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = KluBillDolceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var KluBillDolce $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                KluBillDolceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_ID);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_REF_NO);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_UNIQUEKEYID);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_ACCOUNT_NO);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_SERVICE_TYPE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_DUE_DATE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_BILLING_FROM);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_BILLING_TO);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_BILL_DATE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_BILLING_CYCLE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_ROUTINECHARGES);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_CURRENT_CHARGE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_SERVICE_CHARGE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_TOTAL_DUE);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_DATE_CREATED);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_CREATOR);
            $criteria->addSelectColumn(KluBillDolceTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ref_no');
            $criteria->addSelectColumn($alias . '.UniqueKeyID');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.account_no');
            $criteria->addSelectColumn($alias . '.service_type');
            $criteria->addSelectColumn($alias . '.due_date');
            $criteria->addSelectColumn($alias . '.billing_from');
            $criteria->addSelectColumn($alias . '.billing_to');
            $criteria->addSelectColumn($alias . '.bill_date');
            $criteria->addSelectColumn($alias . '.billing_cycle');
            $criteria->addSelectColumn($alias . '.RoutineCharges');
            $criteria->addSelectColumn($alias . '.current_charge');
            $criteria->addSelectColumn($alias . '.service_charge');
            $criteria->addSelectColumn($alias . '.total_due');
            $criteria->addSelectColumn($alias . '.date_created');
            $criteria->addSelectColumn($alias . '.creator');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(KluBillDolceTableMap::DATABASE_NAME)->getTable(KluBillDolceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(KluBillDolceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(KluBillDolceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new KluBillDolceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a KluBillDolce or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or KluBillDolce object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluBillDolceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \KluBillDolce) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(KluBillDolceTableMap::DATABASE_NAME);
            $criteria->add(KluBillDolceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = KluBillDolceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            KluBillDolceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                KluBillDolceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the klu_bill_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return KluBillDolceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a KluBillDolce or Criteria object.
     *
     * @param mixed               $criteria Criteria or KluBillDolce object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluBillDolceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from KluBillDolce object
        }

        if ($criteria->containsKey(KluBillDolceTableMap::COL_ID) && $criteria->keyContainsValue(KluBillDolceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.KluBillDolceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = KluBillDolceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // KluBillDolceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
KluBillDolceTableMap::buildTableMap();
