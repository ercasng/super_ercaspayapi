<?php

namespace Map;

use \PaymentFctirs;
use \PaymentFctirsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'payment_fctirs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaymentFctirsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaymentFctirsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'payment_fctirs';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PaymentFctirs';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PaymentFctirs';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 24;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 24;

    /**
     * the column name for the id field
     */
    const COL_ID = 'payment_fctirs.id';

    /**
     * the column name for the session_id field
     */
    const COL_SESSION_ID = 'payment_fctirs.session_id';

    /**
     * the column name for the date field
     */
    const COL_DATE = 'payment_fctirs.date';

    /**
     * the column name for the date_time field
     */
    const COL_DATE_TIME = 'payment_fctirs.date_time';

    /**
     * the column name for the existing_tin field
     */
    const COL_EXISTING_TIN = 'payment_fctirs.existing_tin';

    /**
     * the column name for the jtb_tin field
     */
    const COL_JTB_TIN = 'payment_fctirs.jtb_tin';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'payment_fctirs.phone';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'payment_fctirs.name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'payment_fctirs.email';

    /**
     * the column name for the amount_paid field
     */
    const COL_AMOUNT_PAID = 'payment_fctirs.amount_paid';

    /**
     * the column name for the address field
     */
    const COL_ADDRESS = 'payment_fctirs.address';

    /**
     * the column name for the tax field
     */
    const COL_TAX = 'payment_fctirs.tax';

    /**
     * the column name for the month field
     */
    const COL_MONTH = 'payment_fctirs.month';

    /**
     * the column name for the year field
     */
    const COL_YEAR = 'payment_fctirs.year';

    /**
     * the column name for the category field
     */
    const COL_CATEGORY = 'payment_fctirs.category';

    /**
     * the column name for the sector field
     */
    const COL_SECTOR = 'payment_fctirs.sector';

    /**
     * the column name for the source_bank field
     */
    const COL_SOURCE_BANK = 'payment_fctirs.source_bank';

    /**
     * the column name for the destination_bank field
     */
    const COL_DESTINATION_BANK = 'payment_fctirs.destination_bank';

    /**
     * the column name for the destination_account_number field
     */
    const COL_DESTINATION_ACCOUNT_NUMBER = 'payment_fctirs.destination_account_number';

    /**
     * the column name for the destination_account_name field
     */
    const COL_DESTINATION_ACCOUNT_NAME = 'payment_fctirs.destination_account_name';

    /**
     * the column name for the invoice_number field
     */
    const COL_INVOICE_NUMBER = 'payment_fctirs.invoice_number';

    /**
     * the column name for the payment_channel field
     */
    const COL_PAYMENT_CHANNEL = 'payment_fctirs.payment_channel';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'payment_fctirs.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'payment_fctirs.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SessionId', 'Date', 'DateTime', 'ExistingTin', 'JtbTin', 'Phone', 'Name', 'Email', 'AmountPaid', 'Address', 'Tax', 'Month', 'Year', 'Category', 'Sector', 'SourceBank', 'DestinationBank', 'DestinationAccountNumber', 'DestinationAccountName', 'InvoiceNumber', 'PaymentChannel', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'sessionId', 'date', 'dateTime', 'existingTin', 'jtbTin', 'phone', 'name', 'email', 'amountPaid', 'address', 'tax', 'month', 'year', 'category', 'sector', 'sourceBank', 'destinationBank', 'destinationAccountNumber', 'destinationAccountName', 'invoiceNumber', 'paymentChannel', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PaymentFctirsTableMap::COL_ID, PaymentFctirsTableMap::COL_SESSION_ID, PaymentFctirsTableMap::COL_DATE, PaymentFctirsTableMap::COL_DATE_TIME, PaymentFctirsTableMap::COL_EXISTING_TIN, PaymentFctirsTableMap::COL_JTB_TIN, PaymentFctirsTableMap::COL_PHONE, PaymentFctirsTableMap::COL_NAME, PaymentFctirsTableMap::COL_EMAIL, PaymentFctirsTableMap::COL_AMOUNT_PAID, PaymentFctirsTableMap::COL_ADDRESS, PaymentFctirsTableMap::COL_TAX, PaymentFctirsTableMap::COL_MONTH, PaymentFctirsTableMap::COL_YEAR, PaymentFctirsTableMap::COL_CATEGORY, PaymentFctirsTableMap::COL_SECTOR, PaymentFctirsTableMap::COL_SOURCE_BANK, PaymentFctirsTableMap::COL_DESTINATION_BANK, PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NUMBER, PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NAME, PaymentFctirsTableMap::COL_INVOICE_NUMBER, PaymentFctirsTableMap::COL_PAYMENT_CHANNEL, PaymentFctirsTableMap::COL_CREATED_AT, PaymentFctirsTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'session_id', 'date', 'date_time', 'existing_tin', 'jtb_tin', 'phone', 'name', 'email', 'amount_paid', 'address', 'tax', 'month', 'year', 'category', 'sector', 'source_bank', 'destination_bank', 'destination_account_number', 'destination_account_name', 'invoice_number', 'payment_channel', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SessionId' => 1, 'Date' => 2, 'DateTime' => 3, 'ExistingTin' => 4, 'JtbTin' => 5, 'Phone' => 6, 'Name' => 7, 'Email' => 8, 'AmountPaid' => 9, 'Address' => 10, 'Tax' => 11, 'Month' => 12, 'Year' => 13, 'Category' => 14, 'Sector' => 15, 'SourceBank' => 16, 'DestinationBank' => 17, 'DestinationAccountNumber' => 18, 'DestinationAccountName' => 19, 'InvoiceNumber' => 20, 'PaymentChannel' => 21, 'CreatedAt' => 22, 'UpdatedAt' => 23, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'sessionId' => 1, 'date' => 2, 'dateTime' => 3, 'existingTin' => 4, 'jtbTin' => 5, 'phone' => 6, 'name' => 7, 'email' => 8, 'amountPaid' => 9, 'address' => 10, 'tax' => 11, 'month' => 12, 'year' => 13, 'category' => 14, 'sector' => 15, 'sourceBank' => 16, 'destinationBank' => 17, 'destinationAccountNumber' => 18, 'destinationAccountName' => 19, 'invoiceNumber' => 20, 'paymentChannel' => 21, 'createdAt' => 22, 'updatedAt' => 23, ),
        self::TYPE_COLNAME       => array(PaymentFctirsTableMap::COL_ID => 0, PaymentFctirsTableMap::COL_SESSION_ID => 1, PaymentFctirsTableMap::COL_DATE => 2, PaymentFctirsTableMap::COL_DATE_TIME => 3, PaymentFctirsTableMap::COL_EXISTING_TIN => 4, PaymentFctirsTableMap::COL_JTB_TIN => 5, PaymentFctirsTableMap::COL_PHONE => 6, PaymentFctirsTableMap::COL_NAME => 7, PaymentFctirsTableMap::COL_EMAIL => 8, PaymentFctirsTableMap::COL_AMOUNT_PAID => 9, PaymentFctirsTableMap::COL_ADDRESS => 10, PaymentFctirsTableMap::COL_TAX => 11, PaymentFctirsTableMap::COL_MONTH => 12, PaymentFctirsTableMap::COL_YEAR => 13, PaymentFctirsTableMap::COL_CATEGORY => 14, PaymentFctirsTableMap::COL_SECTOR => 15, PaymentFctirsTableMap::COL_SOURCE_BANK => 16, PaymentFctirsTableMap::COL_DESTINATION_BANK => 17, PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NUMBER => 18, PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NAME => 19, PaymentFctirsTableMap::COL_INVOICE_NUMBER => 20, PaymentFctirsTableMap::COL_PAYMENT_CHANNEL => 21, PaymentFctirsTableMap::COL_CREATED_AT => 22, PaymentFctirsTableMap::COL_UPDATED_AT => 23, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'session_id' => 1, 'date' => 2, 'date_time' => 3, 'existing_tin' => 4, 'jtb_tin' => 5, 'phone' => 6, 'name' => 7, 'email' => 8, 'amount_paid' => 9, 'address' => 10, 'tax' => 11, 'month' => 12, 'year' => 13, 'category' => 14, 'sector' => 15, 'source_bank' => 16, 'destination_bank' => 17, 'destination_account_number' => 18, 'destination_account_name' => 19, 'invoice_number' => 20, 'payment_channel' => 21, 'created_at' => 22, 'updated_at' => 23, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('payment_fctirs');
        $this->setPhpName('PaymentFctirs');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PaymentFctirs');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('session_id', 'SessionId', 'VARCHAR', true, 255, null);
        $this->addColumn('date', 'Date', 'DATE', true, null, null);
        $this->addColumn('date_time', 'DateTime', 'TIMESTAMP', true, null, null);
        $this->addColumn('existing_tin', 'ExistingTin', 'VARCHAR', true, 50, null);
        $this->addColumn('jtb_tin', 'JtbTin', 'VARCHAR', true, 50, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', true, 15, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('amount_paid', 'AmountPaid', 'DECIMAL', true, 20, null);
        $this->addColumn('address', 'Address', 'VARCHAR', true, 255, null);
        $this->addColumn('tax', 'Tax', 'VARCHAR', true, 255, null);
        $this->addColumn('month', 'Month', 'VARCHAR', true, 255, null);
        $this->addColumn('year', 'Year', 'VARCHAR', true, 255, null);
        $this->addColumn('category', 'Category', 'VARCHAR', true, 255, null);
        $this->addColumn('sector', 'Sector', 'VARCHAR', true, 255, null);
        $this->addColumn('source_bank', 'SourceBank', 'VARCHAR', true, 255, null);
        $this->addColumn('destination_bank', 'DestinationBank', 'VARCHAR', false, 255, null);
        $this->addColumn('destination_account_number', 'DestinationAccountNumber', 'VARCHAR', false, 50, null);
        $this->addColumn('destination_account_name', 'DestinationAccountName', 'VARCHAR', false, 255, null);
        $this->addColumn('invoice_number', 'InvoiceNumber', 'VARCHAR', false, 50, null);
        $this->addColumn('payment_channel', 'PaymentChannel', 'VARCHAR', true, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PaymentFctirsRecipients', '\\PaymentFctirsRecipients', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':payment_fctirs_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'PaymentFctirsRecipientss', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to payment_fctirs     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        PaymentFctirsRecipientsTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaymentFctirsTableMap::CLASS_DEFAULT : PaymentFctirsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PaymentFctirs object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaymentFctirsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaymentFctirsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaymentFctirsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaymentFctirsTableMap::OM_CLASS;
            /** @var PaymentFctirs $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaymentFctirsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaymentFctirsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaymentFctirsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PaymentFctirs $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaymentFctirsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_ID);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_SESSION_ID);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_DATE);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_DATE_TIME);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_EXISTING_TIN);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_JTB_TIN);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_PHONE);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_NAME);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_EMAIL);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_AMOUNT_PAID);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_ADDRESS);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_TAX);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_MONTH);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_YEAR);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_CATEGORY);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_SECTOR);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_SOURCE_BANK);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_DESTINATION_BANK);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NUMBER);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_DESTINATION_ACCOUNT_NAME);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_INVOICE_NUMBER);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_PAYMENT_CHANNEL);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PaymentFctirsTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.session_id');
            $criteria->addSelectColumn($alias . '.date');
            $criteria->addSelectColumn($alias . '.date_time');
            $criteria->addSelectColumn($alias . '.existing_tin');
            $criteria->addSelectColumn($alias . '.jtb_tin');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.amount_paid');
            $criteria->addSelectColumn($alias . '.address');
            $criteria->addSelectColumn($alias . '.tax');
            $criteria->addSelectColumn($alias . '.month');
            $criteria->addSelectColumn($alias . '.year');
            $criteria->addSelectColumn($alias . '.category');
            $criteria->addSelectColumn($alias . '.sector');
            $criteria->addSelectColumn($alias . '.source_bank');
            $criteria->addSelectColumn($alias . '.destination_bank');
            $criteria->addSelectColumn($alias . '.destination_account_number');
            $criteria->addSelectColumn($alias . '.destination_account_name');
            $criteria->addSelectColumn($alias . '.invoice_number');
            $criteria->addSelectColumn($alias . '.payment_channel');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaymentFctirsTableMap::DATABASE_NAME)->getTable(PaymentFctirsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaymentFctirsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaymentFctirsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaymentFctirsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PaymentFctirs or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PaymentFctirs object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PaymentFctirs) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaymentFctirsTableMap::DATABASE_NAME);
            $criteria->add(PaymentFctirsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PaymentFctirsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaymentFctirsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaymentFctirsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the payment_fctirs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaymentFctirsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PaymentFctirs or Criteria object.
     *
     * @param mixed               $criteria Criteria or PaymentFctirs object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PaymentFctirs object
        }

        if ($criteria->containsKey(PaymentFctirsTableMap::COL_ID) && $criteria->keyContainsValue(PaymentFctirsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PaymentFctirsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PaymentFctirsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaymentFctirsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaymentFctirsTableMap::buildTableMap();
