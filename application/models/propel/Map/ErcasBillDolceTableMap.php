<?php

namespace Map;

use \ErcasBillDolce;
use \ErcasBillDolceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'ercas_bill_dolce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ErcasBillDolceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ErcasBillDolceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'ercas_bill_dolce';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ErcasBillDolce';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ErcasBillDolce';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 31;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 31;

    /**
     * the column name for the UniqueKeyID field
     */
    const COL_UNIQUEKEYID = 'ercas_bill_dolce.UniqueKeyID';

    /**
     * the column name for the AccountNo field
     */
    const COL_ACCOUNTNO = 'ercas_bill_dolce.AccountNo';

    /**
     * the column name for the ServiceDistrict field
     */
    const COL_SERVICEDISTRICT = 'ercas_bill_dolce.ServiceDistrict';

    /**
     * the column name for the LastMeterReading field
     */
    const COL_LASTMETERREADING = 'ercas_bill_dolce.LastMeterReading';

    /**
     * the column name for the CurrentMeterReading field
     */
    const COL_CURRENTMETERREADING = 'ercas_bill_dolce.CurrentMeterReading';

    /**
     * the column name for the UnitsConsumed field
     */
    const COL_UNITSCONSUMED = 'ercas_bill_dolce.UnitsConsumed';

    /**
     * the column name for the LastPayDate field
     */
    const COL_LASTPAYDATE = 'ercas_bill_dolce.LastPayDate';

    /**
     * the column name for the LastPayAmt field
     */
    const COL_LASTPAYAMT = 'ercas_bill_dolce.LastPayAmt';

    /**
     * the column name for the PriorBalance field
     */
    const COL_PRIORBALANCE = 'ercas_bill_dolce.PriorBalance';

    /**
     * the column name for the OutstandingBalance field
     */
    const COL_OUTSTANDINGBALANCE = 'ercas_bill_dolce.OutstandingBalance';

    /**
     * the column name for the AmountDue field
     */
    const COL_AMOUNTDUE = 'ercas_bill_dolce.AmountDue';

    /**
     * the column name for the MeterMaintenanceCharge field
     */
    const COL_METERMAINTENANCECHARGE = 'ercas_bill_dolce.MeterMaintenanceCharge';

    /**
     * the column name for the Discounts field
     */
    const COL_DISCOUNTS = 'ercas_bill_dolce.Discounts';

    /**
     * the column name for the OtherCharges field
     */
    const COL_OTHERCHARGES = 'ercas_bill_dolce.OtherCharges';

    /**
     * the column name for the PenaltyCharges field
     */
    const COL_PENALTYCHARGES = 'ercas_bill_dolce.PenaltyCharges';

    /**
     * the column name for the StampDutyCharges field
     */
    const COL_STAMPDUTYCHARGES = 'ercas_bill_dolce.StampDutyCharges';

    /**
     * the column name for the ServiceCharges field
     */
    const COL_SERVICECHARGES = 'ercas_bill_dolce.ServiceCharges';

    /**
     * the column name for the RoutineCharges field
     */
    const COL_ROUTINECHARGES = 'ercas_bill_dolce.RoutineCharges';

    /**
     * the column name for the BillServiceRate field
     */
    const COL_BILLSERVICERATE = 'ercas_bill_dolce.BillServiceRate';

    /**
     * the column name for the ServiceTypeDesc field
     */
    const COL_SERVICETYPEDESC = 'ercas_bill_dolce.ServiceTypeDesc';

    /**
     * the column name for the BillPeriod field
     */
    const COL_BILLPERIOD = 'ercas_bill_dolce.BillPeriod';

    /**
     * the column name for the UsageType field
     */
    const COL_USAGETYPE = 'ercas_bill_dolce.UsageType';

    /**
     * the column name for the MeterNumber field
     */
    const COL_METERNUMBER = 'ercas_bill_dolce.MeterNumber';

    /**
     * the column name for the MeterType field
     */
    const COL_METERTYPE = 'ercas_bill_dolce.MeterType';

    /**
     * the column name for the MeterCondition field
     */
    const COL_METERCONDITION = 'ercas_bill_dolce.MeterCondition';

    /**
     * the column name for the LeakageStatus field
     */
    const COL_LEAKAGESTATUS = 'ercas_bill_dolce.LeakageStatus';

    /**
     * the column name for the PropertyType field
     */
    const COL_PROPERTYTYPE = 'ercas_bill_dolce.PropertyType';

    /**
     * the column name for the MeterReadDevice field
     */
    const COL_METERREADDEVICE = 'ercas_bill_dolce.MeterReadDevice';

    /**
     * the column name for the Billmethod field
     */
    const COL_BILLMETHOD = 'ercas_bill_dolce.Billmethod';

    /**
     * the column name for the DateCreated field
     */
    const COL_DATECREATED = 'ercas_bill_dolce.DateCreated';

    /**
     * the column name for the is_migrated field
     */
    const COL_IS_MIGRATED = 'ercas_bill_dolce.is_migrated';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Uniquekeyid', 'Accountno', 'Servicedistrict', 'Lastmeterreading', 'Currentmeterreading', 'Unitsconsumed', 'Lastpaydate', 'Lastpayamt', 'Priorbalance', 'Outstandingbalance', 'Amountdue', 'Metermaintenancecharge', 'Discounts', 'Othercharges', 'Penaltycharges', 'Stampdutycharges', 'Servicecharges', 'Routinecharges', 'Billservicerate', 'Servicetypedesc', 'Billperiod', 'Usagetype', 'Meternumber', 'Metertype', 'Metercondition', 'Leakagestatus', 'Propertytype', 'Meterreaddevice', 'Billmethod', 'Datecreated', 'IsMigrated', ),
        self::TYPE_CAMELNAME     => array('uniquekeyid', 'accountno', 'servicedistrict', 'lastmeterreading', 'currentmeterreading', 'unitsconsumed', 'lastpaydate', 'lastpayamt', 'priorbalance', 'outstandingbalance', 'amountdue', 'metermaintenancecharge', 'discounts', 'othercharges', 'penaltycharges', 'stampdutycharges', 'servicecharges', 'routinecharges', 'billservicerate', 'servicetypedesc', 'billperiod', 'usagetype', 'meternumber', 'metertype', 'metercondition', 'leakagestatus', 'propertytype', 'meterreaddevice', 'billmethod', 'datecreated', 'isMigrated', ),
        self::TYPE_COLNAME       => array(ErcasBillDolceTableMap::COL_UNIQUEKEYID, ErcasBillDolceTableMap::COL_ACCOUNTNO, ErcasBillDolceTableMap::COL_SERVICEDISTRICT, ErcasBillDolceTableMap::COL_LASTMETERREADING, ErcasBillDolceTableMap::COL_CURRENTMETERREADING, ErcasBillDolceTableMap::COL_UNITSCONSUMED, ErcasBillDolceTableMap::COL_LASTPAYDATE, ErcasBillDolceTableMap::COL_LASTPAYAMT, ErcasBillDolceTableMap::COL_PRIORBALANCE, ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE, ErcasBillDolceTableMap::COL_AMOUNTDUE, ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE, ErcasBillDolceTableMap::COL_DISCOUNTS, ErcasBillDolceTableMap::COL_OTHERCHARGES, ErcasBillDolceTableMap::COL_PENALTYCHARGES, ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES, ErcasBillDolceTableMap::COL_SERVICECHARGES, ErcasBillDolceTableMap::COL_ROUTINECHARGES, ErcasBillDolceTableMap::COL_BILLSERVICERATE, ErcasBillDolceTableMap::COL_SERVICETYPEDESC, ErcasBillDolceTableMap::COL_BILLPERIOD, ErcasBillDolceTableMap::COL_USAGETYPE, ErcasBillDolceTableMap::COL_METERNUMBER, ErcasBillDolceTableMap::COL_METERTYPE, ErcasBillDolceTableMap::COL_METERCONDITION, ErcasBillDolceTableMap::COL_LEAKAGESTATUS, ErcasBillDolceTableMap::COL_PROPERTYTYPE, ErcasBillDolceTableMap::COL_METERREADDEVICE, ErcasBillDolceTableMap::COL_BILLMETHOD, ErcasBillDolceTableMap::COL_DATECREATED, ErcasBillDolceTableMap::COL_IS_MIGRATED, ),
        self::TYPE_FIELDNAME     => array('UniqueKeyID', 'AccountNo', 'ServiceDistrict', 'LastMeterReading', 'CurrentMeterReading', 'UnitsConsumed', 'LastPayDate', 'LastPayAmt', 'PriorBalance', 'OutstandingBalance', 'AmountDue', 'MeterMaintenanceCharge', 'Discounts', 'OtherCharges', 'PenaltyCharges', 'StampDutyCharges', 'ServiceCharges', 'RoutineCharges', 'BillServiceRate', 'ServiceTypeDesc', 'BillPeriod', 'UsageType', 'MeterNumber', 'MeterType', 'MeterCondition', 'LeakageStatus', 'PropertyType', 'MeterReadDevice', 'Billmethod', 'DateCreated', 'is_migrated', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Uniquekeyid' => 0, 'Accountno' => 1, 'Servicedistrict' => 2, 'Lastmeterreading' => 3, 'Currentmeterreading' => 4, 'Unitsconsumed' => 5, 'Lastpaydate' => 6, 'Lastpayamt' => 7, 'Priorbalance' => 8, 'Outstandingbalance' => 9, 'Amountdue' => 10, 'Metermaintenancecharge' => 11, 'Discounts' => 12, 'Othercharges' => 13, 'Penaltycharges' => 14, 'Stampdutycharges' => 15, 'Servicecharges' => 16, 'Routinecharges' => 17, 'Billservicerate' => 18, 'Servicetypedesc' => 19, 'Billperiod' => 20, 'Usagetype' => 21, 'Meternumber' => 22, 'Metertype' => 23, 'Metercondition' => 24, 'Leakagestatus' => 25, 'Propertytype' => 26, 'Meterreaddevice' => 27, 'Billmethod' => 28, 'Datecreated' => 29, 'IsMigrated' => 30, ),
        self::TYPE_CAMELNAME     => array('uniquekeyid' => 0, 'accountno' => 1, 'servicedistrict' => 2, 'lastmeterreading' => 3, 'currentmeterreading' => 4, 'unitsconsumed' => 5, 'lastpaydate' => 6, 'lastpayamt' => 7, 'priorbalance' => 8, 'outstandingbalance' => 9, 'amountdue' => 10, 'metermaintenancecharge' => 11, 'discounts' => 12, 'othercharges' => 13, 'penaltycharges' => 14, 'stampdutycharges' => 15, 'servicecharges' => 16, 'routinecharges' => 17, 'billservicerate' => 18, 'servicetypedesc' => 19, 'billperiod' => 20, 'usagetype' => 21, 'meternumber' => 22, 'metertype' => 23, 'metercondition' => 24, 'leakagestatus' => 25, 'propertytype' => 26, 'meterreaddevice' => 27, 'billmethod' => 28, 'datecreated' => 29, 'isMigrated' => 30, ),
        self::TYPE_COLNAME       => array(ErcasBillDolceTableMap::COL_UNIQUEKEYID => 0, ErcasBillDolceTableMap::COL_ACCOUNTNO => 1, ErcasBillDolceTableMap::COL_SERVICEDISTRICT => 2, ErcasBillDolceTableMap::COL_LASTMETERREADING => 3, ErcasBillDolceTableMap::COL_CURRENTMETERREADING => 4, ErcasBillDolceTableMap::COL_UNITSCONSUMED => 5, ErcasBillDolceTableMap::COL_LASTPAYDATE => 6, ErcasBillDolceTableMap::COL_LASTPAYAMT => 7, ErcasBillDolceTableMap::COL_PRIORBALANCE => 8, ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE => 9, ErcasBillDolceTableMap::COL_AMOUNTDUE => 10, ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE => 11, ErcasBillDolceTableMap::COL_DISCOUNTS => 12, ErcasBillDolceTableMap::COL_OTHERCHARGES => 13, ErcasBillDolceTableMap::COL_PENALTYCHARGES => 14, ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES => 15, ErcasBillDolceTableMap::COL_SERVICECHARGES => 16, ErcasBillDolceTableMap::COL_ROUTINECHARGES => 17, ErcasBillDolceTableMap::COL_BILLSERVICERATE => 18, ErcasBillDolceTableMap::COL_SERVICETYPEDESC => 19, ErcasBillDolceTableMap::COL_BILLPERIOD => 20, ErcasBillDolceTableMap::COL_USAGETYPE => 21, ErcasBillDolceTableMap::COL_METERNUMBER => 22, ErcasBillDolceTableMap::COL_METERTYPE => 23, ErcasBillDolceTableMap::COL_METERCONDITION => 24, ErcasBillDolceTableMap::COL_LEAKAGESTATUS => 25, ErcasBillDolceTableMap::COL_PROPERTYTYPE => 26, ErcasBillDolceTableMap::COL_METERREADDEVICE => 27, ErcasBillDolceTableMap::COL_BILLMETHOD => 28, ErcasBillDolceTableMap::COL_DATECREATED => 29, ErcasBillDolceTableMap::COL_IS_MIGRATED => 30, ),
        self::TYPE_FIELDNAME     => array('UniqueKeyID' => 0, 'AccountNo' => 1, 'ServiceDistrict' => 2, 'LastMeterReading' => 3, 'CurrentMeterReading' => 4, 'UnitsConsumed' => 5, 'LastPayDate' => 6, 'LastPayAmt' => 7, 'PriorBalance' => 8, 'OutstandingBalance' => 9, 'AmountDue' => 10, 'MeterMaintenanceCharge' => 11, 'Discounts' => 12, 'OtherCharges' => 13, 'PenaltyCharges' => 14, 'StampDutyCharges' => 15, 'ServiceCharges' => 16, 'RoutineCharges' => 17, 'BillServiceRate' => 18, 'ServiceTypeDesc' => 19, 'BillPeriod' => 20, 'UsageType' => 21, 'MeterNumber' => 22, 'MeterType' => 23, 'MeterCondition' => 24, 'LeakageStatus' => 25, 'PropertyType' => 26, 'MeterReadDevice' => 27, 'Billmethod' => 28, 'DateCreated' => 29, 'is_migrated' => 30, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ercas_bill_dolce');
        $this->setPhpName('ErcasBillDolce');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ErcasBillDolce');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('UniqueKeyID', 'Uniquekeyid', 'INTEGER', true, 10, null);
        $this->addColumn('AccountNo', 'Accountno', 'VARCHAR', true, 20, null);
        $this->addColumn('ServiceDistrict', 'Servicedistrict', 'VARCHAR', true, 50, null);
        $this->addColumn('LastMeterReading', 'Lastmeterreading', 'FLOAT', true, null, null);
        $this->addColumn('CurrentMeterReading', 'Currentmeterreading', 'FLOAT', true, null, null);
        $this->addColumn('UnitsConsumed', 'Unitsconsumed', 'FLOAT', true, null, null);
        $this->addColumn('LastPayDate', 'Lastpaydate', 'VARCHAR', true, 20, null);
        $this->addColumn('LastPayAmt', 'Lastpayamt', 'DECIMAL', true, 20, null);
        $this->addColumn('PriorBalance', 'Priorbalance', 'DECIMAL', true, 20, null);
        $this->addColumn('OutstandingBalance', 'Outstandingbalance', 'DECIMAL', true, 20, null);
        $this->addColumn('AmountDue', 'Amountdue', 'DECIMAL', true, 20, null);
        $this->addColumn('MeterMaintenanceCharge', 'Metermaintenancecharge', 'DECIMAL', true, 11, null);
        $this->addColumn('Discounts', 'Discounts', 'DECIMAL', true, 11, null);
        $this->addColumn('OtherCharges', 'Othercharges', 'DECIMAL', true, 11, null);
        $this->addColumn('PenaltyCharges', 'Penaltycharges', 'DECIMAL', true, 11, null);
        $this->addColumn('StampDutyCharges', 'Stampdutycharges', 'DECIMAL', true, 11, null);
        $this->addColumn('ServiceCharges', 'Servicecharges', 'DECIMAL', true, 11, null);
        $this->addColumn('RoutineCharges', 'Routinecharges', 'DECIMAL', true, 11, null);
        $this->addColumn('BillServiceRate', 'Billservicerate', 'DECIMAL', true, 11, null);
        $this->addColumn('ServiceTypeDesc', 'Servicetypedesc', 'VARCHAR', true, 150, null);
        $this->addColumn('BillPeriod', 'Billperiod', 'VARCHAR', true, 79, null);
        $this->addColumn('UsageType', 'Usagetype', 'VARCHAR', true, 255, null);
        $this->addColumn('MeterNumber', 'Meternumber', 'VARCHAR', true, 20, null);
        $this->addColumn('MeterType', 'Metertype', 'VARCHAR', true, 30, null);
        $this->addColumn('MeterCondition', 'Metercondition', 'VARCHAR', true, 50, null);
        $this->addColumn('LeakageStatus', 'Leakagestatus', 'VARCHAR', true, 50, null);
        $this->addColumn('PropertyType', 'Propertytype', 'VARCHAR', true, 255, null);
        $this->addColumn('MeterReadDevice', 'Meterreaddevice', 'VARCHAR', true, 30, null);
        $this->addColumn('Billmethod', 'Billmethod', 'VARCHAR', true, 50, null);
        $this->addColumn('DateCreated', 'Datecreated', 'TIMESTAMP', true, null, null);
        $this->addColumn('is_migrated', 'IsMigrated', 'TINYINT', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return null;
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return '';
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ErcasBillDolceTableMap::CLASS_DEFAULT : ErcasBillDolceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ErcasBillDolce object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ErcasBillDolceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ErcasBillDolceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ErcasBillDolceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ErcasBillDolceTableMap::OM_CLASS;
            /** @var ErcasBillDolce $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ErcasBillDolceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ErcasBillDolceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ErcasBillDolceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ErcasBillDolce $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ErcasBillDolceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_UNIQUEKEYID);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_ACCOUNTNO);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_SERVICEDISTRICT);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_LASTMETERREADING);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_CURRENTMETERREADING);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_UNITSCONSUMED);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_LASTPAYDATE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_LASTPAYAMT);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_PRIORBALANCE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_OUTSTANDINGBALANCE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_AMOUNTDUE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_METERMAINTENANCECHARGE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_DISCOUNTS);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_OTHERCHARGES);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_PENALTYCHARGES);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_STAMPDUTYCHARGES);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_SERVICECHARGES);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_ROUTINECHARGES);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_BILLSERVICERATE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_SERVICETYPEDESC);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_BILLPERIOD);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_USAGETYPE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_METERNUMBER);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_METERTYPE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_METERCONDITION);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_LEAKAGESTATUS);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_PROPERTYTYPE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_METERREADDEVICE);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_BILLMETHOD);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_DATECREATED);
            $criteria->addSelectColumn(ErcasBillDolceTableMap::COL_IS_MIGRATED);
        } else {
            $criteria->addSelectColumn($alias . '.UniqueKeyID');
            $criteria->addSelectColumn($alias . '.AccountNo');
            $criteria->addSelectColumn($alias . '.ServiceDistrict');
            $criteria->addSelectColumn($alias . '.LastMeterReading');
            $criteria->addSelectColumn($alias . '.CurrentMeterReading');
            $criteria->addSelectColumn($alias . '.UnitsConsumed');
            $criteria->addSelectColumn($alias . '.LastPayDate');
            $criteria->addSelectColumn($alias . '.LastPayAmt');
            $criteria->addSelectColumn($alias . '.PriorBalance');
            $criteria->addSelectColumn($alias . '.OutstandingBalance');
            $criteria->addSelectColumn($alias . '.AmountDue');
            $criteria->addSelectColumn($alias . '.MeterMaintenanceCharge');
            $criteria->addSelectColumn($alias . '.Discounts');
            $criteria->addSelectColumn($alias . '.OtherCharges');
            $criteria->addSelectColumn($alias . '.PenaltyCharges');
            $criteria->addSelectColumn($alias . '.StampDutyCharges');
            $criteria->addSelectColumn($alias . '.ServiceCharges');
            $criteria->addSelectColumn($alias . '.RoutineCharges');
            $criteria->addSelectColumn($alias . '.BillServiceRate');
            $criteria->addSelectColumn($alias . '.ServiceTypeDesc');
            $criteria->addSelectColumn($alias . '.BillPeriod');
            $criteria->addSelectColumn($alias . '.UsageType');
            $criteria->addSelectColumn($alias . '.MeterNumber');
            $criteria->addSelectColumn($alias . '.MeterType');
            $criteria->addSelectColumn($alias . '.MeterCondition');
            $criteria->addSelectColumn($alias . '.LeakageStatus');
            $criteria->addSelectColumn($alias . '.PropertyType');
            $criteria->addSelectColumn($alias . '.MeterReadDevice');
            $criteria->addSelectColumn($alias . '.Billmethod');
            $criteria->addSelectColumn($alias . '.DateCreated');
            $criteria->addSelectColumn($alias . '.is_migrated');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ErcasBillDolceTableMap::DATABASE_NAME)->getTable(ErcasBillDolceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ErcasBillDolceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ErcasBillDolceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ErcasBillDolceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ErcasBillDolce or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ErcasBillDolce object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillDolceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ErcasBillDolce) { // it's a model object
            // create criteria based on pk value
            $criteria = $values->buildCriteria();
        } else { // it's a primary key, or an array of pks
            throw new LogicException('The ErcasBillDolce object has no primary key');
        }

        $query = ErcasBillDolceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ErcasBillDolceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ErcasBillDolceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the ercas_bill_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ErcasBillDolceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ErcasBillDolce or Criteria object.
     *
     * @param mixed               $criteria Criteria or ErcasBillDolce object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillDolceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ErcasBillDolce object
        }


        // Set the correct dbName
        $query = ErcasBillDolceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ErcasBillDolceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ErcasBillDolceTableMap::buildTableMap();
