<?php

namespace Map;

use \ErcasBill;
use \ErcasBillQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'ercas_bill' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ErcasBillTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ErcasBillTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'ercas_bill';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ErcasBill';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ErcasBill';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'ercas_bill.id';

    /**
     * the column name for the ref_no field
     */
    const COL_REF_NO = 'ercas_bill.ref_no';

    /**
     * the column name for the UniqueKeyID field
     */
    const COL_UNIQUEKEYID = 'ercas_bill.UniqueKeyID';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'ercas_bill.customer_id';

    /**
     * the column name for the account_no field
     */
    const COL_ACCOUNT_NO = 'ercas_bill.account_no';

    /**
     * the column name for the service_type field
     */
    const COL_SERVICE_TYPE = 'ercas_bill.service_type';

    /**
     * the column name for the due_date field
     */
    const COL_DUE_DATE = 'ercas_bill.due_date';

    /**
     * the column name for the billing_from field
     */
    const COL_BILLING_FROM = 'ercas_bill.billing_from';

    /**
     * the column name for the billing_to field
     */
    const COL_BILLING_TO = 'ercas_bill.billing_to';

    /**
     * the column name for the bill_date field
     */
    const COL_BILL_DATE = 'ercas_bill.bill_date';

    /**
     * the column name for the current_charge field
     */
    const COL_CURRENT_CHARGE = 'ercas_bill.current_charge';

    /**
     * the column name for the service_charge field
     */
    const COL_SERVICE_CHARGE = 'ercas_bill.service_charge';

    /**
     * the column name for the total_due field
     */
    const COL_TOTAL_DUE = 'ercas_bill.total_due';

    /**
     * the column name for the date_created field
     */
    const COL_DATE_CREATED = 'ercas_bill.date_created';

    /**
     * the column name for the creator field
     */
    const COL_CREATOR = 'ercas_bill.creator';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'ercas_bill.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'RefNo', 'Uniquekeyid', 'CustomerId', 'AccountNo', 'ServiceType', 'DueDate', 'BillingFrom', 'BillingTo', 'BillDate', 'CurrentCharge', 'ServiceCharge', 'TotalDue', 'DateCreated', 'Creator', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'refNo', 'uniquekeyid', 'customerId', 'accountNo', 'serviceType', 'dueDate', 'billingFrom', 'billingTo', 'billDate', 'currentCharge', 'serviceCharge', 'totalDue', 'dateCreated', 'creator', 'status', ),
        self::TYPE_COLNAME       => array(ErcasBillTableMap::COL_ID, ErcasBillTableMap::COL_REF_NO, ErcasBillTableMap::COL_UNIQUEKEYID, ErcasBillTableMap::COL_CUSTOMER_ID, ErcasBillTableMap::COL_ACCOUNT_NO, ErcasBillTableMap::COL_SERVICE_TYPE, ErcasBillTableMap::COL_DUE_DATE, ErcasBillTableMap::COL_BILLING_FROM, ErcasBillTableMap::COL_BILLING_TO, ErcasBillTableMap::COL_BILL_DATE, ErcasBillTableMap::COL_CURRENT_CHARGE, ErcasBillTableMap::COL_SERVICE_CHARGE, ErcasBillTableMap::COL_TOTAL_DUE, ErcasBillTableMap::COL_DATE_CREATED, ErcasBillTableMap::COL_CREATOR, ErcasBillTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'ref_no', 'UniqueKeyID', 'customer_id', 'account_no', 'service_type', 'due_date', 'billing_from', 'billing_to', 'bill_date', 'current_charge', 'service_charge', 'total_due', 'date_created', 'creator', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'RefNo' => 1, 'Uniquekeyid' => 2, 'CustomerId' => 3, 'AccountNo' => 4, 'ServiceType' => 5, 'DueDate' => 6, 'BillingFrom' => 7, 'BillingTo' => 8, 'BillDate' => 9, 'CurrentCharge' => 10, 'ServiceCharge' => 11, 'TotalDue' => 12, 'DateCreated' => 13, 'Creator' => 14, 'Status' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'refNo' => 1, 'uniquekeyid' => 2, 'customerId' => 3, 'accountNo' => 4, 'serviceType' => 5, 'dueDate' => 6, 'billingFrom' => 7, 'billingTo' => 8, 'billDate' => 9, 'currentCharge' => 10, 'serviceCharge' => 11, 'totalDue' => 12, 'dateCreated' => 13, 'creator' => 14, 'status' => 15, ),
        self::TYPE_COLNAME       => array(ErcasBillTableMap::COL_ID => 0, ErcasBillTableMap::COL_REF_NO => 1, ErcasBillTableMap::COL_UNIQUEKEYID => 2, ErcasBillTableMap::COL_CUSTOMER_ID => 3, ErcasBillTableMap::COL_ACCOUNT_NO => 4, ErcasBillTableMap::COL_SERVICE_TYPE => 5, ErcasBillTableMap::COL_DUE_DATE => 6, ErcasBillTableMap::COL_BILLING_FROM => 7, ErcasBillTableMap::COL_BILLING_TO => 8, ErcasBillTableMap::COL_BILL_DATE => 9, ErcasBillTableMap::COL_CURRENT_CHARGE => 10, ErcasBillTableMap::COL_SERVICE_CHARGE => 11, ErcasBillTableMap::COL_TOTAL_DUE => 12, ErcasBillTableMap::COL_DATE_CREATED => 13, ErcasBillTableMap::COL_CREATOR => 14, ErcasBillTableMap::COL_STATUS => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'ref_no' => 1, 'UniqueKeyID' => 2, 'customer_id' => 3, 'account_no' => 4, 'service_type' => 5, 'due_date' => 6, 'billing_from' => 7, 'billing_to' => 8, 'bill_date' => 9, 'current_charge' => 10, 'service_charge' => 11, 'total_due' => 12, 'date_created' => 13, 'creator' => 14, 'status' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ercas_bill');
        $this->setPhpName('ErcasBill');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ErcasBill');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 20, null);
        $this->addColumn('ref_no', 'RefNo', 'VARCHAR', true, 50, null);
        $this->addColumn('UniqueKeyID', 'Uniquekeyid', 'INTEGER', true, 50, null);
        $this->addColumn('customer_id', 'CustomerId', 'INTEGER', true, null, null);
        $this->addColumn('account_no', 'AccountNo', 'VARCHAR', true, 50, null);
        $this->addColumn('service_type', 'ServiceType', 'VARCHAR', true, 50, null);
        $this->addColumn('due_date', 'DueDate', 'VARCHAR', true, 50, null);
        $this->addColumn('billing_from', 'BillingFrom', 'VARCHAR', true, 50, null);
        $this->addColumn('billing_to', 'BillingTo', 'VARCHAR', true, 50, null);
        $this->addColumn('bill_date', 'BillDate', 'DATE', true, null, null);
        $this->addColumn('current_charge', 'CurrentCharge', 'DECIMAL', true, 11, null);
        $this->addColumn('service_charge', 'ServiceCharge', 'DECIMAL', true, 11, null);
        $this->addColumn('total_due', 'TotalDue', 'DECIMAL', true, 20, null);
        $this->addColumn('date_created', 'DateCreated', 'TIMESTAMP', true, null, null);
        $this->addColumn('creator', 'Creator', 'VARCHAR', true, 50, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', true, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ErcasBillTableMap::CLASS_DEFAULT : ErcasBillTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ErcasBill object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ErcasBillTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ErcasBillTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ErcasBillTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ErcasBillTableMap::OM_CLASS;
            /** @var ErcasBill $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ErcasBillTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ErcasBillTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ErcasBillTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ErcasBill $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ErcasBillTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ErcasBillTableMap::COL_ID);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_REF_NO);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_UNIQUEKEYID);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_ACCOUNT_NO);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_SERVICE_TYPE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_DUE_DATE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_BILLING_FROM);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_BILLING_TO);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_BILL_DATE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_CURRENT_CHARGE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_SERVICE_CHARGE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_TOTAL_DUE);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_DATE_CREATED);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_CREATOR);
            $criteria->addSelectColumn(ErcasBillTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ref_no');
            $criteria->addSelectColumn($alias . '.UniqueKeyID');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.account_no');
            $criteria->addSelectColumn($alias . '.service_type');
            $criteria->addSelectColumn($alias . '.due_date');
            $criteria->addSelectColumn($alias . '.billing_from');
            $criteria->addSelectColumn($alias . '.billing_to');
            $criteria->addSelectColumn($alias . '.bill_date');
            $criteria->addSelectColumn($alias . '.current_charge');
            $criteria->addSelectColumn($alias . '.service_charge');
            $criteria->addSelectColumn($alias . '.total_due');
            $criteria->addSelectColumn($alias . '.date_created');
            $criteria->addSelectColumn($alias . '.creator');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ErcasBillTableMap::DATABASE_NAME)->getTable(ErcasBillTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ErcasBillTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ErcasBillTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ErcasBillTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ErcasBill or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ErcasBill object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ErcasBill) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ErcasBillTableMap::DATABASE_NAME);
            $criteria->add(ErcasBillTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ErcasBillQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ErcasBillTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ErcasBillTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the ercas_bill table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ErcasBillQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ErcasBill or Criteria object.
     *
     * @param mixed               $criteria Criteria or ErcasBill object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ErcasBillTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ErcasBill object
        }

        if ($criteria->containsKey(ErcasBillTableMap::COL_ID) && $criteria->keyContainsValue(ErcasBillTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ErcasBillTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ErcasBillQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ErcasBillTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ErcasBillTableMap::buildTableMap();
