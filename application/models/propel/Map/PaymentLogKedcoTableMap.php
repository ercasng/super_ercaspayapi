<?php

namespace Map;

use \PaymentLogKedco;
use \PaymentLogKedcoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'payment_log_kedco' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaymentLogKedcoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaymentLogKedcoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'payment_log_kedco';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PaymentLogKedco';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PaymentLogKedco';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id field
     */
    const COL_ID = 'payment_log_kedco.id';

    /**
     * the column name for the cpay_ref field
     */
    const COL_CPAY_REF = 'payment_log_kedco.cpay_ref';

    /**
     * the column name for the transaction_id field
     */
    const COL_TRANSACTION_ID = 'payment_log_kedco.transaction_id';

    /**
     * the column name for the product_id field
     */
    const COL_PRODUCT_ID = 'payment_log_kedco.product_id';

    /**
     * the column name for the product_description field
     */
    const COL_PRODUCT_DESCRIPTION = 'payment_log_kedco.product_description';

    /**
     * the column name for the merchant_id field
     */
    const COL_MERCHANT_ID = 'payment_log_kedco.merchant_id';

    /**
     * the column name for the response_url field
     */
    const COL_RESPONSE_URL = 'payment_log_kedco.response_url';

    /**
     * the column name for the currency field
     */
    const COL_CURRENCY = 'payment_log_kedco.currency';

    /**
     * the column name for the amount field
     */
    const COL_AMOUNT = 'payment_log_kedco.amount';

    /**
     * the column name for the hash field
     */
    const COL_HASH = 'payment_log_kedco.hash';

    /**
     * the column name for the time_stamp field
     */
    const COL_TIME_STAMP = 'payment_log_kedco.time_stamp';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CpayRef', 'TransactionId', 'ProductId', 'ProductDescription', 'MerchantId', 'ResponseUrl', 'Currency', 'Amount', 'Hash', 'TimeStamp', ),
        self::TYPE_CAMELNAME     => array('id', 'cpayRef', 'transactionId', 'productId', 'productDescription', 'merchantId', 'responseUrl', 'currency', 'amount', 'hash', 'timeStamp', ),
        self::TYPE_COLNAME       => array(PaymentLogKedcoTableMap::COL_ID, PaymentLogKedcoTableMap::COL_CPAY_REF, PaymentLogKedcoTableMap::COL_TRANSACTION_ID, PaymentLogKedcoTableMap::COL_PRODUCT_ID, PaymentLogKedcoTableMap::COL_PRODUCT_DESCRIPTION, PaymentLogKedcoTableMap::COL_MERCHANT_ID, PaymentLogKedcoTableMap::COL_RESPONSE_URL, PaymentLogKedcoTableMap::COL_CURRENCY, PaymentLogKedcoTableMap::COL_AMOUNT, PaymentLogKedcoTableMap::COL_HASH, PaymentLogKedcoTableMap::COL_TIME_STAMP, ),
        self::TYPE_FIELDNAME     => array('id', 'cpay_ref', 'transaction_id', 'product_id', 'product_description', 'merchant_id', 'response_url', 'currency', 'amount', 'hash', 'time_stamp', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CpayRef' => 1, 'TransactionId' => 2, 'ProductId' => 3, 'ProductDescription' => 4, 'MerchantId' => 5, 'ResponseUrl' => 6, 'Currency' => 7, 'Amount' => 8, 'Hash' => 9, 'TimeStamp' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'cpayRef' => 1, 'transactionId' => 2, 'productId' => 3, 'productDescription' => 4, 'merchantId' => 5, 'responseUrl' => 6, 'currency' => 7, 'amount' => 8, 'hash' => 9, 'timeStamp' => 10, ),
        self::TYPE_COLNAME       => array(PaymentLogKedcoTableMap::COL_ID => 0, PaymentLogKedcoTableMap::COL_CPAY_REF => 1, PaymentLogKedcoTableMap::COL_TRANSACTION_ID => 2, PaymentLogKedcoTableMap::COL_PRODUCT_ID => 3, PaymentLogKedcoTableMap::COL_PRODUCT_DESCRIPTION => 4, PaymentLogKedcoTableMap::COL_MERCHANT_ID => 5, PaymentLogKedcoTableMap::COL_RESPONSE_URL => 6, PaymentLogKedcoTableMap::COL_CURRENCY => 7, PaymentLogKedcoTableMap::COL_AMOUNT => 8, PaymentLogKedcoTableMap::COL_HASH => 9, PaymentLogKedcoTableMap::COL_TIME_STAMP => 10, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'cpay_ref' => 1, 'transaction_id' => 2, 'product_id' => 3, 'product_description' => 4, 'merchant_id' => 5, 'response_url' => 6, 'currency' => 7, 'amount' => 8, 'hash' => 9, 'time_stamp' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('payment_log_kedco');
        $this->setPhpName('PaymentLogKedco');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PaymentLogKedco');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('cpay_ref', 'CpayRef', 'VARCHAR', true, 50, '0');
        $this->addColumn('transaction_id', 'TransactionId', 'VARCHAR', true, 50, null);
        $this->addColumn('product_id', 'ProductId', 'VARCHAR', true, 50, null);
        $this->addColumn('product_description', 'ProductDescription', 'VARCHAR', true, 60, null);
        $this->addColumn('merchant_id', 'MerchantId', 'VARCHAR', true, 50, null);
        $this->addColumn('response_url', 'ResponseUrl', 'VARCHAR', true, 200, null);
        $this->addColumn('currency', 'Currency', 'VARCHAR', true, 10, null);
        $this->addColumn('amount', 'Amount', 'VARCHAR', true, 20, null);
        $this->addColumn('hash', 'Hash', 'VARCHAR', true, 50, null);
        $this->addColumn('time_stamp', 'TimeStamp', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaymentLogKedcoTableMap::CLASS_DEFAULT : PaymentLogKedcoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PaymentLogKedco object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaymentLogKedcoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaymentLogKedcoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaymentLogKedcoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaymentLogKedcoTableMap::OM_CLASS;
            /** @var PaymentLogKedco $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaymentLogKedcoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaymentLogKedcoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaymentLogKedcoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PaymentLogKedco $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaymentLogKedcoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_ID);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_CPAY_REF);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_TRANSACTION_ID);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_PRODUCT_ID);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_PRODUCT_DESCRIPTION);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_MERCHANT_ID);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_RESPONSE_URL);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_CURRENCY);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_AMOUNT);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_HASH);
            $criteria->addSelectColumn(PaymentLogKedcoTableMap::COL_TIME_STAMP);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.cpay_ref');
            $criteria->addSelectColumn($alias . '.transaction_id');
            $criteria->addSelectColumn($alias . '.product_id');
            $criteria->addSelectColumn($alias . '.product_description');
            $criteria->addSelectColumn($alias . '.merchant_id');
            $criteria->addSelectColumn($alias . '.response_url');
            $criteria->addSelectColumn($alias . '.currency');
            $criteria->addSelectColumn($alias . '.amount');
            $criteria->addSelectColumn($alias . '.hash');
            $criteria->addSelectColumn($alias . '.time_stamp');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaymentLogKedcoTableMap::DATABASE_NAME)->getTable(PaymentLogKedcoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaymentLogKedcoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaymentLogKedcoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaymentLogKedcoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PaymentLogKedco or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PaymentLogKedco object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentLogKedcoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PaymentLogKedco) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaymentLogKedcoTableMap::DATABASE_NAME);
            $criteria->add(PaymentLogKedcoTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PaymentLogKedcoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaymentLogKedcoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaymentLogKedcoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the payment_log_kedco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaymentLogKedcoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PaymentLogKedco or Criteria object.
     *
     * @param mixed               $criteria Criteria or PaymentLogKedco object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentLogKedcoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PaymentLogKedco object
        }

        if ($criteria->containsKey(PaymentLogKedcoTableMap::COL_ID) && $criteria->keyContainsValue(PaymentLogKedcoTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PaymentLogKedcoTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PaymentLogKedcoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaymentLogKedcoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaymentLogKedcoTableMap::buildTableMap();
