<?php

namespace Map;

use \DolceInvoice;
use \DolceInvoiceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'dolce_invoice' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DolceInvoiceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.DolceInvoiceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'dolce_invoice';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\DolceInvoice';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'DolceInvoice';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'dolce_invoice.id';

    /**
     * the column name for the tx_code field
     */
    const COL_TX_CODE = 'dolce_invoice.tx_code';

    /**
     * the column name for the terminal field
     */
    const COL_TERMINAL = 'dolce_invoice.terminal';

    /**
     * the column name for the operator_code field
     */
    const COL_OPERATOR_CODE = 'dolce_invoice.operator_code';

    /**
     * the column name for the operator_name field
     */
    const COL_OPERATOR_NAME = 'dolce_invoice.operator_name';

    /**
     * the column name for the tx_date field
     */
    const COL_TX_DATE = 'dolce_invoice.tx_date';

    /**
     * the column name for the total_amount_string field
     */
    const COL_TOTAL_AMOUNT_STRING = 'dolce_invoice.total_amount_string';

    /**
     * the column name for the total_amount field
     */
    const COL_TOTAL_AMOUNT = 'dolce_invoice.total_amount';

    /**
     * the column name for the payment_mode field
     */
    const COL_PAYMENT_MODE = 'dolce_invoice.payment_mode';

    /**
     * the column name for the payment_type field
     */
    const COL_PAYMENT_TYPE = 'dolce_invoice.payment_type';

    /**
     * the column name for the patient_phone field
     */
    const COL_PATIENT_PHONE = 'dolce_invoice.patient_phone';

    /**
     * the column name for the app_version field
     */
    const COL_APP_VERSION = 'dolce_invoice.app_version';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'dolce_invoice.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'dolce_invoice.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TxCode', 'Terminal', 'OperatorCode', 'OperatorName', 'TxDate', 'TotalAmountString', 'TotalAmount', 'PaymentMode', 'PaymentType', 'PatientPhone', 'AppVersion', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'txCode', 'terminal', 'operatorCode', 'operatorName', 'txDate', 'totalAmountString', 'totalAmount', 'paymentMode', 'paymentType', 'patientPhone', 'appVersion', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(DolceInvoiceTableMap::COL_ID, DolceInvoiceTableMap::COL_TX_CODE, DolceInvoiceTableMap::COL_TERMINAL, DolceInvoiceTableMap::COL_OPERATOR_CODE, DolceInvoiceTableMap::COL_OPERATOR_NAME, DolceInvoiceTableMap::COL_TX_DATE, DolceInvoiceTableMap::COL_TOTAL_AMOUNT_STRING, DolceInvoiceTableMap::COL_TOTAL_AMOUNT, DolceInvoiceTableMap::COL_PAYMENT_MODE, DolceInvoiceTableMap::COL_PAYMENT_TYPE, DolceInvoiceTableMap::COL_PATIENT_PHONE, DolceInvoiceTableMap::COL_APP_VERSION, DolceInvoiceTableMap::COL_CREATED_AT, DolceInvoiceTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'tx_code', 'terminal', 'operator_code', 'operator_name', 'tx_date', 'total_amount_string', 'total_amount', 'payment_mode', 'payment_type', 'patient_phone', 'app_version', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TxCode' => 1, 'Terminal' => 2, 'OperatorCode' => 3, 'OperatorName' => 4, 'TxDate' => 5, 'TotalAmountString' => 6, 'TotalAmount' => 7, 'PaymentMode' => 8, 'PaymentType' => 9, 'PatientPhone' => 10, 'AppVersion' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'txCode' => 1, 'terminal' => 2, 'operatorCode' => 3, 'operatorName' => 4, 'txDate' => 5, 'totalAmountString' => 6, 'totalAmount' => 7, 'paymentMode' => 8, 'paymentType' => 9, 'patientPhone' => 10, 'appVersion' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(DolceInvoiceTableMap::COL_ID => 0, DolceInvoiceTableMap::COL_TX_CODE => 1, DolceInvoiceTableMap::COL_TERMINAL => 2, DolceInvoiceTableMap::COL_OPERATOR_CODE => 3, DolceInvoiceTableMap::COL_OPERATOR_NAME => 4, DolceInvoiceTableMap::COL_TX_DATE => 5, DolceInvoiceTableMap::COL_TOTAL_AMOUNT_STRING => 6, DolceInvoiceTableMap::COL_TOTAL_AMOUNT => 7, DolceInvoiceTableMap::COL_PAYMENT_MODE => 8, DolceInvoiceTableMap::COL_PAYMENT_TYPE => 9, DolceInvoiceTableMap::COL_PATIENT_PHONE => 10, DolceInvoiceTableMap::COL_APP_VERSION => 11, DolceInvoiceTableMap::COL_CREATED_AT => 12, DolceInvoiceTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'tx_code' => 1, 'terminal' => 2, 'operator_code' => 3, 'operator_name' => 4, 'tx_date' => 5, 'total_amount_string' => 6, 'total_amount' => 7, 'payment_mode' => 8, 'payment_type' => 9, 'patient_phone' => 10, 'app_version' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dolce_invoice');
        $this->setPhpName('DolceInvoice');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\DolceInvoice');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('tx_code', 'TxCode', 'BIGINT', true, 14, null);
        $this->addColumn('terminal', 'Terminal', 'BIGINT', true, 15, null);
        $this->addColumn('operator_code', 'OperatorCode', 'INTEGER', true, 40, null);
        $this->addColumn('operator_name', 'OperatorName', 'VARCHAR', true, 40, null);
        $this->addColumn('tx_date', 'TxDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('total_amount_string', 'TotalAmountString', 'VARCHAR', true, 40, null);
        $this->addColumn('total_amount', 'TotalAmount', 'DECIMAL', true, 20, null);
        $this->addColumn('payment_mode', 'PaymentMode', 'VARCHAR', true, 40, null);
        $this->addColumn('payment_type', 'PaymentType', 'VARCHAR', true, 40, null);
        $this->addColumn('patient_phone', 'PatientPhone', 'VARCHAR', true, 100, null);
        $this->addColumn('app_version', 'AppVersion', 'VARCHAR', true, 20, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Item', '\\DolceInvoiceItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':invoice_id',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', 'Items', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to dolce_invoice     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        DolceInvoiceItemTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DolceInvoiceTableMap::CLASS_DEFAULT : DolceInvoiceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DolceInvoice object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DolceInvoiceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DolceInvoiceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DolceInvoiceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DolceInvoiceTableMap::OM_CLASS;
            /** @var DolceInvoice $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DolceInvoiceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DolceInvoiceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DolceInvoiceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DolceInvoice $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DolceInvoiceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_ID);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_TX_CODE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_TERMINAL);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_OPERATOR_CODE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_OPERATOR_NAME);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_TX_DATE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_TOTAL_AMOUNT_STRING);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_TOTAL_AMOUNT);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_PAYMENT_MODE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_PAYMENT_TYPE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_PATIENT_PHONE);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_APP_VERSION);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(DolceInvoiceTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.tx_code');
            $criteria->addSelectColumn($alias . '.terminal');
            $criteria->addSelectColumn($alias . '.operator_code');
            $criteria->addSelectColumn($alias . '.operator_name');
            $criteria->addSelectColumn($alias . '.tx_date');
            $criteria->addSelectColumn($alias . '.total_amount_string');
            $criteria->addSelectColumn($alias . '.total_amount');
            $criteria->addSelectColumn($alias . '.payment_mode');
            $criteria->addSelectColumn($alias . '.payment_type');
            $criteria->addSelectColumn($alias . '.patient_phone');
            $criteria->addSelectColumn($alias . '.app_version');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DolceInvoiceTableMap::DATABASE_NAME)->getTable(DolceInvoiceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DolceInvoiceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DolceInvoiceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DolceInvoiceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DolceInvoice or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DolceInvoice object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DolceInvoiceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \DolceInvoice) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DolceInvoiceTableMap::DATABASE_NAME);
            $criteria->add(DolceInvoiceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DolceInvoiceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DolceInvoiceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DolceInvoiceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the dolce_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DolceInvoiceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DolceInvoice or Criteria object.
     *
     * @param mixed               $criteria Criteria or DolceInvoice object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DolceInvoiceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DolceInvoice object
        }

        if ($criteria->containsKey(DolceInvoiceTableMap::COL_ID) && $criteria->keyContainsValue(DolceInvoiceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DolceInvoiceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = DolceInvoiceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DolceInvoiceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DolceInvoiceTableMap::buildTableMap();
