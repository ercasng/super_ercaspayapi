<?php

namespace Map;

use \PaymentFctirsRecipients;
use \PaymentFctirsRecipientsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'payment_fctirs_recipients' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaymentFctirsRecipientsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaymentFctirsRecipientsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'payment_fctirs_recipients';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PaymentFctirsRecipients';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PaymentFctirsRecipients';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the payment_fctirs_id field
     */
    const COL_PAYMENT_FCTIRS_ID = 'payment_fctirs_recipients.payment_fctirs_id';

    /**
     * the column name for the recipient_id field
     */
    const COL_RECIPIENT_ID = 'payment_fctirs_recipients.recipient_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'payment_fctirs_recipients.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'payment_fctirs_recipients.updated_at';

    /**
     * the column name for the sent field
     */
    const COL_SENT = 'payment_fctirs_recipients.sent';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('PaymentFctirsId', 'RecipientId', 'CreatedAt', 'UpdatedAt', 'Sent', ),
        self::TYPE_CAMELNAME     => array('paymentFctirsId', 'recipientId', 'createdAt', 'updatedAt', 'sent', ),
        self::TYPE_COLNAME       => array(PaymentFctirsRecipientsTableMap::COL_PAYMENT_FCTIRS_ID, PaymentFctirsRecipientsTableMap::COL_RECIPIENT_ID, PaymentFctirsRecipientsTableMap::COL_CREATED_AT, PaymentFctirsRecipientsTableMap::COL_UPDATED_AT, PaymentFctirsRecipientsTableMap::COL_SENT, ),
        self::TYPE_FIELDNAME     => array('payment_fctirs_id', 'recipient_id', 'created_at', 'updated_at', 'sent', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('PaymentFctirsId' => 0, 'RecipientId' => 1, 'CreatedAt' => 2, 'UpdatedAt' => 3, 'Sent' => 4, ),
        self::TYPE_CAMELNAME     => array('paymentFctirsId' => 0, 'recipientId' => 1, 'createdAt' => 2, 'updatedAt' => 3, 'sent' => 4, ),
        self::TYPE_COLNAME       => array(PaymentFctirsRecipientsTableMap::COL_PAYMENT_FCTIRS_ID => 0, PaymentFctirsRecipientsTableMap::COL_RECIPIENT_ID => 1, PaymentFctirsRecipientsTableMap::COL_CREATED_AT => 2, PaymentFctirsRecipientsTableMap::COL_UPDATED_AT => 3, PaymentFctirsRecipientsTableMap::COL_SENT => 4, ),
        self::TYPE_FIELDNAME     => array('payment_fctirs_id' => 0, 'recipient_id' => 1, 'created_at' => 2, 'updated_at' => 3, 'sent' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('payment_fctirs_recipients');
        $this->setPhpName('PaymentFctirsRecipients');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PaymentFctirsRecipients');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('payment_fctirs_id', 'PaymentFctirsId', 'INTEGER' , 'payment_fctirs', 'id', true, 10, null);
        $this->addForeignPrimaryKey('recipient_id', 'RecipientId', 'INTEGER' , 'recipients', 'id', true, 10, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('sent', 'Sent', 'BOOLEAN', true, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PaymentFctirs', '\\PaymentFctirs', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':payment_fctirs_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Recipients', '\\Recipients', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':recipient_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \PaymentFctirsRecipients $obj A \PaymentFctirsRecipients object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getPaymentFctirsId() || is_scalar($obj->getPaymentFctirsId()) || is_callable([$obj->getPaymentFctirsId(), '__toString']) ? (string) $obj->getPaymentFctirsId() : $obj->getPaymentFctirsId()), (null === $obj->getRecipientId() || is_scalar($obj->getRecipientId()) || is_callable([$obj->getRecipientId(), '__toString']) ? (string) $obj->getRecipientId() : $obj->getRecipientId())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \PaymentFctirsRecipients object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \PaymentFctirsRecipients) {
                $key = serialize([(null === $value->getPaymentFctirsId() || is_scalar($value->getPaymentFctirsId()) || is_callable([$value->getPaymentFctirsId(), '__toString']) ? (string) $value->getPaymentFctirsId() : $value->getPaymentFctirsId()), (null === $value->getRecipientId() || is_scalar($value->getRecipientId()) || is_callable([$value->getRecipientId(), '__toString']) ? (string) $value->getRecipientId() : $value->getRecipientId())]);

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \PaymentFctirsRecipients object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('PaymentFctirsId', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('RecipientId', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaymentFctirsRecipientsTableMap::CLASS_DEFAULT : PaymentFctirsRecipientsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PaymentFctirsRecipients object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaymentFctirsRecipientsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaymentFctirsRecipientsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaymentFctirsRecipientsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaymentFctirsRecipientsTableMap::OM_CLASS;
            /** @var PaymentFctirsRecipients $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaymentFctirsRecipientsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaymentFctirsRecipientsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaymentFctirsRecipientsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PaymentFctirsRecipients $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaymentFctirsRecipientsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaymentFctirsRecipientsTableMap::COL_PAYMENT_FCTIRS_ID);
            $criteria->addSelectColumn(PaymentFctirsRecipientsTableMap::COL_RECIPIENT_ID);
            $criteria->addSelectColumn(PaymentFctirsRecipientsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PaymentFctirsRecipientsTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PaymentFctirsRecipientsTableMap::COL_SENT);
        } else {
            $criteria->addSelectColumn($alias . '.payment_fctirs_id');
            $criteria->addSelectColumn($alias . '.recipient_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.sent');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaymentFctirsRecipientsTableMap::DATABASE_NAME)->getTable(PaymentFctirsRecipientsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaymentFctirsRecipientsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaymentFctirsRecipientsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaymentFctirsRecipientsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PaymentFctirsRecipients or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PaymentFctirsRecipients object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsRecipientsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PaymentFctirsRecipients) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaymentFctirsRecipientsTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(PaymentFctirsRecipientsTableMap::COL_PAYMENT_FCTIRS_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(PaymentFctirsRecipientsTableMap::COL_RECIPIENT_ID, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = PaymentFctirsRecipientsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaymentFctirsRecipientsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaymentFctirsRecipientsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the payment_fctirs_recipients table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaymentFctirsRecipientsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PaymentFctirsRecipients or Criteria object.
     *
     * @param mixed               $criteria Criteria or PaymentFctirsRecipients object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctirsRecipientsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PaymentFctirsRecipients object
        }


        // Set the correct dbName
        $query = PaymentFctirsRecipientsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaymentFctirsRecipientsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaymentFctirsRecipientsTableMap::buildTableMap();
