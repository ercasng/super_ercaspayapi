<?php

namespace Map;

use \KedcoInvoice3;
use \KedcoInvoice3Query;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'kedco_invoice3' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class KedcoInvoice3TableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.KedcoInvoice3TableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'kedco_invoice3';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\KedcoInvoice3';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'KedcoInvoice3';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'kedco_invoice3.id';

    /**
     * the column name for the tx_code field
     */
    const COL_TX_CODE = 'kedco_invoice3.tx_code';

    /**
     * the column name for the terminal field
     */
    const COL_TERMINAL = 'kedco_invoice3.terminal';

    /**
     * the column name for the operator_code field
     */
    const COL_OPERATOR_CODE = 'kedco_invoice3.operator_code';

    /**
     * the column name for the operator_name field
     */
    const COL_OPERATOR_NAME = 'kedco_invoice3.operator_name';

    /**
     * the column name for the tx_date field
     */
    const COL_TX_DATE = 'kedco_invoice3.tx_date';

    /**
     * the column name for the tx_status field
     */
    const COL_TX_STATUS = 'kedco_invoice3.tx_status';

    /**
     * the column name for the total_amount_string field
     */
    const COL_TOTAL_AMOUNT_STRING = 'kedco_invoice3.total_amount_string';

    /**
     * the column name for the total_amount field
     */
    const COL_TOTAL_AMOUNT = 'kedco_invoice3.total_amount';

    /**
     * the column name for the payment_mode field
     */
    const COL_PAYMENT_MODE = 'kedco_invoice3.payment_mode';

    /**
     * the column name for the payment_type field
     */
    const COL_PAYMENT_TYPE = 'kedco_invoice3.payment_type';

    /**
     * the column name for the cust_meter_id field
     */
    const COL_CUST_METER_ID = 'kedco_invoice3.cust_meter_id';

    /**
     * the column name for the customer_name field
     */
    const COL_CUSTOMER_NAME = 'kedco_invoice3.customer_name';

    /**
     * the column name for the customer_phone field
     */
    const COL_CUSTOMER_PHONE = 'kedco_invoice3.customer_phone';

    /**
     * the column name for the app_version field
     */
    const COL_APP_VERSION = 'kedco_invoice3.app_version';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'kedco_invoice3.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'kedco_invoice3.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TxCode', 'Terminal', 'OperatorCode', 'OperatorName', 'TxDate', 'TxStatus', 'TotalAmountString', 'TotalAmount', 'PaymentMode', 'PaymentType', 'CustMeterId', 'CustomerName', 'CustomerPhone', 'AppVersion', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'txCode', 'terminal', 'operatorCode', 'operatorName', 'txDate', 'txStatus', 'totalAmountString', 'totalAmount', 'paymentMode', 'paymentType', 'custMeterId', 'customerName', 'customerPhone', 'appVersion', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(KedcoInvoice3TableMap::COL_ID, KedcoInvoice3TableMap::COL_TX_CODE, KedcoInvoice3TableMap::COL_TERMINAL, KedcoInvoice3TableMap::COL_OPERATOR_CODE, KedcoInvoice3TableMap::COL_OPERATOR_NAME, KedcoInvoice3TableMap::COL_TX_DATE, KedcoInvoice3TableMap::COL_TX_STATUS, KedcoInvoice3TableMap::COL_TOTAL_AMOUNT_STRING, KedcoInvoice3TableMap::COL_TOTAL_AMOUNT, KedcoInvoice3TableMap::COL_PAYMENT_MODE, KedcoInvoice3TableMap::COL_PAYMENT_TYPE, KedcoInvoice3TableMap::COL_CUST_METER_ID, KedcoInvoice3TableMap::COL_CUSTOMER_NAME, KedcoInvoice3TableMap::COL_CUSTOMER_PHONE, KedcoInvoice3TableMap::COL_APP_VERSION, KedcoInvoice3TableMap::COL_CREATED_AT, KedcoInvoice3TableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'tx_code', 'terminal', 'operator_code', 'operator_name', 'tx_date', 'tx_status', 'total_amount_string', 'total_amount', 'payment_mode', 'payment_type', 'cust_meter_id', 'customer_name', 'customer_phone', 'app_version', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TxCode' => 1, 'Terminal' => 2, 'OperatorCode' => 3, 'OperatorName' => 4, 'TxDate' => 5, 'TxStatus' => 6, 'TotalAmountString' => 7, 'TotalAmount' => 8, 'PaymentMode' => 9, 'PaymentType' => 10, 'CustMeterId' => 11, 'CustomerName' => 12, 'CustomerPhone' => 13, 'AppVersion' => 14, 'CreatedAt' => 15, 'UpdatedAt' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'txCode' => 1, 'terminal' => 2, 'operatorCode' => 3, 'operatorName' => 4, 'txDate' => 5, 'txStatus' => 6, 'totalAmountString' => 7, 'totalAmount' => 8, 'paymentMode' => 9, 'paymentType' => 10, 'custMeterId' => 11, 'customerName' => 12, 'customerPhone' => 13, 'appVersion' => 14, 'createdAt' => 15, 'updatedAt' => 16, ),
        self::TYPE_COLNAME       => array(KedcoInvoice3TableMap::COL_ID => 0, KedcoInvoice3TableMap::COL_TX_CODE => 1, KedcoInvoice3TableMap::COL_TERMINAL => 2, KedcoInvoice3TableMap::COL_OPERATOR_CODE => 3, KedcoInvoice3TableMap::COL_OPERATOR_NAME => 4, KedcoInvoice3TableMap::COL_TX_DATE => 5, KedcoInvoice3TableMap::COL_TX_STATUS => 6, KedcoInvoice3TableMap::COL_TOTAL_AMOUNT_STRING => 7, KedcoInvoice3TableMap::COL_TOTAL_AMOUNT => 8, KedcoInvoice3TableMap::COL_PAYMENT_MODE => 9, KedcoInvoice3TableMap::COL_PAYMENT_TYPE => 10, KedcoInvoice3TableMap::COL_CUST_METER_ID => 11, KedcoInvoice3TableMap::COL_CUSTOMER_NAME => 12, KedcoInvoice3TableMap::COL_CUSTOMER_PHONE => 13, KedcoInvoice3TableMap::COL_APP_VERSION => 14, KedcoInvoice3TableMap::COL_CREATED_AT => 15, KedcoInvoice3TableMap::COL_UPDATED_AT => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'tx_code' => 1, 'terminal' => 2, 'operator_code' => 3, 'operator_name' => 4, 'tx_date' => 5, 'tx_status' => 6, 'total_amount_string' => 7, 'total_amount' => 8, 'payment_mode' => 9, 'payment_type' => 10, 'cust_meter_id' => 11, 'customer_name' => 12, 'customer_phone' => 13, 'app_version' => 14, 'created_at' => 15, 'updated_at' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('kedco_invoice3');
        $this->setPhpName('KedcoInvoice3');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\KedcoInvoice3');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('tx_code', 'TxCode', 'VARCHAR', false, 100, null);
        $this->addColumn('terminal', 'Terminal', 'BIGINT', false, 15, null);
        $this->addColumn('operator_code', 'OperatorCode', 'VARCHAR', false, 150, null);
        $this->addColumn('operator_name', 'OperatorName', 'VARCHAR', false, 40, null);
        $this->addColumn('tx_date', 'TxDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('tx_status', 'TxStatus', 'VARCHAR', true, 255, null);
        $this->addColumn('total_amount_string', 'TotalAmountString', 'VARCHAR', false, 40, null);
        $this->addColumn('total_amount', 'TotalAmount', 'DECIMAL', false, 20, null);
        $this->addColumn('payment_mode', 'PaymentMode', 'VARCHAR', false, 40, null);
        $this->addColumn('payment_type', 'PaymentType', 'VARCHAR', false, 40, null);
        $this->addColumn('cust_meter_id', 'CustMeterId', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_name', 'CustomerName', 'VARCHAR', false, 200, null);
        $this->addColumn('customer_phone', 'CustomerPhone', 'VARCHAR', false, 100, null);
        $this->addColumn('app_version', 'AppVersion', 'VARCHAR', false, 20, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? KedcoInvoice3TableMap::CLASS_DEFAULT : KedcoInvoice3TableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (KedcoInvoice3 object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = KedcoInvoice3TableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = KedcoInvoice3TableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + KedcoInvoice3TableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = KedcoInvoice3TableMap::OM_CLASS;
            /** @var KedcoInvoice3 $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            KedcoInvoice3TableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = KedcoInvoice3TableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = KedcoInvoice3TableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var KedcoInvoice3 $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                KedcoInvoice3TableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_ID);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TX_CODE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TERMINAL);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_OPERATOR_CODE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_OPERATOR_NAME);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TX_DATE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TX_STATUS);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT_STRING);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_TOTAL_AMOUNT);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_PAYMENT_MODE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_PAYMENT_TYPE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_CUST_METER_ID);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_CUSTOMER_NAME);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_CUSTOMER_PHONE);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_APP_VERSION);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(KedcoInvoice3TableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.tx_code');
            $criteria->addSelectColumn($alias . '.terminal');
            $criteria->addSelectColumn($alias . '.operator_code');
            $criteria->addSelectColumn($alias . '.operator_name');
            $criteria->addSelectColumn($alias . '.tx_date');
            $criteria->addSelectColumn($alias . '.tx_status');
            $criteria->addSelectColumn($alias . '.total_amount_string');
            $criteria->addSelectColumn($alias . '.total_amount');
            $criteria->addSelectColumn($alias . '.payment_mode');
            $criteria->addSelectColumn($alias . '.payment_type');
            $criteria->addSelectColumn($alias . '.cust_meter_id');
            $criteria->addSelectColumn($alias . '.customer_name');
            $criteria->addSelectColumn($alias . '.customer_phone');
            $criteria->addSelectColumn($alias . '.app_version');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(KedcoInvoice3TableMap::DATABASE_NAME)->getTable(KedcoInvoice3TableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(KedcoInvoice3TableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(KedcoInvoice3TableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new KedcoInvoice3TableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a KedcoInvoice3 or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or KedcoInvoice3 object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice3TableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \KedcoInvoice3) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(KedcoInvoice3TableMap::DATABASE_NAME);
            $criteria->add(KedcoInvoice3TableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = KedcoInvoice3Query::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            KedcoInvoice3TableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                KedcoInvoice3TableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the kedco_invoice3 table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return KedcoInvoice3Query::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a KedcoInvoice3 or Criteria object.
     *
     * @param mixed               $criteria Criteria or KedcoInvoice3 object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoice3TableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from KedcoInvoice3 object
        }

        if ($criteria->containsKey(KedcoInvoice3TableMap::COL_ID) && $criteria->keyContainsValue(KedcoInvoice3TableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.KedcoInvoice3TableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = KedcoInvoice3Query::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // KedcoInvoice3TableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
KedcoInvoice3TableMap::buildTableMap();
