<?php

namespace Map;

use \KluUserKedco;
use \KluUserKedcoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'klu_user_kedco' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class KluUserKedcoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.KluUserKedcoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'klu_user_kedco';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\KluUserKedco';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'KluUserKedco';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'klu_user_kedco.id';

    /**
     * the column name for the passw field
     */
    const COL_PASSW = 'klu_user_kedco.passw';

    /**
     * the column name for the account_no field
     */
    const COL_ACCOUNT_NO = 'klu_user_kedco.account_no';

    /**
     * the column name for the customer_name field
     */
    const COL_CUSTOMER_NAME = 'klu_user_kedco.customer_name';

    /**
     * the column name for the customer_address field
     */
    const COL_CUSTOMER_ADDRESS = 'klu_user_kedco.customer_address';

    /**
     * the column name for the street field
     */
    const COL_STREET = 'klu_user_kedco.street';

    /**
     * the column name for the city field
     */
    const COL_CITY = 'klu_user_kedco.city';

    /**
     * the column name for the district field
     */
    const COL_DISTRICT = 'klu_user_kedco.district';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'klu_user_kedco.email';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'klu_user_kedco.phone';

    /**
     * the column name for the date_added field
     */
    const COL_DATE_ADDED = 'klu_user_kedco.date_added';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'klu_user_kedco.last_login';

    /**
     * the column name for the is_active field
     */
    const COL_IS_ACTIVE = 'klu_user_kedco.is_active';

    /**
     * the column name for the key field
     */
    const COL_KEY = 'klu_user_kedco.key';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Passw', 'AccountNo', 'CustomerName', 'CustomerAddress', 'Street', 'City', 'District', 'Email', 'Phone', 'DateAdded', 'LastLogin', 'IsActive', 'Key', ),
        self::TYPE_CAMELNAME     => array('id', 'passw', 'accountNo', 'customerName', 'customerAddress', 'street', 'city', 'district', 'email', 'phone', 'dateAdded', 'lastLogin', 'isActive', 'key', ),
        self::TYPE_COLNAME       => array(KluUserKedcoTableMap::COL_ID, KluUserKedcoTableMap::COL_PASSW, KluUserKedcoTableMap::COL_ACCOUNT_NO, KluUserKedcoTableMap::COL_CUSTOMER_NAME, KluUserKedcoTableMap::COL_CUSTOMER_ADDRESS, KluUserKedcoTableMap::COL_STREET, KluUserKedcoTableMap::COL_CITY, KluUserKedcoTableMap::COL_DISTRICT, KluUserKedcoTableMap::COL_EMAIL, KluUserKedcoTableMap::COL_PHONE, KluUserKedcoTableMap::COL_DATE_ADDED, KluUserKedcoTableMap::COL_LAST_LOGIN, KluUserKedcoTableMap::COL_IS_ACTIVE, KluUserKedcoTableMap::COL_KEY, ),
        self::TYPE_FIELDNAME     => array('id', 'passw', 'account_no', 'customer_name', 'customer_address', 'street', 'city', 'district', 'email', 'phone', 'date_added', 'last_login', 'is_active', 'key', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Passw' => 1, 'AccountNo' => 2, 'CustomerName' => 3, 'CustomerAddress' => 4, 'Street' => 5, 'City' => 6, 'District' => 7, 'Email' => 8, 'Phone' => 9, 'DateAdded' => 10, 'LastLogin' => 11, 'IsActive' => 12, 'Key' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'passw' => 1, 'accountNo' => 2, 'customerName' => 3, 'customerAddress' => 4, 'street' => 5, 'city' => 6, 'district' => 7, 'email' => 8, 'phone' => 9, 'dateAdded' => 10, 'lastLogin' => 11, 'isActive' => 12, 'key' => 13, ),
        self::TYPE_COLNAME       => array(KluUserKedcoTableMap::COL_ID => 0, KluUserKedcoTableMap::COL_PASSW => 1, KluUserKedcoTableMap::COL_ACCOUNT_NO => 2, KluUserKedcoTableMap::COL_CUSTOMER_NAME => 3, KluUserKedcoTableMap::COL_CUSTOMER_ADDRESS => 4, KluUserKedcoTableMap::COL_STREET => 5, KluUserKedcoTableMap::COL_CITY => 6, KluUserKedcoTableMap::COL_DISTRICT => 7, KluUserKedcoTableMap::COL_EMAIL => 8, KluUserKedcoTableMap::COL_PHONE => 9, KluUserKedcoTableMap::COL_DATE_ADDED => 10, KluUserKedcoTableMap::COL_LAST_LOGIN => 11, KluUserKedcoTableMap::COL_IS_ACTIVE => 12, KluUserKedcoTableMap::COL_KEY => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'passw' => 1, 'account_no' => 2, 'customer_name' => 3, 'customer_address' => 4, 'street' => 5, 'city' => 6, 'district' => 7, 'email' => 8, 'phone' => 9, 'date_added' => 10, 'last_login' => 11, 'is_active' => 12, 'key' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('klu_user_kedco');
        $this->setPhpName('KluUserKedco');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\KluUserKedco');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('passw', 'Passw', 'VARCHAR', true, 200, null);
        $this->addColumn('account_no', 'AccountNo', 'VARCHAR', true, 50, null);
        $this->addColumn('customer_name', 'CustomerName', 'VARCHAR', true, 150, null);
        $this->addColumn('customer_address', 'CustomerAddress', 'VARCHAR', true, 255, null);
        $this->addColumn('street', 'Street', 'VARCHAR', true, 255, null);
        $this->addColumn('city', 'City', 'VARCHAR', true, 255, null);
        $this->addColumn('district', 'District', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', true, 15, null);
        $this->addColumn('date_added', 'DateAdded', 'TIMESTAMP', true, null, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', true, null, null);
        $this->addColumn('is_active', 'IsActive', 'INTEGER', true, null, null);
        $this->addColumn('key', 'Key', 'VARCHAR', true, 6, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? KluUserKedcoTableMap::CLASS_DEFAULT : KluUserKedcoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (KluUserKedco object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = KluUserKedcoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = KluUserKedcoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + KluUserKedcoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = KluUserKedcoTableMap::OM_CLASS;
            /** @var KluUserKedco $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            KluUserKedcoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = KluUserKedcoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = KluUserKedcoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var KluUserKedco $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                KluUserKedcoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_ID);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_PASSW);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_ACCOUNT_NO);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_CUSTOMER_NAME);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_CUSTOMER_ADDRESS);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_STREET);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_CITY);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_DISTRICT);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_EMAIL);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_PHONE);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_DATE_ADDED);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_IS_ACTIVE);
            $criteria->addSelectColumn(KluUserKedcoTableMap::COL_KEY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.passw');
            $criteria->addSelectColumn($alias . '.account_no');
            $criteria->addSelectColumn($alias . '.customer_name');
            $criteria->addSelectColumn($alias . '.customer_address');
            $criteria->addSelectColumn($alias . '.street');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.district');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.date_added');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.is_active');
            $criteria->addSelectColumn($alias . '.key');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(KluUserKedcoTableMap::DATABASE_NAME)->getTable(KluUserKedcoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(KluUserKedcoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(KluUserKedcoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new KluUserKedcoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a KluUserKedco or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or KluUserKedco object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluUserKedcoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \KluUserKedco) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(KluUserKedcoTableMap::DATABASE_NAME);
            $criteria->add(KluUserKedcoTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = KluUserKedcoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            KluUserKedcoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                KluUserKedcoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the klu_user_kedco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return KluUserKedcoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a KluUserKedco or Criteria object.
     *
     * @param mixed               $criteria Criteria or KluUserKedco object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KluUserKedcoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from KluUserKedco object
        }

        if ($criteria->containsKey(KluUserKedcoTableMap::COL_ID) && $criteria->keyContainsValue(KluUserKedcoTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.KluUserKedcoTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = KluUserKedcoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // KluUserKedcoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
KluUserKedcoTableMap::buildTableMap();
