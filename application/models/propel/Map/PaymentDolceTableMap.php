<?php

namespace Map;

use \PaymentDolce;
use \PaymentDolceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'payment_dolce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaymentDolceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaymentDolceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'payment_dolce';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PaymentDolce';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PaymentDolce';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'payment_dolce.id';

    /**
     * the column name for the transaction_id field
     */
    const COL_TRANSACTION_ID = 'payment_dolce.transaction_id';

    /**
     * the column name for the product_id field
     */
    const COL_PRODUCT_ID = 'payment_dolce.product_id';

    /**
     * the column name for the event field
     */
    const COL_EVENT = 'payment_dolce.event';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'payment_dolce.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'payment_dolce.updated_at';

    /**
     * the column name for the transaction_date field
     */
    const COL_TRANSACTION_DATE = 'payment_dolce.transaction_date';

    /**
     * the column name for the customer_name field
     */
    const COL_CUSTOMER_NAME = 'payment_dolce.customer_name';

    /**
     * the column name for the amount_paid field
     */
    const COL_AMOUNT_PAID = 'payment_dolce.amount_paid';

    /**
     * the column name for the source_bank field
     */
    const COL_SOURCE_BANK = 'payment_dolce.source_bank';

    /**
     * the column name for the destination_bank field
     */
    const COL_DESTINATION_BANK = 'payment_dolce.destination_bank';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'payment_dolce.phone';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'payment_dolce.email';

    /**
     * the column name for the payment_channel field
     */
    const COL_PAYMENT_CHANNEL = 'payment_dolce.payment_channel';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TransactionId', 'ProductId', 'Event', 'CreatedAt', 'UpdatedAt', 'TransactionDate', 'CustomerName', 'AmountPaid', 'SourceBank', 'DestinationBank', 'Phone', 'Email', 'PaymentChannel', ),
        self::TYPE_CAMELNAME     => array('id', 'transactionId', 'productId', 'event', 'createdAt', 'updatedAt', 'transactionDate', 'customerName', 'amountPaid', 'sourceBank', 'destinationBank', 'phone', 'email', 'paymentChannel', ),
        self::TYPE_COLNAME       => array(PaymentDolceTableMap::COL_ID, PaymentDolceTableMap::COL_TRANSACTION_ID, PaymentDolceTableMap::COL_PRODUCT_ID, PaymentDolceTableMap::COL_EVENT, PaymentDolceTableMap::COL_CREATED_AT, PaymentDolceTableMap::COL_UPDATED_AT, PaymentDolceTableMap::COL_TRANSACTION_DATE, PaymentDolceTableMap::COL_CUSTOMER_NAME, PaymentDolceTableMap::COL_AMOUNT_PAID, PaymentDolceTableMap::COL_SOURCE_BANK, PaymentDolceTableMap::COL_DESTINATION_BANK, PaymentDolceTableMap::COL_PHONE, PaymentDolceTableMap::COL_EMAIL, PaymentDolceTableMap::COL_PAYMENT_CHANNEL, ),
        self::TYPE_FIELDNAME     => array('id', 'transaction_id', 'product_id', 'event', 'created_at', 'updated_at', 'transaction_date', 'customer_name', 'amount_paid', 'source_bank', 'destination_bank', 'phone', 'email', 'payment_channel', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TransactionId' => 1, 'ProductId' => 2, 'Event' => 3, 'CreatedAt' => 4, 'UpdatedAt' => 5, 'TransactionDate' => 6, 'CustomerName' => 7, 'AmountPaid' => 8, 'SourceBank' => 9, 'DestinationBank' => 10, 'Phone' => 11, 'Email' => 12, 'PaymentChannel' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'transactionId' => 1, 'productId' => 2, 'event' => 3, 'createdAt' => 4, 'updatedAt' => 5, 'transactionDate' => 6, 'customerName' => 7, 'amountPaid' => 8, 'sourceBank' => 9, 'destinationBank' => 10, 'phone' => 11, 'email' => 12, 'paymentChannel' => 13, ),
        self::TYPE_COLNAME       => array(PaymentDolceTableMap::COL_ID => 0, PaymentDolceTableMap::COL_TRANSACTION_ID => 1, PaymentDolceTableMap::COL_PRODUCT_ID => 2, PaymentDolceTableMap::COL_EVENT => 3, PaymentDolceTableMap::COL_CREATED_AT => 4, PaymentDolceTableMap::COL_UPDATED_AT => 5, PaymentDolceTableMap::COL_TRANSACTION_DATE => 6, PaymentDolceTableMap::COL_CUSTOMER_NAME => 7, PaymentDolceTableMap::COL_AMOUNT_PAID => 8, PaymentDolceTableMap::COL_SOURCE_BANK => 9, PaymentDolceTableMap::COL_DESTINATION_BANK => 10, PaymentDolceTableMap::COL_PHONE => 11, PaymentDolceTableMap::COL_EMAIL => 12, PaymentDolceTableMap::COL_PAYMENT_CHANNEL => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'transaction_id' => 1, 'product_id' => 2, 'event' => 3, 'created_at' => 4, 'updated_at' => 5, 'transaction_date' => 6, 'customer_name' => 7, 'amount_paid' => 8, 'source_bank' => 9, 'destination_bank' => 10, 'phone' => 11, 'email' => 12, 'payment_channel' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('payment_dolce');
        $this->setPhpName('PaymentDolce');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PaymentDolce');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('transaction_id', 'TransactionId', 'VARCHAR', true, 255, null);
        $this->addColumn('product_id', 'ProductId', 'VARCHAR', true, 255, null);
        $this->addColumn('event', 'Event', 'VARCHAR', true, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('transaction_date', 'TransactionDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('customer_name', 'CustomerName', 'VARCHAR', true, 255, null);
        $this->addColumn('amount_paid', 'AmountPaid', 'DECIMAL', true, 20, null);
        $this->addColumn('source_bank', 'SourceBank', 'VARCHAR', true, 255, null);
        $this->addColumn('destination_bank', 'DestinationBank', 'VARCHAR', false, 255, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', true, 15, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('payment_channel', 'PaymentChannel', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaymentDolceTableMap::CLASS_DEFAULT : PaymentDolceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PaymentDolce object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaymentDolceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaymentDolceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaymentDolceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaymentDolceTableMap::OM_CLASS;
            /** @var PaymentDolce $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaymentDolceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaymentDolceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaymentDolceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PaymentDolce $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaymentDolceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_ID);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_TRANSACTION_ID);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_PRODUCT_ID);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_EVENT);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_TRANSACTION_DATE);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_CUSTOMER_NAME);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_AMOUNT_PAID);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_SOURCE_BANK);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_DESTINATION_BANK);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_PHONE);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_EMAIL);
            $criteria->addSelectColumn(PaymentDolceTableMap::COL_PAYMENT_CHANNEL);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.transaction_id');
            $criteria->addSelectColumn($alias . '.product_id');
            $criteria->addSelectColumn($alias . '.event');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.transaction_date');
            $criteria->addSelectColumn($alias . '.customer_name');
            $criteria->addSelectColumn($alias . '.amount_paid');
            $criteria->addSelectColumn($alias . '.source_bank');
            $criteria->addSelectColumn($alias . '.destination_bank');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.payment_channel');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaymentDolceTableMap::DATABASE_NAME)->getTable(PaymentDolceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaymentDolceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaymentDolceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaymentDolceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PaymentDolce or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PaymentDolce object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentDolceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PaymentDolce) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaymentDolceTableMap::DATABASE_NAME);
            $criteria->add(PaymentDolceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PaymentDolceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaymentDolceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaymentDolceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the payment_dolce table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaymentDolceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PaymentDolce or Criteria object.
     *
     * @param mixed               $criteria Criteria or PaymentDolce object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentDolceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PaymentDolce object
        }

        if ($criteria->containsKey(PaymentDolceTableMap::COL_ID) && $criteria->keyContainsValue(PaymentDolceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PaymentDolceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PaymentDolceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaymentDolceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaymentDolceTableMap::buildTableMap();
