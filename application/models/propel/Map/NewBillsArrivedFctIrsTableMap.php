<?php

namespace Map;

use \NewBillsArrivedFctIrs;
use \NewBillsArrivedFctIrsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'new_bills_arrived_fct_irs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class NewBillsArrivedFctIrsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.NewBillsArrivedFctIrsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'new_bills_arrived_fct_irs';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\NewBillsArrivedFctIrs';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'NewBillsArrivedFctIrs';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the id field
     */
    const COL_ID = 'new_bills_arrived_fct_irs.id';

    /**
     * the column name for the is_admin_notified field
     */
    const COL_IS_ADMIN_NOTIFIED = 'new_bills_arrived_fct_irs.is_admin_notified';

    /**
     * the column name for the is_migrated field
     */
    const COL_IS_MIGRATED = 'new_bills_arrived_fct_irs.is_migrated';

    /**
     * the column name for the is_seen field
     */
    const COL_IS_SEEN = 'new_bills_arrived_fct_irs.is_seen';

    /**
     * the column name for the time_stamp field
     */
    const COL_TIME_STAMP = 'new_bills_arrived_fct_irs.time_stamp';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'IsAdminNotified', 'IsMigrated', 'IsSeen', 'TimeStamp', ),
        self::TYPE_CAMELNAME     => array('id', 'isAdminNotified', 'isMigrated', 'isSeen', 'timeStamp', ),
        self::TYPE_COLNAME       => array(NewBillsArrivedFctIrsTableMap::COL_ID, NewBillsArrivedFctIrsTableMap::COL_IS_ADMIN_NOTIFIED, NewBillsArrivedFctIrsTableMap::COL_IS_MIGRATED, NewBillsArrivedFctIrsTableMap::COL_IS_SEEN, NewBillsArrivedFctIrsTableMap::COL_TIME_STAMP, ),
        self::TYPE_FIELDNAME     => array('id', 'is_admin_notified', 'is_migrated', 'is_seen', 'time_stamp', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'IsAdminNotified' => 1, 'IsMigrated' => 2, 'IsSeen' => 3, 'TimeStamp' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'isAdminNotified' => 1, 'isMigrated' => 2, 'isSeen' => 3, 'timeStamp' => 4, ),
        self::TYPE_COLNAME       => array(NewBillsArrivedFctIrsTableMap::COL_ID => 0, NewBillsArrivedFctIrsTableMap::COL_IS_ADMIN_NOTIFIED => 1, NewBillsArrivedFctIrsTableMap::COL_IS_MIGRATED => 2, NewBillsArrivedFctIrsTableMap::COL_IS_SEEN => 3, NewBillsArrivedFctIrsTableMap::COL_TIME_STAMP => 4, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'is_admin_notified' => 1, 'is_migrated' => 2, 'is_seen' => 3, 'time_stamp' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('new_bills_arrived_fct_irs');
        $this->setPhpName('NewBillsArrivedFctIrs');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\NewBillsArrivedFctIrs');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('is_admin_notified', 'IsAdminNotified', 'BOOLEAN', true, 1, null);
        $this->addColumn('is_migrated', 'IsMigrated', 'BOOLEAN', true, 1, null);
        $this->addColumn('is_seen', 'IsSeen', 'BOOLEAN', true, 1, false);
        $this->addColumn('time_stamp', 'TimeStamp', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? NewBillsArrivedFctIrsTableMap::CLASS_DEFAULT : NewBillsArrivedFctIrsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (NewBillsArrivedFctIrs object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = NewBillsArrivedFctIrsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = NewBillsArrivedFctIrsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + NewBillsArrivedFctIrsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = NewBillsArrivedFctIrsTableMap::OM_CLASS;
            /** @var NewBillsArrivedFctIrs $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            NewBillsArrivedFctIrsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = NewBillsArrivedFctIrsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = NewBillsArrivedFctIrsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var NewBillsArrivedFctIrs $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                NewBillsArrivedFctIrsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(NewBillsArrivedFctIrsTableMap::COL_ID);
            $criteria->addSelectColumn(NewBillsArrivedFctIrsTableMap::COL_IS_ADMIN_NOTIFIED);
            $criteria->addSelectColumn(NewBillsArrivedFctIrsTableMap::COL_IS_MIGRATED);
            $criteria->addSelectColumn(NewBillsArrivedFctIrsTableMap::COL_IS_SEEN);
            $criteria->addSelectColumn(NewBillsArrivedFctIrsTableMap::COL_TIME_STAMP);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.is_admin_notified');
            $criteria->addSelectColumn($alias . '.is_migrated');
            $criteria->addSelectColumn($alias . '.is_seen');
            $criteria->addSelectColumn($alias . '.time_stamp');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(NewBillsArrivedFctIrsTableMap::DATABASE_NAME)->getTable(NewBillsArrivedFctIrsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(NewBillsArrivedFctIrsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(NewBillsArrivedFctIrsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new NewBillsArrivedFctIrsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a NewBillsArrivedFctIrs or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or NewBillsArrivedFctIrs object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NewBillsArrivedFctIrsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \NewBillsArrivedFctIrs) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(NewBillsArrivedFctIrsTableMap::DATABASE_NAME);
            $criteria->add(NewBillsArrivedFctIrsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = NewBillsArrivedFctIrsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            NewBillsArrivedFctIrsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                NewBillsArrivedFctIrsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the new_bills_arrived_fct_irs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return NewBillsArrivedFctIrsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a NewBillsArrivedFctIrs or Criteria object.
     *
     * @param mixed               $criteria Criteria or NewBillsArrivedFctIrs object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NewBillsArrivedFctIrsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from NewBillsArrivedFctIrs object
        }

        if ($criteria->containsKey(NewBillsArrivedFctIrsTableMap::COL_ID) && $criteria->keyContainsValue(NewBillsArrivedFctIrsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.NewBillsArrivedFctIrsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = NewBillsArrivedFctIrsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // NewBillsArrivedFctIrsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
NewBillsArrivedFctIrsTableMap::buildTableMap();
