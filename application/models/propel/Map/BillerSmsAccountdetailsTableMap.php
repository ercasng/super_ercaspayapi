<?php

namespace Map;

use \BillerSmsAccountdetails;
use \BillerSmsAccountdetailsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'biller_sms_accountdetails' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BillerSmsAccountdetailsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.BillerSmsAccountdetailsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'biller_sms_accountdetails';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\BillerSmsAccountdetails';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'BillerSmsAccountdetails';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the id field
     */
    const COL_ID = 'biller_sms_accountdetails.id';

    /**
     * the column name for the biller_id field
     */
    const COL_BILLER_ID = 'biller_sms_accountdetails.biller_id';

    /**
     * the column name for the biller_name field
     */
    const COL_BILLER_NAME = 'biller_sms_accountdetails.biller_name';

    /**
     * the column name for the account_owner field
     */
    const COL_ACCOUNT_OWNER = 'biller_sms_accountdetails.account_owner';

    /**
     * the column name for the subaccount field
     */
    const COL_SUBACCOUNT = 'biller_sms_accountdetails.subaccount';

    /**
     * the column name for the subaccount_password field
     */
    const COL_SUBACCOUNT_PASSWORD = 'biller_sms_accountdetails.subaccount_password';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'BillerId', 'BillerName', 'AccountOwner', 'Subaccount', 'SubaccountPassword', ),
        self::TYPE_CAMELNAME     => array('id', 'billerId', 'billerName', 'accountOwner', 'subaccount', 'subaccountPassword', ),
        self::TYPE_COLNAME       => array(BillerSmsAccountdetailsTableMap::COL_ID, BillerSmsAccountdetailsTableMap::COL_BILLER_ID, BillerSmsAccountdetailsTableMap::COL_BILLER_NAME, BillerSmsAccountdetailsTableMap::COL_ACCOUNT_OWNER, BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT, BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT_PASSWORD, ),
        self::TYPE_FIELDNAME     => array('id', 'biller_id', 'biller_name', 'account_owner', 'subaccount', 'subaccount_password', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'BillerId' => 1, 'BillerName' => 2, 'AccountOwner' => 3, 'Subaccount' => 4, 'SubaccountPassword' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'billerId' => 1, 'billerName' => 2, 'accountOwner' => 3, 'subaccount' => 4, 'subaccountPassword' => 5, ),
        self::TYPE_COLNAME       => array(BillerSmsAccountdetailsTableMap::COL_ID => 0, BillerSmsAccountdetailsTableMap::COL_BILLER_ID => 1, BillerSmsAccountdetailsTableMap::COL_BILLER_NAME => 2, BillerSmsAccountdetailsTableMap::COL_ACCOUNT_OWNER => 3, BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT => 4, BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT_PASSWORD => 5, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'biller_id' => 1, 'biller_name' => 2, 'account_owner' => 3, 'subaccount' => 4, 'subaccount_password' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('biller_sms_accountdetails');
        $this->setPhpName('BillerSmsAccountdetails');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\BillerSmsAccountdetails');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 5, null);
        $this->addColumn('biller_id', 'BillerId', 'INTEGER', true, 5, null);
        $this->addColumn('biller_name', 'BillerName', 'VARCHAR', true, 255, null);
        $this->addColumn('account_owner', 'AccountOwner', 'VARCHAR', true, 255, null);
        $this->addColumn('subaccount', 'Subaccount', 'VARCHAR', true, 255, null);
        $this->addColumn('subaccount_password', 'SubaccountPassword', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BillerSmsAccountdetailsTableMap::CLASS_DEFAULT : BillerSmsAccountdetailsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BillerSmsAccountdetails object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BillerSmsAccountdetailsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BillerSmsAccountdetailsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BillerSmsAccountdetailsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BillerSmsAccountdetailsTableMap::OM_CLASS;
            /** @var BillerSmsAccountdetails $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BillerSmsAccountdetailsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BillerSmsAccountdetailsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BillerSmsAccountdetailsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BillerSmsAccountdetails $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BillerSmsAccountdetailsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_ID);
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_BILLER_ID);
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_BILLER_NAME);
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_ACCOUNT_OWNER);
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT);
            $criteria->addSelectColumn(BillerSmsAccountdetailsTableMap::COL_SUBACCOUNT_PASSWORD);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.biller_id');
            $criteria->addSelectColumn($alias . '.biller_name');
            $criteria->addSelectColumn($alias . '.account_owner');
            $criteria->addSelectColumn($alias . '.subaccount');
            $criteria->addSelectColumn($alias . '.subaccount_password');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BillerSmsAccountdetailsTableMap::DATABASE_NAME)->getTable(BillerSmsAccountdetailsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BillerSmsAccountdetailsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BillerSmsAccountdetailsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BillerSmsAccountdetails or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BillerSmsAccountdetails object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \BillerSmsAccountdetails) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
            $criteria->add(BillerSmsAccountdetailsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BillerSmsAccountdetailsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BillerSmsAccountdetailsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BillerSmsAccountdetailsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the biller_sms_accountdetails table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BillerSmsAccountdetailsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BillerSmsAccountdetails or Criteria object.
     *
     * @param mixed               $criteria Criteria or BillerSmsAccountdetails object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BillerSmsAccountdetailsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BillerSmsAccountdetails object
        }

        if ($criteria->containsKey(BillerSmsAccountdetailsTableMap::COL_ID) && $criteria->keyContainsValue(BillerSmsAccountdetailsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BillerSmsAccountdetailsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BillerSmsAccountdetailsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BillerSmsAccountdetailsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BillerSmsAccountdetailsTableMap::buildTableMap();
