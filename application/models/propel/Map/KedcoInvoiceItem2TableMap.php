<?php

namespace Map;

use \KedcoInvoiceItem2;
use \KedcoInvoiceItem2Query;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'kedco_invoice_item2' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class KedcoInvoiceItem2TableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.KedcoInvoiceItem2TableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'kedco_invoice_item2';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\KedcoInvoiceItem2';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'KedcoInvoiceItem2';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'kedco_invoice_item2.id';

    /**
     * the column name for the service_name field
     */
    const COL_SERVICE_NAME = 'kedco_invoice_item2.service_name';

    /**
     * the column name for the service_paidfor_name field
     */
    const COL_SERVICE_PAIDFOR_NAME = 'kedco_invoice_item2.service_paidfor_name';

    /**
     * the column name for the service_paidfor_code field
     */
    const COL_SERVICE_PAIDFOR_CODE = 'kedco_invoice_item2.service_paidfor_code';

    /**
     * the column name for the unit_name field
     */
    const COL_UNIT_NAME = 'kedco_invoice_item2.unit_name';

    /**
     * the column name for the unit_code field
     */
    const COL_UNIT_CODE = 'kedco_invoice_item2.unit_code';

    /**
     * the column name for the service_fixed_price field
     */
    const COL_SERVICE_FIXED_PRICE = 'kedco_invoice_item2.service_fixed_price';

    /**
     * the column name for the invoice_id field
     */
    const COL_INVOICE_ID = 'kedco_invoice_item2.invoice_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'kedco_invoice_item2.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'kedco_invoice_item2.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ServiceName', 'ServicePaidforName', 'ServicePaidforCode', 'UnitName', 'UnitCode', 'ServiceFixedPrice', 'InvoiceId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'serviceName', 'servicePaidforName', 'servicePaidforCode', 'unitName', 'unitCode', 'serviceFixedPrice', 'invoiceId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(KedcoInvoiceItem2TableMap::COL_ID, KedcoInvoiceItem2TableMap::COL_SERVICE_NAME, KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_NAME, KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_CODE, KedcoInvoiceItem2TableMap::COL_UNIT_NAME, KedcoInvoiceItem2TableMap::COL_UNIT_CODE, KedcoInvoiceItem2TableMap::COL_SERVICE_FIXED_PRICE, KedcoInvoiceItem2TableMap::COL_INVOICE_ID, KedcoInvoiceItem2TableMap::COL_CREATED_AT, KedcoInvoiceItem2TableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'service_name', 'service_paidfor_name', 'service_paidfor_code', 'unit_name', 'unit_code', 'service_fixed_price', 'invoice_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ServiceName' => 1, 'ServicePaidforName' => 2, 'ServicePaidforCode' => 3, 'UnitName' => 4, 'UnitCode' => 5, 'ServiceFixedPrice' => 6, 'InvoiceId' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'serviceName' => 1, 'servicePaidforName' => 2, 'servicePaidforCode' => 3, 'unitName' => 4, 'unitCode' => 5, 'serviceFixedPrice' => 6, 'invoiceId' => 7, 'createdAt' => 8, 'updatedAt' => 9, ),
        self::TYPE_COLNAME       => array(KedcoInvoiceItem2TableMap::COL_ID => 0, KedcoInvoiceItem2TableMap::COL_SERVICE_NAME => 1, KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_NAME => 2, KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_CODE => 3, KedcoInvoiceItem2TableMap::COL_UNIT_NAME => 4, KedcoInvoiceItem2TableMap::COL_UNIT_CODE => 5, KedcoInvoiceItem2TableMap::COL_SERVICE_FIXED_PRICE => 6, KedcoInvoiceItem2TableMap::COL_INVOICE_ID => 7, KedcoInvoiceItem2TableMap::COL_CREATED_AT => 8, KedcoInvoiceItem2TableMap::COL_UPDATED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'service_name' => 1, 'service_paidfor_name' => 2, 'service_paidfor_code' => 3, 'unit_name' => 4, 'unit_code' => 5, 'service_fixed_price' => 6, 'invoice_id' => 7, 'created_at' => 8, 'updated_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('kedco_invoice_item2');
        $this->setPhpName('KedcoInvoiceItem2');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\KedcoInvoiceItem2');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('service_name', 'ServiceName', 'VARCHAR', false, 40, null);
        $this->addColumn('service_paidfor_name', 'ServicePaidforName', 'VARCHAR', false, 40, null);
        $this->addColumn('service_paidfor_code', 'ServicePaidforCode', 'VARCHAR', false, 255, null);
        $this->addColumn('unit_name', 'UnitName', 'VARCHAR', false, 40, null);
        $this->addColumn('unit_code', 'UnitCode', 'INTEGER', false, 6, null);
        $this->addColumn('service_fixed_price', 'ServiceFixedPrice', 'DECIMAL', false, 20, null);
        $this->addColumn('invoice_id', 'InvoiceId', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? KedcoInvoiceItem2TableMap::CLASS_DEFAULT : KedcoInvoiceItem2TableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (KedcoInvoiceItem2 object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = KedcoInvoiceItem2TableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = KedcoInvoiceItem2TableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + KedcoInvoiceItem2TableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = KedcoInvoiceItem2TableMap::OM_CLASS;
            /** @var KedcoInvoiceItem2 $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            KedcoInvoiceItem2TableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = KedcoInvoiceItem2TableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = KedcoInvoiceItem2TableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var KedcoInvoiceItem2 $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                KedcoInvoiceItem2TableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_ID);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_SERVICE_NAME);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_NAME);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_SERVICE_PAIDFOR_CODE);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_UNIT_NAME);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_UNIT_CODE);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_SERVICE_FIXED_PRICE);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_INVOICE_ID);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(KedcoInvoiceItem2TableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.service_name');
            $criteria->addSelectColumn($alias . '.service_paidfor_name');
            $criteria->addSelectColumn($alias . '.service_paidfor_code');
            $criteria->addSelectColumn($alias . '.unit_name');
            $criteria->addSelectColumn($alias . '.unit_code');
            $criteria->addSelectColumn($alias . '.service_fixed_price');
            $criteria->addSelectColumn($alias . '.invoice_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(KedcoInvoiceItem2TableMap::DATABASE_NAME)->getTable(KedcoInvoiceItem2TableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(KedcoInvoiceItem2TableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(KedcoInvoiceItem2TableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new KedcoInvoiceItem2TableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a KedcoInvoiceItem2 or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or KedcoInvoiceItem2 object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoiceItem2TableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \KedcoInvoiceItem2) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(KedcoInvoiceItem2TableMap::DATABASE_NAME);
            $criteria->add(KedcoInvoiceItem2TableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = KedcoInvoiceItem2Query::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            KedcoInvoiceItem2TableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                KedcoInvoiceItem2TableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the kedco_invoice_item2 table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return KedcoInvoiceItem2Query::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a KedcoInvoiceItem2 or Criteria object.
     *
     * @param mixed               $criteria Criteria or KedcoInvoiceItem2 object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KedcoInvoiceItem2TableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from KedcoInvoiceItem2 object
        }

        if ($criteria->containsKey(KedcoInvoiceItem2TableMap::COL_ID) && $criteria->keyContainsValue(KedcoInvoiceItem2TableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.KedcoInvoiceItem2TableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = KedcoInvoiceItem2Query::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // KedcoInvoiceItem2TableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
KedcoInvoiceItem2TableMap::buildTableMap();
