<?php

namespace Map;

use \PaymentFctwb;
use \PaymentFctwbQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'payment_fctwb' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaymentFctwbTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaymentFctwbTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'payment_fctwb';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PaymentFctwb';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PaymentFctwb';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'payment_fctwb.id';

    /**
     * the column name for the account_no field
     */
    const COL_ACCOUNT_NO = 'payment_fctwb.account_no';

    /**
     * the column name for the TransactionID field
     */
    const COL_TRANSACTIONID = 'payment_fctwb.TransactionID';

    /**
     * the column name for the product_id field
     */
    const COL_PRODUCT_ID = 'payment_fctwb.product_id';

    /**
     * the column name for the SourceBank field
     */
    const COL_SOURCEBANK = 'payment_fctwb.SourceBank';

    /**
     * the column name for the DestinationBank field
     */
    const COL_DESTINATIONBANK = 'payment_fctwb.DestinationBank';

    /**
     * the column name for the Email field
     */
    const COL_EMAIL = 'payment_fctwb.Email';

    /**
     * the column name for the Phone field
     */
    const COL_PHONE = 'payment_fctwb.Phone';

    /**
     * the column name for the TransactionDate field
     */
    const COL_TRANSACTIONDATE = 'payment_fctwb.TransactionDate';

    /**
     * the column name for the amount_paid field
     */
    const COL_AMOUNT_PAID = 'payment_fctwb.amount_paid';

    /**
     * the column name for the PaymentChannel field
     */
    const COL_PAYMENTCHANNEL = 'payment_fctwb.PaymentChannel';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'payment_fctwb.created_date';

    /**
     * the column name for the uploadstatus field
     */
    const COL_UPLOADSTATUS = 'payment_fctwb.uploadstatus';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'AccountNo', 'Transactionid', 'ProductId', 'Sourcebank', 'Destinationbank', 'Email', 'Phone', 'Transactiondate', 'AmountPaid', 'Paymentchannel', 'CreatedDate', 'Uploadstatus', ),
        self::TYPE_CAMELNAME     => array('id', 'accountNo', 'transactionid', 'productId', 'sourcebank', 'destinationbank', 'email', 'phone', 'transactiondate', 'amountPaid', 'paymentchannel', 'createdDate', 'uploadstatus', ),
        self::TYPE_COLNAME       => array(PaymentFctwbTableMap::COL_ID, PaymentFctwbTableMap::COL_ACCOUNT_NO, PaymentFctwbTableMap::COL_TRANSACTIONID, PaymentFctwbTableMap::COL_PRODUCT_ID, PaymentFctwbTableMap::COL_SOURCEBANK, PaymentFctwbTableMap::COL_DESTINATIONBANK, PaymentFctwbTableMap::COL_EMAIL, PaymentFctwbTableMap::COL_PHONE, PaymentFctwbTableMap::COL_TRANSACTIONDATE, PaymentFctwbTableMap::COL_AMOUNT_PAID, PaymentFctwbTableMap::COL_PAYMENTCHANNEL, PaymentFctwbTableMap::COL_CREATED_DATE, PaymentFctwbTableMap::COL_UPLOADSTATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'account_no', 'TransactionID', 'product_id', 'SourceBank', 'DestinationBank', 'Email', 'Phone', 'TransactionDate', 'amount_paid', 'PaymentChannel', 'created_date', 'uploadstatus', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'AccountNo' => 1, 'Transactionid' => 2, 'ProductId' => 3, 'Sourcebank' => 4, 'Destinationbank' => 5, 'Email' => 6, 'Phone' => 7, 'Transactiondate' => 8, 'AmountPaid' => 9, 'Paymentchannel' => 10, 'CreatedDate' => 11, 'Uploadstatus' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'accountNo' => 1, 'transactionid' => 2, 'productId' => 3, 'sourcebank' => 4, 'destinationbank' => 5, 'email' => 6, 'phone' => 7, 'transactiondate' => 8, 'amountPaid' => 9, 'paymentchannel' => 10, 'createdDate' => 11, 'uploadstatus' => 12, ),
        self::TYPE_COLNAME       => array(PaymentFctwbTableMap::COL_ID => 0, PaymentFctwbTableMap::COL_ACCOUNT_NO => 1, PaymentFctwbTableMap::COL_TRANSACTIONID => 2, PaymentFctwbTableMap::COL_PRODUCT_ID => 3, PaymentFctwbTableMap::COL_SOURCEBANK => 4, PaymentFctwbTableMap::COL_DESTINATIONBANK => 5, PaymentFctwbTableMap::COL_EMAIL => 6, PaymentFctwbTableMap::COL_PHONE => 7, PaymentFctwbTableMap::COL_TRANSACTIONDATE => 8, PaymentFctwbTableMap::COL_AMOUNT_PAID => 9, PaymentFctwbTableMap::COL_PAYMENTCHANNEL => 10, PaymentFctwbTableMap::COL_CREATED_DATE => 11, PaymentFctwbTableMap::COL_UPLOADSTATUS => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'account_no' => 1, 'TransactionID' => 2, 'product_id' => 3, 'SourceBank' => 4, 'DestinationBank' => 5, 'Email' => 6, 'Phone' => 7, 'TransactionDate' => 8, 'amount_paid' => 9, 'PaymentChannel' => 10, 'created_date' => 11, 'uploadstatus' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('payment_fctwb');
        $this->setPhpName('PaymentFctwb');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PaymentFctwb');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('account_no', 'AccountNo', 'VARCHAR', true, 50, null);
        $this->addColumn('TransactionID', 'Transactionid', 'VARCHAR', true, 255, null);
        $this->addColumn('product_id', 'ProductId', 'INTEGER', true, 10, null);
        $this->addColumn('SourceBank', 'Sourcebank', 'VARCHAR', true, 255, null);
        $this->addColumn('DestinationBank', 'Destinationbank', 'VARCHAR', false, 255, null);
        $this->addColumn('Email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('Phone', 'Phone', 'VARCHAR', true, 15, null);
        $this->addColumn('TransactionDate', 'Transactiondate', 'TIMESTAMP', true, null, null);
        $this->addColumn('amount_paid', 'AmountPaid', 'DECIMAL', true, 20, null);
        $this->addColumn('PaymentChannel', 'Paymentchannel', 'VARCHAR', true, 255, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('uploadstatus', 'Uploadstatus', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaymentFctwbTableMap::CLASS_DEFAULT : PaymentFctwbTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PaymentFctwb object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaymentFctwbTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaymentFctwbTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaymentFctwbTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaymentFctwbTableMap::OM_CLASS;
            /** @var PaymentFctwb $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaymentFctwbTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaymentFctwbTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaymentFctwbTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PaymentFctwb $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaymentFctwbTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_ID);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_ACCOUNT_NO);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_TRANSACTIONID);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_PRODUCT_ID);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_SOURCEBANK);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_DESTINATIONBANK);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_EMAIL);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_PHONE);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_TRANSACTIONDATE);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_AMOUNT_PAID);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_PAYMENTCHANNEL);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(PaymentFctwbTableMap::COL_UPLOADSTATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.account_no');
            $criteria->addSelectColumn($alias . '.TransactionID');
            $criteria->addSelectColumn($alias . '.product_id');
            $criteria->addSelectColumn($alias . '.SourceBank');
            $criteria->addSelectColumn($alias . '.DestinationBank');
            $criteria->addSelectColumn($alias . '.Email');
            $criteria->addSelectColumn($alias . '.Phone');
            $criteria->addSelectColumn($alias . '.TransactionDate');
            $criteria->addSelectColumn($alias . '.amount_paid');
            $criteria->addSelectColumn($alias . '.PaymentChannel');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.uploadstatus');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaymentFctwbTableMap::DATABASE_NAME)->getTable(PaymentFctwbTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaymentFctwbTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaymentFctwbTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaymentFctwbTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PaymentFctwb or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PaymentFctwb object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctwbTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PaymentFctwb) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaymentFctwbTableMap::DATABASE_NAME);
            $criteria->add(PaymentFctwbTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PaymentFctwbQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaymentFctwbTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaymentFctwbTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the payment_fctwb table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaymentFctwbQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PaymentFctwb or Criteria object.
     *
     * @param mixed               $criteria Criteria or PaymentFctwb object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentFctwbTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PaymentFctwb object
        }

        if ($criteria->containsKey(PaymentFctwbTableMap::COL_ID) && $criteria->keyContainsValue(PaymentFctwbTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PaymentFctwbTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PaymentFctwbQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaymentFctwbTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaymentFctwbTableMap::buildTableMap();
