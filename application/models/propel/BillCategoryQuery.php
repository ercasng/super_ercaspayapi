<?php

use Base\BillCategoryQuery as BaseBillCategoryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'bill_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BillCategoryQuery extends BaseBillCategoryQuery
{

}
