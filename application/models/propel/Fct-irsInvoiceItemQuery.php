<?php

use Base\Fct-irsInvoiceItemQuery as BaseFct-irsInvoiceItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'fct-irs_invoice_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Fct-irsInvoiceItemQuery extends BaseFct-irsInvoiceItemQuery
{

}
