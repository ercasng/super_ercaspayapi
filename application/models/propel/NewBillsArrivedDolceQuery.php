<?php

use Base\NewBillsArrivedDolceQuery as BaseNewBillsArrivedDolceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'new_bills_arrived_dolce' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class NewBillsArrivedDolceQuery extends BaseNewBillsArrivedDolceQuery
{

}
