<?php
class Payment_model extends CI_Model 
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getData($biller_table_suffix, $account_no = FALSE)
	{
         if($id === FALSE)
		{
			$query = $this->db->get('payment_'.$biller_table_suffix);
			return $query->result_array();
		}

		$query = $this->db->get_where('payment_'.$biller_table_suffix, array('account_no' => $account_no));
		return $query->row_array();
	}

	public function getDataByUploadStatus($biller_table_suffix, $status = 0)
	{
		$sql = "SELECT * FROM payment_".$biller_table_suffix." WHERE uploadstatus = $status ORDER BY created_date DESC LIMIT 0,1000";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getDataByDate($sdate,$edate)
	{
		$sql = "SELECT * FROM payment WHERE created_date BETWEEN '$sdate' AND '$edate' ";
		$query = $this->db->query($sql);
	    return $query->result_array();
	}
	
	public function getDataByDate2($sdate)
	{
            $sql = "SELECT * FROM payment WHERE created_date > '$edate' ";
            $query = $this->db->query($sql);
	        return $query->result_array();
	}
	
	public function setData($tranc_id, $tranc_date, $acct_no, $amt, $sbank, $dbank, $phone, $email, $payment_channel, $merchant_id, $product_id, $biller_table_suffix)
	{
		$paymentdata = array(
			'TransactionID' => $tranc_id,
			'TransactionDate' => $tranc_date,
			'account_no' => $acct_no,
			'amount_paid' => $amt,
			'SourceBank' => $sbank,
			'biller' => $biller_table_suffix,
            'DestinationBank' => $dbank,
            'PaymentChannel' => $payment_channel
		);
		
		//print_r($data); die();
		$this->db->insert('payment', $paymentdata);
		
		$id = $this->db->insert_id();
		
		if($id > 0)
		{
			$result = true;
		}
		else
		{
			$result = false;
		}
	
		return $result;
	}
	
	public function setData_biller($tranc_id, $tranc_date, $acct_no, $amt, $sbank, $dbank, $phone, $email, $payment_channel, $merchant_id, $product_id, $biller_table_suffix)
	{		
		$biller_paymentdata = array(
			'TransactionID' => $tranc_id,
			'TransactionDate' => $tranc_date,
			'account_no' => $acct_no,
			'amount_paid' => $amt,
			'SourceBank' => $sbank,
            'DestinationBank' => $dbank,
            'PaymentChannel' => $payment_channel,
			'Email'=> $email,
			'Phone'=> $phone,
			'product_id'=> $product_id
		);
		
		//print_r($data); die();
		$this->db->insert('payment_'.$biller_table_suffix, $biller_paymentdata);
		
		$id = $this->db->insert_id();
		
		if($id > 0)
		{
			$result = true;
		}
		else
		{
			$result = false;
		}
	
		return $result;
	}
	
	public function updateStatus($biller_table_suffix)
	{
	   	$id = (int) $this->input->post('ID');

		$data = array(
			'uploadstatus' => 1
		);
	    $this->db->where('id',$id);
		$insertid =  $this->db->update('payment_'.$biller_table_suffix, $data);
		return $insertid;

	}
	
	public function setTestData()
	{	
		
        $postdata = serialize($_POST);		

		$data = array(
			'col1' => $postdata
		);
		
		$insertid = $this->db->insert('test_table_nibss', $data);
	
		return $insertid ;
	}
	
	

}