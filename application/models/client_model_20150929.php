<?php

class Client_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}


	public function getData($id = FALSE)
	{
		if($id === FALSE)
		{
	    $query = $this->db->get('klu_user');
		return $query->result_array();
		}
		
		$query = $this->db->get_where('klu_user', array('account_no' => $id));
		return $query->row_array();
	}

	public function getData2($acctno)
	{
		//$tbl_prefix = substr($acctno, 0, 3); 
		//$user_id = substr($acctno, 4); 
		//$biller_abbrev = substr($acctno, 0, 5); 
		//$user_id = substr($acctno, 6); 
		
		$split = explode('_', $acctno,2);
		$biller_abbrev = $split[0]; 
		$user_id = $split[1]; 

		$sql_biller_info = "SELECT * FROM biller_info WHERE biller_abbrev='$biller_abbrev'";
		$query_biller_info = $this->db->query($sql_biller_info);
		if ($query_biller_info->num_rows() > 0)
		{
			$row = $query_biller_info->row();
		    $tbl_suffix = $row->biller_table_suffix;
		
			$sql_tbl = "select * from biller_info";
			$query = $this->db->query($sql_tbl);
			
			foreach($query->result() as $res)
			{
			//echo $res->biller_table; 
				$sql="SELECT * FROM customer_".$tbl_suffix." where account_no='". $user_id."'";
				$query1 = $this->db->query($sql);
				
				if(!empty($query1->result()))
				{               
					$sql = "SELECT a.account_no, a.customer_name, b.AmountDue, c.merchant_id, c.biller_name 
					FROM customer_".$tbl_suffix." as a, ercas_bill_".$tbl_suffix." as b, biller_info as c
					where a.account_no = b.AccountNo and c.biller_table_suffix ='".$tbl_suffix."'";
					
					//where a.account_no = b.customer_id and a.id = (select max(UniqueKeyID) from ercas_bill_".$tbl_suffix." where customer_id = (select id from customer_".$tbl_suffix." where account_no = '$user_id')) and c.biller_table_suffix ='$tbl_suffix'";
					
					$query2 = $this->db->query($sql); 
					
					if(!empty($query2->result()))
					{
						return $query2->result();
					}
				} 
			}
		
		}
		
		else
		{
			return FALSE;	
		}
	}

	public function setData()
	{
		$data = array(
			'account_no' => $this->input->post('account_no'),
			'customer_name' => $this->input->post('name'),
			'customer_address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'district' => $this->input->post('district')
		);
		
		$insertid =  $this->db->insert('klu_user_ol', $data);
		
		$result = "Data Inserted Successfully";
		return $result;
	}
	
	public function getSender($merchant_id)
	{
		$sql_biller_info = "SELECT * FROM biller_info WHERE merchant_id='$merchant_id'";
		$query_biller_info = $this->db->query($sql_biller_info);
		if ($query_biller_info->num_rows() > 0)
		{
			$row = $query_biller_info->row();
		    $biller_table_suffix = $row->biller_table_suffix;
		}
		return $biller_table_suffix;
	}
	
	public function getSMS_owner($merchant_id)
	{
		$sql_smsaccount_info = "SELECT * FROM biller_sms_accountdetails as a, biller_info as b WHERE a.biller_id = b.id AND b.merchant_id='$merchant_id'";
		$query_smsaccount_info = $this->db->query($sql_smsaccount_info);
		if ($query_smsaccount_info->num_rows() > 0)
		{
			$row = $query_smsaccount_info->row();
		    $account_owner = $row->account_owner;
		}
		return $account_owner;
	}
	
	public function getSMS_subaccount($merchant_id)
	{
		$sql_smsaccount_info = "SELECT * FROM biller_sms_accountdetails as a, biller_info as b WHERE a.biller_id = b.id AND b.merchant_id='$merchant_id'";
		$query_smsaccount_info = $this->db->query($sql_smsaccount_info);
		if ($query_smsaccount_info->num_rows() > 0)
		{
			$row = $query_smsaccount_info->row();
		    $subaccount = $row->subaccount;
		}
		return $subaccount;
	}
	
	public function getSMS_subaccountpassword($merchant_id)
	{
		$sql_smsaccount_info = "SELECT * FROM biller_sms_accountdetails as a, biller_info as b WHERE a.biller_id = b.id AND b.merchant_id='$merchant_id'";
		$query_smsaccount_info = $this->db->query($sql_smsaccount_info);
		if ($query_smsaccount_info->num_rows() > 0)
		{
			$row = $query_smsaccount_info->row();
		    $subaccount_password = $row->subaccount_password;
		}
		return $subaccount_password;
	}


}