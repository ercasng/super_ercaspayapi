<?php defined('BASEPATH') OR exit('No direct script access allowed');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Secured extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		$this->load->model('bill_model');
		$this->load->model('payment_model');
		$this->load->model('client_model');
    }
    
    function bill_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        $bill = $this->bill_model->getData( $this->get('id') );
		//$bill = null;
    	
        if($bill)
        {
            $this->response($bill, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No bill found!'), 404);
        }
    }
    
    function bill_post()
    {
	   	$message = $this->bill_model->setData();
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
        
    function bills_get()
    {
        $bills = $this->bill_model->getData();
                
        if($bills)
        {
            $this->response($bills, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No bills found!'), 404);
        }
    }
	
	
	function client_get()
    {
			
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        $client = $this->client_model->getData2($this->get('id'));
    	
        if($client)
        {
            $this->response($client, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No Customer Record found!'), 404);
        }
    }
    
    function client_post()
    {
       
	   $message = $this->client_model->setData();

        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
        
    function clients_get()
    {
        $clients = $this->client_model->getData();
                
        if($clients)
        {
            $this->response($clients, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No clients found!'), 404);
        }
    }
	
	function payment_get()
    {
		//echo $this->get('id'); die("hi");
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		if(!$this->get('billersuffix'))
        {
        	$this->response(NULL, 400);
        }

        $payment = $this->payment_model->getData($this->get('billersuffix'), $this->get('id') );
    	
        if($payment)
        {
            $this->response($payment, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No payment found!'), 404);
        }
    }
    	
    function payment_post() 
    {
       	//echo $this->get('id'); die();
	   	$tranc_id = $this->input->post('Transaction_id');
	   	$tranc_date = $this->input->post('Transactiondate');
	   	$acct_no = $this->input->post('Account_no');
	   	$amt = $this->input->post('Amount_paid');
	   	$sbank = $this->input->post('Source_bank');
       	$dbank = $this->input->post('Dest_bank');
       	$phone = $this->input->post('phone'); 
	   	$email = $this->input->post('email');
	   	$payment_channel = $this->input->post("payment_channel");
	   	$merchant_id = $this->input->post('merchant_id');
	   	$biller_name = $this->input->post('biller_name');
	   	$product_id = $this->input->post('product_id');
		
		$biller_table_suffix = $this->client_model->getSender($merchant_id);
	   
	   	$message = $this->payment_model->setData($tranc_id, $tranc_date, $acct_no, $amt, $sbank, $dbank, $phone, $email,$payment_channel, $merchant_id, $product_id);

		//log for test purposes
		$this->payment_model->setTestData();
    
		if($message === false)
	  	{
        	$this->response(NULL, 400); // 400 being the HTTP response code
	 	}
		else 
		{
			$this->payment_model->setData_biller($tranc_id, $tranc_date, $acct_no, $amt, $sbank, $dbank, $phone, $email, $payment_channel, $merchant_id, $product_id, $biller_table_suffix);
			$this->SendSMS($amt, $tranc_date, $phone, $acct_no, $merchant_id);
			$this->SendEmail($amt, $tranc_date, $email, $acct_no, $biller_name);
			//$this->SendEmail($amt, $tranc_date, $email, $acct_no, $biller_name, $payment_channel, $product_id, $tranc_id);
			//$this->payments_post($merchant_id);
			$this->response($message, 200); // 200 being the HTTP response code
		}
    }

   	function payments_put()
    {
	   	$message = $this->payment_model->updateStatus();
       	$this->response($message, 200); // 200 being the HTTP response code
    }
	
	function payments_post($biller_table_suffix)
    {
	   	$message = $this->payment_model->updateStatus($biller_table_suffix);
	   	if($message)
	   	{
			echo "Done";
	   	}
	   	else
	   	{
			echo "False";
	   	}
		$this->response($message, 200); // 200 being the HTTP response code
    }
            
    function payments_get()
    {
        $status =  $this->get('status');
		$biller_table_suffix =  $this->get('biller_table_suffix');

        $payments = $this->payment_model->getDataByUploadStatus($biller_table_suffix, $status);
                
        if($payments)
        {
            $this->response($payments, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No payments found!'), 404);
        }
    }

	//utility function to send sms
	public function SendSMS($amount, $date, $phone, $account, $merchant_id)
    {
    	if(!empty($phone))
        {
			
			$sender = ucwords($this->client_model->getSender($merchant_id));
			$sms_owner = $this->client_model->getSMS_owner($merchant_id);
			$sms_subaccount = $this->client_model->getSMS_subaccount($merchant_id);
			$sms_subaccountpassword = $this->client_model->getSMS_subaccountpassword($merchant_id);
			/*
			$sender_info = $this->client_model->getSMSSenderArray($merchant_id);

			$sender = $sender_info['biller_name'];
			$sms_owner = $sender_info['account_owner'];
			$sms_subaccount = $sender_info['subaccount'];
			$sms_subaccountpassword = $sender_info['subaccount_password'];
			*/
			$smsurl="http://www.smslive247.com/http/index.aspx?cmd=sendquickmsg&owneremail=".$sms_owner."&subacct=".$sms_subaccount."&subacctpwd=".$sms_subaccountpassword."&message=".rawurlencode(sprintf(SMS_PAYMENT_NOTIFICATION, $account, $sender, $amount, $date))."&sender=".$sender."&sendto=".$phone."&msgtype=0";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_URL, $smsurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
						
			return 1;
   		}
		
		return 0;
    }
	
	function SendEmail($amt, $tranc_date, $email, $acct_no, $biller)
	{
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			
			//send mail to customer for confirmation of payment
			$tHtml="";
			$tHtml.="<table width='80%' border='0' align='center' cellpadding='0' cellspacing='2' bgcolor='#0995D4'>";
			$tHtml.="<tr><td><table width='100%' border='0' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'>";
			$tHtml.="<tr><td colspan='2'><hr noshade='noshade' color='#0995D4' /></td></tr>";
			$tHtml.="<tr><td colspan='2'><font face='Century Gothic, Book Antiqua, Arial, Helvetica, sans-serif' style='color: #000000; font-size:10.5px;'><b>Dear Customer (".$acct_no."), <br />Your payment transaction of NGN ".$amt." in favor of ".$biller."has been processed succesfully.</b></font></td></tr>";
			$tHtml.="<tr><td align='right' style='padding-right:10px;'>&nbsp;</td></tr></table></td></tr></table>";
			$subject = 'Payment Transacation Email';
			$from = 'olufela@ercasng.com';
			$this->email->from($from, 'Do-Not-Reply');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($tHtml);
			$this->email->set_mailtype('html');
			@$this->email->send();
			//$this->email->send(); 
		
	}
	
	public function SendEmail2($amt, $tranc_date, $email, $acct_no, $biller, $payment_channel, $product_id, $tranc_id)
	{
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			
			//send mail to customer for confirmation of payment
			$tHtml="";
			$tHtml.="<table width='80%' border='0' align='center' cellpadding='0' cellspacing='2' bgcolor='#0995D4'><font face='Century Gothic, Book Antiqua, Arial, Helvetica, sans-serif' style='color: #000000; font-size:10.5px;'>";
			$tHtml.="<tr><td><table width='100%' border='0' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'>";
			$tHtml.="<tr><td colspan='2'><hr noshade='noshade' color='#0995D4' /></td></tr>";
			$tHtml.="<tr>";
			$tHtml.="<td colspan='2'><b>Dear Customer (".$acct_no."), <br />Your payment transaction of NGN ".$amt." in favor of ".$biller."has been processed succesfully.</b></font></td>";
			$tHtml.="</tr>";
			/*
			$tHtml.="<tr><td colspan='3'><hr noshade='noshade' color='#0995D4' /></td></tr>";
			$tHtml.="<tr><td colspan='3'><h3><span style='font-decoration: underline'>Transaction Details:</h3></td></tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Trasaction Date:</td><td width='5%'>&nbsp;</td><td width='65%'>".$tranc_date."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Trasaction ID:</td><td width='5%'>&nbsp;</td><td width='65%'>".$tranc_id."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Biller Name:</td><td width='5%'>&nbsp;</td><td width='65%'>".$biller."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Payment for:</td><td width='5%'>&nbsp;</td><td width='65%'>".$product_id."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Payment Channel:</td><td width='5%'>&nbsp;</td><td width='65%'>".$payment_channel."</td>";
			$tHtml.="</tr>";
			*/
			$tHtml.="<tr><td align='right' style='padding-right:10px;'>&nbsp;</td></tr></table></td></tr></table>";
			$subject = 'Bill Payment Transacation Notification - ';
			$from = 'olufela@ercasng.com';
			$this->email->from($from, 'Do-Not-Reply ');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($tHtml);
			$this->email->set_mailtype('html');
			@$this->email->send();
			//$this->email->send(); 
	}
}