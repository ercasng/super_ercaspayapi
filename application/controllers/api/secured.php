<?php defined('BASEPATH') OR exit('No direct script access allowed');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Secured extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		$this->load->model('bill_model');
		$this->load->model('payment_model');
		$this->load->model('client_model');
    }
    
    function bill_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        $bill = $this->bill_model->getData( $this->get('id') );
		//$bill = null;
    	
        if($bill)
        {
            $this->response($bill, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No bill found!'), 404);
        }
    }
    
    function bill_post()
    {
	   	$message = $this->bill_model->setData();
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
        
    function bills_get()
    {
        $bills = $this->bill_model->getData();
                
        if($bills)
        {
            $this->response($bills, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No bills found!'), 404);
        }
    }
	
	
	function client_get()
    {
			
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        $client = $this->client_model->getData2($this->get('id'));
    	
        if($client)
        {
            $this->response($client, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No Customer Record found!'), 404);
        }
    }
    
    function client_post()
    {
       
	   $message = $this->client_model->setData();

        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
        
    function clients_get()
    {
        $clients = $this->client_model->getData();
                
        if($clients)
        {
            $this->response($clients, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No clients found!'), 404);
        }
    }
	
	function payment_get()
    {
		//echo $this->get('id'); die("hi");
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		if(!$this->get('billersuffix'))
        {
        	$this->response(NULL, 400);
        }

        $payment = $this->payment_model->getData($this->get('billersuffix'), $this->get('id') );
    	
        if($payment)
        {
            $this->response($payment, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No payment found!'), 404);
        }
    }
    	
    function payment_post() 
    {
    	if (empty($this->input->post())) {
    		$this->response(array('error' =>'No payment data received'), 400);	
    	}
	    	$this->response($this->input->post(), 200);
    }

   	function payments_put()
    {
	   	$message = $this->payment_model->updateStatus();
       	$this->response($message, 200); // 200 being the HTTP response code
    }
	
	function payments_post($biller_table_suffix)
    {
	   	$message = $this->payment_model->updateStatus($biller_table_suffix);
	   	if($message)
	   	{
			echo "Done";
	   	}
	   	else
	   	{
			echo "False";
	   	}
		$this->response($message, 200); // 200 being the HTTP response code
    }
            
    function payments_get()
    {
        $status =  $this->get('status');
		$biller_table_suffix =  $this->get('biller_table_suffix');

        $payments = $this->payment_model->getDataByUploadStatus($biller_table_suffix, $status);
                
        if($payments)
        {
            $this->response($payments, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'No payments found!'), 404);
        }
    }

	//utility function to send sms
	public function SendSMS($amount, $date, $phone, $account, $merchant_id)
    {
    	if(!empty($phone))
        {
			
			$sender = ucwords($this->client_model->getSender($merchant_id));
			$sms_owner = $this->client_model->getSMS_owner($merchant_id);
			$sms_subaccount = $this->client_model->getSMS_subaccount($merchant_id);
			$sms_subaccountpassword = $this->client_model->getSMS_subaccountpassword($merchant_id);
			/*
			$sender_info = $this->client_model->getSMSSenderArray($merchant_id);

			$sender = $sender_info['biller_name'];
			$sms_owner = $sender_info['account_owner'];
			$sms_subaccount = $sender_info['subaccount'];
			$sms_subaccountpassword = $sender_info['subaccount_password'];
			*/
			$smsurl="http://www.smslive247.com/http/index.aspx?cmd=sendquickmsg&owneremail=".$sms_owner."&subacct=".$sms_subaccount."&subacctpwd=".$sms_subaccountpassword."&message=".rawurlencode(sprintf(SMS_PAYMENT_NOTIFICATION, $account, $sender, $amount, $date))."&sender=".$sender."&sendto=".$phone."&msgtype=0";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_URL, $smsurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
						
			return 1;
   		}
		
		return 0;
    }
	
	function SendEmail($amt, $tranc_date, $email, $acct_no, $biller)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		//send mail to customer for confirmation of payment
		$tHtml="";
		$tHtml.="<table width='80%' border='0' align='center' cellpadding='0' cellspacing='2' bgcolor='#0995D4'>";
		$tHtml.="<tr><td><table width='100%' border='0' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'>";
		$tHtml.="<tr><td colspan='2'><hr noshade='noshade' color='#0995D4' /></td></tr>";
		$tHtml.="<tr><td colspan='2'><font face='Century Gothic, Book Antiqua, Arial, Helvetica, sans-serif' style='color: #000000; font-size:10.5px;'><b>Dear Customer (".$acct_no."), <br />Your payment transaction has been processed succesfully.</b></font></td></tr>";
		
		$tHtml.="<tr><td colspan='2'><hr noshade='noshade' color='#0995D4' /></td></tr>";
		$tHtml.="<tr><td colspan='2'><h3><span style='font-decoration: underline'>Transaction Details:</h3></td></tr>";
		$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Trasaction Date:</td><td width='65%'>".$tranc_date."</td>";
		$tHtml.="</tr>";
		$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Trasaction Date:</td><td width='65%'>".$acct_no."</td>";
		$tHtml.="</tr>";
		$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Paid Amount:</td><td width='65%'>NGN ".number_format($amt,2)."</td>";
		$tHtml.="</tr>";
		$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Biller Name:</td><td width='65%'>".$biller."</td>";
		$tHtml.="</tr>";
		/*$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Payment for:</td><td width='65%'>".$product_id."</td>";
		$tHtml.="</tr>";
		$tHtml.="<tr>";
		$tHtml.="<td width='30%'>Payment Channel:</td><td width='65%'>".$payment_channel."</td>";
		$tHtml.="</tr>";*/
			
		$tHtml.="<tr><td align='right' style='padding-right:10px;'>&nbsp;</td></tr></table></td></tr></table>";
		$subject = 'Payment Transacation Email';
		$from = 'olufela@ercasng.com';
		$this->email->from($from, 'Do-Not-Reply');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($tHtml);
		$this->email->set_mailtype('html');
		@$this->email->send();
		//$this->email->send(); 
		
	}
	
	public function SendEmail2($amt, $tranc_date, $email, $acct_no, $biller, $payment_channel, $product_id, $tranc_id)
	{
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			
			//send mail to customer for confirmation of payment
			$tHtml="";
			$tHtml.="<table width='80%' border='0' align='center' cellpadding='0' cellspacing='2' bgcolor='#0995D4'><font face='Century Gothic, Book Antiqua, Arial, Helvetica, sans-serif' style='color: #000000; font-size:10.5px;'>";
			$tHtml.="<tr><td><table width='100%' border='0' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'>";
			$tHtml.="<tr><td colspan='2'><hr noshade='noshade' color='#0995D4' /></td></tr>";
			$tHtml.="<tr>";
			$tHtml.="<td colspan='2'><b>Dear Customer (".$acct_no."), <br />Your payment transaction of NGN ".$amt." in favor of ".$biller."has been processed succesfully.</b></font></td>";
			$tHtml.="</tr>";
			/*
			$tHtml.="<tr><td colspan='3'><hr noshade='noshade' color='#0995D4' /></td></tr>";
			$tHtml.="<tr><td colspan='3'><h3><span style='font-decoration: underline'>Transaction Details:</h3></td></tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Trasaction Date:</td><td width='5%'>&nbsp;</td><td width='65%'>".$tranc_date."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Trasaction ID:</td><td width='5%'>&nbsp;</td><td width='65%'>".$tranc_id."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Biller Name:</td><td width='5%'>&nbsp;</td><td width='65%'>".$biller."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Payment for:</td><td width='5%'>&nbsp;</td><td width='65%'>".$product_id."</td>";
			$tHtml.="</tr>";
			$tHtml.="<tr>";
			$tHtml.="<td width='30%'>Payment Channel:</td><td width='5%'>&nbsp;</td><td width='65%'>".$payment_channel."</td>";
			$tHtml.="</tr>";
			*/
			$tHtml.="<tr><td align='right' style='padding-right:10px;'>&nbsp;</td></tr></table></td></tr></table>";
			$subject = 'Bill Payment Transacation Notification - ';
			$from = 'olufela@ercasng.com';
			$this->email->from($from, 'Do-Not-Reply ');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($tHtml);
			$this->email->set_mailtype('html');
			@$this->email->send();
			//$this->email->send(); 
	}

	public function index_get()
	{
		// dd("Foo");
		// die(var_dump(BillerInfoQuery::create()->find()));
		$biller = \BillerInfoQuery::create()
						->join('BillerInfo.Product')
						->findOneByMerchantId("NIBSS00000052");

		$productsByCode = $biller->getProducts()->getArrayCopy('ProductType');
		$products = $biller->getProducts();

		$product = $productsByCode["Order"];
		// dd($product);
		// // echo '<pre>';print_r($biller->getBillerName());die;

		$payment_field1 = new ProductValidation();
		$payment_field1->setLabel("Transacation Reference");
		$payment_field1->setName("tx_code");
		$payment_field1->setType("string");
		
		$product->addProductValidation($payment_field1);
		$product_1  = new Product();
		$product_1->setProductCode("ERC000012");
		$product_1->setProductType("Order");
		$product_1->setRequiresValidation(true);

		$biller->addProduct($product_1);

		$biller->save();

		$this->response($biller->toArray(), 200);
	
			$this->response($invoice->toArray($keyType = \Map\DolceInvoiceTableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(),$includeForeignObjects = true), 200);
		
		
	}


	public function billers_get()
	{
		// die(var_dump($this->get("biller")));
		// NIBSS00000052
		// die(var_dump(BillerInfoQuery::create()->find()));

		$all_billers = \BillerInfoQuery::create()
						->select(["MerchantId", "BillerName"])
						->find();

		$billers = $all_billers->toArray(null, false);

		// $biller_array = clone $billers;
		foreach ($billers as $key => $biller) {

			$billers[$key]["BillerCategory"] = "Simple";
		}

		$this->response($billers, 200);
		// $this->response($all_billers->toArray(null, true, $keyType = \Map\BillerInfoTableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array()), 200);
	}

	public function biller_get()
	{
		die(var_dump($this->get("biller")));
		// die(var_dump(BillerInfoQuery::create()->find()));

		$all_billers = \BillerInfoQuery::create()
						->select(["MerchantId", "BillerName"])
						->find();

		$billers = $all_billers->toArray(null, false);

		// $biller_array = clone $billers;
		foreach ($billers as $key => $biller) {

			$billers[$key]["BillerCategory"] = "Simple";
		}

		$this->response($billers, 200);
	}

	public function products_get()
	{
		if ($this->get("biller")) {
			$biller = \BillerInfoQuery::create()
						->join('BillerInfo.Product')
						->findOneByMerchantId($this->get("biller"));
			$products = $biller->getProducts();
		}
		else {
			$products = ProductQuery::create()->find();
		}
		foreach ($products as $product) {
			$product->getInvoiceFields();
			$product->getPaymentFields();
		}
		$product_array = $products->toArray();

		foreach ($product_array as $key => $product) {
			$product_array[$key]["BillerName"] = \BillerInfoQuery::create()->findOneByMerchantId($product_array[$key]["BillerId"])->getBillerName();
		}


			$this->response($product_array, 200);
	}

	// Invoice Resource
	// 
	public function invoice_get()
	{
		# Parse request variables
		# Calculate Invoice class from request variables
		# Instantiate propel model object
		# find invoice by unique identifier
		# create response
	}

	public function invoice_post()
	{
		# Parse request variables
		# Calculate Invoice class from request variables
		# Instantiate propel model object
		# find invoice by unique identifier
		# create response
	}

	public function invoice_put()
	{
		# Same as invoice_get but find instead of create new
	}

	public function invoice_delete()
	{
		# code...
	}


	// Invoice Validation
	public function validate_get()
	{
		// $map = new Map\DolceInvoiceTableMap();
		// dd($map->getTableMap());
		if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		$split = explode('_', $this->get('id'),2);
		$product_code = $split[0]; 
		$unique_id = $split[1]; 

		$product = ProductQuery::create()->joinBiller()->with('Biller')->findOneByProductCode($product_code);
		$product_validation = $product->getProductValidations()->toArray();
		$biller = $product->getBiller();
		$billerTableSuffix = ucwords($biller->getBillerTableSuffix());
		$invoice_class = $billerTableSuffix . "InvoiceQuery";
		$invoice_table_map = "\\Map\\" . $billerTableSuffix . "InvoiceTableMap";
		
		if (empty($product_validation)) {
        	$this->response(NULL, 400);
		} 
		else {	
			$unique_field = $product_validation[0]["Name"];
			$uniqueIdColumnPhpName = $invoice_table_map::translateFieldName($unique_field, $invoice_table_map::TYPE_FIELDNAME, $invoice_table_map::TYPE_PHPNAME);
			$uniqueIdFilterMethod = "findOneBy" . $uniqueIdColumnPhpName;
			$invoice = $invoice_class::create()->$uniqueIdFilterMethod($unique_id);
		}
		if (!$invoice) {
        	$this->response("Not found", 404);
		}
		else{
			$invoice->getItems();
		}

		$this->response($invoice->toArray($keyType = $invoice_table_map::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(),$includeForeignObjects = true), 200);


		// Return Invoice
	}
}

